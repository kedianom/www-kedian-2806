<?php
add_action('media_buttons_context','add_mycancel_tinymce_media_button');
function add_mycancel_tinymce_media_button($context){

return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=mycancel_shortcode_popup&width=640&height=513\" 
class=\"button thickbox\" id=\"mycancel_shortcode_popup_button\" title=\"Generate DAP Member Cancel Shortcode\"  style=\"display:none;\">DAP Member Cancel</a>");
}
add_action('admin_footer','mycancel_shortcode_media_button_popup');
function mycancel_shortcode_media_button_popup(){
	?>
	<div id="mycancel_shortcode_popup" style="display:none;" class="popup_div_outer">
	<div class="live-links-outer" id="mycancel_shortcode_popup_inner">   
  <h2 class="head_memb">Membership Options</h2>
	 <table width="100%" cellpadding="2" cellspacing="2" class="table_page_select">
		<tr>
			<td valign="top"> Select a Membership page </td>
			<td valign="top" width="50%">
				<select  name="selectmemberpages" id="selectmemberpages">
					<option value="Select" >--Select--</option>
					<option value="DAPMyContent">My Content</option>
					<option value="DAPMyProfile" >My Profile</option>
					<option value="DAPAffiliateInfo">Affiliate Info & Stats</option>
					<option value="DAPMemberInvoice">Member Invoice</option>
					<option value="DAPMemberCancel" selected="selected">Member Cancellation</option>
					<option value="DAPLogin">Member Login</option>
				</select><br />
			</td>
		</tr>
	</table>  
	<table width="100%" cellpadding="2" cellspacing="2">
    
    <tr>
    <td valign="top">
    <strong>Do you want to Show All Transactions?</strong> <br><label>If set to YES, it will show all products that the user has EVEN if the product is not a subscription product.</label>
    </td>
    <td valign="top" width="50%">
        <select  name="showalltrans" id="showalltrans">
         <option value="Y">Yes</option>
		 <option value="N">No</option>
       </select><br />
    </td>
    </tr>
	<tr><td></td><td></td></tr>
	<tr>
	<td valign="top"><strong>Select a Template</strong></td>
	<td>
	<select name="template" id="template">
	<option value="template1">Template1</option>
    <option value="template2">Template2</option>
    <option value="template3">Template3</option>
	</select>
	</td>
	</tr>
	
	<tr><td></td><td></td></tr>
    <tr><td></td><td></td></tr>
	<tr>
    <td valign="top">
    <strong>Show Cancel?</strong> <br><label>Do you want to show a "Cancel" button to allow users to self-cancel their subscription?<br><span style="color:red">The cancel button will ONLY show up for:  <br>
    1) 'Recurring/Subscription' products. <br>
    2)  If the users have an ACTIVE subscription with STRIPE, PAYPAL or AUTHORIZE.net.
    </span></label>
    </td>
    <td valign="top" width="50%">
        <select  name="showcancel" id="showcancel">
         <option value="Y">Yes</option>
		 <option value="N">No</option>
       </select><br/>
    </td>
    </tr>
	<tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
	<tr>
	<td valign="top"><strong>Enter full URL of your CANCEL button image</strong></td>
	<td><textarea rows="3" name="cancelurl" id="cancelurl"><?php echo  plugin_dir_url( __FILE__ ).'includes/images/CancelButtonUp.jpg';?></textarea></td>
	</tr>
	<tr><td></td><td></td></tr>
    <tr>
    <td valign="top">
    <strong>If you want your CANCEL button to be a text hyperlink (instead of a button/image), then enter "Cancel" text here.</strong><br>This will only be used if you do not specify a cancel button/image.
    </td>
    <td valign="top" width="50%">
    <input type="text" name="canceltext" id="canceltext" value="CANCEL">
	<br/>
    </td>
    </tr>
	<tr><td></td><td></td></tr>
    <tr><td></td><td></td></tr>
    <tr>
    <td valign="top">
    <strong>Enter "CONTACT US" text for one-time purchases or for purchases that cannot be cancelled by the member. </strong><br>
    PLEASE NOTE:  For transactions that the members cannot cancel (For e.g. one-time purchases), you can set a "contact us" text and a "contact us" link in the textboxes below. This way your members can click the "contact us" link to contact you.
    </td>
    <td valign="top" width="50%">
    <input type="text" name="cancelcontact" id="cancelcontact" value="">
	<br/>
    </td>
    
    </tr>
    <tr>
    <td valign="top">
    <strong>Enter URL of the page to which the "Contact Us" text will be linked.</strong><br>
    Only applicable if you have entered a "CONTACT US" text above.
    </td>
    <td valign="top" width="50%">
    <input type="text" name="cancelcontacturl" id="cancelcontacturl" value="">
	<br/>
    </td>
    
    </tr>
	<tr><td></td><td></td></tr>
    <tr><td></td><td></td></tr>
	<tr>
	<td valign="top"><strong>Enter Product Id</strong><br>Only transactions related to these products will be listed. If you want all products, set it to ALL.
    If you want specific products, enter comma-separated list of ids.. for e.g. 1,2,4</td>
	<td><input type="text" name="prodid" id="prodid" value="ALL"></td>
	</tr>
	<tr><td></td><td></td></tr>
    <tr>
    <td valign="top">
    <strong>Enter CANCELLED text</strong><br>
    This text will be displayed instead of the cancel button for 'already cancelled' transactions.
    </td>
    <td valign="top" width="50%">
   <textarea name="cancelledtext" id="cancelledtext" rows="3">CANCELLED</textarea>
	<br/>
    </td>
    </tr>
    
	<tr>
    <td valign="top">
    <strong>Enter successful cancellation message</strong>
    </td>
    <td valign="top" width="50%">
   <textarea name="cancelsuccess" id="cancelsuccess" rows="3">Subscription cancellation completed Successfully!</textarea>
	<br/>
    </td>
    </tr>
	<tr><td></td><td></td></tr>
	<tr>
    <td valign="top">
    <strong>Enter failed cancellation message</strong>
    </td>
    <td valign="top" width="50%">
    <textarea name="cancelfailed" id="cancelfailed" rows="3">Sorry, could not cancel the subscription.</textarea>
	<br/>
    </td>
    </tr>
	<tr><td></td><td></td></tr>
	<tr>
    <td valign="top">
    <strong>Enter cancellation confirmation message</strong>
    </td>
    <td valign="top" width="50%">
    <textarea name="confirmmsg" id="confirmmsg" rows="3">Are you sure you want to cancel subscription?</textarea>
	<br/>
    </td>
    </tr>
	<tr><td></td><td></td></tr>
	
	<tr>
    <td valign="top">
    <strong>Enter 'No Transaction' found message</strong>
    </td>
    <td valign="top" width="50%">
    <input type="text" name="notransfoundmsg" id="notransfoundmsg" value="Sorry, no active transactions found">
	<br/>
    </td>
    </tr>
	</table> 
     <div align="center" class="btn_outer"><button class="button-primary back-btn" id="backbtn">Back</button> 
   <button class="button-primary next-btn" id="mycancelscbtn">Generate Shortcode</button></div>     
	 
	
	</div>
	</div>
	<style>
	
#mycancelscbtn {
    font-size: 16px !important;
    height: auto !important;
    line-height: normal !important;
    margin: 20px 0 60px;
    padding: 10px 15px !important;
}

.live-links-outer select, .live-links-outer input, .live-links-outer textarea {
  float: left;
  width: 100% !important;
}
h2.head_memb{margin :10px 0px;font-size:18px;float: left;font-weight: bold;}
	</style>
	<?php
} // my cancel function 

add_action('admin_footer','mycancel_shortcode_add_shortcode_to_editor');
function mycancel_shortcode_add_shortcode_to_editor(){
	?>
	<script>
	jQuery(document).ready(function(){
		jQuery('#mycancelscbtn').click(function(){
			var showalltrans = jQuery('#showalltrans').val();
			var showcancel = jQuery('#showcancel').val();
			var cancelurl = jQuery('#cancelurl').val();
			var cancelcontact = jQuery('#cancelcontact').val();
			var cancelcontacturl = jQuery('#cancelcontacturl').val();
			var template = jQuery('#template').val();
			var prodid = jQuery('#prodid').val();
			var cancelledtext = jQuery('#cancelledtext').val();
			var cancelsuccess = jQuery('#cancelsuccess').val();
			var cancelfailed = jQuery('#cancelfailed').val();
			var confirmmsg = jQuery('#confirmmsg').val();
			var canceltext = jQuery('#canceltext').val();
			var notransfoundmsg = jQuery('#notransfoundmsg').val();
			
			var shortcode = '[DAPUserSubscriptions showalltransactions="'+showalltrans+'" showcancel="'+showcancel+'" cancelimage="'+cancelurl+'" template="'+template+'" cancelledtext="'+cancelledtext+'" cancelsuccess="'+cancelsuccess+'" cancelfailed="'+cancelfailed+'"  confirmmsg="'+confirmmsg+'" canceltext="'+canceltext+'" cancelcontact="'+cancelcontact+'" cancelcontacturl="'+cancelcontacturl+'" notransfoundmsg="'+notransfoundmsg+'" productId="'+prodid+'"][/DAPUserSubscriptions]';
			send_to_editor(shortcode); 
		    self.parent.tb_remove();
		});
	});
	</script>
	<?php
}
?>