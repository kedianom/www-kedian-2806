<?php 


/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_myprofile_tinymce_media_button');
function add_myprofile_tinymce_media_button($context){

return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=myprofile_shortcode_popup&width=640&height=513\" 
class=\"button thickbox\" id=\"myprofile_shortcode_popup_button\" title=\"Generate My Profile Shortcode\" style=\"display:none;\">DAP My Profile</a>");
}

add_action('admin_footer','myprofile_shortcode_media_button_popup');

$lldocroot = defined('SITEROOT') ? SITEROOT : $_SERVER['DOCUMENT_ROOT'];
if(file_exists($lldocroot . "/dap/dap-config.php")) include_once ($lldocroot . "/dap/dap-config.php"); // dap config call

//print_r($customFieldsList);

//Generate inline content for the popup window when the "my shortcode" button is clicked
function myprofile_shortcode_media_button_popup(){
	try{
        $customFieldsList = Dap_CustomFields::loadUserFacingCustomFields();  // get dap custom fields.
      }
    catch (PDOException $e) {
	   return ERROR_DB_OPERATION;
	  } catch (Exception $e) {
	   return ERROR_GENERAL;
	  } 
	?>
 
    <!--".wrap" class div is needed to make thickbox content look good-->
	<style>
	
	/*css start here for popup*/

div#TB_window {
  left: 0;
  margin: 0 auto !important;
  max-width: 1000px !important;
  position: absolute;
  right: 0;
  width:98% !important;
} 
	
	/*css end here for popup*/

#TB_ajaxContent {
    box-sizing: border-box;
    clear: both;
    line-height: 1.4em;
    overflow: auto;
    padding: 14px;
    text-align: left;
    width: 100% !important;
}
	div#TB_window {height: auto!important;}
	div#TB_ajaxContent td {padding: 5px 0;}
	
	div#TB_ajaxContent select {
		padding: 2px;
		line-height: 28px;
	/*	height: 28px; */
		width:100%;
	}
	
.border-div {
    height: auto;
    margin: auto;
    overflow: none;
    padding-left: 105px;
    width: 683px;
} 
#myprofilegenscbtn {
    font-size: 16px !important;
    height: auto !important;
    line-height: normal !important;
    margin: 20px 0 60px;
    padding: 10px 15px !important;
}
.border-div > h2 {
    margin-bottom: 35px;
	margin-top:0px;
} 
.live-links-outer input, .live-links-outer textarea {
  float: left;
  width: 100% !important;
}

.live-links-outer select{
  float: left;
  width: 100% !important;
}
#myprofilegenscbtn{
	background: #0085ba none repeat scroll 0 0;
  border: medium none;
  border-radius: 5px;
  font-size: 18px;
  height: auto;
  margin-top: 15px;
  padding: 8px 18px;
  text-shadow: 0 0 0 transparent;
  text-transform: none;
  color:#fff;
  
}
#backbtn{
	background: #0085ba none repeat scroll 0 0;
  border: medium none;
  border-radius: 5px;
  font-size: 18px;
  height: auto;
  margin-top: 15px;
  padding: 8px 18px;
  text-shadow: 0 0 0 transparent;
  text-transform: none;
  color:#fff;
}
#backbtn {
   float:left;
}

 #myprofilegenscbtn:hover, #backbtn:hover {
  background: #000 none repeat scroll 0 0;
  color:#fff;
}
.select_add_cls{  width: 94% !important;  float: right!important;  margin-right: 15px!important; }
h2.head_memb{margin :10px 0px;font-size:18px;float: left;font-weight: bold;}
</style>

  <div id="myprofile_shortcode_popup" style="display:none;" class="popup_div_outer">
<div class="live-links-outer" id="myprofile_shortcode_popup_inner">
	 	<div class="live-links-outer-div" id="memberpages_shortcode_popup_button">
	 <h2 class="head_memb">Membership Options</h2>
	 <table width="100%" cellpadding="2" cellspacing="2" class="table_page_select">
		<tr>
			<td valign="top"> Select a Membership page </td>
			<td valign="top" width="50%">
				<select  name="selectmemberpages" id="selectmemberpages">
					<option value="Select" >--Select--</option>
					<option value="DAPMyContent">My Content</option>
					<option value="DAPMyProfile" selected="selected">My Profile</option>
					<option value="DAPAffiliateInfo">Affiliate Info & Stats</option>
					<option value="DAPMemberInvoice">Member Invoice</option>
					<option value="DAPMemberCancel">Member Cancellation</option>
					<option value="DAPLogin">Member Login</option>
				</select><br />
			</td>
		</tr>
	</table>  
    <table width="100%" cellpadding="2" cellspacing="2">
     
	
     <tr>
    <td valign="top">
    <strong>Show Account Info Section?
    </td>
    <td valign="top" width="50%">
       <select  name="showaccountsec" id="showaccountsec">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>

	  <tr>
    <td valign="top">
    <strong>Show Additional Info Section?
    </td>
    <td valign="top" width="50%">
       <select  name="showaddsec" id="showaddsec">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
	


	<tr>
    <td valign="top">
    <strong>Show Billing Info Section?
    </td>
    <td valign="top" width="50%">
       <select  name="showbillingsec" id="showbillingsec">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
	
   <tr>
    <td valign="top">
    <strong>Show First Name?
    </td>
    <td valign="top" width="50%">
       <select  name="showfirstname" id="showfirstname">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
    
     
    <tr>
    <td valign="top">
    <strong>Show Last Name?
    </td>
    <td valign="top" width="50%">
       <select  name="showlastname" id="showlastname">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
    
     
	 
    <tr>
    <td valign="top">
    <strong>Show User Name?
    </td>
    <td valign="top" width="50%">
       <select  name="showusername" id="showusername">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
    
     
    <tr>
    <td valign="top">
    <strong>Show Email?
    </td>
    <td valign="top" width="50%">
       <select  name="showemail" id="showemail">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br/></strong> 
    </td>
    </tr>
    
     
    <tr>
    <td valign="top">
    <strong>Show Password?
    </td>
    <td valign="top" width="50%">
       <select  name="showpassword" id="showpassword">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
    
     
    <tr>
    <td valign="top">
    <strong>Show Address1?
    </td>
    <td valign="top" width="50%">
       <select  name="showaddress1" id="showaddress1">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
    
     
    <tr>
    <td valign="top">
    <strong>Show Address2?
    </td>
    <td valign="top" width="50%">
       <select  name="showaddress2" id="showaddress2">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show City?
    </td>
    <td valign="top" width="50%">
       <select  name="showcity" id="showcity">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br />
    </td>
    </tr>
    
     
    <tr>
    <td valign="top">
    <strong>Show State/Province/Region?
    </td>
    <td valign="top" width="50%">
       <select  name="showstate" id="showstate">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Zip/Postal Code?
    </td>
    <td valign="top" width="50%">
       <select  name="showzip" id="showzip">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Country?
    </td>
    <td valign="top" width="50%">
       <select  name="showcountry" id="showcountry">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Phone?
    </td>
    <td valign="top" width="50%">
       <select  name="showphone" id="showphone">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Fax?
    </td>
    <td valign="top" width="50%">
       <select  name="showfax" id="showfax">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Company?
    </td>
    <td valign="top" width="50%">
       <select  name="showcompany" id="showcompany">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Title?
    </td>
    <td valign="top" width="50%">
       <select  name="showtitle" id="showtitle">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Paypal Email?
    </td>
    <td valign="top" width="50%">
       <select  name="showpaypalemail" id="showpaypalemail">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
     
    <tr>
    <td valign="top">
    <strong>Show Opted Out?
    </td>
    <td valign="top" width="50%">
       <select  name="showoptedout" id="showoptedout">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
    
    <tr>
	<td valign="top">
	<strong>Form Alignment
	</td>
	<td valign="top" width="50%">
	<select id="formalign" style="" name="formalign">
		<option value="none">Center</option>
		<option value="left">Left</option>
		<option value="right">Right</option>
	</select>
	</td>
	</tr>
    
	<tr>
    <td valign="top">
    <strong>Heading Label - Account Info
    </td>
    <td valign="top" width="50%">
		<input type="text" id="account_info_heading" value="Account Info"> 
    </tr>
	
	<tr>
    <td valign="top">
    <strong>Heading Label - Billing Info
    </td>
    <td valign="top" width="50%">
		<input type="text" id="billing_info_heading" value="Billing Info"> 
    </tr>
	
	<tr>
    <td valign="top">
    <strong>Heading Label - Additional Info
    </td>
    <td valign="top" width="50%">
		<input type="text" id="additional_info_heading" value="Additional Info"> 
    </tr>
	
    <tr><td></td><td></td></tr>
  
    <tr>
    <td valign="top">
    <strong>Show Custom Fields?
    </td>
    <td valign="top" width="50%">
       <select  name="showcustomfields" id="showcustomfields">
          <option value="Y">YES</option>
          <option value="N" selected="selected">NO</option>
       </select><br /></strong> 
    </td>
    </tr>
	
   <tr id="select_custom_fields" style="display:none;">
   <td width="50%"></td>
	<td valign="top">
    <strong>Select Custom Fields</strong>
    </td>
    </tr>
    <tr id="list_custom_fields" style="display:none;">
    <td width="50%"></td>
    <td valign="top">
    
       <select name="customfields" id="customfields" multiple >
	     <option value="all">All</option>
         <?php 
		 foreach($customFieldsList as $key){
			 echo "<option value='".$key['id']."'>".$key['name']."</option>";
		 }
		 ?>
       </select><br/></strong> 
	</tr>
	
    
	
    <tr>
    <td valign="top">
    <strong>Enter Redirect URL
    </td>
    <td valign="top" width="50%">
	<textarea name="redirect" id="redirect"></textarea>
       <br /><i style="font-size:13px;"> <br />For ex: http://yoursite.com/my-account<br />(Users will be redirected to this page upon profile update.If you leave it empty, the users will remain on the same page.)</i>
       <br /><br /></strong> 
       
    </tr>
    
    
    
    </table>     
         <div align="center" class="btn_outer"><button class="button-primary back-btn" id="backbtn">Back</button> 
   <button class="button-primary next-btn" id="myprofilegenscbtn">Generate Shortcode</button></div>          
       </div>
       </div>
	 
  </div>
  <script>
  jQuery('#showcustomfields').change(function(){
	  var val = jQuery(this).val();
	  if(val == "Y"){
		  jQuery('#select_custom_fields').show();
		  jQuery('#list_custom_fields').show();
	  }
	  else{
		  jQuery('#select_custom_fields').hide();
		  jQuery('#list_custom_fields').hide();
	  }
	
  });
  </script>
<?php
}

//javascript code needed to make shortcode appear in TinyMCE editor
add_action('admin_footer','myprofile_shortcode_add_shortcode_to_editor');
function myprofile_shortcode_add_shortcode_to_editor(){?>
<script>
jQuery('#myprofilegenscbtn').on('click',function() {
												 
  var showfirstname = jQuery('select#showfirstname').val();
  var showlastname = jQuery('select#showlastname').val();
  var showusername = jQuery('select#showusername').val();
  var showemail = jQuery('select#showemail').val();
  var showpassword = jQuery('select#showpassword').val();
  var showaddress1 = jQuery('select#showaddress1').val();
  var showaddress2 = jQuery('select#showaddress2').val();
  var showcity = jQuery('select#showcity').val();
  var showstate = jQuery('select#showstate').val();
  var showzip = jQuery('select#showzip').val();
  var showcountry = jQuery('select#showcountry').val();
  var showphone = jQuery('select#showphone').val();
  var showfax = jQuery('select#showfax').val();
  var showcompany = jQuery('select#showcompany').val();
  var showtitle = jQuery('select#showtitle').val();
  var showpaypalemail = jQuery('select#showpaypalemail').val();
  var showoptedout = jQuery('select#showoptedout').val();
  var redirect = jQuery('#redirect').val();
  var showaccountsec = jQuery('#showaccountsec').val();
  var showbillingsec = jQuery('#showbillingsec').val();
  var showaddsec = jQuery('#showaddsec').val();
  
  var account_info_heading = jQuery('#account_info_heading').val();
  var billing_info_heading = jQuery('#billing_info_heading').val();
  var additional_info_heading = jQuery('#additional_info_heading').val();
  
  var formalign = jQuery('#formalign').val();
  var showcustomfields = jQuery('#showcustomfields').val();
  if(showcustomfields == "Y")
  {
	  customvalues = jQuery('#customfields').val();
	  newvalues = new Array();
	  for(var i = 0;i<(customvalues.length);i++){
		  if(customvalues[i] == "all"){
			  newvalues = [];
			  newvalues.push("all");
			  break;
		  }
		  else{
			    newvalues.push(customvalues[i]);
		  }
	  }
	  fields = newvalues.toString(); 
  }
  else
  {
	  fields = "";
  }
 
  var shortcode = '[DAPMyProfile showAccountSection="'+showaccountsec+'" showBillingSection="'+showbillingsec+'" showAdditionalSection="'+showaddsec+'" showFirstName="'+showfirstname+'" showLastName="'+showlastname+'" showUserName="'+showusername+'" showEmail="'+showemail+'" showPassword="'+showpassword+'" showAddress1="'+showaddress1+'" showAddress2="'+showaddress2+'" showCity="'+showcity+'" showState="'+showstate+'" showZip="'+showzip+'" showCountry="'+showcountry+'" showPhone="'+showphone+'" showFax="'+showfax+'" showCompany="'+showcompany+'" showTitle="'+showtitle+'" showPaypalEmail="'+showpaypalemail+'" account_info_heading="'+account_info_heading +'" billing_info_heading="'+billing_info_heading +'" additional_info_heading="'+additional_info_heading +'" showOptedOut="'+showoptedout +'" redirect="'+redirect+'" formalign="'+formalign+'" showCustomFields="'+showcustomfields+'"  customFieldValues="'+fields+'" /]';
 
  send_to_editor(shortcode); 
 
// tinyMCE.execCommand('mceInsertContent', false, shortcode);
 
 
//  tinyMCE.activeEditor.execCommand( 'mceInsertContent', false, shortcode );
/* if( !tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
	send_to_editor(shortcode);
  } else {
    tinyMCE.execCommand('mceInsertContent', false, shortcode);
  }
*/  
  //close the thickbox after adding shortcode to editor
  self.parent.tb_remove();
});
</script>

<?php
}


add_shortcode('DAPMyProfile', 'dap_myprofile');
add_action( 'wp_enqueue_scripts', 'addMyProfileJS' );

function addMyProfileJS() {
	$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'] : "http://".$_SERVER['SERVER_NAME'];	
	wp_register_script('dap-ajaxWrapper', $url . '/dap/javascript/ajaxWrapper.js');
	wp_register_script('dap-common', $url . '/dap/javascript/common.js');
	//wp_register_script('dap-MyProfile', $url . '/dap/javascript/MyProfileShortcode.js');
	//wp_register_script('dap-jsstrings', $url . '/dap/javascript/jsstrings.php');
    wp_enqueue_script( 'ajaxWrapper' );
    wp_enqueue_script( 'dap-common' );
    wp_enqueue_script( 'dap-MyProfile' );
    //wp_enqueue_script( 'dap-jsstrings' );

}

/**

	[DAPMyProfile showFirstName="Y" showLastName="Y" showcustomfields="cbnick" redirect="http://YourSite.com/url"]
	
	-OR-
	
	[DAPMyProfile 
	 showFirstName="Y" 
	 showLastName="Y" 
	 showUserName="Y" 
	 showEmail="Y"
	 showPassword="Y"
	 showAddress1="Y"
	 showAddress2="Y"
	 showCity="Y"
	 showState="Y"
	 showZip="Y"
	 showCountry="Y"
	 showPhone="Y"
	 showFax="Y"
	 showCompany="Y"
	 showTitle="Y"
	 showPaypalEmail="Y"
	 showOptedOut="Y"
	 showcustomfields="Y"]
*/

function dap_myprofile($atts, $content=null){ 
	extract(shortcode_atts(array(
		'showaccountsection' => '',
		'showbillingsection' => '',
		'showadditionalsection' => '',
		'showfirstname' => 'Y',
		'showlastname' => 'Y',
		'showusername' => 'Y',
		'showemail' => 'Y',
		'showpassword' => 'Y',
		'showaddress1' => 'Y',
		'showaddress2' => 'Y',
		'showcity' => 'Y',
		'showstate' => 'Y',
		'showzip' => 'Y',
		'showcountry' => 'Y',
		'showphone' => 'Y',
		'showfax' => 'Y',
		'showcompany' => 'Y',
		'showtitle' => 'Y',
		'showpaypalemail' => 'Y',
		'showoptedout' => 'Y',
		'showcustomfields' => 'Y',
		'customfieldvalues' => 'all',
		'formalign'=>'Y',
		'showuserprofileheading' => 'Y',
		'account_info_heading'=>'Account Info',
		'billing_info_heading'=>'Billing Info',
		'additional_info_heading'=>'Additional Info',
		'redirect' => '',
		'template' => 'template1'
	), $atts));
	
	$content = do_shortcode(dap_clean_shortcode_content($content));	
	$requestURI = explode("?",$_SERVER['REQUEST_URI']);
	$redirectTo = ($redirect == "") ? $requestURI[0] : $redirect;
	
	$session = Dap_Session::getSession();
	$user = $session->getUser();
	//$content = $content . "<br/><br/>";
	
	if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		//logToFile("Not logged in, returning errmsgtemplate");
		$errorHTML = mb_convert_encoding(MSG_PLS_LOGIN, "UTF-8", "auto") . " <a href=\"" . Dap_Config::get("LOGIN_URL") . "\">". mb_convert_encoding(MSG_CLICK_HERE_TO_LOGIN, "UTF-8", "auto") . "</a>";
		return $errorHTML;
	}
	
	$userId = $user->getId();
	$user = Dap_User::loadUserById($user->getId());
	
	$jqueryForResponsive='<script language="javascript">window.onload=function(){
	  var maxwidth=jQuery("#dapprofile-container").width();
	  window.onresize=function(){
		jQuery("#dapprofile-container").removeClass("resizedform");
		var windowsize = jQuery("html").width()-5;
		
		var containerw=Number(document.getElementById("dapprofile-container").offsetWidth); 
		if(containerw>windowsize){
			jQuery("#dapprofile-container").css("width",(windowsize-4)+"px");
		}else{
			var containerWidth=jQuery("#dapprofile-container").addClass("resizedform").width;
			if(maxwidth>containerWidth) {
				jQuery("#dapprofile-container").addClass("resizedform");
			}
			else {
				jQuery("#dapprofile-container").removeClass("resizedform");
			}
		}
		
	  } 
	};
	 
    </script>';
   
	$myprofile_content = '<script language="javascript" type="text/javascript" src="/dap/javascript/jsstrings.php"></script>';
	
	//$template="template1";
//	$myprofile_template_path=ABSPATH . "/wp-content/plugins/DAP-WP-LiveLinks/includes/myprofiletemplates/".$template;
	$myprofile_template_path=WP_PLUGIN_DIR . "/DAP-WP-LiveLinks/includes/myprofiletemplates/".$template;
	
	//REGULAR DRIP HEADER TEMPLATE CONTENT
	$myProfileTemplateName="/".$template.".html";
	$customMyProfileTemplateName="/custom".$template.".html";
	$myprofile_content.=getMyProfileTemplateContent($myprofile_template_path, $myProfileTemplateName, $customMyProfileTemplateName);
	
	$myCustomFieldsProfileTemplateName="/additionalFieldsTemplate.html";
	$customMyCustomFieldsProfileTemplateName="/customadditionalFieldsTemplate.html";
	$mycustom_fields_profile_content.=getMyProfileTemplateContent($myprofile_template_path, $myCustomFieldsProfileTemplateName, $customMyCustomFieldsProfileTemplateName);
	
	//$cssurl=get_option('siteurl')."/wp-content/plugins/DAP-WP-LiveLinks/includes/myprofiletemplates/".$template."/css";	
	$cssurl=plugins_url()."/DAP-WP-LiveLinks/includes/myprofiletemplates/".$template."/css";	
	$csspath=WP_PLUGIN_DIR."/DAP-WP-LiveLinks/includes/myprofiletemplates/".$template."/css";	
	
	if( file_exists($csspath . "/custommyprofile.css") ) 
		$cssurl = $cssurl . "/custommyprofile.css";
	else 
		$cssurl = $cssurl . "/myprofile.css";
	
	$myprofile_content = str_replace( '[CSSURL]',$cssurl, $myprofile_content); 	
	
	if( isset($_GET['msg']) && ($_GET['msg'] != "") ) {
		if( defined($_GET['msg']) ) {
			$_GET['msg'] = constant($_GET['msg']);
		} else {
			$_GET['msg'] = str_replace("_"," ",$_GET['msg']);
		}
		$myprofile_content = str_replace( '[SUCCESSMSG]',$_GET['msg'], $myprofile_content);
        echo "<script>
		jQuery(document).ready(function(){
		jQuery('.confirmationMessage1').removeAttr('style');
		});
		</script>";
	}	
	

//	if($showuserprofileheading == 'Y') {
	//	$myprofile_content = str_replace( '[USER_PROFILE_HEADING_TEXT]',USER_PROFILE_HEADING_TEXT, $myprofile_content); 	
	//}
	
  	$myprofile_content = str_replace( '[ACCOUNT_DETAILS_HEADING_LABEL]',$account_info_heading, $myprofile_content);
	$myprofile_content = str_replace( '[BILLING_DETAILS_HEADING_LABEL]',$billing_info_heading, $myprofile_content);
	$myprofile_content = str_replace( '[ADDITIONAL_DETAILS_HEADING_LABEL]',$additional_info_heading, $myprofile_content);
	
  	 echo "<script> var ShownElements = [];</script>";	
	 
	 if($showaccountsection == "Y"){
		  echo "<script>ShownElements.push('#acc_info');</script>";	
	 }
	 if($showbillingsection == "Y"){
		  echo "<script>ShownElements.push('#bill_info');</script>";	
	 }
	 if($showadditionalsection == "Y"){
		  echo "<script>ShownElements.push('#add_info');</script>";	
	 }
	 
	//Start creating form fields
	if($showfirstname == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_FIRST_NAME_LABEL]',USER_PROFILE_FIRST_NAME_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_FIRST_NAME]',$user->getFirst_name(), $myprofile_content);
        echo "<script>ShownElements.push('#fname');</script>";		
	}
    else {
		$myprofile_content = str_replace( '[USER_PROFILE_FIRST_NAME]',$user->getFirst_name(), $myprofile_content); 
	}  
  	if($showlastname == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_LAST_NAME_LABEL]',USER_PROFILE_LAST_NAME_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_LAST_NAME]',$user->getLast_name(), $myprofile_content); 
		echo "<script>ShownElements.push('#lname');</script>";
	}
    else {
		$myprofile_content = str_replace( '[USER_PROFILE_LAST_NAME]',$user->getLast_name(), $myprofile_content); 
	}
	
  	if($showusername == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_USER_NAME_LABEL]',USER_PROFILE_USER_NAME_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_USER_NAME]',$user->getUser_name(), $myprofile_content); 

		$readonly = "";
		$style = "";
		if($user->getUser_name() != "") {
			$readonly = ' readonly="" ';
			$style='style="background-color: #CCCCCC;"';
		}
		
		$myprofile_content = str_replace( '[READONLY]',$readonly , $myprofile_content); 
		$myprofile_content = str_replace( '[STYLE_USERNAME]',$style, $myprofile_content); 
	   echo "<script>ShownElements.push('#uname');</script>";
	} else {
		$myprofile_content = str_replace( '[USER_PROFILE_USER_NAME]',$user->getUser_name(), $myprofile_content); 
	}  
	
	$myprofile_content = str_replace( '[USER_PROFILE_EMAIL_LABEL]',USER_PROFILE_EMAIL_LABEL, $myprofile_content); 	
	$myprofile_content = str_replace( '[USER_PROFILE_EMAIL]',$user->getEmail(), $myprofile_content); 
	if($showemail == "Y") {
		echo "<script>ShownElements.push('#email');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_EMAIL]',$user->getEmail(), $myprofile_content); 	
	}

	if($showpassword == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_NEW_PASSWORD_LABEL_DAPUSERPROFILE]',USER_PROFILE_NEW_PASSWORD_LABEL_DAPUSERPROFILE, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_REPEAT_PASSWORD_LABEL]',USER_PROFILE_REPEAT_PASSWORD_LABEL, $myprofile_content); 
		echo "<script>ShownElements.push('#pass');</script>";
		echo "<script>ShownElements.push('#cpass');</script>";
	}
	

	if($showaddress1 == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_ADDRESS1_LABEL]',USER_PROFILE_ADDRESS1_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_ADDRESS1]',$user->getAddress1(), $myprofile_content); 
		echo "<script>ShownElements.push('#add1');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_ADDRESS1]',$user->getAddress1(), $myprofile_content); 	
	}


	if($showaddress2 == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_ADDRESS2_LABEL]',USER_PROFILE_ADDRESS2_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_ADDRESS2]',$user->getAddress2(), $myprofile_content); 	
		echo "<script>ShownElements.push('#add2');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_ADDRESS2]',$user->getAddress2(), $myprofile_content); 	
	}
	
	if($showcity == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_CITY_LABEL]',USER_PROFILE_CITY_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_CITY]',$user->getCity(), $myprofile_content); 	
		echo "<script>ShownElements.push('#city');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_CITY]',$user->getCity(), $myprofile_content); 	
	}
	
	if($showstate == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_STATE_LABEL]',USER_PROFILE_STATE_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_STATE]',$user->getState(), $myprofile_content); 
		echo "<script>ShownElements.push('#state');</script>";		
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_STATE]',$user->getState(), $myprofile_content); 	
	}
	
	
	if($showzip == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_ZIP_LABEL]',USER_PROFILE_ZIP_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_ZIP]',$user->getZip(), $myprofile_content); 	
		echo "<script>ShownElements.push('#zip');</script>";
	}else {
		$myprofile_content = str_replace( '[USER_PROFILE_ZIP]',$user->getZip(), $myprofile_content); 	
	}
	
	if($showcountry == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_COUNTRY_LABEL]',USER_PROFILE_COUNTRY_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_COUNTRY]',$user->getCountry(), $myprofile_content); 	
		echo "<script>ShownElements.push('#country');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_COUNTRY]',$user->getCountry(), $myprofile_content); 	
	}
	
	if($showphone == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_PHONE_LABEL]',USER_PROFILE_PHONE_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_PHONE]',$user->getPhone(), $myprofile_content); 
		echo "<script>ShownElements.push('#phone');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_PHONE]',$user->getPhone(), $myprofile_content); 	
	}
	
	if($showfax == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_FAX_LABEL]',USER_PROFILE_FAX_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_FAX]',$user->getFax(), $myprofile_content); 
		echo "<script>ShownElements.push('#fax');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_FAX]',$user->getFax(), $myprofile_content); 	
	}
	
	if($showcompany == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_COMPANY_LABEL]',USER_PROFILE_COMPANY_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_COMPANY]',$user->getCompany(), $myprofile_content); 
		echo "<script>ShownElements.push('#company');</script>";
	}
	else {
		$myprofile_content = str_replace( '[USER_PROFILE_COMPANY]',$user->getCompany(), $myprofile_content); 	
	}
	
	if($showtitle == "Y") {
		$myprofile_content = str_replace( '[USER_PROFILE_TITLE_LABEL]',USER_PROFILE_TITLE_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_TITLE]',$user->getTitle(), $myprofile_content);
		echo "<script>ShownElements.push('#title');</script>";
	}else {
		$myprofile_content = str_replace( '[USER_PROFILE_TITLE]',$user->getTitle(), $myprofile_content); 	
	}
	
	
	//if( (Dap_Config::get('DISPLAY_AFFILIATE') == "Y") && ($showpaypalemail == "Y") ) {
	if($showpaypalemail == "Y")  {
		$myprofile_content = str_replace( '[USER_PROFILE_PAYPAL_EMAIL_LABEL]',USER_PROFILE_PAYPAL_EMAIL_LABEL, $myprofile_content); 
		$myprofile_content = str_replace( '[USER_PROFILE_PAYPAL_EMAIL_EXTRA_LABEL]',USER_PROFILE_PAYPAL_EMAIL_EXTRA_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_PAYPAL_EMAIL]',$user->getPaypal_email(), $myprofile_content); 
		echo "<script>ShownElements.push('#pay_email');</script>";
	}
  	else {
		$myprofile_content = str_replace( '[USER_PROFILE_PAYPAL_EMAIL]',$user->getPaypal_email(), $myprofile_content); 
	}
	
	if($showoptedout == "Y") {
		$isChecked = ($user->getOpted_out() == "N") ? " checked='.$isChecked.' " : "";
		logToFile("isChecked: $isChecked , user->getOpted_out(): " . $user->getOpted_out());
		$myprofile_content = str_replace( '[USER_PROFILE_UNSUBSCRIBE_LABEL]',USER_PROFILE_UNSUBSCRIBE_LABEL, $myprofile_content); 	
		$myprofile_content = str_replace( '[USER_PROFILE_IS_CHECKED]',$isChecked, $myprofile_content); 
		echo "<script>ShownElements.push('#sub_checkbox');</script>";
	}
	
	if($formalign != "")
	{   
		echo '<script>
		jQuery(document).ready(function(){
			jQuery("#dapprofile-container1").css("float","'.$formalign.'");
		});
		
		</script>';
	}
	
	if( ($showcustomfields == "Y") || ($showcustomfields != "") ) {
		$customFieldsArray = null;
		if($showcustomfields != "Y") {
			$customFieldsArray = explode(",",$showcustomfields);
		}
		

		$customFields = Dap_CustomFields::loadUserFacingCustomFields();
		if($customfieldvalues != "all"){
			$customArray = explode(',',$customfieldvalues);
		}else{
			$customArray = array();
			//$customFields = array_merge($customFields,$customArray);
		}
	
		foreach ($customFields as $custom) {
			if(empty($customArray) || in_array($custom['id'],$customArray)){
			$mycustom_tmp_fields_profile_content=$mycustom_fields_profile_content;
			logToFile("userprofile.inc.php: loadCustomFields(): id =" . $custom['id']);
			logToFile("userprofile.inc.php: loadCustomFields(): userId =" . $userId);
			$required = " ";
			$requiredStar = "";
 
			if ($custom['showonlytoadmin'] == "Y") {
				continue;
			}
			
			//logToFile("custom[name]: " . $custom['name']); 
			if ( !is_null($customFieldsArray) && !in_array($custom['name'],$customFieldsArray) ){
				continue;
			}
			
			if($custom['required'] == "Y") {
				$required = "required";
				$requiredStar = "*";
			}
			
			$user_custom_value = Dap_UserCustomFields::loadUserCustomFieldsByCustomFieldId($custom['id'], $userId);
			$value = "";
			
			if ($user_custom_value) {
				foreach ($user_custom_value as $val) {
					//logToFile("val=" . $val['custom_value']);
					$value = $val['custom_value'];
				}
			}
			
			$mycustom_tmp_fields_profile_content = str_replace( '[REQUIREDSTAR]', $requiredStar, $mycustom_tmp_fields_profile_content);
			$mycustom_tmp_fields_profile_content = str_replace( '[CUSTOM_FIELD_LABEL]', $custom['label'], $mycustom_tmp_fields_profile_content); 	
			$mycustom_tmp_fields_profile_content = str_replace( '[CUSTOM_FIELD_NAME]', $custom["name"], $mycustom_tmp_fields_profile_content); 
			$mycustom_tmp_fields_profile_content = str_replace( '[CUSTOM_FIELD_VALUE]', $value, $mycustom_tmp_fields_profile_content); 
			$mycustom_tmp_fields_profile_content = str_replace( '[REQUIRED]', $required, $mycustom_tmp_fields_profile_content); 
			$custom_fields_content.=$mycustom_tmp_fields_profile_content;
			} // end if in_array
		} //end foreach	
	}
	$myprofile_content = str_replace( '[CUSTOM_FIELDS]',$custom_fields_content, $myprofile_content); 
	
	$myprofile_content = str_replace( '[BUTTON_UPDATE_TEXT]',BUTTON_UPDATE, $myprofile_content); 
	$myprofile_content = str_replace( '[REDIRECT]',$redirectTo, $myprofile_content); 
	$myprofile_content .= MSG_REQUIRED_FIELD;
	$myjquerycode = "<script>
	jQuery(document).ready(function(){
		for(var i =0;i < (ShownElements.length);i++){
			jQuery(ShownElements[i]).removeAttr('style');
		}
		
	});
	</script>";

	return $myprofile_content.$jqueryForResponsive.$myjquerycode;
}


function getMyProfileTemplateContent($template_path, $profileTemplateName, $customProfileTemplateName)
{
	if(file_exists($template_path.$customProfileTemplateName)) {
	  $myprofile_template_content = file_get_contents($template_path.$customProfileTemplateName);
	}
	
	if($myprofile_template_content == "") {
		if(file_exists($template_path.$profileTemplateName)) {
			$myprofile_template_content = file_get_contents($template_path.$profileTemplateName);
		}
		else{
			$myprofile_template_content = "";
		}
	
		logToFile("DAP-ShortCodes-Transactions: no custom template found=".$template_path.$profileTemplateName,LOG_INFO_DAP);
	} else {
		logToFile("DAP-ShortCodes-Transactions: custom template found=".$template_path.$customProfileTemplateName,LOG_INFO_DAP);  
	}	

	return $myprofile_template_content;
	
}

?>