<?php 

add_shortcode('DAPMyContent', 'dap_mycontent');


function dap_mycontent($atts, $content=null){ 
	extract(shortcode_atts(array(
		'showproductimage' => 'Y',
		'showproductname' => 'Y',
		'showaccessstartdate' => 'Y',
		'showaccessenddate' => 'Y',
		'showlinks' => 'Y',
		'orderoflinks' => 'NEWESTFIRST',
		'howmanylinks' => '10000',		
		'errmsgtemplate' => 'SHORT',
		'productid' => 'ALL',
		'dateformat' => 'MM-DD-YYYY',
		'showproductcount' => 'Y',
		'showrenewalhtml' => 'Y',
		'hideproductid' => 'NONE',
		'mycontentheadertext' => 'My Content',
		'availcontentheadertext' => 'Available Content',
		'upcomingcontentheadertext' => 'Upcoming Content',
		'showavailablecontent' => 'Y',
		'showupcomingcontent' => 'Y',
		'howmanyupcominglinks' => '10000',
		'upcomingindaystext' => 'Coming Soon (In Days)',
		'clickherelinktext' => 'Click Here To Access Content',
		'showlinkscollapsed' => 'N',
		'nocontenttext' => 'No Content Available Currently'
	), $atts));
	
	
	$mycontentheadertext=urldecode($mycontentheadertext);
	$availcontentheadertext=urldecode($availcontentheadertext);
	$upcomingcontentheadertext=urldecode($upcomingcontentheadertext);
	$upcomingindaystext=urldecode($upcomingindaystext);
	$clickherelinktext=urldecode($clickherelinktext);
	$nocontenttext=urldecode($nocontenttext);
	
	$jsurl=get_option('siteurl')."/wp-content/plugins/DAP-WP-LiveLinks/includes/js/contentdrip.js";	
	wp_enqueue_script('condcontdripjs', $jsurl, array('jquery'));
	$session = Dap_Session::getSession();
	$user = $session->getUser();
	
	logToFile("mycontentheadertext=".$mycontentheadertext);
	
	if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		//logToFile("Not logged in, returning errmsgtemplate");
		$errorHTML = mb_convert_encoding(MSG_PLS_LOGIN, "UTF-8", "auto") . " <a href=\"" . Dap_Config::get("LOGIN_URL") . "\">". mb_convert_encoding(MSG_CLICK_HERE_TO_LOGIN, "UTF-8", "auto") . "</a>";
		return $errorHTML;
	}
	
	$userId = $user->getId();
	$userProducts = null;
		
	if( isset($user) ) {
		$userProducts = Dap_UsersProducts::loadProducts($user->getId());
	}

	if($showproductcount == 'Y') {
		$content = USER_LINKS_YOUCURRENTLYHAVEACCESSTO_TEXT . count($userProducts). USER_LINKS_PRODUCTS_TEXT . "<br/><br/>";
	}
	
	$template="template1";
	$regulardrip_template_path=ABSPATH . "/wp-content/plugins/DAP-WP-LiveLinks/includes/regularcontdrip/".$template;
	
	//REGULAR DRIP HEADER TEMPLATE CONTENT
	$regularDripHeaderTemplateName="/DAPRegularDripHeaderTemplate.html";
	$customRegularDripHeaderTemplateName="/customDAPRegularDripHeaderTemplate.html";
	$regulardrip_head_temp_content=getDripTemplateContent($regulardrip_template_path, $regularDripHeaderTemplateName, $customRegularDripHeaderTemplateName);
	
	$regcssurl=get_option('siteurl')."/wp-content/plugins/DAP-WP-LiveLinks/includes/regularcontdrip/".$template."/css/regularcontentdrip.css?ver=1";	
	$regulardrip_head_temp_content = str_replace( '[CSSURL]',$regcssurl, $regulardrip_head_temp_content); 	
	
	//REGULAR DRIP  INDIVIDUAL PRODUCT TEMPLATE
	$regularProductTemplateName="/DAPRegularDripIndividualProductTemplate.html";
	$customRegularProductTemplateName="/customDAPRegularDripIndividualProductTemplate.html";
	$regulardrip_prod_temp_content=getDripTemplateContent($regulardrip_template_path, $regularProductTemplateName, $customRegularProductTemplateName);
	
	$regularDripTemplateName="/DAPRegularDripAvailableContentTemplate.html";
	$customRegularDripTemplateName="/customDAPRegularDripAvailableContentTemplate.html";
	$regulardrip_temp_content=getDripTemplateContent($regulardrip_template_path, $regularDripTemplateName, $customRegularDripTemplateName);
	
	//REGULAR DRIP INDIVIDUAL CONTENT TEMPLATE
	$upcomingRegularDripTemplateName="/DAPRegularDripUpcomingContentTemplate.html";
	$customUpcomingRegularDripTemplateName="/customDAPRegularDripUpcomingContentTemplate.html";
	$upcoming_regulardrip_temp_content=getDripTemplateContent($regulardrip_template_path, $upcomingRegularDripTemplateName, $customUpcomingRegularDripTemplateName);
	
	$ind_prod_content="";
	//loop over each product from the list
	logToFile("DAP-ShortCodes-Transactions: Iterate through each user->product",LOG_INFO_DAP);
	
	foreach ($userProducts as $userProduct) {
		if($hideproductid != "NONE") {
			$hideProductIdArray = explode(",",$hideproductid);
			if( in_array($userProduct->getProduct_id(), $hideProductIdArray) ) {
				continue;
			}
		}
		
		if($productid != "ALL") {
			if(strstr($productid,",")!=false) {
			  $productIdArray = explode(",",$productid);
			  if( !in_array($userProduct->getProduct_id(), $productIdArray) ) {
				  continue;
			  }
			}
			else {
				$prodid=$productid;
				if( $userProduct->getProduct_id() != $prodid ) {
				  //logToFile("DAP-ShortCodes-MemberLinks: prodid=" . $prodid . "  prodid=".$userProduct->getProduct_id());
				  continue;
				}
			}
			logToFile("DAP-ShortCodes-MemberLinks: prodid=".$userProduct->getProduct_id());
		}
		
		$product = Dap_Product::loadProduct($userProduct->getProduct_id());

		if(!isset($product)) {
			logToFile("DAP-ShortCodes-MemberLinks: no product found");
			return $content;
		}
		
		logToFile("DAP-ShortCodes-MemberLinks: product found");
		
		//$dripType=$product->getDrip_type();
		
		$dripType=1;
		
		if($dripType==2)  {
			logToFile("DAP-ShortCodes-MemberLinks: can't process conditional drip products");
			continue;
		}
		
		$productId=$product->getId();
		logToFile("DAP-ShortCodes-MemberLinks:  product(id=$productId) found");
		logToFile("DAP-ShortCodes-MemberLinks:  user(id=$userId) found");
		
		$expired = false;
		
		if($user->hasAccessTo($product->getId()) === false) {
			$expired = true;
		}
		
		//access start date
		$oldDate = $userProduct->getAccess_start_date();
		//$middle = strtotime($oldDate);
		$middle = new DateTime($oldDate);
		$stringFormat = "";
		if($dateformat == "MM-DD-YYYY") {
			$stringFormat = "m-d-Y";
		} else if($dateformat == "DD-MM-YYYY") {
			$stringFormat = "d-m-Y";
		}  else if($dateformat == "YYYY-MM-DD") {
			$stringFormat = "Y-m-d";
		}
		//$newDate = date($stringFormat, $middle);
		$newStartDate = $middle->format($stringFormat);

		//access start date
		$oldDate = $userProduct->getAccess_end_date();
		//$middle = strtotime($oldDate);
		$middle = new DateTime($oldDate);
		$stringFormat = "";
		if($dateformat == "MM-DD-YYYY") {
			$stringFormat = "m-d-Y";
		} else if($dateformat == "DD-MM-YYYY") {
			$stringFormat = "d-m-Y";
		}  else if($dateformat == "YYYY-MM-DD") {
			$stringFormat = "Y-m-d";
		}
		//$newDate = date($stringFormat, $middle);
		$newEndDate = $middle->format($stringFormat);
		
		$highlightCode = "";
		if($expired) {
		  $highlightCode = ' class="dap_highlight_bg" ';
		}
		
		$post_cancel_access = Dap_Config::get("POST_CANCEL_ACCESS");
		if(isset($post_cancel_access)) {
			$post_cancel_access = strtolower($post_cancel_access);
		}		
			
		logToFile("DAP-ShortCodes-MemberLinks: user's access has expired to $productId.. check... renewal html");
		
		if($dripType==2) {
			continue;
		}
		else 
			$ind_prod_content = $regulardrip_prod_temp_content;
			
		if( $expired && ($showrenewalhtml == "Y") ) {
			//If user's access to product has expired, then show renewal HTML
			//$content .= '<div>'.$product->getRenewal_html().'</div>';
			$ind_prod_content = str_replace( '[RENEWAL_HTML]',$product->getRenewal_html(), $ind_prod_content);
			$ind_prod_content = str_replace( '[RENEWALHTMLVISIBILITY]',"block", $ind_prod_content); 
			
			logToFile("DAP-ShortCodes-MemberLinks: user's access has expired.. show renewal html");
			
		}
		else {
			$ind_prod_content = str_replace( '[RENEWAL_HTML]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[RENEWALHTMLVISIBILITY]',"none", $ind_prod_content); 
		}
		
		
		logToFile("DAP-ShortCodes-MemberLinks: before showlinks: ".strtolower($showlinks));
		//$dripType=$product->getDrip_type();
		
		
		if(strtolower($showproductimage)=="y") {
			$prod_image=$product->getProduct_image_url();
			if($prod_image=="")
				$prod_image="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRCI53HWVRvF_uRmJxVDLfgKuOLjksNq3xXbRMv8IfbJ7t56Uf8";
				
			$ind_prod_content = str_replace( '[PRODUCT_IMAGE]',$prod_image, $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCTIMGVISIBILITY]',"block", $ind_prod_content); 	
		}
		else {
			$ind_prod_content = str_replace( '[PRODUCTIMGVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_IMAGE]',"", $ind_prod_content); 
		}
		
		
		if(strtolower($showproductname)=="y") {
			$ind_prod_content = str_replace( '[PRODUCT_NAME]',$product->getName(), $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCTNAMEVISIBILITY]',"block", $ind_prod_content); 
		}
		else {
			$ind_prod_content = str_replace( '[PRODUCTNAMEVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_NAME]',"", $ind_prod_content); 
		}
		
		if(strtolower($showaccessstartdate)=="y") {
			$ind_prod_content = str_replace( '[ASD]',$newStartDate, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSSTARTVISIBILITY]',"block", $ind_prod_content); 
		}
		else {
			$ind_prod_content = str_replace( '[ASD]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSSTARTVISIBILITY]',"none", $ind_prod_content); 
		}
		
		if(strtolower($showaccessenddate)=="y") {
			$ind_prod_content = str_replace( '[AED]',$newEndDate, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSENDVISIBILITY]',"block", $ind_prod_content); 
		}
		else {
			$ind_prod_content = str_replace( '[AED]',$newEndDate, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSENDVISIBILITY]',"none", $ind_prod_content); 
		}
	
		
		$ind_prod_content = str_replace( '[PRODUCTID]',$product->getId(), $ind_prod_content); 
		$ind_prod_content = str_replace( '[PRODUCT_DESCRIPTION]',$product->getDescription(), $ind_prod_content); 
		
		
		$course_content="";
		
		if(strtolower($showlinks) == "y") {
			if(strtolower($showlinkscollapsed) == "y") {
				$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
				$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickherelinktext, $ind_prod_content); 
				$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"block", $ind_prod_content); 
				logToFile("DAP-ShortCodes-MemberLinks: showlinkscollapsed: ".strtolower($showlinkscollapsed));
			}
			else{
			
				$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
				$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"block", $ind_prod_content); 	
				$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickherelinktext, $ind_prod_content); 
			}
		}
		else {
				$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
				$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickherelinktext, $ind_prod_content); 
				$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content);
		}
		
		if(strtolower($showlinks) == "y") {
			$orderBy = "desc";
			
			if(strtolower($orderoflinks) == "oldestfirst") {
			  $orderBy = "asc";
			}
			$sss = $product->getSelf_service_allowed();
			
			//get currently available content
			
			$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_LINKS_HREF]',"", $ind_prod_content); 
			
			if( (strtolower($showavailablecontent) == "y") || (strtolower($showupcomingcontent) == "y")) {
				$ind_prod_content = str_replace( '[LINKS]',$mycontentheadertext, $ind_prod_content); 
				$ind_prod_content = str_replace( '[STATUS]',$statusheader, $ind_prod_content); 
			
				if(strtolower($showavailablecontent) == "y") {
					$ind_prod_content = str_replace( '[AVAILABLE_CONTENT_HEADER]',$availcontentheadertext, $ind_prod_content); 
					$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"block", $ind_prod_content); 
					$course_content="";
					
					//$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
					if ($product->getSelf_service_allowed() == "N") { 
						$resources=$userProduct->getActiveResources($product->getSelf_service_allowed(),$orderBy,$howmanylinks,"ARRAY");
					}
					else {
						 $resources=$userProduct->getActiveResources("Y",$orderBy,$howmanylinks,"ARRAY");
					}
					
					$contentFound=false;
					if(isset($resources)) {
						$drip_content="";
						foreach ($resources as $resource) {
							logToFile("DAP-ShortCodes-MemberLinks: course_content: ".$course_content);
							$drip_content .=  getRegularDripAndSSSResources($resource,$userId,$productId,$sss,$post_cancel_access,$regulardrip_temp_content); 
							if($drip_content!="")
								$contentFound=true;
						}
					}
					if($contentFound==false) {
						logToFile("DAP-ShortCodes-MemberLinks: nocontenttext: ".$nocontenttext);
						$drip_content="<tr><td>$nocontenttext</td></tr>";
					}
					$ind_prod_content = str_replace( '[AVAILABLE_CONTENT]',$drip_content, $ind_prod_content); 	
					
				} //showavailablecontent==Y
				else {
					$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"none", $ind_prod_content); 
				}
				
				if( strtolower($showupcomingcontent) == "y") {
				  	$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"block", $ind_prod_content); 
					$ind_prod_content = str_replace( '[UPCOMING_CONTENT_HEADER]',$upcomingcontentheadertext, $ind_prod_content); 
					$ind_prod_content = str_replace( '[UPCOMING_IN_DAYS_HEADER]',$upcomingindaystext, $ind_prod_content);
					
					$upcomingLaterResources=$userProduct->getFutureResources("N",$orderBy,$howmanyupcominglinks,"Y","ARRAY");
					$upcoming_regular_drip_content="";
					$contentFound=false;
					if(isset($upcomingLaterResources)) {
						foreach ($upcomingLaterResources as $resource) {
							logToFile("DAP-ShortCodes-MemberLinks: get upcoming regular drip resources: ". $upcoming_regulardrip_temp_content);
							$upcoming_regular_drip_content .= getRegularDripAndSSSFutureResources($resource,$userId,$productId,$sss,$post_cancel_access,$upcoming_regulardrip_temp_content); 
							if($upcoming_regular_drip_content!="")
								$contentFound=true;
						}
					}
					if($contentFound==false) {
						logToFile("DAP-ShortCodes-MemberLinks: upcoming nocontenttext: ".$nocontenttext);
						$upcoming_regular_drip_content="<tr><td>$nocontenttext</td></tr>";
					}
					
					$ind_prod_content = str_replace( '[UPCOMING_CONTENT]',$upcoming_regular_drip_content, $ind_prod_content); 
					
				}
				else {
					$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
					
				}
			} //if( (strtolower($showavailablecontent) == "y") || (strtolower($showupcomingcontent) == "y")) {
			
		} //showlinks
		else {
			//hidelinks
			$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
			$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"none", $ind_prod_content);
			$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_LINKS_HREF]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',"", $ind_prod_content); 
		}
		
		$productList.=$ind_prod_content;
		//upcoming content
		
	} //end foreach
	
	$content .= '<link rel="stylesheet" href="'. $regcssurl  . '"/>';
	$content .= '<link rel="stylesheet" href="'. $cssurl  . '"/>';
	
	$regulardrip_head_temp_content = str_replace( '[PRODUCTS]', $productList, $regulardrip_head_temp_content); 	
	$content .= $regulardrip_head_temp_content;	
	
	return $content;
}


function getRegularDripAndSSSResources($row,$userId,$productId,$sss,$post_cancel_access,$course_temp_content) {

	$product = Dap_Product::loadProduct($productId);
	$isSSSMaster=$product->getIs_master();
	logToFile("userlinks.inc: isSSSMaster=". $isSSSMaster);
			
	if (($sss == "N") || ($isSSSMaster == "Y")){
		$regdrip=true;
	}
	else {
		$regdrip=false;
		$sssHasAccess=true;
	}
	
	
	$row["name"] = mb_convert_encoding($row["name"], "UTF-8", "auto");
	logToFile("Resource URL:".$row["url"]);
	
	//lets present two modes of operation 
	if($sssHasAccess){
		logToFile("DAP_UsersProducts.class.php: getActiveResources(): SSS product, show content list");
	}
	else if($post_cancel_access == 'y') {
		//we have dates on the resource
		if($row["res_start_days"] <> 0 && $row["res_start_days"] <> "" &&
			$row["res_end_days"] <> 0 && $row["res_end_days"] <> "" ) {
			//set resource start days 
			$resource_start_days = $row["res_start_days"];
			$resource_end_days = $row["res_end_days"];
		}
					
		//we have "days" on the resource
		if($row["start_day"] <> 0 && $row["start_day"] <> "" &&
			$row["end_day"] <> 0 && $row["end_day"] <> "" ) {
			//set resource start days 
			$resource_start_days = $row["access_start_days"] + $row["start_day"] - 1 ;
			$resource_end_days = $row["access_start_days"] + $row["end_day"] - 1 ;
			
			//logToFile("We have 'days' on the resource"); 
			//logToFile("resource_start_days: $resource_start_days"); 
			//logToFile("resource_end_days: $resource_end_days"); 
			//logToFile("User's row[access_start_days]: " . $row["access_start_days"]); 
			
			
			/**
				So if resource "days" are hard-coded, but resource end day is in the past,
				then do not allow access. Will help offer expiring bonuses even though post-cancel-access is yes.
			*/
			if($row["today"] > $resource_end_days ) { //Expiring Bonuses
				//logToFile("Sorry: The 'end day' for this content is in the past...");
				//logToFile("DAP001");
				return "";
			}					
		}
	
		//if resource starts in future, lets not grant access.
		if($row["today"] < $resource_start_days ) {
			//logToFile("Resource Start Date is in future...");
			//logToFile("DAP001");
			return "";
		}					
	
		
		//
		// If resource ends before upj start date - then no acesss. This could only happen 
		//    when calendar dates are used at the resource level.
		// If resource starts after upj end date - then no access. This could only happen
		//    when calendar dates are used at the resource level.
		//
		//
		if($resource_end_days < $row["access_start_days"] ||
			$resource_start_days > $row["access_end_days"]) {
			//logToFile("Product Start Date is in future...");
			//logToFile("Resource Start Days:".$resource_start_days);
			//logToFile("Resource End Days:".$resource_end_days);
			//logToFile("Access Start Days:".$row["access_start_days"]);
			//logToFile("Access End Days:".$row["access_end_days"]);
			//logToFile("DAP005");
			//logToFile($ERROR_CODES["DAP005"]);
			return "";		
		}				
	
	} else {				
		//Product did not lauch yet.
		if($row["today"] < $row["access_start_days"]) {
			//logToFile("Product Start Date is in future...");
			//logToFile("DAP001");
			return "";
			//return $resource;						
			//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP001"];
			//$_SESSION['ERROR_URL'] = $row['error_page_url'];
			//return FALSE;
		}
		//Product expired 
		if($row["today"] > $row["access_end_days"]) {
			//logToFile("Product End Date is in past...");
			//logToFile("DAP002");
			return "";
		}
		//product is available.
		//check start date(days).
		if($row["res_start_days"] <> 0 && $row["res_start_days"] <> "" &&
			$row["res_end_days"] <> 0 && $row["res_end_days"] <> "" ) {
			//resource is available in future.
			if($row["today"] < $row["res_start_days"]) {
				//logToFile("Resource Start Date  is in future...");
				//logToFile("DAP003");
				return "";
			}
			//resource  expired.
			if($row["today"] > $row["res_end_days"]) {
				//logToFile("Resource End Date  is in past...");
				//logToFile("DAP004");
				return "";
			}
			//logToFile("Start Days(Date) and End Days(Date) check passed...");
	
		} else {
			//logToFile("Start Days(Date) and End Days(Date) are empty or ZERO.. not checking...");
		}
		//check start day
		$lag_days = $row["today"] - $row["access_start_days"] + 1;
		//check resource start and end day only if they are both non zero. 
		if($row["start_day"] <> 0 && $row["start_day"] <> "" &&
			$row["end_day"] <> 0 && $row["end_day"] <> "" ) {
	
			//resource is available in future.
			if($lag_days < $row["start_day"]) {
				//logToFile("Lag Days:".$lag_days);
				//logToFile("Start Day:".$row["start_day"]);
				//logToFile("Resource Start Day  is in future...");
				//logToFile("DAP003");
				return "";
				//return $resource;
				//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP003"];
				//$_SESSION['DAP_ERROR_URL'] = $row['error_page_url'];
				//return FALSE;
			}
			//resource availability expired.
			if($lag_days > $row["end_day"]) {
				//logToFile("Lag Days:".$lag_days);
				//logToFile("End Days:".$row["end_day"]);
				//logToFile("Resource Start Day  is in past...");
				//logToFile("DAP004");
				return "";
				//return $resource;
				//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP004"];
				//$_SESSION['DAP_ERROR_URL'] = $row['error_page_url'];
				//return FALSE;
			}
			//logToFile("Start Day and End Day check passed...");
			} else {
				//logToFile("Start Day and End Day are empty or ZERO.. not checking...");
			}
		} //end of config allow post cancel access
		if(!Dap_Resource::isCountAvailable($userId, $row['url'])) {
			//logToFile("Click Count is Negative...");
			return "";
		}
		if(!Dap_Resource::displayResource($row['url'])) {
			//logToFile("This Resource is not displayable...");
			return "";
		}
		//grant access - we should reach here ONLY IF THE PRODUCT RESOURCE RELATIONSHIP IS CLEAN AND ALLOWED.
		//return $resource;
		//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP002"];
		//logToFile("Granting Access To Resource URL:".$row["url"].", Resource ID:".$row["resource_id"]);
		if($row['name'] == "") {
			$name = $row['url'];
		}
		else {
			$name = mb_convert_encoding($row["name"], "UTF-8", "auto");
			//$name = "";
		}
	
		$name = stripslashes($name);
		$current_msg=$course_temp_content;
		$current_msg = str_replace( '[CONTENTNAME]', $name, $current_msg); 
		$current_msg = str_replace( '[CONTENTURL]', $row['url'], $current_msg); 
		$current_msg = str_replace( '[TEXTTYPE]', "text-success", $current_msg); 
		
		logToFile("This Resource = " .  $row['url'] . " is  displayable...");
		
		return $current_msg;
}

function getRegularDripAndSSSFutureResources($row,$userId,$productId,$sss,$post_cancel_access,$course_temp_content) {

	$comingInDays = "";
	
	logToFile("getRegularDripAndSSSFutureResources(): URL=" .$row["url"] ); 
	
	if($row["res_start_days"] <> 0 && $row["res_start_days"] <> "" &&
		$row["res_end_days"] <> 0 && $row["res_end_days"] <> "" ) {
		//set resource start days 
		$resource_start_days = $row["res_start_days"];
		$resource_end_days = $row["res_end_days"];
	}
					
	//we have "days" on the resource
	if($row["start_day"] <> 0 && $row["start_day"] <> "" &&
		$row["end_day"] <> 0 && $row["end_day"] <> "" ) {
		//set resource start days 
		$resource_start_days = $row["access_start_days"] + $row["start_day"] - 1 ;
		$resource_end_days = $row["access_start_days"] + $row["end_day"] - 1 ;
		
		//logToFile("We have 'days' on the resource"); 
		logToFile("resource_start_days: $resource_start_days"); 
	}
	
	//if resource start dau was in past, then user already has access - so ignore for future content list
	if($row["today"] > $resource_start_days ) {
		logToFile("Resource Start Date is in past...");
		logToFile("DAP001");
		return;
	}					

		
	//Product expired 
	if($row["today"] > $row["access_end_days"]) {
		
		logToFile("Product End Date is in past...");
		logToFile("DAP002");
		return;
	}
	//check start day
	$lag_days = $row["today"] - $row["access_start_days"] + 1;
	//check resource start and end day only if they are both non zero. 
	if($row["start_day"] <> 0 && $row["start_day"] <> "" &&
		$row["end_day"] <> 0 && $row["end_day"] <> "" ) {

		//resource is available in future.
		if($lag_days > $row["start_day"]) {
			//logToFile("Lag Days:".$lag_days);
			//logToFile("Start Day:".$row["start_day"]);
			//logToFile("Resource Start Day  is in future...");
			logToFile("DAP003");
			return;
			//return $resource;
			//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP003"];
			//$_SESSION['DAP_ERROR_URL'] = $row['error_page_url'];
			//return FALSE;
		}
		//resource availability expired.
		if($lag_days > $row["end_day"]) {
			//logToFile("Lag Days:".$lag_days);
			//logToFile("End Days:".$row["end_day"]);
			//logToFile("Resource Start Day  is in past...");
			logToFile("DAP004");
			return;
			//return $resource;
			//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP004"];
			//$_SESSION['DAP_ERROR_URL'] = $row['error_page_url'];
			//return FALSE;
		}
		//logToFile("Start Day and End Day check passed...");
	} else {
			//logToFile("Start Day and End Day are empty or ZERO.. not checking...");
	}
	//} //end of config allow post cancel access
	if(!Dap_Resource::isCountAvailable($userId, $row['url'])) {
			//logToFile("Click Count is Negative...");
			return;
	}
	if(!Dap_Resource::displayResource($row['url'])) {
			//logToFile("This Resource is not displayable...");
			return;
	}
		//grant access - we should reach here ONLY IF THE PRODUCT RESOURCE RELATIONSHIP IS CLEAN AND ALLOWED.
		//return $resource;
		//$_SESSION['DAP_ERROR'] = $ERROR_CODES["DAP002"];
		//logToFile("Granting Access To Resource URL:".$row["url"].", Resource ID:".$row["resource_id"]);
	if($row['name'] == "") {
			$name = $row['url'];
	}
	else {
			$name = mb_convert_encoding($row["name"], "UTF-8", "auto");
			//$name = "";
	}

	$name = stripslashes($name);
	$comingInDays = intval($resource_start_days) - intval($row["today"]) + 1;
	

	
	$name = stripslashes($name);
	$current_msg=$course_temp_content;
	$current_msg = str_replace("XXX",$comingInDays,$current_msg);

	$current_msg = str_replace( '[CONTENTNAME]', $name, $current_msg); 
	$current_msg = str_replace( '[CONTENTURL]', $row['url'], $current_msg); 
	$current_msg = str_replace( '[TEXTTYPE]', "text-success", $current_msg); 
	
	logToFile("This Resource = " .  $row['url'] . " is  displayable...");
	return $current_msg;
}



function getDripTemplateContent($template_path, $courseTemplateName, $customCourseTemplateName)
{
	if(file_exists($template_path.$customCourseTemplateName)) {
	  $course_temp_content = file_get_contents($template_path.$customCourseTemplateName);
	}
	
	if($course_temp_content == "") {
		if(file_exists($template_path.$courseTemplateName)) {
			$course_temp_content = file_get_contents($template_path.$courseTemplateName);
		}
		else{
			$course_temp_content = "";
		}
	
		logToFile("DAP-ShortCodes-Transactions: no custom template found=".$template_path.$courseTemplateName,LOG_INFO_DAP);
	} else {
		logToFile("DAP-ShortCodes-Transactions: custom template found=".$template_path.$customCourseTemplateName,LOG_INFO_DAP);  
	}	
	
	return $course_temp_content;
	
}
?>