<?php 

require_once("DAP-ShortCodes-MyContent.php");
require_once("DAP-ShortCodes-MyProfile.php");
require_once("DAP-ShortCodes-AffiliateInfo.php");
require_once("DAP-ShortCodes-Invoice.php");
require_once("DAP-ShortCodes-Cancel.php");
require_once("DAP-ShortCodes-LoginForm.php");


/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_memberpages_tinymce_media_button');
function add_memberpages_tinymce_media_button($context){

return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=memberpages_shortcode_popup&width=640&height=513\"
 class=\"button thickbox dap-icon wp-menu-image dashicons-before dashicons-admin-generic\" onclick=\"dap_open_front_page()\" id=\"memberpages_shortcode_popup_button\" title=\"Generate Member Pages\">DAP Member Pages</a>");
}

add_action('admin_footer','memberpages_shortcode_media_button_popup');


$lldocroot = defined('SITEROOT') ? SITEROOT : $_SERVER['DOCUMENT_ROOT'];
if(file_exists($lldocroot . "/dap/dap-config.php")) include_once ($lldocroot . "/dap/dap-config.php"); // dap config call


//Generate inline content for the popup window when the "my shortcode" button is clicked
function memberpages_shortcode_media_button_popup(){
	try{
        $customFieldsList = Dap_CustomFields::loadUserFacingCustomFields();  // get dap custom fields.
      }
    catch (PDOException $e) {
	   return ERROR_DB_OPERATION;
	  } catch (Exception $e) {
	   return ERROR_GENERAL;
	  } 


?>

<div id="memberpages_shortcode_popup" style="display:none;">
      
	<div class="dap_membership_div col-xs-12 member_outer_main " >
		<div class="col-xs-4 dap_membership_images" >
		  <div class="shadow-box">
			<img class="img-responsive" src="<?php echo $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/dap-my-content.jpg";  ?>" />
		 <span class="member_input-outer"> <input type="radio" name="membership_page_option" id="membership_my_content" value="DAP My Content" checked>My Content</span> 		  
			</div>
			</div>
		<div class="col-xs-4 dap_membership_images" >
		  <div class="shadow-box">
			<img class="img-responsive" src="<?php echo $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/dap-my-profile.jpg";  ?>" />
			<span class="member_input-outer">	<input type="radio" name="membership_page_option" id="membership_my_profile" value="DAP My Profile">My Profile</span>
		</div>
		</div>
		<div class="col-xs-4 dap_membership_images" >
		  <div class="shadow-box">
			<img class="img-responsive" src="<?php echo $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/dap-affiliate-info.jpg";  ?>"  />
			<span class="member_input-outer">	<input type="radio" name="membership_page_option" id="membership_my_affiliate" value="DAP Affiliate Info">Affiliate Info & Stats</span>
		</div>
		</div>
		<div class="col-xs-4 dap_membership_images" >
           <div class="shadow-box">
			<img class="img-responsive" src="<?php echo $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/dap-member-invoice.jpg";  ?>"  />
			<span class="member_input-outer">	<input type="radio" name="membership_page_option" id="membership_my_invoice" value="DAP Member Invoice">Member Invoice </span>
		</div>
		</div>
		<div class="col-xs-4 dap_membership_images" >
		 <div class="shadow-box">
			<img class="img-responsive" src="<?php echo $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/dap-member-cancel.jpg";  ?>" />
			<span class="member_input-outer">	<input type="radio" name="membership_page_option" id="membership_my_cancel" value="DAP Member Cancel">Member Cancellation</span>
		</div>
		</div>
		<div class="col-xs-4 dap_membership_images" >
		<div class="shadow-box">
			<img class="img-responsive" src="<?php echo $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/dap-login.jpg";  ?>" />
			<span class="member_input-outer">	<input type="radio" name="membership_page_option" id="membership_my_login" value="DAP Login">Member Login </span>
		</div>
		</div>
			  
		</div>
		<input id="membershipButton" class="member_button"  name="member_button" value="Next" type="button"/> 
	<!--/div-->
	  
</div>
<style>
 

.live-links-outer input[type="radio"]{width: auto !important;}

.live-links-outer input[type="checkbox"]{width: auto !important;}

#multicolumn{display:none}

.live-links-outer-div table strong {font-size: 17px;}

#ex1Slider .slider-selection {background: #BABABA;}

.dap_membership_div , .table_page_select , .member_button{display:none}

.live-links-outer-div table th{margin :10px 0px;font-size:18px;float: left;}

.live-links-outer-div h2.head_memb{margin :10px 0px;font-size:18px;float: left;font-weight: bold;}

.member_outer_main .member_input-outer input {margin:0px 6px 2px 0px !important;}

.member_outer_main .member_input-outer {float: left;padding: 12px 0 0 !important;width: 100%;text-align: center;font-size: 16px;font-weight: bold;}

.dap-icon.dashicons, .dap-icon.dashicons-before:before{font-size: 18px;line-height: 1.5;margin: 0px 5px 0px 0px;}

.member_button{float: right;background: #0085ba;border: medium none;border-radius: 5px;font-size: 20px;height: auto;margin-top: 10px;margin-bottom: 10px;padding: 12px 35px;text-shadow: 0 0 0 transparent;text-transform: none;color: #fff;}

.member_button:hover {background: #000 none repeat scroll 0 0;color: #fff;}

.member_outer_main{ padding: 10px 12px 0px!important;}
  
.dap_membership_images{padding-left: 8px !important;padding-right: 8px !important;margin :0px 0px 50px;}

.colorpicker.colorpicker-with-alpha {z-index:9999999 !important;}

.table-outer .template_box img{display: block;margin: 0 auto;border: 1px solid #d1d1d1;max-width: 100%;}

.template_box .template-select-outer {padding-left: 4px;padding-right: 4px;}

.template-select-outer .input-outer input[type="radio"] {position: absolute;left: 0;top: 0;z-index: 99;}

.template-select-outer .input-outer {padding-left: 25px;position: relative;min-height: 40px;font-size: 15px;}

div#TB_window {bottom: 6% !important;top: 6% !important;height: 86% !important;}

#TB_ajaxContent {box-sizing: border-box;clear: both;line-height: 1.4em;overflow: visible;padding: 14px;text-align: left;width: 100% !important;overflow: auto;	height: -moz-calc(100% - 50px) !important;height: -webkit-calc(100% - 50px) !important;height: -o-calc(100% - 50px) !important;height: calc(100% - 50px) !important;}

.dap_membership_images img{border: 1px solid #a0a0a0; max-width: 100%;}

.live-links-outer-div .table_page_select td{font-size: 15px;}

.dap_membership_images .shadow-box {border: 1px solid rgba(0, 0, 0, 0.1);box-shadow: 0 5px 6px 0 rgba(0, 0, 0, 0.18);-webkit-transform: translateZ(0);transform: translateZ(0);-webkit-backface-visibility: hidden; backface-visibility: hidden;-webkit-transition-duration: 0.3s; transition-duration: 0.3s;-webkit-transition-property: transform;transition-property: transform;-webkit-transition-timing-function: ease-out;transition-timing-function: ease-out;cursor: pointer;float: left;width: 100%;padding-bottom: 10px;}

.dap_membership_images .shadow-box:hover, .dap_membership_images .shadow-box:focus, .dap_membership_images .shadow-box:active {-webkit-transform: translateY(-6px);transform: translateY(-6px);}

.dap_membership_images img {border: none !important;}

 .member_button {float:right;}

 .member_button ,#TB_ajaxContent .next-btn ,#TB_ajaxContent .back-btn{ background: #0085ba;    border: medium none;  border-radius: 0;    font-size: 18px;    height: auto;    margin-top: 10px;  margin-bottom: 10px;    padding: 12px 35px;  text-shadow: 0 0 0 transparent;text-transform: none;    border-bottom: 3px solid #18334c; color: #fff;    height: 50px;   padding: 0 15px;    min-width: 120px; -webkit-transform: translateZ(0);	transform: translateZ(0);	-webkit-backface-visibility: hidden; backface-visibility: hidden; -webkit-transition-duration: 0.3s;	transition-duration: 0.3s; -webkit-transition-property: transform;	transition-property: transform;	-webkit-transition-timing-function: ease-out;	transition-timing-function: ease-out;	box-shadow:0 0 0 transparent;
}

#TB_ajaxContent .next-btn {float:right;} 

#TB_ajaxContent .back-btn {float:left;background:#999;}

#backbtn {background:#999;}

.member_button:hover ,#TB_ajaxContent .next-btn:hover , #TB_ajaxContent .back-btn:hover{
	background:#333;	border-color:#333;	color:#fff;	
}
.btn_outer { float: left; margin: 40px 0 10px; width: 100%;}

.live-links-outer-div .dap_membership_div{margin :20px 0px;}

.btn_outer #myprofilegenscbtn , .btn_outer #myinvoicescbtn , .btn_outer #myinvoicescbtn , .btn_outer #mycancelscbtn{ font-size: 18px !important;     height: 50px !important; line-height: normal !important;  margin-top: 10px;    margin-bottom: 10px;  padding: 0 15px!important;}

#selectmemberpages { width: 100%;    float: left;  margin-right: 0px;}

.colorpicker {   width: auto;  height: auto;    overflow: hidden;  position: absolute;   background: none; }

.dap_membership_div , .dap_membership_images{box-sizing: border-box;}

.member_outer_main { width: 100%;    float: left;position: relative;}

.dap_membership_images, .template-select-outer { width: 33.33333333%;  float: left;position: relative;}

.dap-icon, #memberpages_shortcode_popup_button{display: inline-block !important; }

</style>
   

<script src="<?php echo $bootstrap_jsurl = plugins_url()."/DAP-WP-LiveLinks/includes/js/bootstrap.min.js";?>"></script>

<link href="<?php echo $colorpicker_cssurl = plugins_url()."/DAP-WP-LiveLinks/includes/css/bootstrap-colorpicker.min.css";?>" rel="stylesheet">

<script src="<?php echo $colorpicker_jsurl = plugins_url()."/DAP-WP-LiveLinks/includes/js/bootstrap-colorpicker.js"; ?>"></script>

<script src="<?php echo $rangeslider_jsurl = plugins_url()."/DAP-WP-LiveLinks/includes/js/bootstrap-slider.js";  ?>"></script>

<link rel="stylesheet" href="<?php echo $rangeslider_cssurl = plugins_url()."/DAP-WP-LiveLinks/includes/css/bootstrap-slider.css";  ?>"/>

<script>

var $jq = jQuery.noConflict();

$jq(document).ready(function(){

	// Colorpicker and range slider

	$jq(function(){

		$jq('#bg_clr_div').colorpicker();

		$jq('#bg_clr_div1').colorpicker();

		$jq('#bg_clr_div2').colorpicker();

		$jq('#bg_clr_div3').colorpicker();

	});



	$jq('#boxWidth').bootstrapSlider({

		formatter: function(value) {

			return 'Current value: ' + value+'px';

		}

	});

	$jq('#imageWidth').bootstrapSlider({

		formatter: function(value) {

			return 'Current value: ' + value+'px';

		}

	});

	$jq('#imageHeight').bootstrapSlider({

		formatter: function(value) {

			return 'Current value: ' + value+'px';

		}

	}); 

	// Add or Remove bootstrap css

	$jq(function(){



		setInterval(function() {

		var filename='<?php echo $bootstrap_min = plugins_url()."/DAP-WP-LiveLinks/includes/css/bootstrap.min.css";?>';

		var filename1='';
		var bodylast = $jq("body.modal-open");
		if (bodylast.length){
			
		//if($jq("body").hasClass( "modal-open" )){

			// console.log("1");// $jq('head').append('<link rel="stylesheet" href="'+filename+'" type="text/css"  id="banSheet"/>');

			var head = $jq("head");

			var headlinklast = head.find("link[rel='stylesheet']:last");

			var linkElement = "<link title='mystyle' rel='stylesheet' href='"+filename+"' type='text/css' media='screen'>";

			if (headlinklast.length){

				headlinklast.after(linkElement);

			}

			else {

				head.append(linkElement);

			}

		}else{

			$jq('link[title="mystyle"]').remove();

			//$jq('head').append('<link rel="stylesheet" href="'+filename1+'" type="text/css"  id="banSheet"/>'); // remove_css_popup();

			}

		}, 1000);  

	});

	



});


$jq(function(){

	$jq('.dap_membership_div').show();

	$jq('.table_page_select').hide();

	$jq('.member_button').show();



	$jq( '.dap_membership_images img' ).dblclick(function() {

		

		var selected_page = $jq(this).parent().find('input').val();



		if(selected_page == "DAP My Content"){

			$jq('#mycontent_shortcode_popup_button').trigger('click');

			$jq('#mycontent_shortcode_popup_inner').show(); 

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(selected_page == "DAP My Profile"){

			$jq('#myprofile_shortcode_popup_button').trigger('click');

			$jq('#myprofile_shortcode_popup_inner').show();

			$jq('.table_page_select').show();						

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

				 

		} else if(selected_page == "DAP Affiliate Info") {

			$jq('#affiliate_shortcode_popup_button').trigger('click');

			$jq('#affiliate_shortcode_popup_inner').show();

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(selected_page == "DAP Member Invoice") {

			$jq('#myinvoice_shortcode_popup_button').trigger('click');

			$jq('#myinvoice_shortcode_popup_inner').show();

			$jq('.table_page_select').show();						

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			 

		} else if(selected_page == "DAP Member Cancel") {

			$jq('#mycancel_shortcode_popup_button').trigger('click');

			$jq('#mycancel_shortcode_popup_inner').show();

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(selected_page == "DAP Login") {

			$jq('#login_shortcode_popup_button').trigger('click');

			$jq('#login_shortcode_popup_inner').show();

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else {

			$jq('.dap_membership_div').show();

			$jq('.table_page_select').hide();

			$jq('.member_button').show();

		}

	}); 

	

	$jq('.member_button').on('click' , function(){

	

		var select_page = $jq('input[name="membership_page_option"]:checked').val();



		if(select_page == "DAP My Content"){

			$jq('#mycontent_shortcode_popup_button').trigger('click');

			$jq('#mycontent_shortcode_popup_inner').show(); 

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(select_page == "DAP My Profile"){

			$jq('#myprofile_shortcode_popup_button').trigger('click');

			$jq('#myprofile_shortcode_popup_inner').show();

			$jq('.table_page_select').show();						

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(select_page == "DAP Affiliate Info") {

			$jq('#affiliate_shortcode_popup_button').trigger('click');

			$jq('#affiliate_shortcode_popup_inner').show();

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(select_page == "DAP Member Invoice") {

			$jq('#myinvoice_shortcode_popup_button').trigger('click');

			$jq('#myinvoice_shortcode_popup_inner').show();

			$jq('.table_page_select').show();						

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(select_page == "DAP Member Cancel") {

			$jq('#mycancel_shortcode_popup_button').trigger('click');

			$jq('#mycancel_shortcode_popup_inner').show();

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

			

		} else if(select_page == "DAP Login") {

			$jq('#login_shortcode_popup_button').trigger('click');

			$jq('#login_shortcode_popup_inner').show();

			$jq('.table_page_select').show();

			$jq('.dap_membership_div').hide();

			$jq('.member_button').hide();

		

		} else {

			$jq('.dap_membership_div').show();

			$jq('.table_page_select').hide();

			$jq('.member_button').show();

		}

	});





	$jq('select[name="selectmemberpages"]').on('change' , function(){



		var Dap_val = $jq(this).val();

		$jq('#mycontent_shortcode_popup_inner').hide(); 

		$jq('#myprofile_shortcode_popup_inner').hide(); 

		$jq('#affiliate_shortcode_popup_inner').hide(); 

		$jq('#myinvoice_shortcode_popup_inner').hide(); 

		$jq('#mycancel_shortcode_popup_inner').hide(); 

		$jq('#login_shortcode_popup_inner').hide(); 

		$jq('.dap_membership_div').hide();

		$jq('.table_page_select').show();

		

		if(Dap_val == "DAPMyContent"){



			$jq('#mycontent_shortcode_popup_button').trigger('click');

			$jq('#mycontent_shortcode_popup_inner').show(); 



		} else if(Dap_val == "DAPMyProfile"){



			$jq('#myprofile_shortcode_popup_button').trigger('click');

			$jq('#myprofile_shortcode_popup_inner').show(); 



		} else if(Dap_val == "DAPAffiliateInfo") {



			$jq('#affiliate_shortcode_popup_button').trigger('click');

			$jq('#affiliate_shortcode_popup_inner').show(); 



		} else if(Dap_val == "DAPMemberInvoice") {



			$jq('#myinvoice_shortcode_popup_button').trigger('click');

			$jq('#myinvoice_shortcode_popup_inner').show(); 



		} else if(Dap_val == "DAPMemberCancel") {



			$jq('#mycancel_shortcode_popup_button').trigger('click');

			$jq('#mycancel_shortcode_popup_inner').show();



		} else if(Dap_val == "DAPLogin") {



			$jq('#login_shortcode_popup_button').trigger('click');

			$jq('#login_shortcode_popup_inner').show();



		} else {

			$jq('.dap_membership_div').show();

			$jq('.table_page_select').hide();

			$jq('.member_button').show();

		}

	});

	

	$jq('.back-btn').on('click' , function(){

		$jq('.dap_membership_div').show();

		$jq('.member_button').show();

		$jq('.table_page_select').hide();

		$jq('#mycontent_shortcode_popup_inner').hide(); 

		$jq('#myprofile_shortcode_popup_inner').hide(); 

		$jq('#affiliate_shortcode_popup_inner').hide(); 

		$jq('#myinvoice_shortcode_popup_inner').hide(); 

		$jq('#mycancel_shortcode_popup_inner').hide(); 

		$jq('#login_shortcode_popup_inner').hide(); 

	});

	

	

});

function dap_open_front_page(){

	$jq('.dap_membership_div').show();

	$jq('.member_button').show();

	$jq('.table_page_select').hide();

	$jq('#mycontent_shortcode_popup_inner').hide(); 

	$jq('#myprofile_shortcode_popup_inner').hide(); 

	$jq('#affiliate_shortcode_popup_inner').hide(); 

	$jq('#myinvoice_shortcode_popup_inner').hide(); 

	$jq('#mycancel_shortcode_popup_inner').hide(); 

	$jq('#login_shortcode_popup_inner').hide(); 

	

}



function remove_css_popup(){

$jq('#banSheet').attr('disabled', 'disabled'); 

}



</script>
<?php
	}
?>