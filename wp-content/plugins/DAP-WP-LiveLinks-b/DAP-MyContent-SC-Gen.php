<?php 


/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_mycontent_tinymce_media_button');
function add_mycontent_tinymce_media_button($context){

return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=mycontent_shortcode_popup&width=640&height=513\" class=\"button thickbox\" id=\"mycontent_shortcode_popup_button\" title=\"Generate My Content Shortcode\">Generate My Content Code</a>");
}

add_action('admin_footer','mycontent_shortcode_media_button_popup');
//Generate inline content for the popup window when the "my shortcode" button is clicked
function mycontent_shortcode_media_button_popup(){?>
  <link rel="stylesheet" href="dapmycontent.css" />
  <div id="mycontent_shortcode_popup" style="display:none;">
    <--".wrap" class div is needed to make thickbox content look good-->
               
    <table width="600" cellpadding="2" cellspacing="2">
    <tr>
    <td valign="top">
    <strong>Enter Product Id(s) 
    </td>
    <td valign="top" width="50%">
		<input type="text" id="productid" value="ALL"> 
       <br /></strong> <i style="font-size:13px;"> (Valid Values - ALL, Prod Id, comma-separated list of Prod IDs):</i>
    <br /><br /></td>
    </tr>
		
	<tr>
    <td valign="top">
    <strong>Hide Product Id(s)
    </td>
    <td valign="top" width="50%">
		<input type="text" id="hideproductid" value="NONE"> 
       <br /></strong> <i style="font-size:13px;"> (Valid Values - NONE, Prod Id, comma-separated list of Prod IDs):</i>
    <br /><br /></td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Display Access Start Date?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showaccessstartdate" id="showaccessstartdate">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong> 
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Display Access End Date?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showaccessenddate" id="showaccessenddate">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong> 
    </td>
    </tr>
    
    
    <tr>
    <td valign="top">
    <strong>Enter DATE Format
    </td>
    <td valign="top" width="50%">
		<input type="text" id="dateformat" value="YYYY-MM-DD"> 
       <br /></strong>
    <br /><br /></td>
    </tr>
    
    
    <tr>
    <td valign="top">
    <strong>Display Product Description?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showdescription" id="showdescription">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong> 
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Enter Order Of Links 
    </td>
    <td valign="top" width="50%">
		<input type="text" id="orderOfLinks" value="SHORT"> 
       <br /></strong>
    <br /><br /></td>
    </tr>
    
    
    <tr>
    <td valign="top">
    <strong>Enter Error Message Template
    </td>
    <td valign="top" width="50%">
		<input type="text" id="errmsgtemplate" value="SHORT"> 
       <br /></strong>
    <br /><br /></td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Display Product Count?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showproductcount" id="showproductcount">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong> 
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Display Member Links?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showlinks" id="showlinks">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong>  <i style="font-size:13px;"> (if set to NO, only products are displayed):</i>
    </td>
    </tr>
    
    
    <tr>
    <td valign="top">
    <strong>"My Content" section collapsed by default?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showlinkscollapsed" id="showlinkscollapsed">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong>  <i style="font-size:13px;"> (if set to YES, product list is displayed with a 'click here' link to expand the My Content section):</i>
    </td>
    </tr>
    
    
        
    <tr>
    <td valign="top">
    <strong>Enter "Click Here To View Content" text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="clickherelinktext" value="SHORT"> 
       <br /></strong>
    <br /><br /><i style="font-size:13px;"> (when users click on it, it will display the links):</i>
    </td>
    </tr>
    
            
    <tr>
    <td valign="top">
    <strong>Enter "No Content Available" Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="nocontenttext" value="SHORT"> 
       <br /></strong>
    <br /><br /><i style="font-size:13px;"> (it will display if no content is available under the product):</i>
    </td>
    </tr>
    
     <tr>
    <td valign="top">
    <strong>Enter "My Content" Header Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="mycontentheadertext" value="SHORT"> 
       <br /></strong>
    <br /><br />
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Enter "Available Content" Header Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="availcontentheadertext" value="SHORT"> 
       <br /></strong>
    <br /><br />
    </td>
    </tr>

    <tr>
    <td valign="top">
    <strong>Display "Available Content" Section?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showavailablecontent" id="showavailablecontent">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong> 
    </td>
    </tr>
    
    
     <tr>
    <td valign="top">
    <strong>How Many Available Links you want to display? 
    </td>
    <td valign="top" width="50%">
		<input type="text" id="howmanylinks" value="9999"> 
       <br /></strong>
    <br /><br />
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Enter "Upcoming Content" Header Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="upcomingcontentheadertext" value="Upcoming Content"> 
       <br /></strong>
    <br /><br />
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Display "Upcoming Content" Section?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showupcomingcontent" id="showupcomingcontent">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
       <br /><br /></strong> 
    </td>
    </tr>
    
    
    <tr>
    <td valign="top">
    <strong>How Many Upcoming Links you want to display?
    </td>
    <td valign="top" width="50%">
		<input type="text" id="howmanyupcominglinks" value="5"> 
       <br /></strong>
    <br /><br />
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    <strong>Enter "Coming Soon (In Days)" Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="comingsoonheadertext" value="Upcoming (In Days)"> 
       <br /></strong>
    <br /><br />
    </td>
    </tr>
    
    
    </table>     
                
    <button class="button-primary" id="mycontentgenscbtn">Generate Shortcode</button>

     
  </div>
<?php
}

//javascript code needed to make shortcode appear in TinyMCE edtor
add_action('admin_footer','mycontent_shortcode_add_shortcode_to_editor');
function mycontent_shortcode_add_shortcode_to_editor(){?>
<script>
jQuery('#mycontentgenscbtn').on('click',function() {
												 
  var productId = jQuery('#productid').val();
  var hideProductId = jQuery('#hideproductid').val();
  var showProductName = jQuery('select#showproductname').val();
  var showAccessStartDate = jQuery('select#showaccessstartdate').val();
  var showAccessEndDate = jQuery('select#showaccessenddate').val();
  
  var dateFormat = jQuery('#dateformat').val();
  var showDescription = jQuery('select#showdescription').val();
  var orderOfLinks = jQuery('#orderoflinks').val();
  var errMsgTemplate = escape(jQuery('#errmsgtemplate').val());
  var showProductCount = jQuery('select#showproductcount').val();
  
  var showLinks = jQuery('#showlinks').val();
  var showLinksCollapsed = jQuery('select#showlinkscollapsed').val();
  var clickHereLinkText = escape(jQuery('#clickherelinktext').val());
  
  var noContentText = escape(jQuery('#nocontenttext').val());
  var myContentHeaderText = escape(jQuery('#mycontentheadertext').val());
  var availContentHeaderText = escape(jQuery('#availcontentheadertext').val());
  var showAvailableContent = jQuery('#selectshowavailablecontent').val();
  
  var howManyLinks = jQuery('#howmanylinks').val();
  var upcomingContentHeaderText = escape(jQuery('#upcomingcontentheadertext').val());
	
  var showUpcomingContent = jQuery('#selectshowupcomingcontent').val();
  var howManyUpcomingLinks = jQuery('#howmanyupcominglinks').val();
  var comingSoonHeaderText = jQuery('#comingsoonheadertext').val();
  
  
  var shortcode = '[DAPMyContent productId="'+productId+'" hideProductId="'+hideProductId+'" showProductName="'+showProductName+'" showAccessStartDate="'+showAccessStartDate+'" showAccessEndDate="'+showAccessEndDate+'" dateFormat="'+dateFormat+'" showDescription="'+showDescription+'" orderOfLinks="'+orderOfLinks+'" errMsgTemplate="'+errMsgTemplate+'" showProductCount="'+showProductCount+'" showLinks="'+showLinks+'" showLinksCollapsed="'+showLinksCollapsed+'" clickHereLinkText="'+clickHereLinkText+'" noContentText="'+noContentText+'" myContentHeaderText="'+myContentHeaderText+'" availContentHeaderText="'+availContentHeaderText+'" showAvailableContent="'+showAvailableContent+'" howManyLinks="'+howManyLinks+'" upcomingContentHeaderText="'+upcomingContentHeaderText+'" showUpcomingContent="'+showUpcomingContent+'" howManyUpcomingLinks="'+howManyUpcomingLinks+'" comingSoonHeaderText="'+comingSoonHeaderText+'" /]<br>';
 

 
// tinyMCE.execCommand('mceInsertContent', false, shortcode);
 
 
//  tinyMCE.activeEditor.execCommand( 'mceInsertContent', false, shortcode );
 if( !tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
//    jQuery('textarea#content').val(shortcode);
	  // var original_text = jQuery('textarea.wp-editor-area').val(); 
	   //jQuery('textarea.wp-editor-area').val(original_text + '\r\n' + shortcode); 
	   //jQuery( '#wp-content-editor-container' ).find( 'textarea' ).val( shortcode );
	    alert(shortcode);
		
		send_to_editor(shortcode);
  } else {
    tinyMCE.execCommand('mceInsertContent', false, shortcode);
  }
  
  //close the thickbox after adding shortcode to editor
  self.parent.tb_remove();
});
</script>
<?php
}


?>