<?php 


/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_login_tinymce_media_button');
function add_login_tinymce_media_button($context){
  
return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=login_shortcode_popup&width=640&height=513\" 
class=\"button thickbox\" id=\"login_shortcode_popup_button\" title=\"Generate Login Shortcode\"  style=\"display:none;\">DAP Login </a>");
}

add_action('admin_footer','login_shortcode_media_button_popup');
//Generate inline content for the popup window when the "my shortcode" button is clicked
function login_shortcode_media_button_popup(){?>

<style>
#sidebartemplate{ display:none }
#login_shortcode_popup_inner .select_add_cls{  width: 94% !important;  float: right!important;  margin-right: 15px!important; }
h2.head_memb{margin :10px 0px;font-size:18px;float: left;font-weight: bold;}
</style>

  <div id="login_shortcode_popup" style="display:none;" class="popup_div_outer">
    <div class="live-links-outer" id="login_shortcode_popup_inner"> 
 	<div class="live-links-outer-div " id="memberpages_shortcode_popup_button">
	 <h2 class="head_memb">Membership Options</h2>
	 <table width="100%" cellpadding="2" cellspacing="2" class="table_page_select">
		<tr>
			<td valign="top"> Select a Membership page </td>
			<td valign="top" width="50%">
				<select  name="selectmemberpages" id="selectmemberpages" class="select_add_cls">
					<option value="Select" >--Select--</option>
					<option value="DAPMyContent">My Content</option>
					<option value="DAPMyProfile">My Profile</option>
					<option value="DAPAffiliateInfo">Affiliate Info & Stats</option>
					<option value="DAPMemberInvoice">Member Invoice</option>
					<option value="DAPMemberCancel">Member Cancellation</option>
					<option value="DAPLogin" selected="selected">Member Login</option>
				</select><br />
			</td>
		</tr>
	</table>  	
		<table width="100%" cellpadding="2" cellspacing="2">
			
				 <tr class="table-outer">	 
			<td valign="middle" width="100%" style="padding-bottom:0;">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr class="my_content_heading">
						<th> Template Options </th>
					</tr>
					<tr>
						<td>Sidebar or Page Template?</td>
						<td valign="middle" width="50%">
							<select style="" name="selecttemp" id="selecttemp" style=" width: 55% !important;">
							<option value="seltemplate1" selected="selected">Page Template</option>
							<option value="seltemplate2">Sidebar Template</option>
							</select>
						</td>
					</tr>
					 <tr id="pagetemplate" class="template_box">  
						<td>Select a Page Template</td>
						<td valign="middle" width="50%">
							<select style="" name="selectpagetemplate" id="selectpagetemplate" style=" width: 55% !important;">
							<option value="template1">Template1</option>
							<option value="template2">Template2</option>
							<option value="template3">Template3</option>
							<option value="template4">Template4</option>
							<option value="template5" selected="selected">Template5</option>
							</select>
						</td>
					</tr>
					 <tr id="sidebartemplate" class="template_box">  
					<td>Select a Sidebar Page Template </td>
						<td valign="middle" width="50%">
							<select style="" name="selectsidebarpagetemplate" id="selectsidebarpagetemplate" style=" width: 55% !important;">
							<option value="sidebar1" selected="selected">Sidebar template1</option>
							<option value="sidebar2">Sidebar template2</option>
							</select>
						</td>
					</tr>
					
				<tr>  
					<td>Post Login URL </td>
						<td valign="middle" width="50%">
					 
							<input type="url" value="" class="redirect_url"  name="redirect" id="redirect"  />							 
						</td>
					</tr>
				</table>
			</td>
    </tr>
	
</table>
		
		 <div align="center" class="btn_outer"><button class="button-primary back-btn" id="backbtn">Back</button> 
   <button class="button-primary next-btn" id="myloginbtn">Generate Shortcode</button></div>     
	 
    <br><br>
    </div>  
  </div>
  </div>

<?php
}  

//javascript code needed to make shortcode appear in TinyMCE editor
add_action('admin_footer','login_shortcode_add_shortcode_to_editor');
function login_shortcode_add_shortcode_to_editor(){?>
<script>

jQuery('#myloginbtn').on('click',function() {
	var selOption = window.Var4;
	
	if(selOption === undefined || selOption === null || selOption === "") {
		 var template = "template5";
	}else{
		var template = window.Var4;
	}

	//var template = jQuery('#selectpagetemplate').val();
	var mylandingpage = jQuery('input#redirect').val();
	var shortcode = '[DAPLoginForm template="'+template+'"';
	if(mylandingpage != ""){
		shortcode = shortcode + ' redirect="'+mylandingpage+'"]';
	}else{
		shortcode = shortcode +']';
	}
	send_to_editor(shortcode);

	//close the thickbox after adding shortcode to editor
	self.parent.tb_remove();
	//form.reset();
});
jQuery(function() {	
	jQuery('#sidebartemplate').hide();
	var selOption = jQuery('select[name="selectpagetemplate"] option:selected').val();
	window.Var4 = selOption;
	jQuery('select[name="selectpagetemplate"]').on('change',function() {
		var selOption = jQuery('select[name="selectpagetemplate"] option:selected').val();
		window.Var4 = selOption;
	});
	jQuery('#selecttemp').on('change',function() {
		var selectdrop = jQuery('select#selecttemp option:selected').val();
		if(selectdrop == "seltemplate1"){
			jQuery('#sidebartemplate').hide();
			jQuery('#pagetemplate').show();
	
			var selOption = jQuery('select[name="selectpagetemplate"] option:selected').val();
			window.Var4 = selOption;
			jQuery('select[name="selectpagetemplate"]').on('change',function() {
				var selOption = jQuery('select[name="selectpagetemplate"] option:selected').val();
				window.Var4 = selOption;
			});
		}else{
			jQuery('#pagetemplate').hide();		 
			jQuery('#sidebartemplate').show();
			
			var selOption = jQuery('select[name="selectsidebarpagetemplate"] option:selected').val();
			window.Var4 = selOption;
			jQuery('select[name="selectsidebarpagetemplate"]').on('change',function() {
				var selOption = jQuery('select[name="selectsidebarpagetemplate"] option:selected').val();
				window.Var4 = selOption;
			});
		}
	});	
});	
    
</script>

<?php
}


add_shortcode('DAPLoginForm', 'dap_loginform');
add_shortcode('DAPLoginFormPermalink', 'dap_loginform_permalink');

	
function dap_loginformold($atts, $content=null){ 
	//return dap_login("%%LOGIN_FORM%%","");
}


function dap_loginform_permalink($atts, $content=null){ 
   
    global $wpdb, $post, $table_prefix;
    if ($post->ID) {
        $current_postid = $post->ID;
        $post_id = get_post($current_postid);
        $permalink = get_permalink($post_id);
        //logToFile("_dapshoppingcart.php: dapshoppingcart_shortcode post permalink=".$permalink);
    }
	return $permalink;
	
}


function dap_loginform($atts, $content=null){ 
	extract(shortcode_atts(array(
		'redirect' => '#',
		//'errpage' => 'N',
		'template' => 'template5'
	), $atts));
	
	if( defined('SITEROOT') ) 
		$lldocroot = SITEROOT;
	else 
		$lldocroot = $_SERVER['DOCUMENT_ROOT'];
	
	
	include_once ($lldocroot . "/dap/dap-config.php");	
	
	
	$content = do_shortcode(dap_clean_shortcode_content($content));	
	$permalink = do_shortcode(dap_loginform_permalink($content));	
	
	//logToFile("DAP-ShortCodes-LoginForm: redirect=".$redirect);
	
	
	$session = Dap_Session::getSession();
	$signup_page = Dap_Config::get("SITE_URL_DAP");
	if ( isset($_GET['request']) && ($_GET['request'] != "") ) {
		//request is available in query string
		//logToFile("There's a request URL: " . $_GET['request']); 
		$redirectURL = $_GET['request'];
	} 
	else {
		if($redirect!="")
			$redirectURL=urldecode($redirect);	
	}
	//logToFile("DAP-ShortCodes-LoginForm: redirectURL=" .$redirectURL); 
	
	if(!Dap_Session::isLoggedIn()) { //Not logged in
		//logToFile("not logged in",LOG_DEBUG_DAP);
		$output = "";
		
		$loginFormFilepath = WP_PLUGIN_DIR . "/DAP-WP-LiveLinks/includes/login/templates/" . $template;
		
		if( file_exists($loginFormFilepath . "/custom" . $template . ".html") ) 
			$loginFormFileName = $loginFormFilepath . "/custom" . $template . ".html";
		else 
			$loginFormFileName = $loginFormFilepath . "/" . $template . ".html";

		//logToFile("loginFormFileName=$loginFormFileName",LOG_DEBUG_DAP);
		
		if(file_exists($loginFormFileName)) 
			$tempContent = file_get_contents($loginFormFileName);
	 	else {
			$errorHTML = mb_convert_encoding("Login Template Not Found", "UTF-8", "auto");
			return $errorHTML;	
		}
		
		$tempContent = str_replace( '%%REQUEST%%',$redirectURL, $tempContent); 
		$cssurl=get_option('siteurl')."/wp-content/plugins/DAP-WP-LiveLinks/includes/login/templates/".$template;	
		$csspath=WP_PLUGIN_DIR."/DAP-WP-LiveLinks/includes/login/templates/".$template;	
		
		if( file_exists($csspath . "/customstyle.css") ) 
			$cssurl = $cssurl . "/customstyle.css";
		else 
			$cssurl = $cssurl . "/style.css";
		
		$tempContent = str_replace( '[CSSURL]',$cssurl, $tempContent); 
		
		if( isset($_GET['msg']) ) {
			$msg = mb_convert_encoding(stripslashes($_GET['msg']), "UTF-8", "auto");
		}
		
		$tempContent = str_replace( '%%ERRORTEXT%%', $msg, $tempContent); 

		//logToFile("content: $content",LOG_DEBUG_DAP);
		return $tempContent;
	} //End Not Logged In
	
	else if(Dap_Session::isLoggedIn()) {
		//Check if there is a request uri
		//logToFile("loggedIn",LOG_DEBUG_DAP);
		$msg = "";
		$session = DAP_Session::getSession();
		$user = $session->getUser();		
		//ob_end_clean();
		
		
		if(strstr($template,"sidebar")) {
			$logoutHTMLFilepath = "/DAP-WP-LiveLinks/includes/logout/logout.html";
			if( file_exists(WP_PLUGIN_DIR . "/DAP-WP-LiveLinks/includes/logout/customlogout.html") ) {
				$logoutHTMLFilepath = "/DAP-WP-LiveLinks/includes/logout/customlogout.html";
			}
			
			$output = file_get_contents(WP_PLUGIN_DIR . $logoutHTMLFilepath);
			return $output;	
		}
	
		if ( isset($_GET['request']) && ($_GET['request'] != "") ) {
		//request is available in query string
		//logToFile("There's a request URL: " . $_GET['request']); 
			$redirectURL = $_GET['request'];
		} 
		else if ($redirect!="") {
			$redirectURL=urldecode($redirect);	
		}
		else 
			$redirectURL = Dap_UsersProducts::getLoggedInURL();
		
		//@header("Location:".$redirectURL);
		
		$output = "[" . MSG_ALREADY_LOGGEDIN_1 . " <a href='" . $redirectURL . "'>" . MSG_ALREADY_LOGGEDIN_2 . "</a>]<br/><br/>";
		
		if( $session->isAdmin() ) {
			$output = $output . ">>> <a href='/dap/admin/'>You are logged in as DAP Admin. Proceed to DAP Admin Dashboard</a> <<< <br/><br/>";
		}
		
		
		//$output = mb_convert_encoding($output, "UTF-8", "auto");
		
		if( isset($_GET['msg']) ) {
			//$msg = mb_convert_encoding($msg, "UTF-8", "auto");
			$output = "<div class='confirmationMessage'>" . stripslashes($_GET['msg']) . "</div><br/><br/>" . $output;
		}
		
		if( isset($_GET['request']) && ($_GET['request'] != "") ) {
			$permalink = $_GET['request'];
			//logToFile("permalink: " . $permalink); 
			
			$salesPageURL = Dap_Config::get("SALES_PAGE_URL"); //global SPU
			if($salesPageURL == "") $salesPageURL = "/";


			$errorHTMLPath = $lldocroot . "/dap/inc/error-loggedin.php";
			//Check for custom error page
			if( file_exists($lldocroot . "/dap/inc/customerror-loggedin.php") ) {
				$errorHTMLPath = $lldocroot . "/dap/inc/customerror-loggedin.php";
			}
			
			//$productCount = $user->hasAccessToHowManyDistinctProducts();
			//$permalink = str_replace( $_SERVER['REQUEST_QUERY'], '', $_SERVER['REQUEST_URI'] );
			//logToFile("permalink: " . $permalink); 
			//logToFile($_SERVER['REQUEST_QUERY']); 
			//logToFile($_SERVER['REQUEST_URI']); 
			$productCount = Dap_Product::isPartOfHowManyDistinctProducts($permalink);
			//logToFile("productCount: $productCount");
			if( $productCount == 1 ) {
				$product = Dap_Product::getProductDetailsByResource($permalink);
				if(!is_null($product) && isset($product)) {
					if ($product->getSales_page_url() != "") {
						$salesPageURL = $product->getSales_page_url();
					}
					if ($product->getLogged_in_url() != "") {
						$loggedInURL = $product->getLogged_in_url();
					}
				}
			}
			$output2 = replaceFillers($errorHTMLPath,"%%LOGIN_URL%%",Dap_Config::get("LOGIN_URL"));
			$output2 = str_replace("%%SALES_PAGE_URL%%",$salesPageURL,$output2);
			//logToFile("About to return");
			
			$output = $output2 . "<br/><br/>" . $output;
		}
		
		return $output;  
		exit;
	}

}

	
?>