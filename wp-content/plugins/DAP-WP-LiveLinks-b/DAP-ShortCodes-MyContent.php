<?php 

/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_mycontent_tinymce_media_button');

define('DAPMYCONTENT_FOLDER', WP_PLUGIN_DIR.'/DAP-WP-LiveLinks');
define('DAPMYCONTENT_URL', WP_PLUGIN_URL.'/DAP-WP-LiveLinks');

function add_mycontent_tinymce_media_button($context){

return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=mycontent_shortcode_popup&width=640&height=513\" 
class=\"button thickbox\" id=\"mycontent_shortcode_popup_button\" title=\"Generate My Content Shortcode\"  style=\"display:none;\">DAP My Content</a>");
}

add_action('admin_footer','mycontent_shortcode_media_button_popup');
//Generate inline content for the popup window when the "my shortcode" button is clicked
function mycontent_shortcode_media_button_popup(){?>


<style>
.table-outer table th { float: left;  font-size: 18px;  line-height: normal;  padding: 15px 10px;  width: 100%;  color:#333;}

.table-outer table {  background: #f9f9f9 none repeat scroll 0 0;}

.table-outer select, .table-outer input{  background: #fff none repeat scroll 0 0;  border: 1px solid #cacaca;  box-shadow: none;  color: #333;  float: left;  font-size: 14px;  font-weight: normal !important;  height: 35px;  margin: 0;  padding: 0 5px !important;  width: 100%;}

.table_page_select{  margin-bottom: 15px;}

.table_page_select select{  background: #fff none repeat scroll 0 0;  border: 1px solid #cacaca;  box-shadow: none;  color: #333;  float: left;  font-size:14px;  font-weight: normal !important;  height: 35px;  margin: 0;  padding: 0 5px !important;  width: 100%;}

.table-outer table td {  padding: 15px 15px 15px !important;  font-size: 15px;}

.template_box h2 {  float: left;  font-size: 16px;  margin: 0 0 15px;  padding: 0;  width: 100%;}

.input-outer {  float: left;  margin: 0 0 10px;  padding: 0;  width: 100%;}

#TB_title {  float: left;  height: auto;  width: 100%;}

#mycontentgenscbtn , #backbtn{	background: #0085ba none repeat scroll 0 0;  border: medium none;  border-radius: 5px;  font-size: 18px;  height: auto; margin-top: 15px;  padding: 8px 18px;  text-shadow: 0 0 0 transparent;  text-transform: none;  color:#fff;}

#backbtn { float:left;}

#mycontentgenscbtn:hover, #backbtn:hover {  background: #000 none repeat scroll 0 0; color:#fff;}

#TB_ajaxWindowTitle {	font-size: 20px;	padding-top: 6px;	padding-bottom: 6px;}

.table-outer input[type="checkbox"] {  height: auto;  margin: 3px 0 0 -25px !important;}

.table-outer tr i{    font-size: 14px !important;    font-weight: normal;}

.table-outer .input-outer input[type="radio"] ,.table-outer input[type="radio"]{    height:15px !important;    margin: 2px 5px 0 0 !important;
  padding: 0 !important;}

.multiselect-container>li{    margin-bottom: 0;}

.multiselect-container>li>a>label{	padding: 2px 13px 3px 5px;}

.multiselect-container>li>a>label>input[type=checkbox] {	padding: 0 !important;    height: 15px !important;}

.sidebar_div{float:right}

.sidebar_div #secondary{width:100%}

.storefront_outer table td {   vertical-align: top;}

.product_ids select:focus option, .product_ids select:focus, .product_ids option:focus { background: #e1e1e1 !important;}

.product_ids select:focus option{background: #e1e1e1 !important;}

#mycontent_shortcode_popup_inner .select_add_cls{  width: 94% !important;  float: right!important;  margin-right: 15px!important; }

h2.head_memb{margin :10px 0px;font-size:18px;float: left;font-weight: bold;}

#hideproductid, #productid{ display:none}

</style>
  
  <div id="mycontent_shortcode_popup" style="display:none;" class="popup_div_outer">

     <div class="live-links-outer" id="mycontent_shortcode_popup_inner"> 
 	<div class="live-links-outer-div" id="memberpages_shortcode_popup_button">
	 <h2 class="head_memb ">Membership Options</h2>
	 <table width="100%" cellpadding="2" cellspacing="2" class="table_page_select">
		<tr>
			<td valign="top"> Select a Membership page </td>
			<td valign="top" width="50%">
				<select  name="selectmemberpages" id="selectmemberpages" class="select_add_cls">
					<option value="Select">--Select--</option>
					<option value="DAPMyContent" selected="selected">My Content</option>
					<option value="DAPMyProfile">My Profile</option>
					<option value="DAPAffiliateInfo">Affiliate Info & Stats</option>
					<option value="DAPMemberInvoice">Member Invoice</option>
					<option value="DAPMemberCancel">Member Cancellation</option>
					<option value="DAPLogin">Member Login</option>
				</select><br />
			</td>
		</tr>
	</table>  
    <table width="100%" cellpadding="2" cellspacing="2">

	 <tr class="table-outer">	 
			<td valign="middle" width="100%" style="padding-bottom:0;">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr class="my_content_heading">
						<th> Template Options </th>
					</tr>
					<tr>
						<td>Select Template Type</td>
						<td valign="middle" width="50%">
							<select style="" name="selecttemplate" id="selecttemplate" style=" width: 55% !important;">
							<option value="template1">Display 1 product per row</option>
							<option value="template2">Display multiple products per row</option>
							</select>
						</td>
					</tr>
					

				</table>
			</td>
    </tr>
	
	<tr class="table-outer">	 
			<td valign="middle" width="100%" style="padding-top:0;">
				<table width="100%" cellpadding="2" cellspacing="2">
				<tr id="onecolumn" class="template_box">  

		<td valign="top" >
		   <h2><strong>Select Display options </strong></h2>
		   <div class="row">
		   <div class="col-sm-4 template-select-outer">
		    <span class="input-outer"> <input type="radio" class="displayoption" name="display_option_single" id="multi_sel1" value="showInpage" checked> Display Available / Upcoming Content In-Page
		   <?php  $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/template1_expand_collapse.jpg"; ?>	  </span>
		   <img src="<?php echo $img_url_template1 ?>" />	
		   </div>
		   <div class="col-sm-4 template-select-outer">		   
		    <span class="input-outer"> <input type="radio" class="displayoption" name="display_option_single" id="multi_sel3" value="showPopup" >Display Available / Upcoming Content in a Popup
		   <?php  $img_url_withpop_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/template1_with-popup.jpg"; ?>   </span>
		     <img src="<?php echo $img_url_withpop_template1 ?>" />
			 </div>
			  <div class="col-sm-4 template-select-outer">		   
		    <span class="input-outer">  <input type="radio" class="displayoption" name="display_option_single" id="multi_sel2" value="showProductPage" >Redirect to a Product Specific Page
		   <?php  $img_url_withpop_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/template1.jpg"; ?>  </span>
		     <img src="<?php echo $img_url_withpop_template1 ?>" />
			 </div>
			</div>
        </td>
	 </tr>
	  <tr id="multicolumn" class="template_box">  

		<td valign="top" >
		   <h2><strong>Select Display options </strong></h2>
		     
		   <div class="row">
		   <div class="col-sm-4 template-select-outer">
		    <span class="input-outer"> <input type="radio" class="displayoption" name="display_option_mul" id="multi_sel1" value="showPopup" checked>Display Available / Upcoming Content in a Popup
		   <?php  $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/template2-popup.jpg"; ?>	 </span>
		   <img src="<?php echo $img_url_template1 ?>"  />	
		   </div>
		    <div class="col-sm-4 template-select-outer">
		    <span class="input-outer">  <input type="radio" class="displayoption" name="display_option_mul" id="multi_sel2" value="showProductPage" >Redirect to a Product Specific Page
		   <?php  $img_url_template1 = plugins_url()."/DAP-WP-LiveLinks/includes/images/template2.jpg"; ?>	 </span>
		   <img src="<?php echo $img_url_template1 ?>"  />	
		   </div>
  
			</div>
        </td>
	
	 </tr>
	 
				</table>
			</td>
    </tr>
	
	 <tr class="table-outer" id="styling_option_box">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Styling Options </th>
					</tr>
					<tr>      
						<td>Select width</td>
						<td valign="middle" width="50%" class="rangeslider">
							 <input class="box_width" id="boxWidth"  name="boxwidth"  data-slider-id='ex1Slider' type="text" data-slider-min="100" data-slider-max="1200" data-slider-step="10" data-slider-value="700"/>    
  
							
						</td>
					</tr>
					
					
  									
				</table>
			</td>
    </tr>
	
	<tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Product Image Options </th>
					</tr>
					<tr>      
						<td>Select Image width</td>
						<td valign="middle" width="50%" class="rangeslider">
							 <input class="width_image" id="imageWidth"  name="imagewidth"  data-slider-id='ex2Slider' type="text" data-slider-min="100" data-slider-max="300" data-slider-step="1" data-slider-value="250"/>    
		
						</td>
					</tr>
					
					<tr>       
						<td>Select Image height</td>
						<td valign="middle" width="50%" class="rangeslider">
							 <input class="height_image" id="imageHeight"  name="imageheight"  data-slider-id='ex3Slider' type="text" data-slider-min="100" data-slider-max="300" data-slider-step="1" data-slider-value="300"/>    
		
						</td>
					</tr>
					
					
  									
				</table>
			</td>
    </tr> 

 <tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Styling Button Options </th>
					</tr>
					<tr>      
						<td>Button Background color for "Available" products</td>
						<td valign="middle" width="50%" class="rangeslider">
							<div id="bg_clr_div" class="input-group colorpicker-component input-group-outer">
							<input type="text" value="#16b123" id="availableButton" class="available_button"  name="availablebutton"  />
							<span class="input-group-addon "><i></i></span>
							</div>							 
						</td>
					</tr>
					<tr>      
						<td>Button Background color for "Not yet available" products</td>
						<td valign="middle" width="50%" class="rangeslider">
							<div id="bg_clr_div1" class="input-group colorpicker-component input-group-outer">
							<input type="text" value="#545454" id="unAvailableButton" class="unavailable_button"  name="unavailablebutton"  />
							<span class="input-group-addon "><i></i></span>
							</div>							 
						</td>
					</tr>
			<tr>      
						<td>Background color for Pagination Buttons</td>
						<td valign="middle" width="50%" class="rangeslider">
							<div id="bg_clr_div2" class="input-group colorpicker-component input-group-outer">
							<input type="text" value="#545454" id="paginationButton" class="pagination_button"  name="paginationbutton"  />
							<span class="input-group-addon "><i></i></span>
							</div>							 
						</td>
					</tr>
				</table>
			</td>
    </tr>
	 	<tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Product Display Options </th>
					</tr>
					<tr>      
						<td style="font-size: 15px;">Display not-yet-available products to the logged-in member.<br><br>
						<span style="font-size: 14px;font-weight:100;color: gray;">A logged-in member will only find the products that they currently own on the "My Content" page. However, if you want to showcase the products that they don't currently own under a "You might be interested in these products" section, select "Display All Products" option here.</span>
					</td>
					 
						<td valign="top" width="50%">
							<div class="row">
							   <div class="col-sm-4">
								<span class="input-outer"> <input type="radio"  class="showunavailableproduct"  id="showUnavailableProduct1" name="showunavailableproducts"  value="N" checked>Only Display  Products that the logged-in member owns.
							  </span>
							
							   </div>
							   <div class="col-sm-4">		   
								<span class="input-outer"> <input type="radio"  class="showunavailableproduct" id="showUnavailableProduct2"  name="showunavailableproducts"  value="Y">Display All Products (even the products that the member does not own currently).
							   </span>
								
								 </div>
								</div>					 
						</td>
					</tr>
					<tr></tr>
					<tr></tr>
				<tr>      
						<td><br>Page with Sidebar or Full Width? <BR><BR>
						<span style="font-size: 14px;font-weight:100;color: gray;">
						If you plan on using this shortcode on a page with a sidebar, select "sidebar" here. Otherwise select "full width". 
						</span>
						</td>
						<td valign="top" width="50%" ><br>
						<select id="sidebarorfull">
							<option value="sidebar">Sidebar</option>
							<option value="fullwidth">Full Width</option>
						</select>
						</td>
				</tr>
					<tr></tr>
										<tr>
						<td> Select list of products that you want to display </td>
						<td valign="top" width="50%"><br><br>
							<select style=""  id="productid" style=" width: 55% !important;" name="productid[]" multiple="multiple" class="product_ids">
							  <option value="ALL" selected="selected">ALL</option>
								 <?php
									$session = Dap_Session::getSession();
									$user = $session->getUser();
										
									if($user){
										$userId = $user->getId();
									}
									$productList = null;
										
									if( isset($user) ) {
										$productList = Dap_Product::loadProducts("","A");
									}
									
									foreach ($productList as $prod) {
										//$product = Dap_Product::loadProduct($userProduct->getProduct_id());
								?>  
									<option value="<?php echo $prod->getId(); ?>" ><?php echo $prod->getName(); ?></option>
							  <?php } ?>	 
							   </select>
							   <select style=""  id="productid1" style=" width: 55% !important;" name="productid[]" multiple="multiple" class="product_ids">
							  <option value="ALL" selected="selected">ALL</option>
								  <?php
									$session = Dap_Session::getSession();
									$user = $session->getUser();
		
									$userProducts = null;
										
									if( isset($user) ) {
										$userProducts = Dap_UsersProducts::loadProducts($user->getId());
									}
									
									foreach ($userProducts as $userProduct) {
										$product = Dap_Product::loadProduct($userProduct->getProduct_id());
								?>  
									<option value="<?php echo $product->getId(); ?>" ><?php echo $product->getName(); ?></option>
							  <?php } ?>
							   <span style="font-size: 12px; font-weight: bold;">(it will show even if the user does not have access )</span>
		 
						</td>
					</tr>
					
					<tr>
						<td>Hide these products</td>
						<td valign="middle" width="50%"><br><br>
								<select style="" id="hideproductid" style=" width: 55% !important;" name="hideproductid[]" multiple="multiple" class="product_ids">
								  <option value="NONE" selected="selected">NONE</option>
									 <?php
										$session = Dap_Session::getSession();
										$user = $session->getUser();

										$productList = null;
											
										if( isset($user) ) {
											$productList = Dap_Product::loadProducts("","A");
										}
										
										foreach ($productList as $prod) {
											//$product = Dap_Product::loadProduct($userProduct->getProduct_id());
									?>  
										<option value="<?php echo $prod->getId(); ?>" ><?php echo $prod->getName(); ?></option>
								  <?php } ?>	 
							   </select>
							   	<select style="" id="hideproductid1" style=" width: 55% !important;" name="hideproductid[]" multiple="multiple" class="product_ids">
								  <option value="NONE" selected="selected">NONE</option>
								  <?php
									$session = Dap_Session::getSession();
									$user = $session->getUser();
	
									$userProducts = null;
										
									if( isset($user) ) {
										$userProducts = Dap_UsersProducts::loadProducts($user->getId());
									}
									
									foreach ($userProducts as $userProduct) {
										$product = Dap_Product::loadProduct($userProduct->getProduct_id());
								?>  
									<option value="<?php echo $product->getId(); ?>" ><?php echo $product->getName(); ?></option>
							  <?php } ?>
							   </select>
						</td>
					</tr>
					
				</table>
			</td>
    </tr>
											

					
	
		 <tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Sorting Options </th>
					</tr>
					<tr>
						<td>Order of Products</td>
						<td valign="middle" width="50%">
							<select style="" name="sorttype" id="sorttype" style=" width: 55% !important;">
								<option value="purchasedate" selected="selected">By Purchase Date</option>
								<option value="id">By ID</option>
								<option value="name">By Name</option>
											
							</select>
						</td>
					</tr>
					
					<tr>
						<td>Order Of Links </td>
					<td valign="middle" width="50%">
						<select style="font-weight:bold;" name="orderoflinks" id="orderoflinks">
						<option value="NEWESTFIRST" selected="selected">NEWESTFIRST</option>
						<option value="OLDESTFIRST">OLDESTFIRST</option>
						</select>

		            </td>
					</tr>
					
				</table>
			</td>
    </tr>
	
		 <tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Pagination Options </th>
					</tr>
							 
					 <tr>
					<td valign="top">
					How Many Available Links you want to display? 
					</td>
					<td valign="top" width="50%">
						<input type="text" id="howmanylinks" value="9999"> 
					</td>
					</tr>

					
					<tr>
					<td valign="top">
					How Many Upcoming Links you want to display?
					</td>
					<td valign="top" width="50%">
						<input type="text" id="howmanyupcominglinks" value="5"> 
					</td>
					</tr>

					
									
				</table>
			</td>
		</tr>

		 <tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Display Options </th>
					</tr>
					<tr>
						<td>Display Product Image?</td>
							<td valign="top" width="50%">
							<select style="font-weight:bold;" name="showproductimage" id="showproductimage">
							<option value="Y" selected="selected">YES</option>
							<option value="N">NO</option>
							</select><i style="font-size:13px;"> (Set "Product Image URL" in DAP Admin=>Products/Levels Page=>Product Image field)</i>
							</td>
					</tr>
					
					<tr>
					<td valign="top">Display Product Name?</td>
						<td valign="top" width="50%">
						<select style="font-weight:bold;" name="showproductname" id="showproductname">
						<option value="Y" selected="selected">YES</option>
						<option value="N">NO</option>
						</select>						
					  </td>
					</tr>
					<tr>
					<td valign="top">Display Product Description? </td>
						<td valign="top" width="50%">
						<select style="font-weight:bold;" name="showdescription" id="showdescription">
						<option value="Y" >YES</option>
						<option value="N" >NO</option>
						</select>						
					  </td>
					</tr>
							
		
					
					    <tr>
    <td valign="top">
    Display Access Start Date? 
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showaccessstartdate" id="showaccessstartdate">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
     
    </td>
    </tr>
	
	    <tr>
    <td valign="top">
    Display Access End Date?  
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showaccessenddate" id="showaccessenddate">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
    </td>
    </tr>
	
	    <tr>
    <td valign="top">
    Enter DATE Format
    </td>
    <td valign="top" width="50%">
		<input type="text" id="dateformat" value="YYYY-MM-DD"> 
		</td>
    </tr>

	
	
	 <tr>
    <td valign="top">
    Display Renewal HTML (if access to product has expired)?
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showrenewalhtml" id="showrenewalhtml">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><i style="font-size:13px;">(Set Renewal HTML in DAP Products Page=>Cancellation And Expiration tab)</i>
       <br /><br /> 
    </td>
    </tr>
    
    
    <tr>
    <td valign="top">
    Display Product Count? 
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showproductcount" id="showproductcount">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
    </td>
    </tr>
    
    <tr>
    <td valign="top">
    Display Member Links?  
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showlinks" id="showlinks">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><i style="font-size:13px;"> (if set to NO, only products are displayed)</i>
    </td>
    </tr>
    
    
    <tr>
    <td valign="top">
    "My Content" section collapsed by default?  
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showlinkscollapsed" id="showlinkscollapsed">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select><i style="font-size:13px;"> (if set to YES, product list is displayed with a 'click here' link to expand the My Content section)</i>
    </td>
    </tr>
	
	<tr>
    <td valign="top">
    Display "Available Content" Section? 
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showavailablecontent" id="showavailablecontent">
          <option value="Y" selected="selected">YES</option>
          <option value="N">NO</option>
       </select>
    </td>
    </tr>
	 

        <tr>
    <td valign="top">
    Display "Upcoming Content" Section? 
    </td>
    <td valign="top" width="50%">
       <select style="font-weight:bold;" name="showupcomingcontent" id="showupcomingcontent">
          <option value="N" selected="selected">NO</option>
          <option value="Y">YES</option>

       </select>
    </td>
    </tr>

									
				</table>
			</td>
    </tr>

    	 <tr class="table-outer">	 
			<td valign="middle" width="100%">
				<table width="100%" cellpadding="2" cellspacing="2">
					<tr>
						<th> Label/Text Options </th>
					</tr>
					<tr>
						<td>Enter "Click Here To View Content" text  for Available Products </td>
						    <td valign="top" width="50%">
		<input type="text" id="clickheretext" value="Access your Content"> 
       <i style="font-size:13px;"> (when users click on it, it will display the links)</i>
    </td>
					</tr>
					<tr>
						<td>Enter "Click Here To View Content" text  for Unavailable Products </td>
						    <td valign="top" width="50%">
							<input type="text" id="clickheretextunavilable" value="Click here to Get Access"> 
						   <i style="font-size:13px;"> (when users click on it, it will display the links)</i>
						</td>
					</tr>
					
					
					 <tr>
    <td valign="top">
    Enter "No Content Available" Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="nocontenttext" value="No Content Available"> 
     <i style="font-size:13px;"> (it will display if no content is available under the product)</i>
    </td>
    </tr>
    
     <tr>
    <td valign="top">
    Enter "My Content" Header Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="mycontentheadertext" value="My Content"> 

    </td>
    </tr>
    
    <tr>
    <td valign="top">
    Enter "Available Content" Header Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="availcontentheadertext" value="Available Content"> 
    </td>
    </tr>

    
   
    <tr>
    <td valign="top">
    Enter "Upcoming Content" Header Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="upcomingcontentheadertext" value="Upcoming Content"> 
    </td>
    </tr>
    

    
    <tr>
    <td valign="top">
    Enter "Coming Soon (In Days)" Text
    </td>
    <td valign="top" width="50%">
		<input type="text" id="comingsoonheadertext" value="Upcoming (In Days)"> 
    </td>
    </tr>
					
				</table>
			</td>
    </tr>
    
    </table>     
    
   <div align="center" class="btn_outer"><button class="button-primary back-btn" id="backbtn">Back</button> 
   <button class="button-primary next-btn" id="mycontentgenscbtn">Generate Shortcode</button></div>
    <br><br>
    </div>  
  </div>
 </div>

<?php
}

//javascript code needed to make shortcode appear in TinyMCE editor
add_action('admin_footer','mycontent_shortcode_add_shortcode_to_editor');
function mycontent_shortcode_add_shortcode_to_editor(){?>

<script>
var $jq = jQuery.noConflict();


$jq('#mycontentgenscbtn').on('click',function() {

	var selectTemplate = $jq('#selecttemplate').val();

	var displayOption = window.Var1;

	if(displayOption === undefined || displayOption === null || displayOption === "") {

		 var displayOption = "showPopup";

	}else{

		var displayOption = window.Var1;

	}

	var sortType = $jq('#sorttype').val();

	//var productId = $jq('#productid').val();

	//var hideProductId = $jq('#hideproductid').val();


	var showUnavailableProducts = window.Var2;

	if(showUnavailableProducts === undefined || showUnavailableProducts === null || showUnavailableProducts === "") {

		 var showUnavailableProducts = "N";

	}else{

		var showUnavailableProducts = window.Var2;

	}

	if(showUnavailableProducts == "N"){

		var productId = $jq('#productid1').val();

		var hideProductId = $jq('#hideproductid1').val();

	}else{

		var productId = $jq('#productid').val();

		var hideProductId = $jq('#hideproductid').val();

	}



	var sidebarorfull = $jq('#sidebarorfull').val();

	var imageWidth = $jq('#imageWidth').val();  

	var imageHeight = $jq('#imageHeight').val();  

	// var width_sidebar = $jq('.width_sidebar').val();  

	//var sidebarOption = $jq('.sidebaroption:checked').val();  



	var availableButton = $jq('#availableButton').val();  

	var unavailableButton = $jq('#unAvailableButton').val();  

	var paginationButton = $jq('#paginationButton').val();  


	var showProductImage = $jq('select#showproductimage').val();

	var showProductName = $jq('select#showproductname').val();

	var showAccessStartDate = $jq('select#showaccessstartdate').val();

	var showAccessEndDate = $jq('select#showaccessenddate').val();

	var dateFormat = $jq('#dateformat').val();

	var showDescription = $jq('select#showdescription').val();

	var orderOfLinks = $jq('#orderoflinks').val();

	//var errMsgTemplate = escape($jq('#errmsgtemplate').val());

	//var errMsgTemplate = "SHORT";

	var showProductCount = $jq('select#showproductcount').val();

	var showLinks = $jq('#showlinks').val();

	var showLinksCollapsed = $jq('select#showlinkscollapsed').val();

	var clickHereText = escape($jq('#clickheretext').val());

	var clickHereTextUnavilable = escape($jq('#clickheretextunavilable').val());


	var noContentText = escape($jq('#nocontenttext').val());

	var myContentHeaderText = escape($jq('#mycontentheadertext').val());

	var availContentHeaderText = escape($jq('#availcontentheadertext').val());

	var showAvailableContent = $jq('select#showavailablecontent').val();


	var howManyLinks = $jq('#howmanylinks').val();

	var upcomingContentHeaderText = escape($jq('#upcomingcontentheadertext').val());


	var showUpcomingContent = $jq('select#showupcomingcontent').val();

	var howManyUpcomingLinks = $jq('#howmanyupcominglinks').val();

	var comingSoonHeaderText = $jq('#comingsoonheadertext').val();

	// var prev_btn_text = $jq('.prev_btn_text').val();

	//var next_btn_text = $jq('.next_btn_text').val();


	if(selectTemplate == "template1"){

	 var boxWidth = $jq('#boxWidth').val();

	 var shortcode = '[DAPMyContent selectTemplate="'+selectTemplate +'" displayOption="'+displayOption +'" sortType="'+sortType+'" boxWidth="'+boxWidth+'" imageWidth="'+imageWidth+'" imageHeight="'+imageHeight+'" availableButton="'+availableButton+'" unavailableButton="'+unavailableButton+'" paginationButton="'+paginationButton+'" showUnavailableProducts="'+showUnavailableProducts+'" sidebarorfullwidth="'+sidebarorfull+'" productId="'+productId+'" hideProductId="'+hideProductId+'" showProductImage="'+showProductImage+'" showProductName="'+showProductName+'" showAccessStartDate="'+showAccessStartDate+'" showAccessEndDate="'+showAccessEndDate+'" dateFormat="'+dateFormat+'" showDescription="'+showDescription+'" orderOfLinks="'+orderOfLinks+'" showProductCount="'+showProductCount+'" showLinks="'+showLinks+'" showLinksCollapsed="'+showLinksCollapsed+'" clickHereText="'+clickHereText+'"  clickHereTextUnavilable="'+clickHereTextUnavilable+'" noContentText="'+noContentText+'" myContentHeaderText="'+myContentHeaderText+'" availContentHeaderText="'+availContentHeaderText+'" showAvailableContent="'+showAvailableContent+'" howManyLinks="'+howManyLinks+'" upcomingContentHeaderText="'+upcomingContentHeaderText+'" showUpcomingContent="'+showUpcomingContent+'" howManyUpcomingLinks="'+howManyUpcomingLinks+'" comingSoonHeaderText="'+comingSoonHeaderText+'" ]';		

	}else{

	  boxWidth = imageWidth;

	  var shortcode = '[DAPMyContent selectTemplate="'+selectTemplate +'" displayOption="'+displayOption +'" sortType="'+sortType+'" imageWidth="'+imageWidth+'" imageHeight="'+imageHeight+'" availableButton="'+availableButton+'" unavailableButton="'+unavailableButton+'" paginationButton="'+paginationButton+'" showUnavailableProducts="'+showUnavailableProducts+'" sidebarorfullwidth="'+sidebarorfull+'" productId="'+productId+'" hideProductId="'+hideProductId+'" showProductImage="'+showProductImage+'" showProductName="'+showProductName+'" showAccessStartDate="'+showAccessStartDate+'" showAccessEndDate="'+showAccessEndDate+'" dateFormat="'+dateFormat+'" showDescription="'+showDescription+'" orderOfLinks="'+orderOfLinks+'" showProductCount="'+showProductCount+'" showLinks="'+showLinks+'" clickHereText="'+clickHereText+'"  clickHereTextUnavilable="'+clickHereTextUnavilable+'" noContentText="'+noContentText+'" myContentHeaderText="'+myContentHeaderText+'" availContentHeaderText="'+availContentHeaderText+'" showAvailableContent="'+showAvailableContent+'" howManyLinks="'+howManyLinks+'" upcomingContentHeaderText="'+upcomingContentHeaderText+'" showUpcomingContent="'+showUpcomingContent+'" howManyUpcomingLinks="'+howManyUpcomingLinks+'" comingSoonHeaderText="'+comingSoonHeaderText+'" ]';		

	}   

 	  

	send_to_editor(shortcode);

	//close the thickbox after adding shortcode to editor

	self.parent.tb_remove();

	//remove_css_popup();


});

   

$jq(function() {	

	var displayOption = $jq('input[name="display_option_single"]:checked').val();

	window.Var1 = displayOption;

	$jq('input[name="display_option_single"]').on('change',function() {

		var displayOption = $jq('input[name="display_option_single"]:checked').val();

		window.Var1 = displayOption;

	});	

	$jq('#selecttemplate').on('change',function() {

		var selectdrop = $jq('select#selecttemplate option:selected').val();

		if(selectdrop == "template1"){

			$jq('#multicolumn').hide();

			$jq('#onecolumn').show();

			$jq('#styling_option_box').show(); 

			$jq('#showdescription').val('Y');

			var displayOption = $jq('input[name="display_option_single"]:checked').val();

			window.Var1 = displayOption;

			$jq('input[name="display_option_single"]').on('change',function() {

				var displayOption = $jq('input[name="display_option_single"]:checked').val();

				window.Var1 = displayOption;

			});

		}else{

			$jq('#onecolumn').hide();		 

			$jq('#multicolumn').show();

			$jq('#styling_option_box').hide(); 

			$jq('#showdescription').val('N');

			var displayOption = $jq('input[name="display_option_mul"]:checked').val();

			window.Var1 = displayOption;

			$jq('input[name="display_option_mul"]').on('change',function() {

				var displayOption = $jq('input[name="display_option_mul"]:checked').val();

				window.Var1 = displayOption;

			});

		}

	});	

});	



$jq(function() {	

	var showUnavailableProducts = $jq('input[name="showunavailableproducts"]:checked').val();

	window.Var2 = showUnavailableProducts;

	$jq('input[name="showunavailableproducts"]').on('change',function() {

		var showUnavailableProducts = $jq('input[name="showunavailableproducts"]:checked').val();

		if(showUnavailableProducts == "N"){

			$jq('#productid').hide();

			$jq('#productid1').show();

			$jq('#hideproductid').hide();

			$jq('#hideproductid1').show();

			window.Var2 = showUnavailableProducts;

		}else{

			$jq('#productid1').hide();

			$jq('#productid').show();

			$jq('#hideproductid').show();

			$jq('#hideproductid1').hide();

			window.Var2 = showUnavailableProducts;

		}

	});	

});	

  

</script>
<?php
}

add_shortcode('DAPMyContent', 'dap_mycontent');
 
function dap_mycontent($atts, $content=null){ 
	extract(shortcode_atts(array(
		'showproductimage' => 'Y',
		'showproductname' => 'Y',
		'showaccessstartdate' => 'Y',
		'showaccessenddate' => 'Y',
		'showdescription' => 'Y',
		'showlinks' => 'Y',
		'orderoflinks' => 'NEWESTFIRST',
		'howmanylinks' => '10000',		
		//'errmsgtemplate' => 'SHORT',
		'showunavailableproducts' => 'N',
		'sidebarorfullwidth' => 'sidebar',
		'productid' => 'ALL',
		'boxwidth' => '700',
		'imagewidth' => '250',
		'imageheight' => '300',
		'width_sidebar' => '250',
		//'sidebaroption' => 'N',
		'availablebutton' => '#16b123',
		'unavailablebutton' => '#545454',
		'paginationbutton' => '#545454',
		'sorttype' => 'purchasedate',
		'dateformat' => 'MM-DD-YYYY',
		'showproductcount' => 'Y',
		'showrenewalhtml' => 'Y',
		'hideproductid' => 'NONE',
		'mycontentheadertext' => 'My Content',
		'availcontentheadertext' => 'Available Content',
		'upcomingcontentheadertext' => 'Upcoming Content',
		'showavailablecontent' => 'Y',
		'showupcomingcontent' => 'N',
		'howmanyupcominglinks' => '10000',
		'comingsoonheadertext' => 'Coming Soon (In Days)',
		'clickheretext' => 'Click Here to Access',
		'clickheretextunavilable' => 'Click Here to Get Access',
		'showlinkscollapsed' => 'Y',
		'nocontenttext' => 'No Content Available Currently',
		'template' => 'template1',
		'selecttemplate' => 'template1',
		'displayoption' => 'showPopup'
	), $atts)); 
	
	
	$lldocroot = defined('SITEROOT') ? SITEROOT : $_SERVER['DOCUMENT_ROOT'];
	

	$mycontentheadertext=urldecode($mycontentheadertext);
	$availcontentheadertext=urldecode($availcontentheadertext);
	$upcomingcontentheadertext=urldecode($upcomingcontentheadertext);
	$comingsoonheadertext=urldecode($comingsoonheadertext);
	$clickheretext=urldecode($clickheretext);
	$clickheretextunavilable=urldecode($clickheretextunavilable);
	$nocontenttext=urldecode($nocontenttext);
	/*$jsurl=get_option('siteurl')."/wp-content/plugins/DAP-WP-LiveLinks/includes/js/contentdrip.js";	
	wp_enqueue_script('condcontdripjs', $jsurl, array('jquery'));
	*/
	
	add_action('wp_footer','mydap_content_js');
	
	$session = Dap_Session::getSession();
	$user = $session->getUser();
	
	logToFile("mycontentheadertext=".$mycontentheadertext);
	
	/*if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		//logToFile("Not logged in, returning errmsgtemplate");
		$errorHTML = mb_convert_encoding(MSG_PLS_LOGIN, "UTF-8", "auto") . " <a href=\"" . Dap_Config::get("LOGIN_URL") . "\">". mb_convert_encoding(MSG_CLICK_HERE_TO_LOGIN, "UTF-8", "auto") . "</a>";
		return $errorHTML;
	}*/

	$userProducts = null;
	$productList = null;

	if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		$errorHTML = mb_convert_encoding(MSG_PLS_LOGIN, "UTF-8", "auto") . " <a href=\"" . Dap_Config::get("LOGIN_URL") . "\">". mb_convert_encoding(MSG_CLICK_HERE_TO_LOGIN, "UTF-8", "auto") . "</a>";
		if($showunavailableproducts == "N"){
		}else{
			$productList = Dap_Product::loadProducts("","A");
		}
	}else{
		if($user){
			$userId = $user->getId();	
		}
		if( isset($user) ) {
			$userProducts = Dap_UsersProducts::loadProducts($user->getId());
			$productList = Dap_Product::loadProducts("","A");
		}
		$accessuserProducts = Dap_UsersProducts::loadProducts($user->getId());
		foreach ($accessuserProducts as $userProduct1) {
			$userProduct_ids[] =  $userProduct1->getProduct_id();
		}
		foreach ($accessuserProducts as $userProduct_date) {
			$userProduct_alldate[$userProduct_date->getProduct_id()] =  array(
			'access_start_date'=>$userProduct_date->getAccess_start_date(),
			'access_end_date'=>$userProduct_date->getAccess_end_date());
		}
	}

	//$template="template1";
	$template=urldecode($selecttemplate);
		$regulardrip_template_path=ABSPATH . "/wp-content/plugins/DAP-WP-LiveLinks/includes/mycontenttemplates/".$template;
	$regulardrip_template_path1=ABSPATH . "/wp-content/plugins/DAP-WP-LiveLinks/includes/mycontenttemplates/";

	//REGULAR DRIP HEADER TEMPLATE CONTENT
	$regularDripHeaderTemplateName="/DAPMyContentHeaderTemplate.html";
	$customRegularDripHeaderTemplateName="/customDAPMyContentHeaderTemplate.html";     
	  if($showproductcount == 'Y') {
		//$content = USER_LINKS_YOUCURRENTLYHAVEACCESSTO_TEXT . count($userProducts). USER_LINKS_PRODUCTS_TEXT . "<br/><br/>";
		if($template == "template1"){
				$regulardrip_head_temp_content='<div class="product_div"><p style="padding: 20px 20px 10px 0; float: left; width: 100%;font-size: 18px; color: #000; line-height: normal; margin: 5px 0px 25px 0px;">'.USER_LINKS_YOUCURRENTLYHAVEACCESSTO_TEXT . count($userProducts). USER_LINKS_PRODUCTS_TEXT . '</p><br/><br/>'.getDripTemplateContent($regulardrip_template_path, $regularDripHeaderTemplateName, $customRegularDripHeaderTemplateName).'</div>';
		}else{
			$regulardrip_head_temp_content='<div class="product_div"><p style="padding: 20px 20px 10px 0; float: left; width: 100%;font-size: 18px; color: #000; line-height: normal; margin: 5px 0px 25px 10px;">'.USER_LINKS_YOUCURRENTLYHAVEACCESSTO_TEXT . count($userProducts). USER_LINKS_PRODUCTS_TEXT . '</p><br/><br/>'.getDripTemplateContent($regulardrip_template_path, $regularDripHeaderTemplateName, $customRegularDripHeaderTemplateName).'</div>';
		}
	}  else{
		$regulardrip_head_temp_content='<div class="product_div">'.getDripTemplateContent($regulardrip_template_path, $regularDripHeaderTemplateName, $customRegularDripHeaderTemplateName).'</div>';
	}   
	
	
	$cssurl= DAPMYCONTENT_URL."/includes/mycontenttemplates/".$template."/css";	
	$fancyboxurl= DAPMYCONTENT_URL."/includes/css/jquery.fancybox.css";	
	$datatableurl= DAPMYCONTENT_URL."/includes/css/dataTables.bootstrap.min.css";	
	//$sidebarcsspath= DAPMYCONTENT_URL."/includes/mycontenttemplates/sidebar/css/mycontentsidebar.css";	
	$csspath= DAPMYCONTENT_FOLDER."/includes/mycontenttemplates/".$template."/css";		

	if( file_exists($csspath . "/custommycontent.css") ) 
		$cssurl = $cssurl . "/custommycontent.css";
	else 
		$cssurl = $cssurl . "/mycontent.css";
	
	  $regulardrip_head_temp_content = str_replace( '[CSSURL]',$cssurl, $regulardrip_head_temp_content); 	
	
	//REGULAR DRIP  INDIVIDUAL PRODUCT TEMPLATE
	//$regularProductTemplateName="/DAPMyContentIndividualProductTemplate.html";
	$regularProductTemplateName="/DAPMyContentIndividualProductTemplate.html";
	
	
	
	if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		$errorHTML = mb_convert_encoding(MSG_PLS_LOGIN, "UTF-8", "auto") . " <a href=\"" . Dap_Config::get("LOGIN_URL") . "\">". mb_convert_encoding(MSG_CLICK_HERE_TO_LOGIN, "UTF-8", "auto") . "</a>";
		
		if($showunavailableproducts == "N"){
			return $errorHTML;
		}else{
			echo $errorHTML;
			$productList = Dap_Product::loadProducts("","A");
		}
	}
	if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		$regularProductTemplateName="/DAPMyContentIndividualProductTemplate.html";
	}else{	
		if($displayoption == "showProductPage"){
			$regularProductTemplateName="/DAPMyContentIndividualProductTemplate.html";
		}elseif($displayoption == "showPopup"){
			$regularProductTemplateName="/popup/DAPMyContentIndividualProductTemplate.html";
		}else{
			$regularProductTemplateName="/inpage/DAPMyContentIndividualProductTemplate.html";
		}
	}	
	
	$customRegularProductTemplateName="/customDAPMyContentIndividualProductTemplate.html";
	$regulardrip_prod_temp_content=getDripTemplateContent($regulardrip_template_path, $regularProductTemplateName, $customRegularProductTemplateName);
	
	$regularProductNoImageTemplateName="/DAPMyContentIndividualProductNoImageTemplate.html";
	$customRegularProductNoImageTemplateName="/customDAPMyContentIndividualProductNoImageTemplate.html";
	$regulardrip_prod_noimage_temp_content=getDripTemplateContent($regulardrip_template_path, $regularProductNoImageTemplateName, $customRegularProductNoImageTemplateName);
	
	$regularDripTemplateName="/DAPMyContentAvailableContentTemplate.html";
	$customRegularDripTemplateName="/customDAPMyContentAvailableContentTemplate.html";
	$regulardrip_temp_content=getDripTemplateContent($regulardrip_template_path, $regularDripTemplateName, $customRegularDripTemplateName);
	
	//REGULAR DRIP INDIVIDUAL CONTENT TEMPLATE
	$upcomingRegularDripTemplateName="/DAPMyContentUpcomingContentTemplate.html";
	$customUpcomingRegularDripTemplateName="/customDAPMyContentUpcomingContentTemplate.html";
	$upcoming_regulardrip_temp_content=getDripTemplateContent($regulardrip_template_path, $upcomingRegularDripTemplateName, $customUpcomingRegularDripTemplateName);
	
	//Sidebar	
	//$regularMyContentSidebarTemplate = "/sidebar/DAPMyContentSidebarTemplate.html";
	//$customRegularMyContentSidebarTemplate = "/sidebar/customDAPMyContentSidebarTemplate.html";
	//$regulardrip_prod_sidebar_temp = getDripTemplateContent($regulardrip_template_path1, $regularMyContentSidebarTemplate, $customRegularMyContentSidebarTemplate);
	
	$ind_prod_content="";
	//loop over each product from the list
	logToFile("DAP-ShortCodes-Transactions: Iterate through each user->product",LOG_INFO_DAP);
	
	if(file_exists($lldocroot."/dap/dap-mycontent.php")) 
		include_once ($lldocroot."/dap/dap-mycontent.php"); 
	else {
		$errorHTML = "Missing Required File (/dap/dap-mycontent.php)";
		return $errorHTML;	
	}
	//echo "<pre>"; print_r($userProducts);echo "</pre>"; 
	
	/*************** Condition for sorting  products******************/
	
	if( !Dap_Session::isLoggedIn() || !isset($user) ) { 
			if($sorttype == "name") {
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
						$product_sort[$product->name] = $prod;  
				}	
				ksort($product_sort);
				$productList = $product_sort;
			}else{
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
						$product_sort[$product->id] = $prod;
					}	
				ksort($product_sort);
				$productList = $product_sort;
			}
	}else{
		if($showunavailableproducts == "N"){
			if($sorttype == "name") {
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
					if(in_array($prod->getId(), $userProduct_ids)){
						$product_sort[$product->name] = $prod;  
					}	
				}	
				ksort($product_sort);
				$productList = $product_sort;
				
			}elseif($sorttype == "id"){
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
					if(in_array($prod->getId(), $userProduct_ids)){
						$product_sort[$product->id] = $prod;
					}	
				}	
				ksort($product_sort);
				$productList = $product_sort;
							
			}else{	
				$access_prod = array();
				$date_array =array();
				$now = time(); 
				foreach($productList as $key=>$prod){
					$product = Dap_Product::loadProduct($prod->getId());
					$access_start_date_new = $userProduct_alldate[$prod->getId()]['access_start_date'];
					$your_date = strtotime($access_start_date_new);
					$datediff = $now - $your_date;
					if(in_array($prod->getId(), $userProduct_ids)){
						$date_array[$key] = floor($datediff/(60*60*24));
					}
					if(in_array($prod->getId(), $userProduct_ids)){
						$access_prod[] = $prod;
					}
				} 
				$date_array_new = asort($date_array);					
				$array_prods= array();
				foreach($date_array as $key=>$value){
					$array_prods[] =$productList[$key];
				}
				$productList = $array_prods;		
			
			}
			
		}else{
			
			if($sorttype == "name") {
				$access_prod = array();
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
					if(in_array($prod->getId(), $userProduct_ids)){
						$access_prod[$product->name] = $prod;
					}
				}
				ksort($access_prod);	
				$non_access_prod = array();
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
					if(!in_array($prod->getId(), $userProduct_ids)){
						$non_access_prod[$product->name] = $prod;
					}
				}	
				ksort($non_access_prod);
				$productList = array_merge($access_prod, $non_access_prod);				
							
			}elseif($sorttype == "id"){
				$access_prod = array();
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
					if(in_array($prod->getId(), $userProduct_ids)){
						$access_prod[$product->id] = $prod;
					}
				}
				ksort($access_prod);	
				$non_access_prod = array();
				foreach($productList as $prod){
					$product = Dap_Product::loadProduct($prod->getId());
					if(!in_array($prod->getId(), $userProduct_ids)){
						$non_access_prod[$product->id] = $prod;
					}
				}	
				ksort($non_access_prod);
				$productList = array_merge($access_prod, $non_access_prod);		
				
			}else{ //($sorttype == "purchasedate")
				$access_prod = array();
				$date_array =array();
				$now = time(); 
				
				foreach($productList as $key=>$prod){
					$product = Dap_Product::loadProduct($prod->getId());
					$access_start_date_new = $userProduct_alldate[$prod->getId()]['access_start_date'];
					$your_date = strtotime($access_start_date_new);
					$datediff = $now - $your_date;
					$date_array[$key] = floor($datediff/(60*60*24));
					
					if(in_array($prod->getId(), $userProduct_ids)){
						$access_prod[] = $prod;
					}
				} 
				$date_array_new = asort($date_array);					
				$array_prods= array();
				foreach($date_array as $key=>$value){
					$array_prods[] =$productList[$key];
				}
				$productList = $array_prods;
				
			}

		}
	}
	//echo "<pre>"; print_r($productList);echo "</pre>";die;
	
	/*************** Product loop starts ******************/


	$i=0;	
		
	foreach ($productList as $prod) {	
		if($hideproductid != "NONE") {
			$hideProductIdArray = explode(",",$hideproductid);
			if( in_array($prod->getId(), $hideProductIdArray) ) {
				continue;
			}
		}
		
		if($productid != "ALL") {
			if(strstr($productid,",")!=false) {
			  $productIdArray = explode(",",$productid);
			  if( !in_array($prod->getId(), $productIdArray) ) {
				  continue;
			  }
			}
			else {
				$prodid=$productid;
				if( $prod->getId() != $prodid ) {
				  //logToFile("DAP-ShortCodes-MemberLinks: prodid=" . $prodid . "  prodid=".$userProduct->getProduct_id());
				  continue;
				}
			}
			logToFile("DAP-ShortCodes-MemberLinks: prodid=".$prod->getId());
		}
		
		$product = Dap_Product::loadProduct($prod->getId());
	//	echo "<pre>"; print_r($product); 	echo "</pre>";die;  

		if(!isset($product)) {
			logToFile("DAP-ShortCodes-MemberLinks: no product found");
			return $content;
		}
		
		logToFile("DAP-ShortCodes-MemberLinks: product found");
		
		//$dripType=$product->getDrip_type();
		  
		$dripType=1;
		
		if($dripType==2)  {
			logToFile("DAP-ShortCodes-MemberLinks: can't process conditional drip products");
			continue;
		}
		
		$productId=$product->getId();
		logToFile("DAP-ShortCodes-MemberLinks:  product(id=$productId) found");
		logToFile("DAP-ShortCodes-MemberLinks:  user(id=$userId) found");
		
		$expired = false;
		if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		}else{
			if($user->hasAccessTo($product->getId()) === false) {
				$expired = true;
			}
		}
		//access start date
		$oldDate = $userProduct_alldate[$prod->getId()]['access_start_date'];

		//$middle = strtotime($oldDate);
		$middle = new DateTime($oldDate);
		$stringFormat = "";
		if($dateformat == "MM-DD-YYYY") {
			$stringFormat = "m-d-Y";
		} else if($dateformat == "DD-MM-YYYY") {
			$stringFormat = "d-m-Y";
		}  else if($dateformat == "YYYY-MM-DD") {
			$stringFormat = "Y-m-d";
		}
		//$newDate = date($stringFormat, $middle);
		$newStartDate = $middle->format($stringFormat);

		//access start date
		$oldDate = $userProduct_alldate[$prod->getId()]['access_end_date'];
		
		//$middle = strtotime($oldDate);
		$middle = new DateTime($oldDate);
		$stringFormat = "";
		if($dateformat == "MM-DD-YYYY") {
			$stringFormat = "m-d-Y";
		} else if($dateformat == "DD-MM-YYYY") {
			$stringFormat = "d-m-Y";
		}  else if($dateformat == "YYYY-MM-DD") {
			$stringFormat = "Y-m-d";
		}
		//$newDate = date($stringFormat, $middle);
		$newEndDate = $middle->format($stringFormat);

 
		$highlightCode = "";
		if($expired) {
		  $highlightCode = ' class="dap_highlight_bg" ';
		}
		
		$post_cancel_access = Dap_Config::get("POST_CANCEL_ACCESS");
		if(isset($post_cancel_access)) {
			$post_cancel_access = strtolower($post_cancel_access);
		}		
			
		logToFile("DAP-ShortCodes-MemberLinks: user's access has expired to $productId.. check... renewal html");
		
		if($dripType==2) {
			continue;
		}
		else if(strtolower($showproductimage)=="y") {
			$ind_prod_content = $regulardrip_prod_temp_content;
			
		}
		else {
			$ind_prod_content = $regulardrip_prod_noimage_temp_content;
				
		}  
		
		if( $expired && ($showrenewalhtml == "Y") ) {
			//If user's access to product has expired, then show renewal HTML
			//$content .= '<div>'.$product->getRenewal_html().'</div>';
			$ind_prod_content = str_replace( '[RENEWAL_HTML]',$product->getRenewal_html(), $ind_prod_content);
			$ind_prod_content = str_replace( '[RENEWALHTMLVISIBILITY]',"block", $ind_prod_content); 
			
			logToFile("DAP-ShortCodes-MemberLinks: user's access has expired.. show renewal html");
			
		}
		else {
			$ind_prod_content = str_replace( '[RENEWAL_HTML]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[RENEWALHTMLVISIBILITY]',"none", $ind_prod_content); 
		}
		
		
		logToFile("DAP-ShortCodes-MemberLinks: before showlinks: ".strtolower($showlinks));
		//$dripType=$product->getDrip_type();
		
		
		if(strtolower($showproductimage)=="y") {
			
			$prod_image = $product->getProduct_image_url();
			 			
			if($prod_image==""){
				//$prod_image="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRCI53HWVRvF_uRmJxVDLfgKuOLjksNq3xXbRMv8IfbJ7t56Uf8";
				$prod_image=  plugins_url()."/DAP-WP-LiveLinks/includes/images/no-image.jpg";
				
			}	
			$ind_prod_content = str_replace( '[PRODUCT_IMAGE]',$prod_image, $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCTIMGVISIBILITY]',"block", $ind_prod_content); 	
		}
		else {
			$ind_prod_content = str_replace( '[PRODUCTIMGVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_IMAGE]',"", $ind_prod_content); 
		}
		
		if(strtolower($showproductname)=="y") {
			$ind_prod_content = str_replace( '[PRODUCT_NAME]',$product->getName(), $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCTNAMEVISIBILITY]',"block", $ind_prod_content); 
		}
		else {
			$ind_prod_content = str_replace( '[PRODUCTNAMEVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_NAME]',"", $ind_prod_content); 
		}

		if(strtolower($showdescription)=="y") {
			$ind_prod_content = str_replace( '[PRODUCT_DESCRIPTION]',$product->getDescription(), $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCTDESCRIPTIONVISIBILITY]',"block", $ind_prod_content); 	
		}
		else {
			$ind_prod_content = str_replace( '[PRODUCTDESCRIPTIONVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_DESCRIPTION]',"", $ind_prod_content); 
			
		}
		if(strtolower($showaccessstartdate)=="y") {
			$access_start_date_label="Access Start Date";
			if(defined("USER_LINKS_ACCESS_START_DATE_TEXT")) {
				$access_start_date_label=USER_LINKS_ACCESS_START_DATE_TEXT;
			}
			$ind_prod_content = str_replace( '[ACCESS_START_DATE]',$access_start_date_label, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ASD]',$newStartDate, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSSTARTVISIBILITY]',"block", $ind_prod_content); 
			$ind_prod_content = str_replace( '[ASDLABEL]',USER_LINKS_ACCESS_START_DATE_TEXT, $ind_prod_content); 
		}
		else {
			$access_start_date_label="Access Start Date";
			if(defined("USER_LINKS_ACCESS_START_DATE_TEXT")) {
				$access_start_date_label=USER_LINKS_ACCESS_START_DATE_TEXT;
			}
			$ind_prod_content = str_replace( '[ACCESS_START_DATE]',$access_start_date_label, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ASD]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSSTARTVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[ASDLABEL]',"", $ind_prod_content);
		}
		
		if(strtolower($showaccessenddate)=="y") {
			$access_end_date_label="Access End Date";
			if(defined("USER_LINKS_ACCESS_END_DATE_TEXT")) {
				$access_end_date_label=USER_LINKS_ACCESS_END_DATE_TEXT;
			}
			$ind_prod_content = str_replace( '[AEDLABEL]',$access_end_date_label, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESS_END_DATE]',$access_end_date_label, $ind_prod_content); 
			$ind_prod_content = str_replace( '[AED]',$newEndDate, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSENDVISIBILITY]',"block", $ind_prod_content); 
		}
		else {
			$access_start_date_label="Access End Date";
			if(defined("USER_LINKS_ACCESS_END_DATE_TEXT")) {
				$access_start_date_label=USER_LINKS_ACCESS_END_DATE_TEXT;
			}
			$ind_prod_content = str_replace( '[ACCESS_END_DATE]',$access_start_date_label, $ind_prod_content); 
			$ind_prod_content = str_replace( '[AED]',$newEndDate, $ind_prod_content); 
			$ind_prod_content = str_replace( '[ACCESSENDVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[AEDLABEL]',"", $ind_prod_content); 
		}
	
		$ind_prod_content = str_replace( '[PRODUCTID]',$product->getId(), $ind_prod_content); 
				
		//echo $logged_in_url = $userProduct->getlogged_in_url();
		//echo $sales_page_url = $userProduct->getSales_page_url();
		
		
	/*************** Condition for display options starts ******************/	
		if( !Dap_Session::isLoggedIn() || !isset($user) ) {
		
			if($product->getsales_page_url() == ""){
					$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#", $ind_prod_content);
					$ind_prod_content = str_replace( '[PRODUCT_CLASS]',"product_container_grey", $ind_prod_content);
					$ind_prod_content = str_replace( '[TARGET]','', $ind_prod_content);
					$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
					//$ind_prod_content = str_replace( '[HREFCONTENT]','#', $ind_prod_content);
					
			}else{	
					$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',$product->getsales_page_url(), $ind_prod_content);
					$ind_prod_content = str_replace( '[PRODUCT_CLASS]',"product_container_grey", $ind_prod_content);
					$ind_prod_content = str_replace( '[TARGET]','target="_blank"', $ind_prod_content);
					$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
					//$ind_prod_content = str_replace( '[HREFCONTENT]',"javascript:showHideContentDIV([PRODUCTID],'block');", $ind_prod_content);
					
			}		
		}else{
			if(in_array($prod->getId(), $userProduct_ids)){
				
				if($product->getlogged_in_url() == ""){
					
						$ind_prod_content = str_replace( '[PRODUCT_CLASS]',"product_container_white", $ind_prod_content);
						$ind_prod_content = str_replace( '[TARGET]','', $ind_prod_content);
						if($displayoption == "showProductPage"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#", $ind_prod_content);
						}elseif($displayoption == "showPopup"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','fancybox fancybox_box click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#mainswitch_".$prod->getId(), $ind_prod_content);
						}else{
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button',$ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"javascript:showHideContentDIV(".$prod->getId().",'block');", $ind_prod_content);
						}
						
					 	
				}else{		
 
					$ind_prod_content = str_replace( '[PRODUCT_CLASS]',"product_container_white", $ind_prod_content);
					
					if($displayoption == "showProductPage"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',$product->getlogged_in_url(), $ind_prod_content);
							$ind_prod_content = str_replace( '[TARGET]','target="_blank"', $ind_prod_content);
						}elseif($displayoption == "showPopup"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','fancybox  fancybox_box click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#mainswitch_".$prod->getId(), $ind_prod_content);
							$ind_prod_content = str_replace( '[TARGET]','', $ind_prod_content);
						}else{
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button',$ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"javascript:showHideContentDIV(".$prod->getId().",'block');", $ind_prod_content);
							$ind_prod_content = str_replace( '[TARGET]','', $ind_prod_content);
						}
				}
			
			}else{
					if($product->getsales_page_url() == ""){
						
						$ind_prod_content = str_replace( '[PRODUCT_CLASS]',"product_container_grey", $ind_prod_content);
						$ind_prod_content = str_replace( '[TARGET]','', $ind_prod_content);
						$ind_prod_content = str_replace( '[LINK_CLASS]','click_button', $ind_prod_content);
						if($displayoption == "showProductPage"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#", $ind_prod_content);
						}elseif($displayoption == "showPopup"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#", $ind_prod_content);
						}else{
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button',$ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',"#", $ind_prod_content);
						}	
					}else{	
						
						$ind_prod_content = str_replace( '[PRODUCT_CLASS]',"product_container_grey", $ind_prod_content);
						$ind_prod_content = str_replace( '[TARGET]','target="_blank"', $ind_prod_content);
						if($displayoption == "showProductPage"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',$product->getsales_page_url(), $ind_prod_content);
						}elseif($displayoption == "showPopup"){
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button', $ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',$product->getsales_page_url(), $ind_prod_content);
						}else{
							$ind_prod_content = str_replace(  '[LINK_CLASS]','click_button',$ind_prod_content);
							$ind_prod_content = str_replace( '[LINK_SALES_POST_LINK]',$product->getsales_page_url(), $ind_prod_content);
						}
						
					}					
			}
		}
			
		
		/*************** Condition for display options ends ******************/	
		
		$course_content="";

		if(strtolower($showlinks) == "y") {
		 	
			if( !Dap_Session::isLoggedIn() || !isset($user) ) {
				if( (strtolower($showavailablecontent) == "n") && (strtolower($showupcomingcontent) == "n")) {
					$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
					$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 
					//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content);	
				}
				//if($displayoption == "showInpage"){
					if(strtolower($showlinkscollapsed) == "y") {
						$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
						$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 
						//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"block", $ind_prod_content); 
						logToFile("DAP-ShortCodes-MemberLinks: showlinkscollapsed: ".strtolower($showlinkscollapsed));
					}
					else{
						
						//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
						$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
						$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 
					}
				//}

			}else{	
			
				if(in_array($prod->getId(), $userProduct_ids)){
					if( (strtolower($showavailablecontent) == "n") && (strtolower($showupcomingcontent) == "n")) {
						$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
						$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretext, $ind_prod_content); 
						//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content);	
					}
					if($displayoption == "showInpage"){
						if(strtolower($showlinkscollapsed) == "y") {
							$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
							$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretext, $ind_prod_content); 
							$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"block", $ind_prod_content); 
							logToFile("DAP-ShortCodes-MemberLinks: showlinkscollapsed: ".strtolower($showlinkscollapsed));
						}
						else{
							$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
							$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"block", $ind_prod_content); 	
							$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretext, $ind_prod_content); 
						}
					}else{
						if(strtolower($showlinkscollapsed) == "y") {
							$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"block", $ind_prod_content); 	
							$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretext, $ind_prod_content); 
							//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"block", $ind_prod_content); 
							logToFile("DAP-ShortCodes-MemberLinks: showlinkscollapsed: ".strtolower($showlinkscollapsed));
						}
						else{
							
							//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"block", $ind_prod_content); 
							$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"block", $ind_prod_content); 	
							$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretext, $ind_prod_content); 
						}
					}

				}else{

						if( (strtolower($showavailablecontent) == "n") && (strtolower($showupcomingcontent) == "n")) {
								$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
								$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 
								//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content);	
							}
							if(strtolower($showlinkscollapsed) == "y") {
								$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
								$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content);  
								//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"block", $ind_prod_content); 
								logToFile("DAP-ShortCodes-MemberLinks: showlinkscollapsed: ".strtolower($showlinkscollapsed));
							}
							else{
								
								//$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
								$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"block", $ind_prod_content); 	
								$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 
							}

				}
			}			
		
		}
		else {
							
			if( !Dap_Session::isLoggedIn() || !isset($user) ) {
				
					$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 

			}else{	
				
				if(in_array($prod->getId(), $userProduct_ids)){
					$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretext, $ind_prod_content); 
				}else{
					$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',$clickheretextunavilable, $ind_prod_content); 
				}
				
			}			
			$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
			$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"none", $ind_prod_content);
			$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 	
			$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content);

			$ind_prod_content = str_replace( '[UPCOMING_CONTENT]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[UPCOMING_CONTENT_HEADER]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[UPCOMING_IN_DAYS_HEADER]',"", $ind_prod_content);
				
			$ind_prod_content = str_replace( '[AVAILABLE_CONTENT]',"", $ind_prod_content); 	
			$ind_prod_content = str_replace( '[AVAILABLE_CONTENT_HEADER]',"", $ind_prod_content); 	
				
		}
		
		
		//echo $ind_prod_content;
		if(strtolower($showlinks) == "y") {
			$orderBy = "desc";
			
			if(strtolower($orderoflinks) == "oldestfirst") {
			  $orderBy = "asc";
			}
			$sss = $product->getSelf_service_allowed();
			
			//get currently available content
			
						
			if( (strtolower($showavailablecontent) == "y") || (strtolower($showupcomingcontent) == "y")) {
				$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 
				$ind_prod_content = str_replace( '[PRODUCT_LINKS_HREF]',"", $ind_prod_content); 
			
				$ind_prod_content = str_replace( '[LINKS]',$mycontentheadertext, $ind_prod_content); 
				$ind_prod_content = str_replace( '[STATUS]',$statusheader, $ind_prod_content); 
			
				if(strtolower($showavailablecontent) == "y") {
					$ind_prod_content = str_replace( '[AVAILABLE_CONTENT_HEADER]',$availcontentheadertext, $ind_prod_content); 
					$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"block", $ind_prod_content); 
					$course_content="";
					
					//$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
					if ($product->getSelf_service_allowed() == "N") { 
					//	$resources=$userProduct->getActiveResources($product->getSelf_service_allowed(),$orderBy,$howmanylinks,"ARRAY");
						$resources=getActiveRes($userId,$productId,$product->getSelf_service_allowed(),$orderBy,$howmanylinks,"ARRAY");
					}
					else {
						// $resources=$userProduct->getActiveResources("Y",$orderBy,$howmanylinks,"ARRAY");
						$resources=getActiveRes($userId,$productId,"Y",$orderBy,$howmanylinks,"ARRAY");
					}
					     
					$contentFound=false;
					if(isset($resources)) {
						$drip_content="";
						foreach ($resources as $resource) {
							logToFile("DAP-ShortCodes-MemberLinks: course_content: ".$course_content);
							$drip_content .=  getRegularDripAndSSSResources($resource,$userId,$productId,$sss,$post_cancel_access,$regulardrip_temp_content); 
							if($drip_content!="")
								$contentFound=true;
						}
					}
					if($contentFound==false) {
						logToFile("DAP-ShortCodes-MemberLinks: nocontenttext: ".$nocontenttext);
						$drip_content="<tr><td>$nocontenttext</td></tr>";
					}
					$ind_prod_content = str_replace( '[AVAILABLE_CONTENT]',$drip_content, $ind_prod_content); 	
					
				} //showavailablecontent==Y
				else {
					$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"none", $ind_prod_content); 
				}
				
				if( strtolower($showupcomingcontent) == "y") {
				  	$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"block", $ind_prod_content); 
					$ind_prod_content = str_replace( '[UPCOMING_CONTENT_HEADER]',$upcomingcontentheadertext, $ind_prod_content); 
					$ind_prod_content = str_replace( '[UPCOMING_IN_DAYS_HEADER]',$comingsoonheadertext, $ind_prod_content);
					
					$upcomingLaterResources=getFutureRes($userId,$productId,"N",$orderBy,$howmanyupcominglinks,"Y","ARRAY");
					$upcoming_regular_drip_content="";
					$contentFound=false;
					if(isset($upcomingLaterResources)) {
						foreach ($upcomingLaterResources as $resource) {
							logToFile("DAP-ShortCodes-MemberLinks: get upcoming regular drip resources: ". $upcoming_regulardrip_temp_content);
							$upcoming_regular_drip_content .= getRegularDripAndSSSFutureResources($resource,$userId,$productId,$sss,$post_cancel_access,$upcoming_regulardrip_temp_content); 
							if($upcoming_regular_drip_content!="")
								$contentFound=true;
						}
					}
					if($contentFound==false) {
						logToFile("DAP-ShortCodes-MemberLinks: upcoming nocontenttext: ".$nocontenttext);
						$upcoming_regular_drip_content="<tr><td>$nocontenttext</td></tr>";
					}
					
					$ind_prod_content = str_replace( '[UPCOMING_CONTENT]',$upcoming_regular_drip_content, $ind_prod_content); 
					
					
				}
				else {
					$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
					
				}
			} //if( (strtolower($showavailablecontent) == "y") || (strtolower($showupcomingcontent) == "y")) {
			else {
			//hidelinks
			  $ind_prod_content = str_replace( '[LINKS]',"", $ind_prod_content); 
			  $ind_prod_content = str_replace( '[STATUS]',"", $ind_prod_content); 
			  $ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
			  $ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"none", $ind_prod_content);
			  $ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 
			  $ind_prod_content = str_replace( '[PRODUCT_LINKS_HREF]',"", $ind_prod_content); 
			  $ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
			  $ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',"", $ind_prod_content); 
			}
		} //showlinks
		else {

			//hidelinks
			$ind_prod_content = str_replace( '[LINKS]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[STATUS]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[UPCOMINGCONTENTVISIBILITY]',"none", $ind_prod_content); 	
			$ind_prod_content = str_replace( '[AVAILABLECONTENTVISIBILITY]',"none", $ind_prod_content);
			$ind_prod_content = str_replace( '[PRODUCTLINKSVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_LINKS_HREF]',"", $ind_prod_content); 
			$ind_prod_content = str_replace( '[LINKSTEXTVISIBILITY]',"none", $ind_prod_content); 
			$ind_prod_content = str_replace( '[PRODUCT_LINKS_TEXT]',"", $ind_prod_content); 
		}
		if ($i == (count($userProducts)-1)){
			if($showunavailableproducts == "Y"){
				$ind_prod_content.='</div><div class="inactive_prod_div product_div"><h5 class="separater_line"><span>You might also be interested in these products....</span></h5>';
			}	
		}
		$i++;
		$productAll.=$ind_prod_content;
			   
	} //end foreach  
 
	//$content .= '<link rel="stylesheet" href="'. $sidebarcsspath  . '"/>';
	//$content .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>';
	$content .= '<link rel="stylesheet" href="'. $cssurl  . '"/>';
	
	$content .= ' <script type="text/javascript">

	var $jq = jQuery.noConflict();

	$jq(function(){

		$jq(".matchheight").matchHeight();$jq(".matchheight1").matchHeight();$jq(".matchheight2").matchHeight();

	}); 

	$jq(document).ready(function(){


		$jq.fn.dataTable.ext.errMode = "none"; 

		$jq(".report_Table").on("error.dt", function ( e, settings, techNote, message ) {

		console.log( "An error has been reported by DataTables: ", message );

		}).DataTable();

		$jq(".report_table").DataTable({bSort: false});

	
		$jq(".fancybox").fancybox({"arrows" : false });

	});

	$jq(document).ready(function(){

		var prod_parent_width = $jq(".product_outer").parent().innerWidth();

		var prod_width_child = $jq(".product_div .product-box-item").innerWidth();;

		var number_of_box = parseInt(prod_parent_width / prod_width_child);

		var box_outer_width = parseInt(prod_width_child * number_of_box);

	/*	var getmargin_right = $jq(".product-box-item").css("margin-right");
		var getmargin_left = $jq(".product-box-item").css("margin-left");
		var res_margin_right = getmargin_right.split("px");
		var res_margin_left = getmargin_left.split("px");
		var marginright = parseInt(res_margin_right[0]);
		var marginleft = parseInt(res_margin_left[0]);
		var finalres_margin = parseInt(marginright + marginleft);*/
		

		var  border_width = parseInt(2 * number_of_box);

		var new_number_of_box = parseInt(number_of_box); 

		var getmargin = parseInt(20 * new_number_of_box);

		var getmargin_final = parseInt(getmargin + border_width);

		var oneboxwidth = parseInt(box_outer_width + getmargin_final);

		

		$jq("#productoutercontainter .product_div").css("width", oneboxwidth);



	});

</script>'; 

	$desired_width = '300';
	$desired_height = '300';
	$desired_boxwidth = '700';
	if($imagewidth > 300){
		$imagewidth = $desired_width;
	}
	if($imageheight > 300){
		$imageheight = $desired_height;
	}
	/**** template condtion*******/		
	if($template == "template1"){
		/****imagewidth*******/
		if(strtolower($showproductimage)=="y") {
			if($imagewidth == 0 || $imagewidth == "undefined" || $imagewidth == '' ){
				$content .= '<style>.product-image{width :'.$desired_width.'px !important}</style>'; 
				$content .= '<style>.product-item-details {width: -moz-calc(100% - '.$desired_width.'px);width: -webkit-calc(100% - '.$desired_width.'px);width: -o-calc(100% - '.$desired_width.'px);width: calc(100% - '.$desired_width.'px);}</style>';
			}elseif($imagewidth > $boxwidth ){	
			
				$content .= '<style>.product-image{width :'.$boxwidth.'px !important}</style>'; 
				$content .= '<style>.product-item-details {width: -moz-calc(100% - '.$boxwidth.'px);width: -webkit-calc(100% - '.$boxwidth.'px);width: -o-calc(100% - '.$boxwidth.'px);width: calc(100% - '.$boxwidth.'px);}</style>';
				
			}else{
				$content .= '<style>.product-image {width :'.$imagewidth.'px !important}</style>'; 
                $content .= '<style>.product-item-details {width: -moz-calc(100% - '.$imagewidth.'px);width: -webkit-calc(100% - '.$imagewidth.'px);width: -o-calc(100% - '.$imagewidth.'px);width: calc(100% - '.$imagewidth.'px);}</style>';				
			}
			/****imageheight*******/
			if($imageheight == 0 || $imageheight == "undefined" || $imageheight == '' ){
				$content .= '<style>.product-image {height :'.$desired_height.'px !important}</style>'; 	
			
			}else{
				$content .= '<style>.product-image {height :'.$imagewidth.'px !important}</style>'; 			
			}
			/****boxwidth*******/ 
			if($boxwidth == 0 || $boxwidth == "undefined" || $boxwidth == ''){
				$content .= '<style>.product-box-item{width :700px !important}</style>';             
			}elseif($boxwidth < 605 && $boxwidth > 310){

				 $content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';  
				 $content .= '<style>.product-box-item .product-item-details{width :100% !important;}.product-box-item {margin-right: 15px;}.product-item-details{padding: 5px !important;}.product-box-item .product-image-outer {width: 100%;}.product-box-item .click_button {bottom: 10px;left: 10px;margin: 0;position: absolute;right: 10px;float:left;width:100%;}.product-box-item .product-item-details {position: static !important;}.product-box-item {position: relative;}.product-item-details {text-align: center;}.product-image {display: inline-block;margin: 0 auto;}.product-box-item .product-image-outer {float: left;text-align: center;width: 100%;}.product-box-item .product-item-details .click_button {left: 15px;margin: 0 auto;max-width: 220px;right: 15px;width: auto;} .product-box-item  .click_button{ margin-bottom: 10px !important;}</style>'; 
   
			 }elseif($boxwidth < $imagewidth || $boxwidth <= 310){
				$content .= '<style>.product-image{max-width: 100% !important;}</style>'; 
				$content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';  
				$content .= '<style>.product-box-item .product-item-details{width :100% !important;}.product-box-item {margin-right: 15px;}.product-item-details{padding: 5px !important;}.product-box-item .product-image-outer {width: 100%;}.product-box-item .product-item-details {position: static !important;}.product-box-item {position: relative;}.product-item-details {text-align: center;}.product-image {display: inline-block;margin: 0 auto;}.product-box-item .product-image-outer {float: left;text-align: center;width: 100%;}.product-box-item .product-item-details .click_button {left: 15px;margin: 0 auto;max-width: 220px;right: 15px;width: auto;} .product-box-item  .click_button{ margin-bottom: 10px !important;}</style>'; 
				$content .= '<style>.product-image {height: auto !important;}.product-box-item .product-image img {position: static;}</style>'; 	
			}else{
				   $content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';                                                
			}
			
		}else{
			
			if($imagewidth == 0 || $imagewidth == "undefined" || $imagewidth == '' ){
				$content .= '<style>.product-image{width :'.$desired_width.'px !important}</style>'; 
				$content .= '<style>.product-item-details {width: -moz-calc(100%);width: -webkit-calc(100%);width: -o-calc(100%);width: calc(100%);}</style>';
			}elseif($imagewidth > $boxwidth ){	
			
				$content .= '<style>.product-image{width :'.$boxwidth.'px !important}</style>'; 
				$content .= '<style>.product-item-details {width: -moz-calc(100%);width: -webkit-calc(100%);width: -o-calc(100%);width: calc(100% );}</style>';
				
			}else{
				$content .= '<style>.product-image {width :'.$imagewidth.'px !important}</style>'; 
                $content .= '<style>.product-item-details {width: -moz-calc(100%);width: -webkit-calc(100%);width: -o-calc(100%);width: calc(100%);}</style>';				
			}
			/****imageheight*******/
			if($imageheight == 0 || $imageheight == "undefined" || $imageheight == '' ){
				$content .= '<style>.product-image {height :'.$desired_height.'px !important}</style>'; 	
			
			}else{
				$content .= '<style>.product-image {height :'.$imagewidth.'px !important}</style>'; 			
			}
			/****boxwidth*******/ 
			if($boxwidth == 0 || $boxwidth == "undefined" || $boxwidth == ''){
				$content .= '<style>.product-box-item{width :700px !important}</style>';             
			}elseif($boxwidth < 605 && $boxwidth > 310){

				 $content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';  
				 $content .= '<style>.product-box-item .product-item-details{width :100% !important;}.product-box-item {margin-right: 15px;}.product-item-details{padding: 5px !important;}.product-box-item .product-image-outer {width: 100%;}.product-box-item .click_button {bottom: 10px;left: 10px;margin: 0;position: absolute;right: 10px;float:left;width:100%;}.product-box-item .product-item-details {position: static !important;}.product-box-item {position: relative;}.product-item-details {text-align: center;}.product-image {display: inline-block;margin: 0 auto;}.product-box-item .product-image-outer {float: left;text-align: center;width: 100%;}.product-box-item .product-item-details .click_button {left: 15px;margin: 0 auto;max-width: 220px;right: 15px;width: auto;} .product-box-item  .click_button{ margin-bottom: 10px !important;}</style>'; 
   
			 }elseif($boxwidth < $imagewidth || $boxwidth <= 310){
				$content .= '<style>.product-image{max-width: 100% !important;}</style>'; 
				$content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';  
				$content .= '<style>.product-box-item .product-item-details{width :100% !important;}.product-box-item {margin-right: 15px;}.product-item-details{padding: 5px !important;}.product-box-item .product-image-outer {width: 100%;}.product-box-item .product-item-details {position: static !important;}.product-box-item {position: relative;}.product-item-details {text-align: center;}.product-image {display: inline-block;margin: 0 auto;}.product-box-item .product-image-outer {float: left;text-align: center;width: 100%;}.product-box-item .product-item-details .click_button {left: 15px;margin: 0 auto;max-width: 220px;right: 15px;width: auto;} .product-box-item  .click_button{ margin-bottom: 10px !important;} </style>'; 
				$content .= '<style>.product-image {height: auto !important;}.product-box-item .product-image img {position: static;}</style>'; 	
			}else{
				   $content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';                                                
			}
			
		}
	}elseif($template == "template2"){
		/****imagewidth*******/
		if($imagewidth == 0 || $imagewidth == "undefined" || $imagewidth == '' ){
			$content .= '<style>.product-image{width :'.$desired_width.'px !important}</style>'; 
		}else{
			$content .= '<style>.product-image {width :'.$imagewidth.'px !important}</style>'; 			
		}
		/****imageheight*******/
		if($imageheight == 0 || $imageheight == "undefined" || $imageheight == '' ){
			$content .= '<style>.product-image {height :'.$desired_height.'px !important}</style>'; 	
		
		}else{
			$content .= '<style>.product-image {height :'.$imagewidth.'px !important}</style>'; 			
		}
		/****boxwidth*******/
		$boxwidth = $imagewidth;
		if($boxwidth == 0 || $boxwidth == "undefined" || $boxwidth == ''){
			$content .= '<style>.product-box-item{width :'.$desired_width.'px !important}</style>';
		}else{
			 $content .= '<style>.product-box-item{width :'.$boxwidth.'px !important}</style>';  
		}		
	}

	 $content .= '<style>.product_container_white .click_button {background :'.$availablebutton.' !important}</style>';
	 $content .= '<style>.product_container_grey .click_button {background :'.$unavailablebutton.' !important}</style>'; 		
	 $content .= '<style>.paginate_button.previous, .paginate_button.next, .paginate_button{background :'.$paginationbutton.' !important}</style>';


		if($sidebarorfullwidth == "sidebar"){
			$content .=  '<style>.product_div .product-box-item{ float: left; text-align: left;} .product_div {margin: 0 auto;  float: left;  max-width: 100%; } </style>';
		}
		else{ 
			$content .=  '<style>.product_div .product-box-item{ float: left;}.product_div {margin: 0 auto;  float: none;  max-width: 100%;}  </style>';
		}
		
	 
	$content .=  '<div class="product_outer" id="productoutercontainter" style="float:left; width:100%;">';
	$regulardrip_head_temp_content = str_replace( '[PRODUCTS]', $productAll, $regulardrip_head_temp_content); 	
	$content .= $regulardrip_head_temp_content;	
	$content .=  '</div>';
	
	return $content;
	
}

 /******* get template  ********/    
function getDripTemplateContent($template_path, $courseTemplateName, $customCourseTemplateName)
{
	if(file_exists($template_path.$customCourseTemplateName)) {
	  $course_temp_content = file_get_contents($template_path.$customCourseTemplateName);
	}
	
	if($course_temp_content == "") {
		if(file_exists($template_path.$courseTemplateName)) {
			$course_temp_content = file_get_contents($template_path.$courseTemplateName);
		}
		else{
			$course_temp_content = "";
		}
	  
		logToFile("DAP-ShortCodes-Transactions: no custom template found=".$template_path.$courseTemplateName,LOG_INFO_DAP);
	} else {
		logToFile("DAP-ShortCodes-Transactions: custom template found=".$template_path.$customCourseTemplateName,LOG_INFO_DAP);  
	}	
	
	return $course_temp_content;
	
}

 /******* include js and css ********/
function mydap_content_js(){
	
	wp_enqueue_script(jQuery);
	wp_enqueue_script('jquery.matchHeight-min', DAPMYCONTENT_URL.'/includes/js/jquery.matchHeight-min.js', array('jquery'));
	wp_enqueue_script('jquery.fancybox-latest', DAPMYCONTENT_URL.'/includes/js/jquery.fancybox.js', array('jquery'));  
	wp_enqueue_script('jquery.dataTables.min', DAPMYCONTENT_URL.'/includes/js/jquery.dataTables.min.js', array('jquery'));
	
	wp_enqueue_style('jquery.fancybox.min', DAPMYCONTENT_URL.'/includes/css/jquery.fancybox.css');
	wp_enqueue_style('jquery.dataTables.min', DAPMYCONTENT_URL.'/includes/css/dataTables.bootstrap.min.css');
	
}
//add_action('wp_footer','mydap_content_js');
?>