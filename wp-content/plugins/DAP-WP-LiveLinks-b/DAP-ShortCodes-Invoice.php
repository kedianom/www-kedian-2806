<?php
add_action('media_buttons_context','add_myinvoice_tinymce_media_button');
function add_myinvoice_tinymce_media_button($context){

return $context.=__("
<a href=\"#TB_inline?width=480&inlineId=myinvoice_shortcode_popup&width=640&height=513\"
 class=\"button thickbox\" id=\"myinvoice_shortcode_popup_button\" title=\"Generate Invoice Shortcode\"  style=\"display:none;\">DAP Member Invoice</a>");
}

add_action('admin_footer','myinvoice_shortcode_media_button_popup');

function myinvoice_shortcode_media_button_popup(){
	?>
	<style>
	#myinvoicescbtn{
    font-size: 16px !important;
    height: auto !important;
    line-height: normal !important;
    margin: 20px 0 60px;
    padding: 10px 15px !important;
}
.live-links-outer select, .live-links-outer input, .live-links-outer textarea {
  float: left;
  width: 100% !important;
}
h2.head_memb{margin :10px 0px;font-size:18px;float: left;font-weight: bold;}
	</style>
	<div id="myinvoice_shortcode_popup" style="display:none;"  class="popup_div_outer">
	 <div class="live-links-outer" id="myinvoice_shortcode_popup_inner">  
 <h2 class="head_memb">Membership Options</h2>
	 <table width="100%" cellpadding="2" cellspacing="2" class="table_page_select">
		<tr>
			<td valign="top"> Select a Membership page </td>
			<td valign="top" width="50%">
				<select  name="selectmemberpages" id="selectmemberpages">
					<option value="Select" >--Select--</option>
					<option value="DAPMyContent">My Content</option>
					<option value="DAPMyProfile" >My Profile</option>
					<option value="DAPAffiliateInfo">Affiliate Info & Stats</option>
					<option value="DAPMemberInvoice" selected="selected">Member Invoice</option>
					<option value="DAPMemberCancel">Member Cancellation</option>
					<option value="DAPLogin">Member Login</option>
				</select><br />
			</td>
		</tr>
	</table>  	 
	<!-- <h2>Generate Invoice Shortcode</h2>-->
	  <form method="post" action="" enctype="multipart/form-data">
	  <table width="100%" cellpadding="2" cellspacing="2">
    
    <tr>
    <td valign="top">
    <strong>Select a Page Template
    </td>
    <td valign="top" width="50%">
       <select  name="ptemplate" id="ptemplate">
         <option value="template1">Template1</option>
       </select><br /></strong> 
    </td>
    </tr>
	<tr><td></td></tr>
	 <tr>
    <td valign="top">
    <strong>Select an Invoice Template
    </td>
    <td valign="top" width="50%">
       <select  name="itemplate" id="itemplate">
         <option value="template1">Template1</option>
		 <option value="template2">Template2</option>
       </select><br /></strong> 
    </td>
    </tr>
	
	<tr>
    <td valign="top">
    <strong>Show a "View Invoice" Link
    </td>
    <td valign="top" width="50%">
	<select name="showinvoice" id="showinvoice">
	<option value="Y">Yes</option>
	<option value="N">No</option>
	</select>
    </strong> 
    </td>
    </tr>
	
	<tr>
    <td valign="top">
    <strong>Enter Admin Email
    </td>
    <td valign="top" width="50%">
      <input type="text" name="adminemail" id="adminemail" value=""></strong> 
    </td>
    </tr>
	 
	 <tr>
    <td valign="top">
    <strong>Enter Company Name
    </td>
    <td valign="top" width="50%">
      <input type="text" name="companyname" id="companyname" value=""></strong> 
    </td>
    </tr>
	
	<tr>
    <td valign="top">
    <strong>Enter Company URL
    </td>
    <td valign="top" width="50%">
      <input type="text" name="companyurl" id="companyurl" value=""></strong> 
    </td>
    </tr>
	
	
	<tr>
    <td valign="top">
    <strong>Enter "No Transaction Found" message
    </td>
    <td valign="top" width="50%">
      <textarea name="notranmsg" id="notranmsg" rows="3">Sorry, no transactions found</textarea></strong> 
    </td>
    </tr>
	
	<tr>
    <td valign="top">
    <strong>Enter full URL of your Logo image
    </td>
    <td valign="top" width="50%">
	 <input type="file" name="logobrowse" id="logobrowse"><strong>OR</strong>
     <label><br>Enter full URL of your logo image<br></label>
     <textarea name="logourl" id="logourl" rows="5"></textarea></strong> 
    </td>
    </tr>
	</table>
	 </form>
	      <div align="center" class="btn_outer"><button class="button-primary back-btn" id="backbtn">Back</button> 
   <button class="button-primary next-btn" id="myinvoicescbtn">Generate Shortcode</button></div>     
	 
	</div>
	
	</div>
	<?php
}

add_action('admin_footer','myinvoice_shortcode_add_shortcode_to_editor');
function myinvoice_shortcode_add_shortcode_to_editor(){
	?>
	<script>
	jQuery(document).ready(function(){
	
	jQuery('#logobrowse').change(function(){
		if(jQuery(this).val()!=""){
			jQuery('#logourl').prop("disabled",true);
		}
		else{
			jQuery('#logourl').prop("disabled",false);
		}
	});	
		
	jQuery('#myinvoicescbtn').click(function(){
		var ptemplate = jQuery('#ptemplate').val();
		var itemplate = jQuery('#itemplate').val();
		var adminemail = jQuery('#adminemail').val();
		var companyname = jQuery('#companyname').val();
		var companyurl = jQuery('#companyurl').val();
		var notranmsg = jQuery('#notranmsg').val();
		var showinvoice = jQuery('#showinvoice').val();
		var logourl = jQuery('#logourl').val();
		var logobrowse = jQuery('#logobrowse').val();
	
	    if(ptemplate == ""){
			alert("Please select a Page template");
			jQuery('#ptemplate').focus();
			return false;
		}
		if(itemplate == ""){
			alert("Please select an Invoice template");
			jQuery('#itemplate').focus();
			return false;
		} 
		if(adminemail == ""){
			alert("Please enter admin email");
			jQuery('#adminemail').focus();
			return false;
			
		}
		if(companyname == "" ){
			alert("Please enter company name");
			jQuery('#companyname').focus();
			return false;
		}
		if(showinvoice == ""){
			alert("Please select Show invoice");
			jQuery('#showinvoice').focus();
			return false;
		}
		if(logourl == "" && logobrowse == ""){
			alert("Please select a logo file or enter logo URL");
			jQuery('#logobrowse').focus();
			return false;
		}

		if(companyurl == ""){
			alert("Please Enter company URL");
			return false;
		}
	    
		if(logobrowse != "")
		{
		   var formData = new FormData();
           formData.append('action', 'live_links_invoice_logo_image_url');
           formData.append('file', jQuery('input[type=file]')[0].files[0]);
           jQuery.ajax({
               type: 'POST',
               url: ajaxurl,
               data: formData,
               cache: false,
               contentType: false,
               processData: false,
               success: function(data) {
                   logourl = data;
				   var shortcode = '[DAPShowTransactions template="'+ptemplate+'" invoicetemplate="'+itemplate+'" adminemail="'+adminemail+'" companyname="'+companyname+'" companyurl="'+companyurl+'" notranmsg="'+notranmsg+'" showinvoice="'+showinvoice+'" logo="'+logourl+'"][/DAPShowTransactions]';
		           send_to_editor(shortcode); 
		           self.parent.tb_remove();
               },
               error: function(data) {
               }
           });
		}
	
		
	});  // click
	}); // ready
	
	</script>
	<?php
}


?>