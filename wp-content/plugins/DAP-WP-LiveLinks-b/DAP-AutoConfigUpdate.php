<?php 

add_action( 'save_post', 'autoUpdateDAPConfig' );

function autoUpdateDAPConfig( $post_id ) {
	global $current_user;
	if( defined('SITEROOT') ) {
		$lldocroot = SITEROOT;
	} else {
		$lldocroot = $_SERVER['DOCUMENT_ROOT'];
	}
	include_once ($lldocroot . "/dap/dap-config.php");
	
	$content_post = get_post($post_id);
	$content = $content_post->post_content;
	
	if( (stristr($content, '%%LOGIN_FORM%%') !== FALSE) || (stristr($content, '[DAPLoginForm]') !== FALSE) ){
    	logToFile("%%LOGIN_FORM%% or [DAPLoginForm] found");
		$permalink = get_permalink( $post_id );
		//logToFile("permalink: " . $permalink); 
		$session = Dap_Session::getSession();
		
		if( is_admin() && user_can($current_user->ID, 'administrator') && isset($session) && Dap_Session::isLoggedIn() && $session->isAdmin() ) {
			Dap_Config::loadConfig();
			$currentLoginLink = Dap_Config::get("LOGIN_URL");
			//logToFile("currentLoginLink: " . $currentLoginLink);
			if($currentLoginLink == "/dap/login.php") {
				//If user is WordPress admin, and DAP admin, and is logged in...
				//Update permalink of Login URL in DAP config
				logToFile("updating config: with " . $permalink);
				Dap_Config::updateConfigName("LOGIN_URL", $permalink);
			}
		}
  	}
}

?>