<?php
/**
 * CC Image Search main functionality class
 *
 * This is the main script that adds in the plugin's main shortcode/behavior/etc.
 * Each important component has it's own class-shortcode.php file.
 *
 * For example, in Parallax, we have a class-parallax-row.php for the parallax row element,
 * and class-fullwidth-row.php for the full-width row element.
 *
 * @package CC Image Search
 */

if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

// Initializes plugin class.
if ( ! class_exists( 'CCImageSearch' ) ) {

	/**
	 * This is where all the plugin's functionality happens.
	 */
	class CCImageSearch {

		/**
		 * Hook into WordPress.
		 *
		 * @return	void
		 * @since	1.0
		 */
		function __construct() {

			// Our admin-side scripts & styles.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

			// Enqueues scripts and styles specific for all parts of the plugin.
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts_and_css' ), 99 );

			// Add the frame template used for the Media Manager search View.
			add_action( 'admin_footer', array( $this, 'include_frame_template' ) );
			add_action( 'customize_controls_print_footer_scripts', array( $this, 'include_frame_template' ) );

			// Ajax handler for downloading images.
			add_action( 'wp_ajax_ccimage_download_image', array( $this, 'ajax_download_image' ) );
		}



		/**
		 * Includes admin scripts and styles needed.
		 *
		 * @return	void
		 * @since	1.0
		 */
		public function enqueue_admin_scripts() {

			// Admin styles.
			wp_enqueue_style( __CLASS__ . '-admin', plugins_url( 'cc_image_search/css/admin.css', __FILE__ ), array(), VERSION_GAMBIT_CC_IMAGE_SEARCH );

			// Admin javascript.
			wp_enqueue_script( __CLASS__ . '-admin', plugins_url( 'cc_image_search/js/min/admin-min.js', __FILE__ ), array( 'media-views', 'jquery' ), VERSION_GAMBIT_CC_IMAGE_SEARCH );
			wp_localize_script( __CLASS__ . '-admin', 'CCImageParams', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
	            'nonce' => wp_create_nonce( __CLASS__ ),
				'admin_post_url' => admin_url( 'post.php' ),
	            'media_default_url' => includes_url( 'images/media/default.png' ),
			) );
		}


		/**
		 * Includes normal scripts and css purposed globally by the plugin.
		 * Do not use this section to initialize styles and scripts used in shortcodes! Use the shortcode section instead for that.
		 *
		 * @return	void
		 * @since	1.0
		 */
		public function enqueue_frontend_scripts_and_css() {

			if ( wp_script_is( 'media-editor', 'enqueued' ) ) {
				$this->enqueue_admin_scripts();
				add_action( 'wp_footer', array( $this, 'include_frame_template' ) );
			}
		}


		public function include_frame_template() {
			include 'search-template.php';
		}

		public function sort_by_size( $a, $b ) {
			return $a->width > $b->width;
		}

		/**
		 * @see https://codex.wordpress.org/Function_Reference/media_handle_sideload
		 */
		public function download_single_image( $url, $title ) {

			// Need to require these files.
			if ( ! function_exists( 'media_handle_upload' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
			}

			$tmp = download_url( $url );
			if ( is_wp_error( $tmp ) ) {
				// download failed, handle error
				return false;
			}

			$post_id = 0;
			$file_array = array();

			// Set variables for storage
			// fix file filename for query strings
			preg_match( '/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
			$file_array['name'] = 'ccimage-' . basename( $matches[0] );
			$file_array['tmp_name'] = $tmp;

			// If error storing temporarily, unlink
			if ( is_wp_error( $tmp ) ) {
				@unlink( $file_array['tmp_name'] );
				$file_array['tmp_name'] = '';
			}

			// do the validation and storage stuff
			$id = media_handle_sideload( $file_array, $post_id, $title );

			// If error storing permanently, unlink
			if ( is_wp_error( $id ) ) {
				@unlink( $file_array['tmp_name'] );
				return false;
			}

			return $id;
		}


		/**
		 * @see https://wiki.creativecommons.org/wiki/Best_practices_for_attribution#Examples_of_attribution
		 */
		public function form_attribution( $data ) {

			// Public domains don't need attribution.
			if ( in_array( 'zero', $data->badges ) ) {
				return '';
			}

			$title = __( 'Photo', GAMBIT_CC_IMAGE_SEARCH );
			if ( ! empty( $data->url ) ) {
				$title = sprintf( '<a href="%s" target="_blank">%s</a>', esc_url( $data->url ), __( 'Photo', GAMBIT_CC_IMAGE_SEARCH ) );
			}
			if ( ! empty( $data->title ) ) {
				$title = '"' . $data->title . '"';
				if ( ! empty( $data->url ) ) {
					$title = sprintf( '"<a href="%s" target="_blank">%s</a>"', esc_url( $data->url ), $data->title );
				}
			}

			return sprintf( __( '%s by %s is licensed under %s', GAMBIT_CC_IMAGE_SEARCH ),
				$title,
				sprintf( '<a href="%s" target="_blank">%s</a>', esc_url( $data->user_link ), $data->user ),
				sprintf( '<a href="%s" target="_blank">%s</a>', esc_url( $data->license_link ), $data->license_shortname )
			);
		}

		public function download_image() {
			$data = json_decode( stripslashes( $_POST['data'] ) );

			usort( $data->sizes, array( $this, 'sort_by_size' ) );

			$minSize = 1400;
			$numLarger = 0;
			$lastIndex = -1;
			foreach ( $data->sizes as $i => $size ) {
				if ( $data->sizes[ $i ]->width > $minSize || $data->sizes[ $i ]->height > $minSize ) {
					$numLarger++;
					if ( $lastIndex == -1 ) {
						$lastIndex = $i;
					}
				}
			}
			if ( $numLarger > 1 ) {
				while ( count( $data->sizes ) > $lastIndex + 1 ) {
					array_pop( $data->sizes );
				}
			}

			// Create the title.
			$title = sprintf( __( 'Image by %s from %s', GAMBIT_CC_IMAGE_SEARCH ), $data->user, $data->provider_name );
			if ( ! empty( $data->title ) ) {
				$title = $data->title;
			}

			// Download one of the images (try largest first).
			for ( $i = count( $data->sizes ) - 1; $i >= 0; $i-- ) {
				$post_id = $this->download_single_image( $data->sizes[ $i ]->url, $title );
				if ( $post_id ) {
					break;
				}
			}

			if ( empty( $post_id ) ) {
				return;
			}

			wp_update_post( array(
				'ID' => $post_id,
				'post_excerpt' => $this->form_attribution( $data ),
			) );

			update_post_meta( $post_id, 'ccimage', '1' );

			return $post_id;
		}

		public function generate_media_manager_data( $post_id ) {
			$post = get_post( $post_id, ARRAY_A );
			$meta_data = wp_generate_attachment_metadata( $post_id, get_attached_file( $post_id ) );

			// We need these to update the media manager.
			return array(
	            'id' => $post_id,
	            'attachment_data' => $post,
	            'sizes_data' => $meta_data,
	            'attachment_url' => wp_get_attachment_url( $post_id ),
	            'attachment_link' => get_attachment_link( $post_id ),
	            'delete_nonce' => wp_create_nonce( 'delete-post_' . $post_id ),
	            'update_nonce' => wp_create_nonce( 'update-post_' . $post_id ),
	            'edit_nonce' => wp_create_nonce( 'image_editor-' . $post_id ),
	            'compat_fields' => get_compat_media_markup( $post_id ),
	        );
		}

		public function ajax_download_image() {
	        check_ajax_referer( __CLASS__, 'nonce' );

			$post_id = $this->download_image();
			echo json_encode( $this->generate_media_manager_data( $post_id ) );
			die();
		}

		public function testing() {
			echo 'JAMI';
		}
	}

	new CCImageSearch();

}
?>
