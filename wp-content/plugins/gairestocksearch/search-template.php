<?php
/**
 * Template file for the widget picker modal popup.
 *
 * @package Page Builder Sandwich
 */

?>
<script type="text/html" id="tmpl-ccimage-search">
	<div class="media-toolbar">
		<div class="media-toolbar-secondary">
			<label for="ccimage-provider-filters" class="screen-reader-text"><?php _e( 'Filter by provider', GAMBIT_CC_IMAGE_SEARCH ) ?></label>
			<select id="ccimage-provider-filters" class="attachment-filters">
				<option value=""><?php _e( 'All image providers', GAMBIT_CC_IMAGE_SEARCH ) ?></option>
				<option value="flickr"><?php _e( 'Flickr', GAMBIT_CC_IMAGE_SEARCH ) ?></option>
				<option value="pixabay"><?php _e( 'Pixabay', GAMBIT_CC_IMAGE_SEARCH ) ?></option>
			</select>
			<label for="ccimage-license-filters" class="screen-reader-text"><?php _e( 'Filter by license', GAMBIT_CC_IMAGE_SEARCH ) ?></label>
			<select id="ccimage-license-filters" class="attachment-filters">
				<option value=""><?php _e( 'Licenses that allow commercial use', GAMBIT_CC_IMAGE_SEARCH ) ?></option>
				<option value="noncommercial"><?php _e( 'Include noncommercial licenses', GAMBIT_CC_IMAGE_SEARCH ) ?></option>
				<option value="noattribution"><?php _e( 'Licenses with no attribution required', GAMBIT_CC_IMAGE_SEARCH ) ?></option>
			</select>
			<span class="spinner"></span>
		</div>
		<div class="media-toolbar-primary search-form">
			<label for="media-search-input" class="screen-reader-text"><?php _e( 'Search Image', GAMBIT_CC_IMAGE_SEARCH ) ?></label>
			<input type="search" placeholder="Search" id="media-search-input" class="search"/>
		</div>
	</div>
	<div class="media-sidebar">
		<div class="ccimage-details" style="display: none;">
			<h2><?php _e( 'Image Details', GAMBIT_CC_IMAGE_SEARCH ) ?></h2>
			<div class='attachment-info'>
				<div class='thumbnail thumbnail-image'>
					<img class='preview' draggable="false"/>
				</div>
			</div>
			<div class='provider'><strong><?php _e( 'Image Provider:', GAMBIT_CC_IMAGE_SEARCH ) ?></strong> <span></span></div>
			<div class='title'><strong><?php _e( 'Title:', GAMBIT_CC_IMAGE_SEARCH ) ?></strong> <span></span></div>
			<div class='date'><strong><?php _e( 'Date:', GAMBIT_CC_IMAGE_SEARCH ) ?></strong> <span></span></div>
			<div class='owner'><strong><?php _e( 'Owner:', GAMBIT_CC_IMAGE_SEARCH ) ?></strong> <span></span></div>
			<div class='license'><strong><?php _e( 'License:', GAMBIT_CC_IMAGE_SEARCH ) ?></strong> <span></span></div>
			<div class='sizes'><strong><?php _e( 'Sizes:', GAMBIT_CC_IMAGE_SEARCH ) ?></strong> <span></span></div>
		</div>
	</div>
	<ul tabindex="-1" class="attachments ccimage-result-container">
	</ul>
</script>


<script type="text/html" id="tmpl-ccimage-search-result">
	<div class="attachment-preview {{ data.orientation }}">
		<div class="thumbnail">
			<div class="centered">
				<img src="{{ data.preview }}" draggable="false" alt="" width="{{ data.preview_width }}" height="{{ data.preview_height }}">
			</div>
		</div>
		<div class="ccimage-overlay">
			<# if ( data.provider === 'flickr' ) { #>
				<div class="ccimage-provider-flickr" role="presentation" title="<?php echo esc_attr( __( 'From Flickr', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div>
			<# } else if ( data.provider === 'pixabay' ) { #>
				<div class="ccimage-provider-pixabay" role="presentation" title="<?php echo esc_attr( __( 'From Pixabay', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div>
			<# } #>
			<div class="ccimage-info"></div>
			<div class="ccimage-downloading"></div>
			<div class="ccimage-download-label"><?php _e( 'Download', GAMBIT_CC_IMAGE_SEARCH ) ?></div>
			<div class="ccimage-download" title="<?php echo esc_attr( __( 'Download to site', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div>
			<# if ( data.badges.indexOf( 'attribution' ) !== -1 ) { #>
				<div class="ccimage-badge-cc" role="presentation" title="<?php echo esc_attr( __( 'Needs attribution', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div>
			<# } #>
			<# if ( data.badges.indexOf( 'noncommercial' ) !== -1 ) { #>
				<div class="ccimage-badge-noncommercial" role="presentation" title="<?php echo esc_attr( __( 'Noncommercial', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div>
			<# } #>
			<# if ( data.badges.indexOf( 'zero' ) !== -1 ) { #>
				<!-- <div class="ccimage-badge-zero" role="presentation" title="<?php echo esc_attr( __( 'Public Domain', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div> -->
			<# } #>
			<# if ( data.badges.indexOf( 'warning' ) !== -1 ) { #>
				<div class="ccimage-badge-warning" role="presentation" title="<?php echo esc_attr( __( 'Check License', GAMBIT_CC_IMAGE_SEARCH ) ) ?>"></div>
			<# } #>
		</div>
	</div>
</script>
