��             +         �     �     �     �               !     *     ;     M     `     g     s     �     �     �     �     �     �  A   �     9  "   B  %   e     �     �     �     �     �     �     �     �     �  L  �     6     T     h     z     �     �     �     �     �     �     �     �     �               !     1     E  A   d     �  "   �  %   �     �     
	     	     	     %	     -	     ;	     H	     O	                                       
                                                                                                 	        %s by %s is licensed under %s All image providers Automatic Updates Check License Date: Download Download to site Filter by license Filter by provider Flickr From Flickr From Pixabay Get Customer Support Get More Plugins Image Details Image Provider: Image by %s from %s Include noncommercial licenses Keep CC Image Search updated by entering your purchase code here. License: Licenses that allow commercial use Licenses with no attribution required Needs attribution Noncommercial Owner: Photo Pixabay Public Domain Search Image Sizes: Title: Project-Id-Version: CC Image Search
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-MO-DA HO:MI+ZONE
Last-Translator: Automatically generated
Language-Team: none
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
Plural-Forms: nplurals=2; plural=(n != 1);

Language: en_US
 %s by %s is licensed under %s All image providers Automatic Updates Check License Date: Download Download to site Filter by license Filter by provider Flickr From Flickr From Pixabay Get Customer Support Get More Plugins Image Details Image Provider: Image by %s from %s Include noncommercial licenses Keep CC Image Search updated by entering your purchase code here. License: Licenses that allow commercial use Licenses with no attribution required Needs attribution Noncommercial Owner: Photo Pixabay Public Domain Search Image Sizes: Title: 