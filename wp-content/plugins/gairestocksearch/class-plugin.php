<?php
/**
Plugin Name: GAIRE STOCK SEARCH
Description: Search For Free Stock Images
Author: GAIRE GROUP
 */

/**
 * IMPORTANT: DO NOT PUT SHORTCODE CODE IN THIS FILE, DO THAT INSIDE `class-cc_image_search.php`
 * ALL CODE IN THIS MAIN FILE IS ONLY FOR BASIC PLUGIN FUNCTIONALITY LIKE ADDING
 * ADMIN POINTERS TO SHOW ON ACTIVATION.
 */


if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

// Identifies the current plugin version.


// The slug used for translations & other identifiers.
defined( 'GAMBIT_CC_IMAGE_SEARCH' ) or define( 'GAMBIT_CC_IMAGE_SEARCH', 'cc_image_search' );

// Plugin automatic updates.

// This is the main plugin functionality.
require_once( 'class-cc-image-search.php' );

// Initializes plugin class.
if ( ! class_exists( 'CCImageSearchPlugin' ) ) {

	/**
	 * Initializes core plugin that is readable by WordPress.
	 *
	 * @return	void
	 * @since	1.0
	 */
	class CCImageSearchPlugin {

		/**
		 * Hook into WordPress.
		 *
		 * @return	void
		 * @since	1.0
		 */
		function __construct() {

			// Admin pointer reminders for automatic updates.
			require_once( 'class-admin-pointers.php' );
			if ( class_exists( 'GambitAdminPointers' ) ) {
				new GambitAdminPointers( array(
					'pointer_name' => 'CCImageSearchPlugin', // This should also be placed in uninstall.php.
					'header' => __( 'Automatic Updates', GAMBIT_CC_IMAGE_SEARCH ),
					'body' => __( 'Keep CC Image Search updated by entering your purchase code here.', GAMBIT_CC_IMAGE_SEARCH ),
				) );
			}

			// Our translations.
			add_action( 'plugins_loaded', array( $this, 'load_text_domain' ), 1 );

			// Gambit links.
			add_filter( 'plugin_row_meta', array( $this, 'plugin_links' ), 10, 2 );
		}


		/**
		 * Loads the translations.
		 *
		 * @return	void
		 * @since	1.0
		 */
		public function load_text_domain() {
			load_plugin_textdomain( GAMBIT_CC_IMAGE_SEARCH, false, basename( dirname( __FILE__ ) ) . '/languages/' );
		}


		/**
		 * Adds plugin links.
		 *
		 * @access	public
		 * @param	array  $plugin_meta The current array of links.
		 * @param	string $plugin_file The plugin file.
		 * @return	array The current array of links together with our additions.
		 * @since	1.0
		 **/
		public function plugin_links( $plugin_meta, $plugin_file ) {
			if ( plugin_basename( __FILE__ ) == $plugin_file ) {
				$pluginData = get_plugin_data( __FILE__ );

				$plugin_meta[] = sprintf( "<a href='%s' target='_blank'>%s</a>",
					'http://GaireGroup.net?utm_source=' . urlencode( $pluginData['Name'] ) . '&utm_medium=plugin_link',
					__( 'Get Customer Support', GAMBIT_CC_IMAGE_SEARCH )
				);
				$plugin_meta[] = sprintf( "<a href='%s' target='_blank'>%s</a>",
					'https://greatsteals.com/',
					__( 'More Awesome Products', GAMBIT_CC_IMAGE_SEARCH )
				);
			}
			return $plugin_meta;
		}
	}

	new CCImageSearchPlugin();
}
