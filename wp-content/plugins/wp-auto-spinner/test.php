<?php 

echo 'Cron started at '. date( 'h:i:s');

//wp-load
require_once('../../../wp-load.php');

remove_all_filters('pre_get_posts');
 
// The Query
$the_query = new WP_Query ( array (
 
		'posts_per_page' => 1,
			
		'ignore_sticky_posts' => true,
		'post_type' => 'any',
			
		'meta_query' => array(

				array(
						'key' => 'wp_auto_spinner_scheduled',
						'compare' => 'EXISTS',
				),array(
						'key' => 'spinned_cnt',
						'compare' => 'NOT EXISTS',
				)

		)

) );

// The Loop
if ($the_query->have_posts ()) {
		
		
	while ( $the_query->have_posts () ) {

			
		$the_query->the_post ();

		$newid=get_the_id();
		
		echo ' Pid:'.$newid;
		
	}
}

?>