<?php

// EDIT THE LIST BELOW (line 21 to line 25)
// Left part 	: your result title
// Right part 	: the redirection
// 
// --------------------
// Example:
// 		'Pikachu' => 'http://www.pikachu-fanclub.com/page2.html',
// 		'Mew' => 'http://www.mew-fanclub.com/',
// --------------------
// 
//  You can add as many new line as you want in the list.
// 	
// 	The "error" result is used when the final result of the user doesn't match
// 	any of the result listed below. It could happen if you add a new result in your quiz
// 	but you forgot to add it here too.

$redirection = array
(
	'title of result #1' => 'http://www.website-for-result1.com/page2.html',
	'title of result #2' => 'http://www.website-for-result2.com/',
	'title of result #3' => 'http://www.website-for-result3.com/',

	'error'  => 'http://www.your-website.com/',
);

// STOP — DON'T CHANGE ANYTHING BELOW.
if (isset($_GET['wpvqresults']))
{
	$result = $_GET['wpvqresults'];
	if (isset($redirection[$result])) {
		header('Location:' . $redirection[$result]);
	} else if (isset($redirection['error'])) {
		header('Location:' . $redirection['error']);
	}
}

die();
