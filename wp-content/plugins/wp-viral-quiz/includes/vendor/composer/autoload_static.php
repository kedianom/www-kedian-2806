<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite22a6fbb96cdc54ac7d4b89f745f6269
{
    public static $files = array (
        '5255c38a0faeba867671b61dfda6d864' => __DIR__ . '/..' . '/paragonie/random_compat/lib/random.php',
        'c964ee0ededf28c96ebd9db5099ef910' => __DIR__ . '/..' . '/guzzlehttp/promises/src/functions_include.php',
        'a0edc8309cc5e1d60e3047b5df6b7052' => __DIR__ . '/..' . '/guzzlehttp/psr7/src/functions_include.php',
        '72579e7bd17821bb1321b87411366eae' => __DIR__ . '/..' . '/illuminate/support/helpers.php',
        '37a3dc5111fe8f707ab4c132ef1dbc62' => __DIR__ . '/..' . '/guzzlehttp/guzzle/src/functions_include.php',
    );

    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Psr\\Http\\Message\\' => 17,
        ),
        'M' => 
        array (
            'Mailchimp\\' => 10,
        ),
        'I' => 
        array (
            'Illuminate\\Support\\' => 19,
            'Illuminate\\Contracts\\' => 21,
        ),
        'G' => 
        array (
            'GuzzleHttp\\Psr7\\' => 16,
            'GuzzleHttp\\Promise\\' => 19,
            'GuzzleHttp\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Mailchimp\\' => 
        array (
            0 => __DIR__ . '/..' . '/pacely/mailchimp-apiv3/src',
        ),
        'Illuminate\\Support\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/support',
        ),
        'Illuminate\\Contracts\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/contracts',
        ),
        'GuzzleHttp\\Psr7\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/psr7/src',
        ),
        'GuzzleHttp\\Promise\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/promises/src',
        ),
        'GuzzleHttp\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/guzzle/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'D' => 
        array (
            'Doctrine\\Common\\Inflector\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/inflector/lib',
            ),
        ),
    );

    public static $classMap = array (
        'AC_Account' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Account.class.php',
        'AC_Auth' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Auth.class.php',
        'AC_Automation' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Automation.class.php',
        'AC_Campaign' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Campaign.class.php',
        'AC_Connector' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Connector.class.php',
        'AC_Contact' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Contact.class.php',
        'AC_Deal' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Deal.class.php',
        'AC_Design' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Design.class.php',
        'AC_Form' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Form.class.php',
        'AC_Group' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Group.class.php',
        'AC_List_' => __DIR__ . '/..' . '/activecampaign/api-php/includes/List.class.php',
        'AC_Message' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Message.class.php',
        'AC_Settings' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Settings.class.php',
        'AC_Subscriber' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Subscriber.class.php',
        'AC_Tracking' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Tracking.class.php',
        'AC_User' => __DIR__ . '/..' . '/activecampaign/api-php/includes/User.class.php',
        'AC_Webhook' => __DIR__ . '/..' . '/activecampaign/api-php/includes/Webhook.class.php',
        'AWeberAPI' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber.php',
        'AWeberAPIBase' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber.php',
        'AWeberAPIException' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberCollection' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber_collection.php',
        'AWeberEntry' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber_entry.php',
        'AWeberEntryDataArray' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber_entry_data_array.php',
        'AWeberException' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberMethodNotImplemented' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberOAuthAdapter' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/oauth_adapter.php',
        'AWeberOAuthDataMissing' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberOAuthException' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberResourceNotImplemented' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberResponse' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber_response.php',
        'AWeberResponseError' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/exceptions.php',
        'AWeberServiceProvider' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/aweber.php',
        'ActiveCampaign' => __DIR__ . '/..' . '/activecampaign/api-php/includes/ActiveCampaign.class.php',
        'CurlInterface' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/curl_object.php',
        'CurlObject' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/curl_object.php',
        'CurlResponse' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/curl_response.php',
        'OAuthApplication' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/oauth_application.php',
        'OAuthServiceProvider' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/oauth_application.php',
        'OAuthUser' => __DIR__ . '/..' . '/aweber/aweber/aweber_api/oauth_application.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite22a6fbb96cdc54ac7d4b89f745f6269::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite22a6fbb96cdc54ac7d4b89f745f6269::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInite22a6fbb96cdc54ac7d4b89f745f6269::$prefixesPsr0;
            $loader->classMap = ComposerStaticInite22a6fbb96cdc54ac7d4b89f745f6269::$classMap;

        }, null, ClassLoader::class);
    }
}
