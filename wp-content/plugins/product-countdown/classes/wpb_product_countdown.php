<?php

/**
 * @package    WPBuddy Plugin
 * @subpackage Purple Heart Rating
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * @version 4.0.9
 */
class WPB_Product_Countdown extends WPB_Plugin {

	/**
	 * The plugins textdomain name
	 *
	 * @var string
	 * @access protected
	 * @since  1.0
	 */
	protected $_plugin_textdomain = 'product-countdown';


	/**
	 * The plugin slug name
	 *
	 * @var string
	 * @access protected
	 * @since  1.0
	 */
	protected $_plugin_name = 'product-countdown';


	/**
	 * The plugin version
	 *
	 * @var string
	 * @access private
	 * @since  1.0
	 */
	public $_plugin_version = '4.0.9';


	/**
	 * The tabs for the settings pages
	 *
	 * @var array
	 * @since 1.0
	 */
	public $_settings_tabs = array();


	/**
	 * If the countdown is active on the current page
	 *
	 * @var bool
	 */
	public $_countdown_active = false;

	/**
	 * @since 3.0
	 * @var string
	 */

	public $_countdown_usage = '';


	/**
	 * The date till where the product price is valid
	 *
	 * @var int timestamp
	 */
	public $_dates_to = 0;


	/**
	 * The date since when the product price is valid
	 *
	 * @var int timestamp
	 */
	public $_dates_from = 0;


	/**
	 * Checks if a message should appear after the countdown has expired.
	 *
	 * @since  4.0.0
	 * @access public
	 *
	 * @var bool
	 */
	public $_msg_after_timeout = false;


	/**
	 * Just do the normal startup stuff (adding actions and so on ...)
	 *
	 * @param null   $file
	 * @param null   $plugin_url
	 * @param string $inclusion
	 *
	 * @since  1.0
	 *
	 * @access public
	 * @return \WPB_Product_Countdown
	 */
	function __construct( $file = null, $plugin_url = null, $inclusion = 'plugin' ) {

		// call the parent constructor first
		parent::__construct( $file, $plugin_url, $inclusion );

		$this->_purchase_code_settings_page_url = admin_url( 'admin.php?page=woocommerce_wpbpc' );

		$this->_settings_tabs = array( 'general' => __( 'Settings', $this->get_textdomain() ) );

		add_action( 'wp', array( &$this, 'is_countdown_active' ), 5 );

		add_action( 'init', array( &$this, 'add_shortcodes' ) );

		// do the admin stuff
		$this->do_admin();

		// do the non-admin stuff
		$this->do_non_admin();

		add_action( 'wpbpc_hourly_event', array( &$this, 'send_mail' ) );

		return $this;
	}


	/**
	 * Doing the admin stuff
	 *
	 * @since 1.0
	 */
	private function do_admin() {

		if ( ! is_admin() ) {
			return;
		}

		add_action( 'admin_notices', array( &$this, 'admin_notices' ) );

		// Add 'Countdown' link under WooCommerce menu
		add_action( 'admin_menu', array( &$this, 'admin_menu' ) );

		// Enqueue Javascript and CSS
		add_action( 'woocommerce_screen_ids', array( &$this, 'woocommerce_styles_scripts' ), 100 );

		add_action( 'admin_init', array( &$this, 'add_post_metaboxes' ) );

		add_action( 'save_post', array( &$this, 'save_post' ) );

		add_action( 'admin_head-edit.php', array( &$this, 'admin_head_posts' ) );
		add_action( 'admin_head-post-new.php', array( &$this, 'admin_head_posts' ) );
		add_action( 'admin_head-post.php', array( &$this, 'admin_head_posts' ) );

		add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_scripts_backend' ), 500 );

		add_action( 'wp_ajax_wpbpc_ajax_sendmail', array( &$this, 'ajax_sendmail' ) );
		add_action( 'wp_ajax_nopriv_wpbpc_ajax_sendmail', array( &$this, 'ajax_sendmail' ) );

		add_action( 'wp_ajax_wpbpc_search_products', array( &$this, 'ajax_search_products' ) );

		// add setting links to the plugins admin page
		if ( 'plugin' == $this->_inclusion ) {
			add_filter( 'plugin_action_links_' . plugin_basename( $this->_plugin_file ), array(
				&$this,
				'plugin_action_links',
			) );
		}

		add_action( 'wp_ajax_wpbpc_ajax_get_timestamp', array( &$this, 'ajax_get_timestamp' ) );
		add_action( 'wp_ajax_nopriv_wpbpc_ajax_get_timestamp', array( &$this, 'ajax_get_timestamp' ) );
	}

	/*
	public function woocommerce_product_options_sku() {
		error_log('options_sku');
		add_filter( 'date_i18n', array( &$this, 'wpbpc_date_i18n' ), 10, 4 );
	}

	public function wpbpc_date_i18n( $j, $req_format, $i, $gmt ) {
		error_log('filter');
		remove_filter( 'date_i18n', array( &$this, 'wpbpc_date_i18n' ), 10 );

		$j = date_i18n( 'Y-m-d H:i', $i, $gmt );

		error_log($j);
		return $j;
	}
	*/


	/**
	 * Doing the non-admin stuff
	 *
	 * @since 1.0
	 */
	private function do_non_admin() {

		if ( is_admin() ) {
			return;
		}

		add_action( 'wp_enqueue_scripts', array( &$this, 'enqueue_scripts_frontend' ), 100 );

		add_action( 'wp_footer', array( &$this, 'wp_footer' ) );

		add_action( 'template_redirect', array( &$this, 'redirect_on_countdown_end' ) );

		add_action( 'wp_head', array( &$this, 'wp_head' ) );

		//add_filter( 'widget_text', array( &$this, 'do_shortcode' ) );

		add_filter( 'woocommerce_available_variation', array( &$this, 'woocommerce_available_variation' ), 10, 3 );

	}


	/**
	 * Creates the admin menu
	 *
	 * @since 1.0
	 */
	public function admin_menu() {

		$this->_settings_menu_slug = $this->_pagehook = add_submenu_page( 'woocommerce', __( 'Countdown', $this->get_textdomain() ), __( 'Countdown', $this->get_textdomain() ), 'manage_woocommerce', 'woocommerce_wpbpc', array(
			&$this,
			'settings_page',
		) );
		add_action( 'load-' . $this->_settings_menu_slug, array( &$this, 'save_settings' ) );
	}

	/**
	 * Saves the settings on the settings page
	 *
	 * @since 3.5.5
	 */
	public function save_settings() {

		$current_tab = $this->get_current_tab();

		//check post & verify nonce
		if ( empty( $_POST ) ) {
			return;
		}

		if ( ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'wc-wpbpc-settings' ) ) {
			wp_die( __( 'Action failed. Please go back and try again.', $this->get_textdomain() ) );
		}

		$this->save_fields( $this->load_fields(), $current_tab );

		// Redirect to settings
		wp_redirect( esc_url_raw( add_query_arg( 'saved', 'true' ) ) );
		exit;


	}


	/**
	 * Returns the standard rating option values
	 *
	 * @param $option
	 *
	 * @since 1.0
	 * @return string
	 */
	public function options_standards( $option ) {

		$standards = array();

		if ( isset( $standards[ $option ] ) ) {
			return $standards[ $option ];
		}

		return '';
	}


	/**
	 * Returns the content of a rating option
	 *
	 * @param      $option
	 *
	 * @param bool $is_static whether the function is called statically
	 *
	 * @since 1.0
	 * @return string
	 */
	public function get_option( $option, $is_static = false ) {

		$options = get_option( 'woocommerce_wpbpc' );
		if ( isset( $options[ $option ] ) ) {
			return $options[ $option ];
		}

		if ( $is_static ) {
			return '';
		}

		return $this->options_standards( $option );
	}


	/**
	 * The same as get_option but can be accessed statically
	 *
	 * @param $option
	 *
	 * @uses self::get_option
	 *
	 * @return string
	 */
	public static function get_option_static( $option ) {

		return self::get_option( $option, true );
	}


	/**
	 * settings_page function.
	 * This displays the settings page for the the plugin
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	public function settings_page() {

		if ( false === ( $current_tab = $this->get_current_tab() ) ) {
			return;
		}

		$this->send_mail();
		?>
		<div class="wrap woocommerce">
			<h1><?php _e( 'Countdown', $this->get_textdomain() ); ?></h1>

			<form method="post" id="mainform"
			      action="<?php admin_url( 'admin.php?page=' . $this->_settings_menu_slug ); ?>"
			      enctype="multipart/form-data">
				<?php

				wp_nonce_field( 'wc-wpbpc-settings', '_wpnonce', true, true );

				// show error / success message
				if ( ! empty( $_GET['saved'] ) ) {
					echo '<div id="message" class="updated fade"><p><strong>' . __( 'Your customizations have been saved.', $this->get_textdomain() ) . '</strong></p></div>';
				}


				$this->output_fields( $this->load_fields(), $current_tab );

				?>
				<p class="submit">
					<input name="save" class="button-primary" type="submit"
					       value="<?php _e( 'Save', $this->get_textdomain() ); ?>"/>
				</p>
			</form>
		</div>

		<?php

	}


	/**
	 * Return admin fields in proper format for outputting / saving
	 *
	 * @since  1.0
	 * @return array
	 */
	private function load_fields() {

		$frontpage = get_option( 'page_on_front', 0 );

		return array(

			'general' =>

				array(

					array(
						'name' => __( 'General options', $this->get_textdomain() ),
						'type' => 'title',
					),
					array(
						'id'   => 'from_top_helper',
						'name' => __( 'Show "Fade in" helper', $this->get_textdomain() ),
						'desc' => '<br />' . __( 'This shows a little helper on the top left of every page that has a countdown message. It is only shown to administration users.', $this->get_textdomain() ),
						'type' => 'checkbox',
					),
					/* @deprecated since 3.7.0
					 * array(
					 * 'id'       => 'using_cache',
					 * 'name'     => __( 'I\'m using a caching plugin', $this->get_textdomain() ),
					 * 'desc_tip' => __( 'Check this if you\'re using a caching plugin. This is needed to correct the countdown.', $this->get_textdomain() ),
					 * 'type'     => 'checkbox'
					 * ),
					 */

					/*
					array(
						'id'       => 'datetimepicker',
						'name'     => __( 'Activate Datetime picker', $this->get_textdomain() ),
						'desc_tip' => __( 'Allows to add times to WooCommerce Sales Dates, too.', $this->get_textdomain() ),
						'type'     => 'checkbox'
					),
					*/

					array(
						'id'   => 'homepage_product',
						'name' => __( 'The product to use on the home page', $this->get_textdomain() ),
						'desc' => '<br />' . ( ( empty( $frontpage ) )
								? __( 'If you want to use the countdown on the home page, please select the product that belongs to the countdown. The settings for the countdown can be set on the product page.', $this->get_textdomain() )
								: sprintf( __( '%sIt seems that your homepage is a static page. %sClick here to edit the countdown timer on the static page directly%s instead of selecting a product here.', $this->get_textdomain() ), '<strong style="color: red;">', '</strong><a href="' . admin_url( 'post.php?post=' . $frontpage . '&action=edit' ) . '">', '</a>' ) ),
						'type' => 'select',
					),
					array(
						'id'   => 'global_css',
						'name' => __( 'Global styles', $this->get_textdomain() ),
						'desc' => '<br />' . __( 'Enter the stylesheets you want to use globally in every countdown.', $this->get_textdomain() ),
						'type' => 'textarea',
					),
					array(
						'id'       => 'purchase_code',
						'name'     => __( 'Purchase Code', $this->get_textdomain() ),
						'desc_tip' => false,
						'desc'     => '<br />' . sprintf( __( 'In order to get automatic updates you should enter your purchase code. If you do not know where you can find your purchase code, read this: %s', $this->get_textdomain() ), '<a href="http://wp-buddy.com/wiki/where-to-find-your-purchase-code/" target="_blank">' . __( 'How to find your purchase code', $this->get_textdomain() ) . '</a>' ),
						'type'     => 'text',
					),
					array( 'type' => 'sectionend' ),

				),

		);
	}


	/**
	 * Get current tab slug
	 *
	 * @access private
	 * @since  1.0
	 * @return string slug of current tab
	 */
	private function get_current_tab() {

		if ( empty( $_GET['tab'] ) ) {
			// default to the first tab
			reset( $this->_settings_tabs );

			return key( $this->_settings_tabs );

		} elseif ( array_key_exists( $_GET['tab'], $this->_settings_tabs ) ) {

			return urldecode( $_GET['tab'] );

		} else {

			return false;
		}
	}


	/**
	 * Output fields to page
	 *
	 * @access public
	 * @since  1.0
	 *
	 * @param array  $fields
	 * @param string $tab current tab slug
	 *
	 * @return void
	 */
	public function output_fields( $fields, $tab ) {

		global $woocommerce;

		// only use fields for current tab
		$fields = $fields[ $tab ];

		foreach ( $fields as $field ) :

			if ( ! isset( $field['type'] ) ) {
				continue;
			}
			if ( ! isset( $field['name'] ) ) {
				$field['name'] = '';
			}
			if ( ! isset( $field['class'] ) ) {
				$field['class'] = '';
			}
			if ( ! isset( $field['css'] ) ) {
				$field['css'] = '';
			}
			if ( ! isset( $field['std'] ) ) {
				$field['std'] = '';
			}
			if ( ! isset( $field['desc'] ) ) {
				$field['desc'] = '';
			}
			if ( ! isset( $field['desc_tip'] ) ) {
				$field['desc_tip'] = false;
			}

			if ( $field['desc_tip'] === true ) {
				$description = '<img height="16" width="16" class="help_tip" data-tip="' . esc_attr( $field['desc'] ) . '" src="' . $woocommerce->plugin_url() . '/assets/images/help.png" />';
			} elseif ( $field['desc_tip'] ) {
				$description = '<img height="16" width="16" class="help_tip" data-tip="' . esc_attr( $field['desc_tip'] ) . '" src="' . $woocommerce->plugin_url() . '/assets/images/help.png" />';
			} else {
				$description = '<span class="description">' . $field['desc'] . '</span>';
			}

			switch ( $field['type'] ) :

				case 'title':
					if ( isset( $field['name'] ) && $field['name'] ) {
						echo '<h3>' . $field['name'] . '</h3>';
					}
					if ( isset( $field['desc'] ) && $field['desc'] ) {
						echo wpautop( wptexturize( $field['desc'] ) );
					}
					echo '<table class="form-table">' . "\n\n";
					break;

				case 'sectionend':
					echo '</table>';
					break;

				case 'checkbox':
					?>
					<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
					</th>
					<td class="forminp">
						<input name="<?php echo esc_attr( $field['id'] ); ?>"
						       id="<?php echo esc_attr( $field['id'] ); ?>"
						       type="<?php echo esc_attr( $field['type'] ); ?>"
						       style="<?php echo esc_attr( $field['css'] ); ?>" value="1" <?php
						if ( (bool) $this->get_option( $field['id'] ) ) {
							echo 'checked="checked"';
						}
						?>/> <?php echo $description; ?></td>
					</tr><?php
					break;

				case 'textarea':
					?>
					<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
					</th>
					<td class="forminp">
						<textarea style="width:100%; height: 75px;" name="<?php echo esc_attr( $field['id'] ); ?>"
						          id="<?php echo esc_attr( $field['id'] ); ?>"
						          type="<?php echo esc_attr( $field['type'] ); ?>"
						          style="<?php echo esc_attr( $field['css'] ); ?>"><?php if ( $this->get_option( $field['id'] ) ) {
								echo esc_textarea( stripslashes( $this->get_option( $field['id'] ) ) );
							} else {
								echo esc_textarea( $field['std'] );
							} ?></textarea> <?php echo $description; ?></td>
					</tr><?php
					break;

				case 'text':
					?>
					<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
					</th>
					<td class="forminp">
						<input class="regular-text" name="<?php echo esc_attr( $field['id'] ); ?>"
						       id="<?php echo esc_attr( $field['id'] ); ?>"
						       type="<?php echo esc_attr( $field['type'] ); ?>"
						       style="<?php echo esc_attr( $field['css'] ); ?>"
						       value="<?php if ( $this->get_option( $field['id'] ) ) {
							       echo esc_attr( stripslashes( $this->get_option( $field['id'] ) ) );
						       } else {
							       echo esc_attr( $field['std'] );
						       } ?>"/> <?php echo $description; ?></td>
					</tr><?php
					break;

				case 'info':
					?>
					<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
					</th>
					<td class="forminp">
						<?php echo $description; ?></td>
					</tr><?php
					break;

				case 'select':
					?>
					<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
					</th>
					<td class="forminp">
						<?php
						$featured_product    = $this->get_wc_product( $this->get_option( 'homepage_product' ) );
						$featured_product_id = 0;

						if ( ( $featured_product instanceof WC_Product ) ) {
							if ( method_exists( $featured_product, 'get_id' ) ) {
								$featured_product_id = $featured_product->get_id();
							} else {
								$featured_product_id = $featured_product->post->ID;
							}
						}

						printf(
							'<select type="text" data-ajax_action="wpbpc_search_products" data-ajax_nonce="%1$s" data-name="%2$s" class="wpbpc-select2" name="%3$s" id="%3$s" data-noselection="%4$s" ><option selected="selected" value="%5$d">%2$s</option></select>',
							wp_create_nonce( 'wpbpc_search_products' ),
							( $featured_product_id ) ? esc_attr( get_the_title( $featured_product_id ) ) : esc_html__( 'No selection', $this->get_textdomain() ),
							esc_attr( $field['id'] ),
							esc_attr( __( 'No selection', $this->get_textdomain() ) ),
							$featured_product_id
						);

						echo $description;
						?>
					</tr><?php
					break;
				default:
					break;

			endswitch;

		endforeach;
	}


	/**
	 * Save admin fields into serialized options
	 *
	 * @since  1.0
	 *
	 * @param array  $fields
	 * @param string $tab current tab slug
	 *
	 * @return bool
	 */
	public function save_fields( $fields, $tab ) {

		// only use fields for current tab
		$fields = $fields[ $tab ];

		$customizations = maybe_unserialize( get_option( 'woocommerce_wpbpc' ) );

		foreach ( $fields as $field ) :

			if ( ! isset( $field['id'] ) ) {
				continue;
			}

			if ( ! isset( $field['type'] ) ) {
				continue;
			}

			if ( 'checkbox' == $field['type'] ) {
				if ( ! isset( $_POST[ $field['id'] ] ) ) {
					$customizations[ $field['id'] ] = 0;
				} else {
					$customizations[ $field['id'] ] = 1;
				}
				continue;
			}

			if ( isset( $field['id'] ) && ! empty( $_POST[ $field['id'] ] ) ) {

				if ( function_exists( 'wc_clean' ) ) {
					$customizations[ $field['id'] ] = wc_clean( $_POST[ $field['id'] ] );
				} else {
					$customizations[ $field['id'] ] = woocommerce_clean( $_POST[ $field['id'] ] );
				}

			} elseif ( isset( $field['id'] ) ) {

				unset( $customizations[ $field['id'] ] );

			}

		endforeach;

		update_option( 'woocommerce_wpbpc', $customizations );

		return true;
	}


	/**
	 * Adds CSS and Javascripts to the woocommerce countdown settings page
	 *
	 * @access public
	 * @since  1.0
	 *
	 * @param array $screen_ids
	 *
	 * @return array
	 */
	public function woocommerce_styles_scripts( $screen_ids ) {

		$screen_ids[] = 'woocommerce_page_woocommerce_wpbpc';

		return $screen_ids;
	}


	/**
	 * Enqueues the js and css files for the frontend
	 *
	 * @since 1.0
	 * @global WP_Post $post
	 * @global string  $wp_version
	 */
	public function enqueue_scripts_frontend() {

		if ( ! $this->_countdown_active ) {
			return;
		}

		wp_register_style( 'wpbpc_frontend_style', $this->plugins_url( 'assets/css/product-countdown-frontend.css', $this->_plugin_file ), false, $this->_plugin_version );
		wp_enqueue_style( 'wpbpc_frontend_style' );

		wp_register_script( 'wpbpc_frontend_js', $this->plugins_url( 'assets/js/product-countdown-frontend.min.js', $this->_plugin_file ), array( 'jquery' ), $this->_plugin_version );
		wp_enqueue_script( 'wpbpc_frontend_js' );

		$redirect = 0;
		$sendmail = 0;

		/**
		 * Reset the query to the original query so that the post is the right one
		 */
		wp_reset_query();

		global $post;
		if ( $post instanceof WP_Post ) {
			$action = get_post_meta( $post->ID, '_wpbpc_action', true );
			/**
			 * Check if redirection is on
			 */
			$redirect_url = get_post_meta( $post->ID, '_wpbpc_redirect_url', true );
			if (
				(bool) get_post_meta( $post->ID, '_wpbpc_countdown', true ) &&
				'redirect' == $action &&
				! empty( $redirect_url )
			) {
				$redirect = 1;
			}

			/**
			 * Check if email sending is on
			 */
			$email         = get_post_meta( $post->ID, '_wpbpc_email', true );
			$email_message = get_post_meta( $post->ID, '_wpbpc_email_message', true );

			if ( 'email' == $action && ! empty( $email ) && ! empty( $email_message ) ) {
				$sendmail = 1;
			}
		}

		wp_localize_script( 'wpbpc_frontend_js', 'WPBPC', array(
				'ajax_url'             => admin_url( 'admin-ajax.php' ),
				'redirect'             => $redirect,
				'redirect_url'         => $redirect_url,
				'sendmail'             => $sendmail,
				'is_msg_after_timeout' => absint( $this->_msg_after_timeout ),
				//'using_cache'  => intval( $this->get_option( 'using_cache' ) ) // @deprecated since 3.7.0
			)
		);
	}


	/**
	 * Includes admin scripts
	 *
	 * @since 2.0
	 */
	public function enqueue_scripts_backend( $hook_suffix ) {

		//global $typenow, $post_type;

		if ( ! wp_style_is( 'select2', 'registered' ) ) {

			$assets_path = str_replace( array(
					'http:',
					'https:',
				), '', untrailingslashit( plugins_url( '/', 'woocommerce/woocommerce.php' ) ) ) . '/assets/';

			wp_register_style( 'select2', $assets_path . 'css/select2.css' );
		}

		wp_register_script( 'wpbpc_backend_js', $this->plugins_url( 'assets/js/product-countdown-backend.js', $this->_plugin_file ), array(
			'jquery',
			'wp-color-picker',
			'select2',
		), $this->_plugin_version );

		wp_register_style( 'wpbpc_backend_css', $this->plugins_url( 'assets/css/product-countdown-backend.css', $this->_plugin_file ), array( 'select2' ) );

		if ( 'post.php' == $hook_suffix OR 'post-new.php' == $hook_suffix ) {
			wp_enqueue_script( 'wpbpc_backend_js' );
			wp_enqueue_style( 'wpbpc_backend_css' );
			wp_enqueue_style( 'wp-color-picker' );
		}

		if ( 'woocommerce_page_woocommerce_wpbpc' == $hook_suffix ) {
			wp_enqueue_script( 'wpbpc_backend_js' );
			wp_enqueue_style( 'wpbpc_backend_css' );
		}
	}


	/**
	 * Activate the plugin
	 *
	 * @uses  WPB_Purple_Heart_Rating_Db::create_db_tables
	 * @since 1.0
	 */
	public function on_activation() {

		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			wp_die( __( 'It seems that WooCommerce is not installed. Please activate WooCommerce to use the Product Countdown Plugin.', $this->get_textdomain() ) );
		}

		wp_schedule_event( time(), 'hourly', 'wpbpc_hourly_event' );
	}


	/**
	 * Deactivate the plugin
	 *
	 * @since 1.0
	 */
	public function on_deactivation() {

		wp_clear_scheduled_hook( 'wpbpc_hourly_event' );
	}


	/**
	 * Adds links to the plugins menu (where the plugins are listed)
	 *
	 * @param array $links
	 *
	 * @since 1.0
	 * @return array
	 */
	public function plugin_action_links( $links ) {

		$links[] = '<a href="' . get_admin_url( null, 'admin.php?page=woocommerce_wpbpc' ) . '">' . __( 'Settings', $this->get_textdomain() ) . '</a>';
		$links[] = '<a href="http://wp-buddy.com/products/" target="_blank">' . __( 'More Plugins by WPBuddy', $this->get_textdomain() ) . '</a>';

		return $links;
	}


	/**
	 * Returns the purchase code to test for automatic updates
	 *
	 * @since 1.0
	 * @return string
	 */
	public function get_purchase_code() {

		return $this->get_option( 'purchase_code' );
	}


	/**
	 * Retrieves the setting of the current countdown
	 * if there is no setting for the countdown it's looks in the product for a setting
	 * if there is no setting in the product, it returnes a standard value
	 *
	 * @since 3.0
	 *
	 * @param int    $post_id
	 * @param string $key
	 * @param bool   $return_standard
	 *
	 * @return mixed
	 */
	private function get_setting_value( $post_id, $key, $return_standard = false ) {

		$parent_post_id = get_post_meta( $post_id, '_wpbpc_product_id', true );

		$value = get_post_meta( $post_id, $key, true );

		if ( '_wpbpc_fade_in_at' == $key && $value == 0 ) {
			return 0;
		}

		if ( empty( $value ) && $parent_post_id != $post_id ) {
			$value = get_post_meta( $parent_post_id, $key, true );
		}

		if ( ! empty( $value ) && ! $return_standard ) {
			return $value;
		}

		switch ( $key ) {
			case '_wpbpc_info_text':
				return __( 'Hurry up! The price rises in', $this->get_textdomain() );

			case '_wpbpc_background_color':
				return '#FF3333';

			case '_wpbpc_font_color':
				return '#fff';

			case '_wpbpc_action':
				return 'nothing';

			default:
				return '';
		}
	}

	/**
	 * Gets the countdown_html
	 *
	 * @param string  $id
	 * @param bool    $close_button Whether the closing button should be displayed
	 * @param bool    $return
	 * @param WP_Post $post
	 *
	 * @return string
	 */
	public function countdown_html( $id = 'product_countdown', $close_button = true, $return = false, $post = null ) {

		/**
		 * Reset the query to the original query so that the post is the right one
		 */
		wp_reset_query();

		if ( ! $post instanceof WP_Post ) {
			global $post;
			if ( ! $post instanceof WP_Post ) {
				return '';
			}
		}

		// get the info text of the current page
		$info_text = $this->get_setting_value( $post->ID, '_wpbpc_info_text' );

		// get the info text of the product page
		$product = $this->get_wc_product( get_post_meta( $post->ID, '_wpbpc_product_id', true ) );

		if ( ! $product instanceof WC_Product ) {
			return '';
		}

		$product_post_id = method_exists( $product, 'get_id' ) ? $product->get_id() : $product->post->ID;

		/**
		 * Check if this is a variable product
		 * if yes, search for the first variation that is on sale
		 */
		if ( $product->is_type( 'variable' ) ) {
			/**
			 * @var WC_Product_Variable $product
			 * @var array               $product_variation_array
			 * @var WC_Product_Variable $product_variation
			 */
			foreach ( $product->get_available_variations() as $product_variation_array ) {
				$product_variation = $this->get_wc_product( $product_variation_array['variation_id'] );
				if ( ! $product_variation instanceof WC_Product ) {
					continue;
				}
				if ( $product_variation->is_on_sale() ) {
					$product         = $product_variation;
					$product_post_id = $product_variation_array['variation_id'];
					break;
				}
			}
		}

		$regular_price = $this->wc_format_decimal( get_post_meta( $product_post_id, '_regular_price', true ) );
		$sale_price    = $this->wc_format_decimal( get_post_meta( $product_post_id, '_sale_price', true ) );

		$difference_in_percent = 0;
		if ( 0 != $regular_price ) {
			$difference_in_percent = round( ( $regular_price - $sale_price ) / $regular_price * 100 );
		}

		$sale_price_in_percent = 0;
		if ( 0 != $regular_price ) {
			$sale_price_in_percent = round( $sale_price * 100 / $regular_price );
		}

		$product_post_id = method_exists( $product, 'get_id' ) ? $product->get_id() : $product->post->ID;

		// replace variables
		$info_text = str_replace( '$price', '<span class="wpbpc_price">' . $this->wc_price( $regular_price ) . '</span>', $info_text );
		$info_text = str_replace( '$sales_price', '<span class="wpbpc_sales_price">' . $this->wc_price( $sale_price ) . '</span>', $info_text );
		$info_text = str_replace( '$sold_items', '<span class="wpbpc_sold_items">' . get_post_meta( $product_post_id, 'total_sales', true ) . '</span>', $info_text );
		$info_text = str_replace( '$difference_in_percent', '<span class="wpbpc_difference_in_percent">' . $difference_in_percent . '%</span>', $info_text );
		$info_text = str_replace( '$sale_price_in_percent', '<span class="wpbpc_sale_price_in_percent">' . $sale_price_in_percent . '%</span>', $info_text );

		if ( $product->is_in_stock() ) {
			$info_text = str_replace( '$stock_items', '<span class="wpbpc_stock_items">' . $product->get_stock_quantity() . '</span>', $info_text );
		} else {
			$info_text = str_replace( '$stock_items', '', $info_text );
		}

		$button_label = $this->get_setting_value( $post->ID, '_wpbpc_button_label' );
		$button_url   = $this->get_setting_value( $post->ID, '_wpbpc_button_url' );
		if ( empty( $button_url ) ) {
			$button_url = $product->add_to_cart_url();
		}

		$button_target = (bool) $this->get_setting_value( $post->ID, '_wpbpc_button_new_window' ) ? 'target="_blank"' : '';

		$html = '<div id="' . $id . '" '
		        . 'class="product_countdown" '
		        . 'data-until_seconds="' . ( date_i18n( 'U', $this->_dates_to ) - current_time( 'timestamp' ) ) . 's" '
		        . 'data-dates_to="' . $this->_dates_to . '" '
		        . 'data-fade="' . intval( $this->get_setting_value( $post->ID, '_wpbpc_fade_in_at' ) ) . '" '
		        . 'data-labels_years="' . __( 'Years', $this->get_textdomain() ) . '" '
		        . 'data-labels_months="' . __( 'Months', $this->get_textdomain() ) . '" '
		        . 'data-labels_weeks="' . __( 'Weeks', $this->get_textdomain() ) . '" '
		        . 'data-labels_days="' . __( 'Days', $this->get_textdomain() ) . '" '
		        . 'data-labels_hours="' . __( 'Hours', $this->get_textdomain() ) . '" '
		        . 'data-labels_minutes="' . __( 'Minutes', $this->get_textdomain() ) . '" '
		        . 'data-labels_seconds="' . __( 'Seconds', $this->get_textdomain() ) . '" '
		        . 'data-labels_year="' . __( 'Year', $this->get_textdomain() ) . '" '
		        . 'data-labels_month="' . __( 'Month', $this->get_textdomain() ) . '" '
		        . 'data-labels_week="' . __( 'Week', $this->get_textdomain() ) . '" '
		        . 'data-labels_day="' . __( 'Day', $this->get_textdomain() ) . '" '
		        . 'data-labels_hour="' . __( 'Hour', $this->get_textdomain() ) . '" '
		        . 'data-labels_minute="' . __( 'Minute', $this->get_textdomain() ) . '" '
		        . 'data-labels_second="' . __( 'Second', $this->get_textdomain() ) . '" '
		        . 'data-display_from_top="' . intval( get_post_meta( $post->ID, '_wpbpc_display_from_top', true ) ) . '" '
		        . 'data-post_id="' . $post->ID . '" '
		        . '>'
		        . '<div class="product_countdown_inner">'
		        . '<span class="product_countdown_end_message">' . $this->get_setting_value( $post->ID, '_wpbpc_end_message' ) . '</span>'
		        . '<span class="product_countdown_info_text">' . $info_text
		        // @deprecated since 3.7.0
		        //. '<img class="product_countdown_load_spinner" width="16" height="16" src="' . $this->plugins_url( 'assets/img/spinner.svg' ) . '" alt="' . __( 'Refreshing countdown', $this->get_textdomain() ) . '" />'
		        . '</span>'
		        . '<span id="' . $id . '_counter" class="product_countdown_counter"></span>'
		        . ( ( empty( $button_label ) ) ? '' : sprintf( '<a href="%s" %s class="product_countdown_button">%s</a>', esc_url( $button_url ), $button_target, $button_label ) )
		        . ( ( $close_button ) ? '<span id="' . $id . '_close"><img src="' . $this->plugins_url( 'assets/img/countdown-close.png' ) . '" title="' . __( 'Close Countdown', $this->get_textdomain() ) . '" alt="' . __( 'Close Countdown', $this->get_textdomain() ) . '" /></span>' : '' )
		        . '<div class="product_countdown_clear"></div>'
		        . '</div>'
		        . '</div>';


		if ( $return ) {
			return $html;
		}

		echo $html;

		return '';
	}


	/**
	 * Adds the countdown window to the footer
	 *
	 * @since 1.0
	 */
	public function wp_footer() {

		if ( ! $this->_countdown_active ) {
			return;
		}


		if ( is_home() ) {
			$homepage_product = $this->get_option( 'homepage_product' );
			if ( ! $homepage_product ) {
				return;
			}

			$product = $this->get_wc_product( $homepage_product );

			if ( ! $product instanceof WC_Product ) {
				return;
			}

			if ( method_exists( $product, 'get_id' ) ) {
				$post = get_post( $product->get_id() );
			} else {
				$post = $product->post;
			}

			$this->countdown_html( 'product_countdown', true, false, $post );
		} else {

			if ( 'normal' != $this->_countdown_usage ) {
				return;
			}

			$this->countdown_html();
		}

		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			return;
		}
		if ( ! (bool) $this->get_option( 'from_top_helper' ) ) {
			return;
		}

		echo '<div id="product_countdown_helper"><img src="' . $this->plugins_url( '/assets/img/countdown-helper.png' ) . '" alt="Countdown Helper" /><span>0</span></div>';
	}


	/**
	 * Upgrade function
	 *
	 * @since 1.0
	 */
	public function on_upgrade() {

		// check if the cron is set already. if not schedule the event
		if ( false === wp_get_schedule( 'wpbpc_hourly_event' ) ) {
			wp_schedule_event( time(), 'hourly', 'wpbpc_hourly_event' );
		}
	}


	/**
	 * Shows an error if WooCommerce is not yet installed correctly
	 *
	 * @since 1.0
	 */
	public function admin_notices() {

		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			return;
		}
		?>
		<div class="error">
			<p><?php _e( 'It seems that WooCommerce is not installed. Please activate WooCommerce to use the Product Countdown Plugin.', $this->get_textdomain() ); ?></p>
		</div>
		<?php
	}


	/**
	 * Adds the settings-metabox on the posts/pages/custom post types
	 *
	 * @since 1.0
	 */
	public function add_post_metaboxes() {

		// then read all post types
		$post_types_array = get_post_types( array( 'public' => true ), 'names' );

		// iterate through all post types and add metaboxes
		foreach ( $post_types_array as $post_type ) {

			// add metabox settings to the page-post-type
			add_meta_box( 'wpbpc-metabox-settings', __( 'Product Countdown Settings', $this->get_textdomain() ), array(
				&$this,
				'page_metabox',
			), $post_type, 'normal' );
		}
	}


	/**
	 * Prints the outpuot of the metabox for pages, posts and custom post types
	 *
	 * @param WP_Post $post
	 */
	public function page_metabox( $post ) {

		if ( 'product' == $post->post_type ) {
			$featured_product = $this->get_wc_product( $post->ID );
			$is_product_page  = true;
		} else {
			$featured_product = $this->get_wc_product( get_post_meta( $post->ID, '_wpbpc_product_id', true ) );
			$is_product_page  = false;
		}

		echo '<div class="options_group">';

		echo '<p>' . __( 'Choose if the countdown should be on or off on this page.', $this->get_textdomain() ) . '</p>';

		echo '<span class="wpbpc-data" data-allow-datetimepicker="' . intval( $this->get_option( 'datetimepicker' ) ) . '"></span>';

		echo '<p class="form-field"><label for="_wpbpc_countdown">' . __( 'Switch on/off', $this->get_textdomain() ) . ': </label><br />'
		     . '<select class="" name="_wpbpc_countdown" id="_wpbpc_countdown">'
		     . '<option value="0" ' . ( ( 0 == get_post_meta( $post->ID, '_wpbpc_countdown', true ) ) ? 'selected="selected"' : '' ) . '>' . __( 'OFF', $this->get_textdomain() ) . '</option>'
		     . '<option value="1" ' . ( ( 1 == get_post_meta( $post->ID, '_wpbpc_countdown', true ) ) ? 'selected="selected"' : '' ) . '>' . __( 'ON', $this->get_textdomain() ) . '</option>'
		     . '</select>'
		     . ( ( (bool) get_post_meta( $post->ID, '_wpbpc_email_sent', true ) ) ? ' <strong style="color: red;">' . __( 'An email has been sent because the countdown has expired.', $this->get_textdomain() ) . '</strong>' : '' )
		     . '</p>';

		echo '<div class="wpbpc-settings">';
		if ( $is_product_page ) {
			// only save the field to meta content
			echo '<input type="hidden" value="' . $post->ID . '" name="_wpbpc_product_id" />';
		} else {
			// show the select screen
			echo '<p class="form-field"><label for="wpbpc_product_id">' . __( 'Product to refer to', $this->get_textdomain() ) . ': </label><br />';

			printf(
				'<select type="text" data-ajax_action="wpbpc_search_products" data-ajax_nonce="%s" data-name="%s" class="wpbpc-select2" name="_wpbpc_product_id" id="wpbpc_product_id" data-noselection="%s"><option value="%s">%s</option></select>',
				wp_create_nonce( 'wpbpc_search_products' ),
				( $featured_product instanceof WC_Product ) ? esc_attr( $featured_product->post->post_title ) : 'No selection',
				( $featured_product instanceof WC_Product ) ? esc_attr( $featured_product->post->ID ) : 0,
				esc_attr( __( 'No selection', $this->get_textdomain() ) ),
				( $featured_product instanceof WC_Product ) ? esc_html( get_the_title( $featured_product->post->ID ) ) : ''
			);

			echo '</p>';
		}

		echo '<p class="form-field"><label for="_wpbpc_info_text">' . __( 'Info text. Use $price, $sales_price, $sold_items, $stock_items, $difference_in_percent and $sale_price_in_percent for a placeholder.', $this->get_textdomain() ) . ': </label><br />'
		     . '<input placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_info_text', $is_product_page ) . '" class="regular-text" id="_wpbpc_info_text" type="text" name="_wpbpc_info_text" value="' . get_post_meta( $post->ID, '_wpbpc_info_text', true ) . '" />'
		     . '</p>';

		$action = get_post_meta( $post->ID, '_wpbpc_action', true );

		echo '<p class="form-field"><label for="_wpbpc_action">' . __( 'Action after the countdown has expired', $this->get_textdomain() ) . ': </label><br />'
		     . '<select id="_wpbpc_action" type="text" name="_wpbpc_action">'
		     . '<option value="nothing">' . __( 'Nothing', $this->get_textdomain() ) . '</option>'
		     . '<option ' . ( 'email' == $action ? 'selected="selected"' : '' ) . ' value="email">' . __( 'Send E-Mail', $this->get_textdomain() ) . '</option>'
		     . '<option ' . ( 'redirect' == $action ? 'selected="selected"' : '' ) . 'value="redirect">' . __( 'Redirect', $this->get_textdomain() ) . '</option>'
		     . '<option ' . ( 'message' == $action ? 'selected="selected"' : '' ) . 'value="message">' . __( 'Message', $this->get_textdomain() ) . '</option>'
		     . '</select>'
		     . '<br /><span class="description">' . __( 'Please note that actions will not work for variable products because in this case one product can have multiple sales dates.', $this->get_textdomain() ) . '</span>'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_end_message">' . __( 'Message when countdown ends.', $this->get_textdomain() ) . ': </label><br />'
		     . '<input class="regular-text" placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_end_message', $is_product_page ) . '" id="_wpbpc_end_message" type="text" name="_wpbpc_end_message" value="' . esc_attr( get_post_meta( $post->ID, '_wpbpc_end_message', true ) ) . '" />'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_button_label">' . __( 'Button label', $this->get_textdomain() ) . ': </label><br />'
		     . '<input class="regular-text" placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_button_label', $is_product_page ) . '" id="_wpbpc_button_label" type="text" name="_wpbpc_button_label" value="' . esc_attr( get_post_meta( $post->ID, '_wpbpc_button_label', true ) ) . '" />'
		     . '</p>'
		     . '<span class="description">' . __( 'If a button should be displayed enter the label here. If no button should be displayed, leave the field empty.', $this->get_textdomain() ) . '</span>';

		echo '<p class="form-field"><label for="_wpbpc_button_url">' . __( 'Button URL', $this->get_textdomain() ) . ': </label><br />'
		     . '<input class="regular-text" placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_button_url', $is_product_page ) . '" id="_wpbpc_button_url" type="text" name="_wpbpc_button_url" value="' . esc_attr( get_post_meta( $post->ID, '_wpbpc_button_url', true ) ) . '" />'
		     . '</p>'
		     . '<span class="description">' . __( 'The URL where the button should link to. If this is empty, the button will be an add-to-cart button.', $this->get_textdomain() ) . '</span>';

		echo '<p class="form-field"><label for="_wpbpc_button_new_window">' . __( 'Open in new window', $this->get_textdomain() ) . ': </label><br />'
		     . '<input id="_wpbpc_button_new_window" type="checkbox" name="_wpbpc_button_new_window" value="1" ' . checked( (bool) get_post_meta( $post->ID, '_wpbpc_button_new_window', true ), true, false ) . ' />'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_redirect_url">' . __( 'Redirect URL', $this->get_textdomain() ) . ': </label><br />'
		     . '<input class="regular-text" placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_redirect_url', $is_product_page ) . '" id="_wpbpc_redirect_url" type="text" name="_wpbpc_redirect_url" value="' . get_post_meta( $post->ID, '_wpbpc_redirect_url', true ) . '" />'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_email">' . __( 'E-Mail', $this->get_textdomain() ) . ': </label><br />'
		     . '<input class="regular-text" placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_email', $is_product_page ) . '" id="_wpbpc_email" type="text" name="_wpbpc_email" value="' . get_post_meta( $post->ID, '_wpbpc_email', true ) . '" />'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_email_message">' . __( 'E-Mail Message', $this->get_textdomain() ) . ': </label><br />'
		     . '<textarea id="_wpbpc_email_message" placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_email_message', $is_product_page ) . '" name="_wpbpc_email_message" >' . esc_textarea( get_post_meta( $post->ID, '_wpbpc_email_message', true ) ) . '</textarea>'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_fade_in_at">' . __( 'Fade in at position', $this->get_textdomain() ) . ': </label><br />'
		     . '<input placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_fade_in_at', $is_product_page ) . '" class="small-text" id="_wpbpc_fade_in_at" type="text" name="_wpbpc_fade_in_at" value="' . get_post_meta( $post->ID, '_wpbpc_fade_in_at', true ) . '" /> px'
		     . '</p>';

		$background_color = get_post_meta( $post->ID, '_wpbpc_background_color', true );

		echo '<p class="form-field"><label for="_wpbpc_background_color">' . __( 'Background color', $this->get_textdomain() ) . ': </label><br />'
		     . '<input placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_background_color', $is_product_page ) . '" class="small-text" id="_wpbpc_background_color" type="text" name="_wpbpc_background_color" data-standard-value="#FF3333" value="' . $background_color . '" />'
		     . '</p>';

		$font_color = get_post_meta( $post->ID, '_wpbpc_font_color', true );

		echo '<p class="form-field"><label for="_wpbpc_font_color">' . __( 'Font color', $this->get_textdomain() ) . ': </label><br />'
		     . '<input placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_font_color', $is_product_page ) . '" class="small-text" id="_wpbpc_font_color" type="text" name="_wpbpc_font_color" data-standard-value="#fff" value="' . $font_color . '" />'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_display_from_top">' . __( 'Fade in from top', $this->get_textdomain() ) . ': </label><br />'
		     . '<input id="_wpbpc_display_from_top" type="checkbox" name="_wpbpc_display_from_top" value="1" ' . ( ( (bool) get_post_meta( $post->ID, '_wpbpc_display_from_top', true ) ) ? 'checked="checked"' : '' ) . ' />'
		     . '</p>';

		echo '<p class="form-field"><label for="_wpbpc_custom_css">' . __( 'Custom CSS', $this->get_textdomain() ) . ': </label><br />'
		     . '<textarea placeholder="' . $this->get_setting_value( $post->ID, '_wpbpc_custom_css', $is_product_page ) . '" id="_wpbpc_custom_css"  name="_wpbpc_custom_css" >' . esc_textarea( get_post_meta( $post->ID, '_wpbpc_custom_css', true ) ) . '</textarea>'
		     . '</p>'
		     . '<p class="description"><a href="' . admin_url( 'admin.php?page=woocommerce_wpbpc' ) . '" target="_blank">' . __( 'Click here to edit the global CSS styles', $this->get_textdomain() ) . '</a></p>';

		echo '</div>'; # /.wpbpc-settings
		echo '</div>';
	}

	/**
	 * Saves the post fields of the metaboxes
	 *
	 * @since 1.0
	 *
	 * @param int $post_id
	 *
	 * @return int $post_id
	 */
	public function save_post( $post_id ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( isset( $_POST['_wpbpc_product_id'] ) ) {
			update_post_meta( $post_id, '_wpbpc_product_id', intval( $_POST['_wpbpc_product_id'] ) );
		}

		if ( isset( $_POST['_wpbpc_countdown'] ) ) {
			update_post_meta( $post_id, '_wpbpc_countdown', intval( $_POST['_wpbpc_countdown'] ) );

			if ( (bool) $_POST['_wpbpc_countdown'] ) {
				update_post_meta( $post_id, '_wpbpc_email_sent', 0 );
			}
		}

		if ( isset( $_POST['_wpbpc_info_text'] ) ) {
			update_post_meta( $post_id, '_wpbpc_info_text', sanitize_text_field( $_POST['_wpbpc_info_text'] ) );
		}

		if ( isset( $_POST['_wpbpc_fade_in_at'] ) ) {
			update_post_meta( $post_id, '_wpbpc_fade_in_at', sanitize_text_field( $_POST['_wpbpc_fade_in_at'] ) );
		}

		if ( isset( $_POST['_wpbpc_background_color'] ) ) {
			update_post_meta( $post_id, '_wpbpc_background_color', sanitize_text_field( $_POST['_wpbpc_background_color'] ) );
		}

		if ( isset( $_POST['_wpbpc_font_color'] ) ) {
			update_post_meta( $post_id, '_wpbpc_font_color', sanitize_text_field( $_POST['_wpbpc_font_color'] ) );
		}

		if ( isset( $_POST['_wpbpc_custom_css'] ) ) {
			update_post_meta( $post_id, '_wpbpc_custom_css', $_POST['_wpbpc_custom_css'] );
		}

		if ( isset( $_POST['_wpbpc_action'] ) ) {
			update_post_meta( $post_id, '_wpbpc_action', $_POST['_wpbpc_action'] );
		}

		if ( isset( $_POST['_wpbpc_redirect_url'] ) ) {
			update_post_meta( $post_id, '_wpbpc_redirect_url', sanitize_text_field( $_POST['_wpbpc_redirect_url'] ) );
		}

		if ( isset( $_POST['_wpbpc_email'] ) ) {
			update_post_meta( $post_id, '_wpbpc_email', sanitize_email( $_POST['_wpbpc_email'] ) );
		}

		if ( isset( $_POST['_wpbpc_email_message'] ) ) {
			update_post_meta( $post_id, '_wpbpc_email_message', $_POST['_wpbpc_email_message'] );
		}

		if ( isset( $_POST['_wpbpc_end_message'] ) ) {
			update_post_meta( $post_id, '_wpbpc_end_message', sanitize_text_field( $_POST['_wpbpc_end_message'] ) );
		}

		if ( isset( $_POST['_wpbpc_button_label'] ) ) {
			update_post_meta( $post_id, '_wpbpc_button_label', sanitize_text_field( $_POST['_wpbpc_button_label'] ) );
		}

		if ( isset( $_POST['_wpbpc_button_url'] ) ) {
			update_post_meta( $post_id, '_wpbpc_button_url', esc_url( sanitize_text_field( $_POST['_wpbpc_button_url'] ) ) );
		}

		if ( isset( $_POST['_wpbpc_display_from_top'] ) ) {
			update_post_meta( $post_id, '_wpbpc_display_from_top', 1 );
		} else {
			update_post_meta( $post_id, '_wpbpc_display_from_top', 0 );
		}

		if ( isset( $_POST['_wpbpc_button_new_window'] ) ) {
			update_post_meta( $post_id, '_wpbpc_button_new_window', absint( $_POST['_wpbpc_button_new_window'] ) );
		} else {
			update_post_meta( $post_id, '_wpbpc_button_new_window', 0 );
		}

		return $post_id;
	}


	/**
	 * Checks if the countdown is active on the current page
	 *
	 * @since 1.0
	 * @global WP_Post $post
	 * @use   WC_Product
	 * @return bool
	 */
	public function is_countdown_active() {

		$filter = apply_filters_ref_array( 'wpbpc_is_countdown_active_pre', array( null, &$this ) );
		if ( ! is_null( $filter ) ) {
			$this->_countdown_active = $filter;

			return $filter;
		}

		if ( is_home() ) {
			$homepage_product = $this->get_option( 'homepage_product' );
			$main_product     = $this->get_wc_product( $homepage_product );

			if ( ! $main_product instanceof WC_Product ) {
				$this->_countdown_active = false;

				return false;
			}

			$main_product->get_data();

			if ( method_exists( $main_product, 'get_id' ) ) {
				$post = get_post( $main_product->get_id() );
			} else {
				$post = $main_product->post;
			}

		} else {
			if ( ! is_singular() ) {
				$this->_countdown_active = false;

				return false;
			}

			/**
			 * Reset the query to the original query so that the post is the right one
			 */
			wp_reset_query();

			global $post;
			if ( ! $post instanceof WP_Post ) {
				$this->_countdown_active = false;

				return false;
			}

			// get the product
			$main_product = $this->get_wc_product( get_post_meta( $post->ID, '_wpbpc_product_id', true ) );

			if ( ! $main_product instanceof WC_Product ) {
				$this->_countdown_active = false;

				return false;
			}
		}

		if ( ! (bool) get_post_meta( $post->ID, '_wpbpc_countdown', true ) ) {
			$this->_countdown_active = false;

			return false;
		}

		/**
		 * Check if this is a variable product, if yes run through all variable products and search for at least one product that is on sale
		 */
		if ( $main_product->is_type( 'variable' ) ) {

			/**
			 * @var WC_Product_Variable $main_product
			 */

			// check if product is on sale
			if ( ! $main_product->is_on_sale() ) {
				$this->_countdown_active = false;

				return false;
			}

			$variation_on_sale  = false;
			$variation_dates_to = array();

			/**
			 * @var array                $product_variation_array
			 * @var WC_Product_Variation $product_variation
			 */
			foreach ( $main_product->get_available_variations() as $product_variation_array ) {
				$product_variation = $this->get_wc_product( $product_variation_array['variation_id'] );

				if ( ! $product_variation instanceof WC_Product ) {
					continue;
				}

				if ( $product_variation->is_on_sale() ) {
					$variation_on_sale                                              = true;
					$variation_dates_to[ $product_variation_array['variation_id'] ] = isset( $product_variation_array['date_on_sale_to'] ) && is_a( $product_variation_array['date_on_sale_to'], 'WC_DateTime' ) ? intval( $product_variation_array['date_on_sale_to']->date( 'U' ) ) : 0;
				}
			}


			if ( ! $variation_on_sale ) {
				$this->_countdown_active = false;

				return false;
			}

			/**
			 * Change main product
			 */
			arsort( $variation_dates_to );
			$main_product_post_id = key( $variation_dates_to );
			$main_product         = wc_get_product( $main_product_post_id );

		} else {

			/**
			 * @var WC_Product $main_product
			 */

			// check if product is on sale
			if ( ! $main_product->is_on_sale() ) {
				$this->_countdown_active = false;

				return false;
			}

			$main_product_post_id = method_exists( $main_product, 'get_id' ) ? $main_product->get_id() : $main_product->post->ID;

		}

		// check if stock management is ON
		if ( $main_product->managing_stock() ) {
			if ( ! $main_product->is_in_stock() ) {
				$this->_countdown_active = false;

				return false;
			}
		}

		if ( method_exists( $main_product, 'get_date_on_sale_from' ) ) {
			$dates_from = $main_product->get_date_on_sale_from( 'object' );
			$dates_from = is_a( $dates_from, 'WC_DateTime' ) ? $dates_from->date( 'U' ) : 0;
		} else {
			$dates_from = get_post_meta( $main_product_post_id, '_sale_price_dates_from', true );
		}

		/**
		 * Date correction: WooCommerce saves dates in GMT/UTC format (GMT+0)
		 * We have to correct the timestamp with the GMT offset that WordPress uses
		 */
		$dates_from -= + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );

		$this->_dates_from = $dates_from;

		if ( empty( $dates_from ) ) {
			$this->_countdown_active = false;

			return false;
		}

		if ( method_exists( $main_product, 'get_date_on_sale_to' ) ) {
			$dates_to = $main_product->get_date_on_sale_to( 'object' );
			$dates_to = is_a( $dates_to, 'WC_DateTime' ) ? $dates_to->date( 'U' ) : 0;
		} else {
			$dates_to = get_post_meta( $main_product_post_id, '_sale_price_dates_to', true );
		}

		/**
		 * Date correction: WooCommerce saves dates in GMT/UTC format (GMT+0)
		 * We have to correct the timestamp with the GMT offset that WordPress uses
		 */
		$dates_to -= + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );

		$this->_dates_to = $dates_to;

		if ( empty( $dates_to ) ) {
			$this->_countdown_active = false;

			return false;
		}

		# show the popup even when we're over time
		if ( 'message' != $this->get_setting_value( $main_product_post_id, '_wpbpc_action' ) ) {
			$now = current_time( 'timestamp', true ); # second parameter is true because we're working with UTC/GMT here

			if ( $now > $dates_to ) {
				$this->_countdown_active = false;

				return false;
			}
			if ( $now < $dates_from ) {
				$this->_countdown_active = false;

				return false;
			}
		} else {
			$this->_msg_after_timeout = true;
		}

		$this->_countdown_active = true;

		// add it if there is a shortcode in the content
		if ( false !== stripos( $post->post_content, '[product_countdown' ) OR
		     false !== stripos( $post->post_excerpt, '[product_countdown' )
		) {
			$this->_countdown_usage = 'shortcode';
		} else {
			$this->_countdown_usage = 'normal';
		}

		$filter = apply_filters_ref_array( 'wpbpc_is_countdown_active', array( true, &$this ) );

		return $filter;
	}


	/**
	 * Adds CSS to the header to add more space to the footer so that the footer-area is still visible correctly
	 */
	public function wp_head() {

		if ( ! $this->_countdown_active ) {
			return;
		}
		?>
		<style type="text/css">
			/* Product Countdown Styles */
			body {
				margin-bottom: 120px;
			}

			<?php

			echo wp_unslash($this->get_option('global_css'));

			global $post;
			if( $post instanceof WP_Post ) {

				// custom background color
				$background_color = $this->get_setting_value($post->ID, '_wpbpc_background_color');

				if( ! empty( $background_color ) AND '#FF3333' != $background_color ) {
					$is_bg_color = true;
					$darken = $this->color_darken( $background_color, 20 );
					echo '#product_countdown, #product_countdown_shortcode {'
						. 'background-color: mix(' . $background_color . ', ' . $darken . ', 60%);'
						. 'background-image: -moz-linear-gradient(top, ' . $background_color . ', ' . $darken . ');'
						. 'background-image: -webkit-gradient(linear, 0 0, 0 100%, from(' . $background_color . '), to(' . $darken . '));'
						. 'background-image: -webkit-linear-gradient(top, ' . $background_color . ', ' . $darken . ');'
						. 'background-image: -o-linear-gradient(top, ' . $background_color . ', ' . $darken . ');'
						. 'background-image: linear-gradient(to bottom, ' . $background_color . ', ' . $darken . ');'
						. 'background-repeat: repeat-x;'
						. 'filter: e(%("progid:DXImageTransform.Microsoft.gradient(startColorstr=\'%d\', endColorstr=\'%d\', GradientType=0)",argb(' . $background_color . '), argb(' . $darken . '))); '
						. '}';
				}

				// custom font color
				$font_color = $this->get_setting_value($post->ID, '_wpbpc_font_color');

				if( ! empty( $font_color ) AND ('#fff' != $font_color OR '#ffffff' == $font_color ) ) {
					$is_font_color = true;
					echo '#product_countdown .product_countdown_info_text, #product_countdown_shortcode .product_countdown_info_text, #product_countdown .product_countdown_end_message, #product_countdown_shortcode .product_countdown_end_message {'
						. 'color: '. $font_color .';'
						. '}';
				}

				if(isset($is_bg_color) && isset($is_font_color)){
					echo '#product_countdown .product_countdown_button, #product_countdown_shortcode .product_countdown_button { '
					.'border-color: '.$font_color.'; background-color: '.$font_color.'; color: '.$background_color.';'
					.'}';
				}

				// custom css
				echo $this->get_setting_value($post->ID, '_wpbpc_custom_css');
			}

			?>
		</style>
		<?php
	}


	/**
	 * Darkens a hex value
	 *
	 * @since 2.0
	 */
	private function color_darken( $hex, $steps ) {

		$steps = max( - 255, min( 255, $steps ) );

		// Format the hex color string
		$hex = str_replace( '#', '', $hex );
		if ( strlen( $hex ) == 3 ) {
			$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr( $hex, 2, 1 ), 2 );
		}

		// Get decimal values
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );

		// Adjust number of steps and keep it inside 0 to 255
		$r = max( 0, min( 255, $r + $steps ) );
		$g = max( 0, min( 255, $g + $steps ) );
		$b = max( 0, min( 255, $b + $steps ) );

		$r_hex = str_pad( dechex( $r ), 2, '0', STR_PAD_LEFT );
		$g_hex = str_pad( dechex( $g ), 2, '0', STR_PAD_LEFT );
		$b_hex = str_pad( dechex( $b ), 2, '0', STR_PAD_LEFT );

		return '#' . $r_hex . $g_hex . $b_hex;
	}


	/**
	 * Adds CSS to the edit screen
	 *
	 * @since 1.0
	 */
	public function admin_head_posts() {

		?>
		<style type="text/css">
			/* Product Countdown Styles */
			#_wpbpc_info_text,
			#_wpbpc_redirect_url,
			#_wpbpc_email {
				width: 500px;
			}

			#_wpbpc_background_color,
			#_wpbpc_font_color {
				width: 70px;
			}

			#_wpbpc_fade_in_at {
				width: 50px;
			}

			#_wpbpc_background_color + input.button,
			#_wpbpc_font_color + input.button {
				width: auto;
			}

			#_wpbpc_display_from_top {
				width: auto;
			}

			.wpbpc_ui_slider {
				top: 0 !important;
				width: 100%;
				background: none repeat scroll 0 0 transparent;
				height: 100%;
				border: 0 none !important;
			}

		</style>
		<?php
	}


	/**
	 * Sends an email if the countdown has expired
	 *
	 * @since 3.0
	 */
	public function send_mail() {

		$posts = get_posts( array(
			'posts_per_page' => - 1,
			'post_type'      => 'any',
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => '_wpbpc_countdown',
					'value'   => 1,
					'compare' => '=',
				),
				array(
					'key'     => '_wpbpc_action',
					'value'   => 'email',
					'compare' => '=',
				),
				array(
					'key'     => '_wpbpc_email',
					'value'   => '',
					'compare' => '!=',
				),
				array(
					'key'     => '_wpbpc_email_message',
					'value'   => '',
					'compare' => '!=',
				),
				array(
					'key'     => '_wpbpc_email_sent',
					'value'   => '1',
					'compare' => '!=',
				),
			),
		) );

		if ( count( $posts ) <= 0 ) {
			return;
		}

		/**
		 * @var WP_post $post
		 */
		foreach ( $posts as $post ) {

			$parent_post_id = get_post_meta( $post->ID, '_wpbpc_product_id', true );

			$product = $this->get_wc_product( $parent_post_id );

			if ( ! $product instanceof WC_Product ) {
				continue;
			}

			// do not send mails for variable products because there can be more than one sales date
			if ( $product->is_type( 'variable' ) ) {
				continue;
			}

			if ( method_exists( $product, 'get_date_on_sale_to' ) ) {
				$dates_to = $product->get_date_on_sale_to( 'object' );
				$dates_to = is_a( $dates_to, 'WC_DateTime' ) ? $dates_to->date( 'U' ) : 0;
			} else {
				$dates_to = get_post_meta( $parent_post_id, '_sale_price_dates_to', true );
			}

			if ( empty( $dates_to ) ) {
				continue;
			}

			if ( current_time( 'timestamp' ) < $dates_to ) {
				continue;
			}

			$email = sanitize_email( $this->get_setting_value( $post->ID, '_wpbpc_email' ) );
			if ( empty( $email ) ) {
				continue;
			}

			$email_message = $this->get_setting_value( $post->ID, '_wpbpc_email_message' );
			if ( empty( $email_message ) ) {
				continue;
			}

			$email_message .= chr( 10 ) . chr( 10 ) . sprintf( __( 'This is the URL to the countdown page: %s', $this->get_textdomain() ), get_permalink( $post->ID ) );

			if ( $parent_post_id != $post->ID ) {
				$email_message .= chr( 10 ) . sprintf( __( 'This is the URL to the product page: %s', $this->get_textdomain() ), get_permalink( $parent_post_id ) );
			}

			if ( wp_mail( $email, __( 'Sales Price has expired', $this->get_textdomain() ), $email_message ) ) {
				update_post_meta( $post->ID, '_wpbpc_email_sent', 1 );
				//update_post_meta( $post->ID, '_wpbpc_countdown', 0 );
			}
		}
	}


	/**
	 * Redirects a user if this is set up on the settings page
	 *
	 * @since 3.0
	 * @global WP_Post $post
	 */
	public function redirect_on_countdown_end() {

		/**
		 * Reset the query to the original query so that the post is the right one
		 */
		wp_reset_query();

		if ( is_singular() ) {
			// this is a singular page call
			global $post;
			if ( ! $post instanceof WP_Post ) {
				return;
			}

			// stop if countdown is not active
			if ( ! (bool) get_post_meta( $post->ID, '_wpbpc_countdown', true ) ) {
				return;
			}

			// stop if the action is not 'redirect'
			if ( 'redirect' != get_post_meta( $post->ID, '_wpbpc_action', true ) ) {
				return;
			}

			$parent_post_id = get_post_meta( $post->ID, '_wpbpc_product_id', true );

			$product = $this->get_wc_product( $parent_post_id );
		} elseif ( is_home() ) {
			// this is the homepage and the homepage product was set up

			$homepage_product = $this->get_option( 'homepage_product' );

			// stop here if the homepage product was not set up
			if ( empty( $homepage_product ) ) {
				return;
			}

			$product = $this->get_wc_product( $homepage_product );
		}

		if ( ! isset( $product ) ) {
			return;
		}

		if ( ! $product instanceof WC_Product ) {
			return;
		}

		// on homepages we have to set the post to the product post
		if ( is_home() ) {
			if ( method_exists( $product, 'get_id' ) ) {
				$post = get_post( $product->get_id() );
			} else {
				$post = $product->post;
			}
		}


		// do not redirect if this is a variable product
		if ( $product->is_type( 'variable' ) ) {
			return;
		}

		$redirect_url = $this->get_setting_value( $post->ID, '_wpbpc_redirect_url' );

		// stop if there is no redirect URL
		if ( empty( $redirect_url ) ) {
			return;
		}

		// stop when the rest countdown time is higher 0
		if ( $this->rest_countdown_time( $post->ID ) > 0 ) {
			return;
		}

		wp_redirect( $redirect_url );
		die();
	}


	/**
	 * Returns the time in seconds
	 *
	 * @param null|int $post_id
	 *
	 * @return int Returns 0 on error or the substraction of the to-date and the current time (in seconds)
	 */
	public function rest_countdown_time( $post_id = null ) {

		if ( is_null( $post_id ) ) {

			/**
			 * Reset the query to the original query so that the post is the right one
			 */
			wp_reset_query();

			global $post;
			if ( ! $post instanceof WP_Post ) {
				return 0;
			}
			$post_id = $post->ID;
		}

		// get the product
		$product = $this->get_wc_product( get_post_meta( $post_id, '_wpbpc_product_id', true ) );

		// stop if there is no product
		if ( ! $product instanceof WC_Product ) {
			return 0;
		}

		$product_post_id = method_exists( $product, 'get_id' ) ? $product->get_id() : $product->post->ID;

		if ( method_exists( $product, 'get_date_on_sale_to' ) ) {
			$dates_to = $product->get_date_on_sale_to( 'object' );
			$dates_to = is_a( $dates_to, 'WC_DateTime' ) ? $dates_to->date( 'U' ) : 0;
		} else {
			$dates_to = get_post_meta( $product_post_id, '_sale_price_dates_to', true );
		}

		// stop if there is no 'to' date
		if ( empty( $dates_to ) ) {
			return 0;
		}

		return $dates_to - current_time( 'timestamp' );

	}


	/**
	 * Sends an email if the countdown has expired
	 *
	 * @since 3.0
	 */
	public function ajax_sendmail() {

		if ( ! defined( 'DOING_AJAX' ) ) {
			return;
		}
		if ( ! isset( $_REQUEST['action'] ) ) {
			return;
		}
		if ( ! isset( $_REQUEST['post_id'] ) ) {
			return;
		}
		if ( 'wpbpc_ajax_sendmail' != $_REQUEST['action'] ) {
			return;
		}

		$post_id = intval( $_REQUEST['post_id'] );

		if ( ! $post_id ) {
			return;
		}

		$this->send_mail();
	}


	/**
	 * Adds the shortcodes
	 *
	 * @since 3.0
	 */
	public function add_shortcodes() {

		add_shortcode( 'product_countdown', array( &$this, 'shortcode' ) );
	}


	/**
	 * @since 3.0
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	public function shortcode( $atts, $content ) {

		// $atts = shortcode_atts( array(), $atts );

		if ( $this->_countdown_active ) {
			return $this->countdown_html( 'product_countdown_shortcode', false, true );
		}

		return '';
	}

	/**
	 * @since 3.1
	 *
	 * @param array                $variation
	 * @param WC_Product_Variable  $product_variable
	 * @param WC_Product_Variation $variation_product
	 *
	 * @return array
	 */
	public function woocommerce_available_variation( $variation, $product_variable, $variation_product ) {

		$main_product_post_id = $product_variable->get_id();

		$sale_price = get_post_meta( $variation['variation_id'], '_sale_price', true );

		if ( method_exists( $variation_product, 'get_date_on_sale_to' ) ) {
			$dates_to = $variation_product->get_date_on_sale_to( 'object' );
			$dates_to = is_a( $dates_to, 'WC_DateTime' ) ? $dates_to->date( 'U' ) : 0;
		} else {
			$dates_to = get_post_meta( $variation['variation_id'], '_sale_price_dates_to', true );
		}

		if ( empty( $dates_to ) ) {
			$on_sale = false;
		} else {
			$now     = current_time( 'timestamp' );
			$on_sale = $dates_to > $now;
		}

		$regular_price = get_post_meta( $variation['variation_id'], '_regular_price', true );

		$info_text = $this->get_setting_value( $main_product_post_id, '_wpbpc_info_text' );

		$difference_in_percent = 0;
		$sale_price_in_percent = 0;

		if ( 0 != $this->wc_format_decimal( $regular_price ) ) {
			$difference_in_percent = round( ( $this->wc_format_decimal( $regular_price ) - $this->wc_format_decimal( $sale_price ) ) / $this->wc_format_decimal( $regular_price ) * 100 );
			$sale_price_in_percent = round( $this->wc_format_decimal( $sale_price ) * 100 / $this->wc_format_decimal( $regular_price ) );
		}

		// currency symbol decode
		add_filter( 'woocommerce_currency_symbol', 'wpbpc_currency_symbol_decode' );

		// replace variables
		$info_text = str_replace( '$price', '<span class="wpbpc_price">' . $this->wc_price( $regular_price ) . '</span>', $info_text );
		$info_text = str_replace( '$sales_price', '<span class="wpbpc_sales_price">' . $this->wc_price( $sale_price ) . '</span>', $info_text );
		$info_text = str_replace( '$sold_items', '<span class="wpbpc_sold_items">' . get_post_meta( $main_product_post_id, 'total_sales', true ) . '</span>', $info_text );
		$info_text = str_replace( '$difference_in_percent', '<span class="wpbpc_difference_in_percent">' . $difference_in_percent . '%</span>', $info_text );
		$info_text = str_replace( '$sale_price_in_percent', '<span class="wpbpc_sale_price_in_percent">' . $sale_price_in_percent . '%</span>', $info_text );

		// number of stock items in the variation
		$stock = get_post_meta( $variation['variation_id'], '_stock', true );
		if ( $variation_product->is_in_stock() ) {
			// if empty get the number of stock items from the parent
			if ( '' == $stock ) {
				$stock = get_post_meta( $main_product_post_id, '_stock', true );
			}
			$info_text = str_replace( '$stock_items', '<span class="wpbpc_stock_items">' . $stock . '</span>', $info_text );
		} else {
			$info_text = str_replace( '$stock_items', '', $info_text );
		}

		$variation['wpbpc_sales_price']         = strip_tags( $this->wc_price( $sale_price ) );
		$variation['wpbpc_price']               = strip_tags( $this->wc_price( $regular_price ) );
		$variation['wpbpc_stock']               = $stock;
		$variation['wpbpc_is_on_sale']          = $on_sale;
		$variation['wpbpc_not_on_sale_message'] = __( 'This product variation is not on sale!', $this->get_textdomain() );
		$variation['wpbpc_info_text']           = $info_text;

		if ( $on_sale ) {
			$variation['wpbpc_timer'] = array(
				'until_seconds' => ( date_i18n( 'U', $dates_to ) - current_time( 'timestamp' ) ) . 's',
			);
		}

		remove_filter( 'woocommerce_currency_symbol', 'wpbpc_currency_symbol_decode' );

		return $variation;
	}

	/**
	 * Returns json formatted list of products
	 *
	 * @since 3.1.5
	 */
	public function ajax_search_products() {

		if ( ! isset( $_REQUEST['term'] ) ) {
			wp_send_json_error();
		}
		if ( ! isset( $_REQUEST['_wpb_nonce_name'] ) ) {
			wp_send_json_error();
		}

		if ( ! wp_verify_nonce( $_REQUEST['_wpb_nonce_name'], 'wpbpc_search_products' ) ) {
			wp_send_json_error();
		}

		global $wpdb;

		if ( ! $wpdb instanceof wpdb ) {
			wp_send_json_error();
		}

		$results = $wpdb->get_results( 'SELECT post_title, ID, post_type FROM `' . $wpdb->posts . '` WHERE post_title LIKE "%' . sanitize_text_field( $_REQUEST['term'] ) . '%" AND post_status="publish" AND post_type="product"' );

		$postlist = array();

		foreach ( $results as $post ) {
			$postlist[] = array(
				'id'   => (int) $post->ID,
				'text' => '#' . $post->ID . ' - ' . $post->post_title,
			);
		}

		wp_send_json_success( $postlist );

	}


	/**
	 * Requests the timestamp of the blog
	 *
	 * @since 3.5.1
	 */
	public function ajax_get_timestamp() {

		wp_send_json_success( array( 'timestamp' => current_time( 'timestamp' ) ) );
	}


	/**
	 * Returns a WooCommerce product.
	 *
	 * @since 3.7.1
	 *
	 * @param mixed $the_product Post object or post ID of the product.
	 * @param array $args (default: array()) Contains all arguments to be used to get this product.
	 *
	 * @return WC_Product|bool
	 */
	public static function get_wc_product( $the_product = false, $args = array() ) {

		if ( function_exists( 'wc_get_product' ) ) {
			return wc_get_product( $the_product, $args );
		}

		if ( function_exists( 'get_product' ) ) {
			return get_product( $the_product, $args );
		}

		return false;
	}

	/**
	 * Format the price with a currency symbol.
	 *
	 * @since  3.7.1
	 * @access public
	 *
	 * @param float $price
	 * @param array $args (default: array())
	 *
	 * @return string
	 */
	public static function wc_price( $price, $args = array() ) {

		if ( function_exists( 'wc_price' ) ) {
			return wc_price( $price, $args );
		}

		if ( function_exists( 'woocommerce_price' ) ) {
			return woocommerce_price( $price, $args );
		}

		return $price;
	}

	/**
	 * Format decimal numbers ready for DB storage
	 *
	 * Sanitize, remove locale formatting, and optionally round + trim off zeros
	 *
	 * @since 3.7.1
	 *
	 * @param  float|string $number Expects either a float or a string with a decimal separator only (no thousands)
	 * @param  mixed        $dp number of decimal points to use, blank to use woocommerce_price_num_decimals,
	 *                                  or false to avoid all rounding.
	 * @param  boolean      $trim_zeros from end of string
	 *
	 * @return string
	 */
	function wc_format_decimal( $number, $dp = false, $trim_zeros = false ) {

		if ( function_exists( 'wc_format_decimal' ) ) {
			return wc_format_decimal( $number, $dp, $trim_zeros );
		}

		if ( function_exists( 'wc_format_decimal' ) ) {
			woocommerce_format_total( $number );
		}

		return $number;
	}

}


function wpbpc_currency_symbol_decode( $currency_symbol ) {

	$currency_symbol = html_entity_decode( $currency_symbol );

	return $currency_symbol;
}
