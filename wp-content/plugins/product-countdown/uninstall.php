<?php
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

if ( ! WP_UNINSTALL_PLUGIN ) {
	exit();
}

/**
 * @var wpdb $wpdb
 */
global $wpdb;

if ( is_a( $wpdb, 'wpdb' ) ) {
	// Deletes user meta stuff (like closed meta boxes, etc.)
	$wpdb->prepare( 'DELETE FROM ' . $wpdb->postmeta. ' WHERE meta_key LIKE %s',  '_wpbpc_%' );
}

delete_option( 'wpb_plugin_product-countdown_version' );
delete_option( 'wpbp_u_product-countdown' );

