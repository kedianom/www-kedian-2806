<?php
/*
Plugin Name: Product Countdown for WooCommerce by WP-Buddy
Plugin URI: http://wp-buddy.com/products/plugins/product-countdown-wordpress-plugin-for-woocommerce/
Description: Displays a countdown for WooCommerce products that are on sale
Version: 4.2.0
Author: wp-buddy
Author URI: http://wp-buddy.com
Text Domain: product-countdown
*/
/*  Copyright 2013  WP-Buddy  (email : info@wp-buddy.com)
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 *
 * PHP Version check.
 *
 */
if ( version_compare( PHP_VERSION, '5.6.0', '<' ) ) {
	add_action( 'admin_notices', 'wpbpc_old_php_notice' );

	function wpbpc_old_php_notice() {

		printf(
			'<div class="notice notice-warning notice-alt"><p>%s</p></div>',
			sprintf(
				__( 'Hey mate! Sorry for interrupting you. It seem\'s that you\'re using an old PHP version (your current version is %s). You should upgrade to at least 5.6.0 or higher in order to use the Countdown plugin. Thank you!', 'product-countdown' ),
				esc_html( PHP_VERSION )
			)
		);
	}

	# sorry. The plugin will not work with an old PHP version.
	return;
}

/**
 * The autoloader class
 *
 * @param string $class_name
 *
 * @return bool
 * @since 1.0
 */
function wpbpc_autoloader( $class_name ) {

	$file = trailingslashit( dirname( __FILE__ ) ) . 'classes/' . strtolower( $class_name ) . '.php';
	if ( is_file( $file ) ) {
		require_once( $file );

		return true;
	}

	$file = trailingslashit( dirname( __FILE__ ) ) . 'classes/widgets/' . strtolower( $class_name ) . '.php';
	if ( is_file( $file ) ) {
		require_once( $file );

		return true;
	}

	return false;
}


// registering the autoloader function
try {
	spl_autoload_register( 'wpbpc_autoloader', true );
} catch ( Exception $e ) {
	function __autoload( $class_name ) {

		wpbpc_autoloader( $class_name );
	}
}

function wpbpc_init() {

	if ( ! function_exists( 'WC' ) ) {
		return;
	}

	if ( version_compare( WC()->version, '3.0.0', '>=' ) ) {
		$GLOBALS['wpb_product_countdown'] = new WPB_Product_Countdown( __FILE__ );

		# WooCommerce Timer extension
		if ( true === apply_filters( 'wpbpc_ext_load_time', true ) ) {
			require_once __DIR__ . '/extensions/woocommerce-time.php';
		}
	} else {

		add_action( 'admin_notices', function () {

			printf(
				'<div class="notice notice-warning notice-alt"><p>%s</p></div>',
				__( 'Hey mate! Sorry to bother you. Unfortunately you are using WooCommerce in a version lower than 3. Due to heavy changes in WooCommerce 3 and up the Countdown Plugin is no longer supporting older versions. Please update WooCommerce in order to use this plugin.', 'product-countdown' )
			);
		} );
	}

}

add_action( 'plugins_loaded', 'wpbpc_init' );

