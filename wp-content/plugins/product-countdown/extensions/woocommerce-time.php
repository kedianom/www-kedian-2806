<?php

namespace wpbuddy\product_countdown\woocommerce\time;

use WurflCache\Exception;

if ( ! defined( 'ABSPATH' ) ) {
	die();
}

add_action( 'woocommerce_product_options_pricing', '\wpbuddy\product_countdown\woocommerce\time\options_pricing' );
add_action( 'woocommerce_variation_options_pricing', '\wpbuddy\product_countdown\woocommerce\time\options_pricing' );

/**
 * Hook into schedule dates and add times, too.
 *
 * @note The parameters maybe not set due to the action hook involved.
 *
 * @since 5.0.0
 *
 * @param int           $loop
 * @param array         $variation_data
 * @param \WP_Post|null $variation
 */
function options_pricing( $loop = 0, $variation_data = array(), $variation = null ) {

	if ( is_a( $variation, '\WP_Post' ) ) {
		$post = $variation;
	} else {
		global $post;
	}

	$product_object = wc_get_product( $post );

	if ( is_a( $product_object, '\WC_Product_Variable' ) ) {
		$children = $product_object->get_children();
		if ( isset( $children[ $loop ] ) ) {
			$product_object = wc_get_product( $children[ $loop ] );
		}
	}

	$sale_price_time_from = ( $product_object->get_date_on_sale_from( 'edit' ) && $date = $product_object->get_date_on_sale_from( 'edit' )->getOffsetTimestamp() ) ? date_i18n( 'H:i', $date ) : '';
	$sale_price_time_to   = $product_object->get_date_on_sale_to( 'edit' ) && ( $date = $product_object->get_date_on_sale_to( 'edit' )->getOffsetTimestamp() ) ? date_i18n( 'H:i', $date ) : '';

	?>
	<p class="form-field sale_price_times_fields hidden">
		<?php
		if ( is_int( $loop ) ) {
			echo '<span class="form-row form-row-first">';
			printf(
				'<label for="_sale_price_times_from">%s</label>',
				__( 'Sale start time', 'product-countdown' )
			);
		} else {
			printf(
				'<label for="_sale_price_times_from">%s</label>',
				__( 'Sale price times', 'product-countdown' )
			);
		}
		?>
		<input type="text" class="short"
		       name="_sale_price_times_from<?php echo is_int( $loop ) ? '[' . $loop . ']' : ''; ?>"
		       id="_sale_price_times_from"
		       value="<?php echo esc_attr( $sale_price_time_from ); ?>"
		       placeholder="<?php echo _x( 'From&hellip; HH:MM', 'placeholder', 'product-countdown' ); ?>" maxlength="5"
		       pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}"/>
		<?php
		if ( is_int( $loop ) ) {
			echo '</span><span class="form-row form-row-last">';
			printf(
				'<label for="_sale_price_times_to">%s</label>',
				__( 'Sale end time', 'product-countdown' )
			);
		}
		?>
		<input type="text" class="short"
		       name="_sale_price_times_to<?php echo is_int( $loop ) ? '[' . $loop . ']' : ''; ?>"
		       id="_sale_price_times_to"
		       value="<?php echo esc_attr( $sale_price_time_to ); ?>"
		       placeholder="<?php echo _x( 'To&hellip; HH:MM', 'placeholder', 'product-countdown' ); ?>"
		       maxlength="5"
		       pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}"/>
		<?php echo is_int( $loop ) ? '</span>' : ''; ?>
	</p>
	<?php
}


add_action( 'admin_enqueue_scripts', '\wpbuddy\product_countdown\woocommerce\time\enqueue_scripts' );

function enqueue_scripts( $hook_suffix ) {

	if ( ! ( 'post.php' === $hook_suffix || 'post-new.php' === $hook_suffix ) ) {
		return;
	}

	$screen = get_current_screen();

	if ( 'product' !== $screen->id ) {
		return;
	}

	/**
	 * @var \WPB_Product_Countdown $wpb_product_countdown
	 */
	global $wpb_product_countdown;

	wp_enqueue_style(
		'wpbpc-time-css',
		$wpb_product_countdown->plugins_url( 'assets/css/time.css' )
	);

	wp_enqueue_script(
		'wpbpc-time-js',
		$wpb_product_countdown->plugins_url( 'assets/js/time.js' ),
		array( 'jquery' )
	);
}

add_action( 'woocommerce_process_product_meta', '\wpbuddy\product_countdown\woocommerce\time\save_product', PHP_INT_MAX );
add_action( 'woocommerce_ajax_save_product_variations', '\wpbuddy\product_countdown\woocommerce\time\save_product', PHP_INT_MAX );

/**
 * @param int $post_id
 */
function save_product( $post_id ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$product = wc_get_product( $post_id );

	# do not overwrite variable products as these data will be saved via AJAX
	if ( 'woocommerce_process_product_meta' === current_action() && is_a( $product, '\WC_Product_Variable' ) ) {
		return;
	}

	if ( is_a( $product, '\WC_Product_Variable' ) ) {
		$products = $product->get_children();
	} else {
		$products = array( $post_id );
	}

	foreach ( $products as $i => $product_id ) {
		$product = wc_get_product( $product_id );


		/**
		 * Update date/time-to
		 */
		$sale_date_to = $product->get_date_on_sale_to( 'edit' );

		if ( is_a( $sale_date_to, '\WC_DateTime' ) ) {
			$sale_date_to = clone $sale_date_to;
			$sale_time_to = get_time_from_post( '_sale_price_times_to', $i );
			if ( empty( $sale_time_to ) ) {
				$sale_time_to = '23:59';
			}
			$sale_time_to = sanitize_time( $sale_time_to );

			$sale_time_to_parts = explode_time( $sale_time_to );
			$interval_spec      = sprintf( 'PT%sH%sM', $sale_time_to_parts[0], $sale_time_to_parts[1] );
			try {
				$sale_date_to->add( new \DateInterval( $interval_spec ) );
				$product->set_date_on_sale_to( $sale_date_to );
			} catch ( Exception $e ) {

			}
		}

		/**
		 * Update date/time-from
		 */
		$sale_date_from = $product->get_date_on_sale_from( 'edit' );
		if ( is_a( $sale_date_from, '\WC_DateTime' ) ) {

			$sale_date_from = clone $sale_date_from;
			$sale_time_from = get_time_from_post( '_sale_price_times_from', $i );
			$sale_time_from = sanitize_time( $sale_time_from );

			$sale_time_from_parts = explode_time( $sale_time_from );
			$interval_spec        = sprintf( 'PT%sH%sM', $sale_time_from_parts[0], $sale_time_from_parts[1] );
			try {
				$sale_date_from->add( new \DateInterval( $interval_spec ) );
				$product->set_date_on_sale_from( $sale_date_from );
			} catch ( Exception $e ) {

			}
		}

		/**
		 * Update meta.
		 */
		$product->save();

	}
}


/**
 * Fetches a value from the $_POST variable.
 *
 * Checks if the value is an array and fetches the array value if necessary.
 *
 * @param string $name
 * @param int    $i
 *
 * @return string
 */
function get_time_from_post( $name, $i = 0 ) {

	if ( is_array( $_POST[ $name ] ) && isset( $_POST[ $name ][ $i ] ) && is_scalar( $_POST[ $name ][ $i ] ) ) {
		return $_POST[ $name ][ $i ];
	}

	if ( isset( $_POST[ $name ] ) && is_scalar( $_POST[ $name ] ) ) {
		return $_POST[ $name ];
	}

	return '';

}

/**
 * Sanitizes a time string in HH:MM format.
 *
 * @param string $time
 *
 * @return string
 */
function sanitize_time( $time ) {

	if ( false === strpos( $time, ':' ) ) {
		return '00:00';
	}

	$times = explode( ':', $time );

	$hour   = \zeroise( isset( $times[0] ) ? absint( $times[0] ) : 0, 2 );
	$minute = \zeroise( isset( $times[1] ) ? absint( $times[1] ) : 0, 2 );

	return sprintf( '%s:%s', $hour, $minute );

}


/**
 * Explodes a time string.
 *
 * @param string $time
 *
 * @return array
 */
function explode_time( $time ) {

	return explode( ':', sanitize_time( $time ) );
}