��    $      <  5   \      0     1  9   B  	   |  
   �     �     �     �  
   �     �     �     �     �  l   �     ^     e     m     s     z     �     �     �     �     �     �     �     �     �     �     	  �        �     �     �     �  $   �  L  �        F   2     y  	   �     �     �     �  
   �     �     �     �  "   �  }   �     n	     u	     }	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     3
  �   E
     �
     �
     �
     �
      �
               #                           	            
                                                    "   $                                            !                           Background color Choose if the countdown should be on or off on this page. Countdown Custom CSS Day Days Fade in at position Font color General options Hour Hours Hurry up! The price rises in It seems that WooCommerce is not installed. Please activate WooCommerce to use the Product Countdown Plugin. Minute Minutes Month Months More Plugins by WPBuddy OFF ON Product Countdown Settings Product to refer to Purchase Code Save Second Seconds Settings Show "Fade in" helper Switch on/off This shows a little helper on the top left of every page that has a countdown message. It is only shown to administration users. Week Weeks Year Years Your customizations have been saved. Project-Id-Version: Product Countdown for WooCommerce
POT-Creation-Date: 2017-10-18 11:00+0200
PO-Revision-Date: 2017-10-18 11:00+0200
Last-Translator: WP-Buddy <info@wp-buddy.com>
Language-Team: Kay van Aarssen <kay@kayvanaarssen.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SearchPath-0: ../..
 Achtergrond kleur Kies als het aftellen moet worden in- of uitgeschakeld op deze pagina. Aftellen Eigen CSS Dag Dagen Fade-in op positie Font kleur Algemene opties Uur Uren Schiet eens op! De prijs stijgt in Het lijkt erop dat WooCommerce niet is geïnstalleerd. Activeer WooCommerce voor het gebruik van de Product Countdown Plugin. Minuut Minuten Maand Maanden Meer Plugin's door WPBuddy UIT AAN Product Countdown instellingen Product om te verwijzen naar Aankoop Code Opslaan Seconde Seconden Instellingen Toon "Fade in" helper Aan/uit schakelen Dit laat een kleine helper zien links boven in elke pagina die een aftel bericht heeft. Het is alleen zichtbaar voor administrator gebruikers. Week Weken Jaar Jaren Uw aanpassingen zijn opgeslagen. 