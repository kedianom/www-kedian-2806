jQuery( document ).ready( function () {
	"use strict";

	function wpbpc_show_hide_settings() {
		var $settings = jQuery( '.wpbpc-settings' );
		if ( jQuery( '#_wpbpc_countdown' ).val() === '0' ) {
			$settings.addClass( 'wpbpc-hidden' );
		} else {
			$settings.removeClass( 'wpbpc-hidden' );
		}
	}

	wpbpc_show_hide_settings();

	jQuery( document ).on( 'click', '#_wpbpc_countdown', function () {
		wpbpc_show_hide_settings();
	} );

	if ( jQuery.isFunction( jQuery.fn.wpColorPicker ) ) {
		jQuery( '#_wpbpc_background_color, #_wpbpc_font_color' ).wpColorPicker();
	}
	jQuery( '#_wpbpc_background_color, #_wpbpc_font_color' ).parent().parent().find( '.ui-slider' ).addClass( 'wpbpc_ui_slider' );

	function action_display() {
		var action = jQuery( '#_wpbpc_action' ).val();

		switch ( action ) {
			case 'email':
				jQuery( '#_wpbpc_redirect_url, #_wpbpc_end_message' ).parent().hide();
				jQuery( '#_wpbpc_email, #_wpbpc_email_message' ).parent().show();
				break;
			case 'redirect':
				jQuery( '#_wpbpc_redirect_url' ).parent().show();
				jQuery( '#_wpbpc_email, #_wpbpc_email_message, #_wpbpc_end_message' ).parent().hide();
				break;
			case 'message':
				jQuery( '#_wpbpc_end_message' ).parent().show();
				jQuery( '#_wpbpc_email, #_wpbpc_email_message, #_wpbpc_redirect_url' ).parent().hide();
				break;
			default:
				jQuery( '#_wpbpc_email, #_wpbpc_email_message, #_wpbpc_end_message, #_wpbpc_redirect_url' ).parent().hide();
		}
	}

	action_display();

	jQuery( '#_wpbpc_action' ).change( function () {
		action_display();
	} );

	function variable_product_action_hint() {
		if ( jQuery( '#product-type' ).val() != 'variable' ) {
			jQuery( '#_wpbpc_action' ).parent().find( 'span.description' ).css( 'color', '' );
		} else {
			jQuery( '#_wpbpc_action' ).parent().find( 'span.description' ).css( 'color', 'red' );
		}
	}

	jQuery( '#product-type' ).change( function () {
		variable_product_action_hint();
	} );

	variable_product_action_hint();

	/**
	 * AJAX Select2
	 */
	if ( jQuery().select2 ) {
		jQuery( '.wpbpc-select2' ).each( function () {
			var thisObj = jQuery( this );
			thisObj.select2( {
				'allowClear':         true,
				'minimumInputLength': 1,
				'placeholder':        thisObj.data( 'noselection' ),
				'ajax':               {
					'url':            ajaxurl,
					'dataType':       'json',
					'data':           function ( params ) {
						return {
							'action':          thisObj.data( 'ajax_action' ),
							'_wpb_nonce_name': thisObj.data( 'ajax_nonce' ),
							'term':            params.term
						};
					},
					'processResults': function ( data, params ) {
						return {
							'results': data[ 'data' ]
						};
					}
				}
			} );
		} );
	}

} );
