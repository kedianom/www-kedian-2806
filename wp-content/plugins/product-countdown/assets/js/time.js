"use strict";

jQuery( document ).ready( function () {
	jQuery( '#woocommerce-product-data' ).on( 'click', '.sale_schedule', function () {
		var $wrap = jQuery( this ).closest( 'div, table' );

		$wrap.find( '.sale_price_times_fields' ).removeClass( 'hidden' );

		return false;
	} );

	jQuery( '#woocommerce-product-data' ).on( 'click', '.cancel_sale_schedule', function () {
		var $wrap = jQuery( this ).closest( 'div, table' );

		$wrap.find( '.sale_price_times_fields' ).addClass( 'hidden' );
		$wrap.find( '.sale_price_times_fields input' ).val( '' );

		return false;
	} );

	jQuery( '.sale_price_dates_fields' ).each( function () {
		var $these_sale_dates = jQuery( this );
		var sale_schedule_set = false;
		var $wrap             = $these_sale_dates.closest( 'div, table' );

		$these_sale_dates.find( 'input' ).each( function () {
			if ( '' !== jQuery( this ).val() ) {
				sale_schedule_set = true;
			}
		} );

		if ( sale_schedule_set ) {
			$wrap.find( '.sale_price_times_fields' ).removeClass( 'hidden' );
		} else {
			$wrap.find( '.sale_price_times_fields' ).addClass( 'hidden' );
		}
	} );
} );
