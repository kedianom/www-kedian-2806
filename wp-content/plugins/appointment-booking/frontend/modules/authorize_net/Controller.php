<?php
namespace Bookly\Frontend\Modules\AuthorizeNet;

use Bookly\Lib;

/**
 * Class Controller
 * @package Bookly\Frontend\Modules\AuthorizeNet
 */
class Controller extends Lib\Base\Controller
{

    protected function getPermissions()
    {
        return array( '_this' => 'anonymous' );
    }

    /**
     * Do AIM payment.
     */
    public function executeAuthorizeNetAIM()
    {
        $response = null;
        $userData = new Lib\UserBookingData( $this->getParameter( 'form_id' ) );

        if ( $userData->load() ) {
            $failed_cart_key = $userData->cart->getFailedKey();
            if ( $failed_cart_key === null ) {
                list( $total, $deposit ) = $userData->cart->getInfo();
                $card       = $this->getParameter( 'card' );
                $first_name = $userData->get( 'first_name' );
                $last_name  = $userData->get( 'last_name' );
                // Check if defined First name
                if ( ! $first_name ) {
                    $full_name  = $userData->get( 'full_name' );
                    $first_name = strtok( $full_name, ' ' );
                    $last_name  = strtok( '' );
                }

                // Authorize.Net AIM Payment.
                $authorize = new Lib\Payment\AuthorizeNet( get_option( 'bookly_pmt_authorize_net_api_login_id' ), get_option( 'bookly_pmt_authorize_net_transaction_key' ), (bool) get_option( 'bookly_pmt_authorize_net_sandbox' ) );
                $authorize->setField( 'amount',     $deposit );
                $authorize->setField( 'card_num',   $card['number'] );
                $authorize->setField( 'card_code',  $card['cvc'] );
                $authorize->setField( 'exp_date',   $card['exp_month'] . '/' . $card['exp_year'] );
                $authorize->setField( 'email',      $userData->get( 'email' ) );
                $authorize->setField( 'phone',      $userData->get( 'phone' ) );
                $authorize->setField( 'first_name', $first_name );
                if ( $last_name ) {
                    $authorize->setField( 'last_name', $last_name );
                }

                $aim_response = $authorize->authorizeAndCapture();
                if ( $aim_response->approved ) {
                    $coupon = $userData->getCoupon();
                    if ( $coupon ) {
                        $coupon->claim();
                        $coupon->save();
                    }
                    $payment = new Lib\Entities\Payment();
                    $payment->setType( Lib\Entities\Payment::TYPE_AUTHORIZENET )
                        ->setStatus( Lib\Entities\Payment::STATUS_COMPLETED )
                        ->setTotal( $total )
                        ->setPaid( $deposit )
                        ->setPaidType( $total == $deposit ? Lib\Entities\Payment::PAY_IN_FULL : Lib\Entities\Payment::PAY_DEPOSIT )
                        ->setCreated( current_time( 'mysql' ) )
                        ->save();
                    $order = $userData->save( $payment );
                    Lib\NotificationSender::sendFromCart( $order );
                    $payment->setDetails( $order, $coupon )->save();
                    $response = array( 'success' => true );
                } else {
                    $response = array( 'success' => false, 'error_code' => 7, 'error' => $aim_response->response_reason_text );
                }
            } else {
                $response = array(
                    'success'         => false,
                    'error_code'      => 3,
                    'failed_cart_key' => $failed_cart_key,
                    'error'           => Lib\Utils\Common::getTranslatedOption( Lib\Config::showStepCart() ? 'bookly_l10n_step_cart_slot_not_available' : 'bookly_l10n_step_time_slot_not_available' ),
                );
            }
        } else {
            $response = array( 'success' => false, 'error_code' => 1, 'error' => __( 'Session error.', 'bookly' ) );
        }

        wp_send_json( $response );
    }

    /**
     * Override parent method to register 'wp_ajax_nopriv_' actions too.
     *
     * @param bool $with_nopriv
     */
    protected function registerWpAjaxActions( $with_nopriv = false )
    {
        parent::registerWpAjaxActions( true );
    }
}
