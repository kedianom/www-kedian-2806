jQuery(function($) {

    var $custom_notifications = $('#bookly-js-custom-notifications');

    Ladda.bind( 'button[type=submit]' );

    // menu fix for WP 3.8.1
    $('#toplevel_page_ab-system > ul').css('margin-left', '0px');

    /* exclude checkboxes in form */
    var $checkboxes = $('.bookly-js-collapse .panel-title > input:checkbox');

    $checkboxes.change(function () {
        $(this).parents('.panel-heading').next().collapse(this.checked ? 'show' : 'hide');
    });

    $('[data-toggle="popover"]').popover({
        html: true,
        placement: 'top',
        trigger: 'hover',
        template: '<div class="popover bookly-font-xs" style="width: 220px" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });

    $custom_notifications
        .on('change', "select[name$='[type]']", function () {
            var $panel    = $(this).closest('.panel'),
                $settings = $panel.find('.bookly-js-settings'),
                set       = $(this).find(':selected').data('set'),
                value     = $(this).find(':selected').val();

            $panel.find("[name$='[to_staff]']").closest('.check-inline').toggle(value != 'customer_birthday');
            $panel.find("[name$='[to_admin]']").closest('.check-inline').toggle(value != 'customer_birthday');

            $settings.each(function () {
                $(this).toggle($(this).hasClass('bookly-js-' + set));
            });
        })
        .on('click', '.bookly-js-delete', function () {
            if (confirm(BooklyL10n.are_you_sure)) {
                var $button = $(this),
                    id = $button.data('notification_id');
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'bookly_delete_custom_notification',
                        id: id,
                        csrf_token: BooklyL10n.csrf_token
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $button.closest('.panel').remove();
                        }
                    }
                });
            }
        })
        .find("select[name$='[type]']").trigger('change');

    $('button[type=reset]').on('click', function () {
        setTimeout(function () {
            $("select[name$='[type]']", $custom_notifications).trigger('change');
        }, 0);
    });

    $("#bookly-js-new-notification").on('click', function () {
        var ladda = Ladda.create(this);
            ladda.start();
        $.ajax({
            url : ajaxurl,
            type: 'POST',
            data: {
                action    : 'bookly_create_custom_notification',
                csrf_token: BooklyL10n.csrf_token
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $custom_notifications.append(response.data.html);
                    var $subject = $custom_notifications.find('#notification_' + response.data.id + '_subject'),
                        $panel   = $subject.closest('.panel-collapse');
                    $panel.collapse('show');
                    $panel.find("select[name$='[type]']").trigger('change');

                    tinyMCE.execCommand('mceAddEditor', false, 'notification_' + response.data.id + '_message');
                    tinyMCE.execCommand('mceAddControl', false, 'notification_' + response.data.id + '_message');

                    $subject.focus();
                }
            },
            complete: function () {
                ladda.stop();
            }
        });
    });

    booklyAlert(BooklyL10n.alert);
});