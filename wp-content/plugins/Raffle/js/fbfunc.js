facebookPopupTimer = 0;

function initFacebook(){
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/ga_IE/sdk.js', function(){
		resizeFBCanvas(0);
		FB.init({
			  appId:  APP_ID,
			  version: 'v2.0',
			  status: true,
			  cookie: true,
			  xfbml:  true,
			  oauth: true,
			  channelUrl: BASE_URL+'channel.html'
		});			 
		FB.Event.subscribe('edge.create', function(href, widget) {
			userLiked();
		});		
		checkAppInstalled(); 
		fbInitComplete();
	});
}

function checkAppInstalled(){
	FB.getLoginStatus(function(response) {
		if(response.status === 'connected'){	// app installed and user logged in Facebook
			getUserData(response,false);		// get user data, without loginComplete function is triggered
			APP_INSTALLED = 1;
		}
		else if(response.status === 'not_authorized') {	// user doesn't install the app yet
			APP_INSTALLED = 0;
			userCanPlay();
		}
		else{  // the user isn't logged in to Facebook.
			userNotLoggedInFacebook();
		}
		fbLoginStatusComplete();
	});			
	
	if(LOCALHOST){
		setTimeout(function() {
			if(LOCAL_APP_INSTALLED){
				APP_INSTALLED = 1;
				fbSilentLoginComplete();				
			}
			else{
				APP_INSTALLED = 0;
				userCanPlay();
			}
			fbLoginStatusComplete();
		}, 2000);
	}
}
	
function startLogin(skipCompleteEvent){
	if(FORCE_INSTALL_ENABLED){
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected')
				getUserData(response,!skipCompleteEvent);
			else if (response.status === 'not_authorized')
				loginUser();
			else {  /* the user isn't logged in to Facebook. */ }
		});			
		
		if(LOCALHOST){
			adminFBLoginComplete();
			fbLoginComplete();	
		}
	}
}

function resizeFBCanvas(add){
	if(!IS_TEST_NAV){
		add = add ? add :  0;
		FB.Canvas.setSize({ height: $('body').height() + add });
	}
}

function loginUser(){
	FB.login(function(response){
		if(response.authResponse) {   // Basic permissions accepted
			FB.api('/me/permissions',function(response){
				trackPage('/basic_permission_accepted','Facebook - Basic permissions accepted');	
				trackPage('/app-installed','Facebook - Application installed'); 
				
				if(FORCE_INSTALLED)
					trackPage('/permission-tutorial-accepted','Facebook - Permission tutorial accepted');
					
				NEW_INSTALL = 1;
			})
			
			getUserData(response,true);
		}
		else{ 						// Basic permissions denied
			trackPage('/basic_permission_denied','Facebook - Basic permissions denied');
			showPage(forcedInstallTarget);
		}
	},{scope:APP_SCOPE}); // missing publish permission
}	

function getUserData(response,fireCompleteEvent){
	FB_ID = response.authResponse.userID;
	ACCESS_TOKEN = response.authResponse.accessToken
	FB.api('/me', 'GET',{"fields":"id,name,email,first_name,last_name,gender,locale"}, function(response) {
		FB_NAME = response.name;
		FB_EMAIL = response.email;
		FB_FIRSTNAME = response.first_name;
		FB_LASTNAME = response.last_name;	
		FB_GENDER = response.gender;	
		FB_LOCALE = response.locale;	
		
		adminFBLoginComplete();
		if(fireCompleteEvent)
			fbLoginComplete();
		else
			fbSilentLoginComplete();
	});		
}

function trackPermission(name, val){
	var pageName = name.toLowerCase(), pageState = val ? 'accepted' : 'denied', titleState = val ? 'granted' : 'denied';
	trackPage('/'+pageName+'_permission_'+pageState,'Facebook - '+name+' permission '+titleState);
}	

function handleForcedInstall(){
	$('#btn-forced-no').click(function(){
		if(!MULTI_FORCE_INSTALL)
			FORCE_INSTALL_ENABLED = 0;
		trackPage('/permission-tutorial-denied','Facebook - Permission tutorial denied');
	});
	$('#btn-forced-ok').click(function(e){
		FORCE_INSTALLED = 1;
		startLogin();
	});
}	

function adminFBLoginComplete(){
	$('#admin-login').html('Permission added').removeClass('load').addClass('pressed disabled');
	$('.remove-perm,.remove-db').removeClass('pressed disabled');
}

function updateStatus(){
	var message = PAGE['status'];

	var params = {};
	params['message'] = message.replace("FB_NAME", FB_NAME);
	params['name'] = PAGE['title'];
	params['description'] = PAGE['desc'];
	params['link'] = PAGE['appUrl'];
	params['picture'] = PAGE['logo'];
	params['caption'] = PAGE['brand'];
	
	FB.api('/me/feed', 'post', params);
	
	trackPage('/status-updated','Facebook - Autopost successful');
}

function shareApp(){
	if(LOCALHOST){
		shareCompleted();
	}
	else{
		FB.ui(
			{
				method: 'feed',
				name: PAGE['title'],
				link: PAGE['appUrl'],
				picture: PAGE['logo'],
				caption: PAGE['brand'],
				description: PAGE['share']
			},
			function (response) {
				if (response && response.post_id) {			
					trackPage('/facebook-share','Facebook - Share successful');
					shareCompleted();
				} else {
					var title = TEXTS['shareFailedTitleTxt'];
					var body = TEXTS['shareFailedBodyTxt'];
					showSystemMsg(title,body);
					trackPage('/facebook-share-denied','Facebook - Share denied');
				}
			}
		);	
	}
}

function inviteFriends(){
	if(LOCALHOST){
		inviteCompleted(INVITE_LIMIT);
	}
	else{
		FB.ui({method: 'apprequests',
			app_id: APP_ID,
			filters: ['app_non_users'],
			title: PAGE['title'],
			message: PAGE['invite']
		}, proccessFriends);
		if((GAME_PLATFORM == 'website' || GAME_PLATFORM == 'facebook-website') && !FACEBOOK_FRAME && self !== top){
			facebookPopupTimer = setInterval(function(){ $('.fb_dialog_iframe iframe').height(479);}, 500);
		}
	}
}
	
function proccessFriends(response){
	var length = typeof(response.to) != 'undefined' ? response.to.length : 0;
	if(facebookPopupTimer)
		clearInterval(facebookPopupTimer);
	if($('#customizer').length && length>=1)
		length = INVITE_LIMIT;
	if(length> INVITE_LIMIT-1){
		trackPage('/facebook-invite','Facebook - Invite successful');
		inviteCompleted(length);
	}
	else{
		var title = TEXTS['inviteFailedTitleTxt'];
		var body = TEXTS['inviteFailedBodyTxt'].replace('{invitedCnt}', length);
		var body = body.replace('{inviteLimit}', INVITE_LIMIT);
		showSystemMsg(title,body);
		trackPage('/facebook-invite-denied','Facebook - Invite failed');
	}
}

function sendWallPost(to, from) {
	FB.ui({method: 'feed',
	description: PAGE['wallpost'],
	caption: PAGE['brand'],
	link: PAGE['appUrl'],
	picture: PAGE['logo'],
	to: to,
	from: from
}, proccessWallPost);}

function proccessWallPost(){

}

function twitterShare(){
	window.open('https://twitter.com/home?status='+encodeURIComponent(PAGE['desc'] + ' - ' + PAGE['appUrl'])+'',"twitter","width=640,height=480");
	trackPage('/app-twitter-share','Twitter - Share successful');
}

function uploadPhoto(fileName) {
	var filePath = BASE_URL+'uploads/'+fileName;	
	var params = {};
	params['url'] = filePath;
	params['access_token'] = ACCESS_TOKEN;
	params['upload file'] = true;
	try{
		FB.api('/me/photos', 'post', params, function(response) {
			if (!response || response.error) {
				console.log(response.error);
				trackPage('/fb-photo-upload-error','Facebook - Image upload failed');
			} else {
				trackPage('/fb-photo-upload-success','Facebook - Image upload completed');
			}
		});	        	
	}catch(e){}
}

function sendNotification(userID, msg){
	$.get('https://graph.facebook.com/oauth/access_token?client_id='+APP_ID+'&client_secret='+APP_SECRET+'&grant_type=client_credentials', function(data) {
		var token = data.substring(13);
		FB.api('/'+userID+'/notifications','POST',{
			access_token	:	token,
			href			:	'',
			template		:	msg
		},function(response){
			if(response){
				trackPage('/fb-notification-success','Facebook - Notification sent');
			}
		});
	});
}