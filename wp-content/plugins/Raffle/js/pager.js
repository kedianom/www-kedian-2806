/*============================================================================
	HTML data tag options 
==============================================================================
		
	For page item:
	- data-effect: Type of page animation (slide-left,slide-right,slide-up,slide-down,fade-in,fade-out,popup-in,popup-out)
	- data-final-target: if there are branch, such as permission.
	- data-mode: It can be: permission, subscribe, likebox
	- data-forced-install-target: If permission denied, where to redirect user.
	
	For page-control item:
	- data-forced-effect: It can be override page default animation effect.
	- data-target: Destination page after animation.
	- data-ajax-loading: If 'on', after click, we show a preloader. Then we hide it when ajax request is complete.

*/

$pagerTarget = 0
$pagerCurrent = 0;
$pagerPrevPage = 0;
$pager = $('#pager');
$navItems = $('nav .page-control');
$popupFader = IS_MOBILE_URL ? $('#popup-fader,.mobile-fader') : $('#popup-fader');
animStartCallBack = 'pageAnimStarted';
animEndCallBack = 'pageAnimEnded';
isLoading = 0;
permissionTarget = '';
likeTarget = '';
forcedInstallTarget = '';
isTestCall = 0;
var pageWidth = $('#main').width();
var pageHeight = $('#main').height();
var animTime = 0.5;
var fadeTime = 0.3;
var animEasing = 'swing';

$navItems.click(function(){
	$navItems.removeClass('active');
	$(this).addClass('active');
});

function initPager(startEvent, endEvent){	
	$pager.width(pageWidth).height(pageHeight);
	handleForcedInstall();
	initPopups();
	animStartCallBack = startEvent;
	animEndCallBack = endEvent;
	
	$('.page-control').click(function(){
		var $btn = $(this);
		var targetId = $btn.data('target');
		var forcedEffect = $btn.data('forcedEffect');	
		var btnMode = $btn.data('mode');
		handleAjaxLoading($btn);
		showPage(targetId,forcedEffect,btnMode);
	});
}

function initPopups(){
	$('.page').each(function(){
		if($(this).data('close-button')=='on'){
			if(!$('#fixed .popup-close').length)
				$('#fixed').append('<div class="popup-close button page-control" data-target="HIDE_POPUP">x</div>');		
			$(this).click(function(e){
				if(e.target !== this) 
					return;
				$('#fixed .popup-close').click();				
			});
		}
	});
}

function adjustPopupWidth(){
	$('.page-enlarge').each(function(){
		var imgWidth = $('.enlarged-img',this).width();
		$('.page-inner',this).css('width',imgWidth);
	});
}

function handleAjaxLoading($el){
	if($el.data('ajax-loading') == 'on'){
		var pos = $el.position();
		var posX = pos.left;
		var posY = pos.top;
		var h = $el.outerHeight();
		var w = $el.outerWidth();

		$el.myFadeOut(500,function(){
			if($el.next('.ajax-loader').length)
				$el.next().show();
			else
				$el.after('<div class="loading ajax-loader" style="left:'+pos.left+'px;top:'+pos.top+'px;"><span>loading...</span><img src="images/ajax-loader.gif" alt="" /></div>');
		});
		isLoading = 1;
	}
}

function showPage(targetId,forcedEffect,btnMode,isTest){
	if(!targetId)
		return;

	$pagerTarget = $('#'+targetId);
	$pagerCurrent = $('.page.active',$pager);	
	isTestCall = isTest;
	var currentId = $pagerCurrent.attr('id');
	var finalTarget = $pagerTarget.data('final-target');
	var mode = $pagerTarget.data('mode');
	var skip = handlePageMode(finalTarget,mode,btnMode);
	
	
	if(skip)			// if need to skip this page
		return false;
	
	if(isAnimatable($pagerTarget,targetId)){	
		var effect = forcedEffect ? forcedEffect : $pagerTarget.data('effect');
		var isPopup = $pagerCurrent.data('effect') == 'popup-in' ? 1 : 0;
		
		// remove loader is active
		if(isLoading){
			$('.ajax-loader').hide();
			isLoading = 0;
		}
		
		// call anim started event
		var callbackParams = [currentId, targetId, $pagerCurrent, $pagerTarget];
		var startEventFn = window[animStartCallBack];
		if (typeof startEventFn === "function") startEventFn.apply(null, callbackParams);
		
		if(isPopup)
			effect = 'popup-out';
		
		$pager.addClass('animate');

		switch(effect){
			case "slide-left":
				$pagerTarget.css({left:pageWidth,top:0});
				TweenLite.to($pagerCurrent,animTime,{left:-pageWidth, ease:Power2.easeOut, onComplete:pageAnimEnd});
				TweenLite.to($pagerTarget,animTime,{left:0, ease:Power2.easeOut});
				break;
			case "slide-right":
				$pagerTarget.css({left:-pageWidth,top:0});
				TweenLite.to($pagerCurrent,animTime,{left:pageWidth, ease:Power2.easeOut, onComplete:pageAnimEnd});
				TweenLite.to($pagerTarget,animTime,{left:0, ease:Power2.easeOut});
				break;
			case "slide-down":			
				$pagerTarget.css({left:0,top:-pageHeight});
				TweenLite.to($pagerCurrent,animTime,{top:pageHeight, ease:Power2.easeOut, onComplete:pageAnimEnd});
				TweenLite.to($pagerTarget,animTime,{top:0, ease:Power2.easeOut});	
				break;				
			case "slide-up":
				$pagerTarget.css({left:0,top:pageHeight});
				TweenLite.to($pagerCurrent,animTime,{top:-pageHeight, ease:Power2.easeOut, onComplete:pageAnimEnd});
				TweenLite.to($pagerTarget,animTime,{top:0, ease:Power2.easeOut});					
				break;
			case "fade-in":
				$pagerTarget.css({left:0,top:0,opacity:0,zIndex:500});
				TweenLite.to($pagerTarget,animTime,{opacity:1,onComplete:function(){
					$pagerCurrent.removeAttr('style');
					$pagerTarget.css({zIndex:1});
					pageAnimEnd();
				}});					
				break;				
			case "fade-out":
				$pagerTarget.css({left:0,top:0});
				TweenLite.to($pagerCurrent,animTime,{opacity:0,onComplete:function(){
					$pagerCurrent.removeAttr('style');
					pageAnimEnd();
				}});					
				break;				
			case "popup-in":
				var boxHeight = $('.page-inner',$pagerTarget).outerHeight();
				var adjustY = $pagerTarget.data('vertical-pos-adjust');
				if(adjustY)
					boxHeight -= adjustY;
				var offsetY = (pageHeight-boxHeight) / 2;
				if(offsetY<0)
					offsetY = 0;
					
				$pagerCurrent.css({left:0,top:0});
				$popupFader.css({zIndex:1000});
				$pagerTarget.css({top:-boxHeight,left:0,zIndex:1500,opacity:0});
				TweenLite.to($popupFader,fadeTime,{opacity:0.8,onComplete:function(){
					TweenLite.to($pagerTarget,animTime,{top:offsetY,opacity:1,onComplete:function(){
						if($pagerTarget.data('close-button')=='on')
							$('#fixed .popup-close').myFadeIn();
					}});
					pageAnimEnd();					
				}});
				break;
			case "popup-out":
				if($pagerCurrent.data('close-button')=='on')
					$('#fixed .popup-close').myFadeOut();
				TweenLite.to($pagerCurrent,animTime,{top:pageHeight,opacity:0,onComplete:function(){
					TweenLite.to($popupFader,fadeTime,{opacity:0,onComplete:function(){
						$popupFader.css({zIndex:-1});
						if($pagerPrevPage.attr('id') != targetId && targetId != 'HIDE_POPUP'){
							$pagerTarget.css({left:0,top:0,opacity:0,zIndex:500});
								TweenLite.to($pagerTarget,animTime,{opacity:1,onComplete:function(){
									$pagerCurrent.removeAttr('style');
									$pagerPrevPage.removeAttr('style');
									$pagerTarget.css({zIndex:1});
									pageAnimEnd();
								}});	
						}
						else{
							$pagerCurrent.removeAttr('style');
							$pagerTarget = $pagerPrevPage;
							pageAnimEnd();					
						}
					}});
				}});
				break;					
		}
		
	}
}

function handlePageMode(finalTarget,mode,btnMode){
	var skip = false;
	mode = mode ? mode : btnMode;
	
	switch(mode){
		case 'subscribe':
			if(APP_INSTALLED){
				showPage(finalTarget,'slide-left');
				skip = true;
			} 		
			break;
			
		case 'permission':
			if(!APP_LOGGED_IN){
				permissionTarget = finalTarget;
				forcedInstallTarget = $pagerCurrent.data('forced-install-target');
				startLogin();
				skip = true;
			} 		
			else if(btnMode=='permission'){
				skip = true;	
			}
			break;		
			
		case 'likebox':
			if(IS_LIKED || APP_LIKE != 'likebox'){
				showPage(finalTarget);
				skip = true;
			}	
			else{
				likeTarget = finalTarget;
			}
			break;			
	}
	return skip;
}

function pageAnimEnd(){
	$pager.removeClass('animate');
	$('.active',$pager).removeClass('active');
	$pagerTarget.addClass('active');
	
	var currentId = $pagerCurrent.attr('id');
	var targetId = $pagerTarget.attr('id');
	
	// call anim finished event
	var callbackParams = [currentId, targetId, $pagerCurrent, $pagerTarget, isTestCall];
	var endEventFn = window[animEndCallBack];
	if (typeof endEventFn === "function") 
		endEventFn.apply(null, callbackParams);
		
	$pagerPrevPage = $pagerCurrent;
}

function isAnimatable($pagerTarget, targetId){		// if valid target and not animating
	var valid = $pagerTarget.length || targetId == 'HIDE_POPUP';
	return valid && !$pager.hasClass('animate') && !$pagerTarget.hasClass('active');
}

function showSystemMsg(title, content){
	var $el = $('#system-msg');
	$('.com-box-top h3', $el).html(title);
	$('.msg-content', $el).html(content);
	showPage('system-msg');
}
