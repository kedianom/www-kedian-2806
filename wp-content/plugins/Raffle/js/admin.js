/*============================================================================
	Admin panel
=========================================================================== */

$(document).ready(function(){
	if(IS_TEST_NAV)
		renderTestNav();
		
	$('.remove-perm,.remove-db').addClass('pressed disabled');
		
	/**** Init admin slider *****/
	var panelWidth = $('.admin-pages-con').width();
	$('.admin-panel').width(panelWidth);
	var sliderWidth = $('.admin-panel').size()*panelWidth;
	$('.admin-pages').width(sliderWidth);		
		
	/**** Admin tabs *****/
	$('.admin-nav').click(function(){
		var el = $(this);
		if(!el.hasClass('active')){
			$('.admin-nav').removeClass('active');
			el.addClass('active');
			$('.admin-pages').css('marginLeft',-el.index()*panelWidth);
		}
	});
	
	maximalizeWidth('#admin-downloads .button',4);
	maximalizeWidth('#admin-debug .button',4);
	
});

/**** render test navigation *****/
function renderTestNav(){
	$('body').append('<div id="test-nav"><h3>Test Navigation:</h3></div>');
	$('#pager .page').each(function(){
		var page_id = $(this).attr('id');
		var clss = $(this).hasClass('active') ? 'pressed' : ''; 
		$('#test-nav').append('<div class="button '+clss+'" data-target="'+page_id+'">'+page_id+'</div>');
	});	
	$('#test-nav .button').each(function(){
		var target = $(this).data('target');
		$(this).click(function(){
			$('#test-nav .button').removeClass('pressed');
			$(this).addClass('pressed');
			showPage(target,'','',1);
		});
	});
}

$('#show-admin').click(function(){
	$(this).toggleClass('active');
	var $adminFader = IS_MOBILE_URL ? $('#admin-fader,.mobile-fader') : $('#admin-fader');
	var isMobileFaderVisible = $('#popup-fader').css('opacity') != 0;
	
	if($(this).hasClass('active')){
		$adminFader.css('z-index',11000);
		TweenLite.to($adminFader,0.5,{opacity:0.8});
		TweenLite.to('#admin',0.5,{top:84});
	}
	else{
		if(isMobileFaderVisible)
			$adminFader = $('#admin-fader');
		TweenLite.to('#admin',0.5,{top:'110%', onComplete:function(){ $('#admin').css('top','-100%'); }});
		TweenLite.to($adminFader,0.5,{opacity:0,onComplete:function(){ 
			$adminFader.css('z-index','-1'); 
		}});		
	}
});

$('#remove-perm').click(function(){
	if(!$(this).hasClass('disabled')){
		var $el = $(this);
		$el.addClass('load');
		FB.api("/me/permissions","DELETE",function(response){
			$el.removeClass('load');
			if(response){
				console.log('Permission successfully removed!'); 
				$el.html('Permission removed').addClass('pressed disabled');
			}
			else{
				console.log(response); 
				$el.html('Error, check console');
			}
		});
	}
});

$('#remove-db').click(function(){
	if(!$(this).hasClass('disabled')){
		var $el = $(this);
		$el.addClass('load');	
		$.post( 'bin/admin/delete_user.php', {fbid: FB_ID, secret: APP_SECRET}, function(data) {
			$el.removeClass('load');
			if(data == 'deleted'){
				console.log('User successfully removed from database!'); 
				$el.html('User removed').addClass('pressed disabled');
			}
		}, 'text' );
	}
});
 
$('#truncate-tables').click(function(){
	if(!$(this).hasClass('disabled')){
		var $el = $(this);
		$el.addClass('load');	
		$.post( 'bin/admin/truncate_tables.php', {secret: APP_SECRET}, function(data) {
			$el.removeClass('load');
			if(data == 'truncated'){
				console.log('Tables truncated'); 
				$el.html('Tables truncated').addClass('pressed disabled');
				$('#new-game-info').myFadeIn();
			}
		}, 'text' );
	}
});

$('#admin-login').click(function(){
	if(!$(this).hasClass('disabled')){
		$(this).addClass('load');
		startLogin(true); 
	}
});

