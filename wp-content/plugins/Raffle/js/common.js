/**** Global variables *****/
raffle_cnt = INIT_TICKET_CNT;
game_started = 0;
show_played = 0;
phone_orientation = 0;
play_again = 0;
ticket_height = 67;
max_ticket_row_cnt = 5;
social_buttons_created = 0;

/**** Init application *****/ 
$(document).ready(function(){
	
	if(IS_MOBILE && !IS_MOBILE_URL)	// if mobile device, but not mobile version
		top.location = PAGE['mobileUrl'];
	
 	if(!IS_MOBILE && IS_MOBILE_URL)	// if mobile preview
		$('#show-admin,#show-customizer').hide();
	
	if(IS_MOBILE_URL){	// if mobile version -> index.php?mobile=1
		trackPage('/mobile-version','Facebook - Mobile version');
		initMobileVersion();
		window.addEventListener('orientationchange', initMobileVersion, false);
		$(window).on('resize',initMobileVersion);
	}
	
	if(IS_TEASER)	// game not started, track users
		trackPage('/teaser-mode','Teaser mode');
	
	preloadApp();			// start preloading
	initFacebook();			// async load Facebook JS SDK
	
	/**** Don't start until valid coupon code  *****/
	if(ONLY_COUPON){
		$('#btn-start').click(function(e){	
			var code = $('#coupon-code').val();
			var isUsed = code.indexOf(TEXTS['couponUsedTxt']);
			if(!code || code == TEXTS['couponInvalidTxt'] || isUsed >= 0)
				$('#coupon-code').val('').focus();
			else{
				$('#coupon-code').removeClass('invalid')
				$(this).html($(this).html() + '...').addClass('pressed');
				validateCouponCode(code);
			}
			e.stopImmediatePropagation();
		});
		$('#coupon-code').focusin(function(){
			if($(this).val() == TEXTS['couponInvalidTxt'])
				$(this).val('');
		});
		$('#like-gate').hide();
		$('#btn-start').show();
		}
	
	/**** Disable extra chances buttons *****/
	$('#btn-gotobox').click(function(e){
		if(raffle_cnt){
			disableExtraChances();
			trackPage('/throwing-to-box','Throwing to box');
			if(play_again)
			saveUser();			
		}
		else{
			var delay = 0;
			$('.extra-chance-btn:not(.pressed)').each(function(){
				var $el = $(this);
				TweenLite.to($el, 0.2, {scale:0.8, delay:delay, ease:Power0.easeOut, onComplete:function(){
					TweenLite.to($el, 0.3, {scale:1, ease:Power2.easeOut});	
				}});
				delay += 0.1;
			});
			e.stopImmediatePropagation();
		}
	});	
	
	/**** Initialize page handler plugin *****/
	initPager('pageAnimStarted','pageAnimEnded');	
		
	/**** Custom scroll *****/
	$('.custom-scroll').mCustomScrollbar({theme:'dark-3',contentTouchScroll:IS_MOBILE_URL ? false : 25});
	$('.custom-scroll-mini').mCustomScrollbar({theme:'dark-3',contentTouchScroll:IS_MOBILE_URL ? false : 25,mouseWheel:{ scrollAmount: 80 }});
		
	/**** Custom scroll buttons for mobile *****/
	RULES_SCROLL_OFFSET = 260;
	var $rulesScroll = $('.custom-scroll');
	$('#rules-scroll-down').click(function(){	customScrollTo($rulesScroll,getCustomScrollTop($rulesScroll)+RULES_SCROLL_OFFSET);	});
	$('#rules-scroll-up').click(function(){	customScrollTo($rulesScroll,getCustomScrollTop($rulesScroll)-RULES_SCROLL_OFFSET);	});	
	
	EXTRA_SCROLL_OFFSET = 150;
	var $extraScroll = $('.custom-scroll-mini');
	$('#extra-scroll-down').click(function(){	customScrollTo($extraScroll,getCustomScrollTop($extraScroll)+EXTRA_SCROLL_OFFSET);	});
	$('#extra-scroll-up').click(function(){	customScrollTo($extraScroll,getCustomScrollTop($extraScroll)-EXTRA_SCROLL_OFFSET);	});	
	
	
	/**** Sound effects *****/
	$('#btn-mute').click(function(){
		$(this).toggleClass('pressed');
		if($(this).hasClass('pressed')){
			$.cookie('sound', 'off', { expires: 365, path: '/' });
			buzz.all().mute();	
		}
		else{
			buzz.all().unmute();	
			$.cookie('sound', 'on', { expires: 365, path: '/' });			
			trackPage('/mute','Game sound muted');
		}
	});	

	themeSnd = new buzz.sound( BASE_URL+'sounds/theme', { formats: [ 'ogg', 'mp3' ], preload: true, loop: true, volume:100 });
	btnSnd = new buzz.sound( BASE_URL+'sounds/button', { formats: [ 'ogg', 'mp3' ], preload: true, volume:100 });
	cutSnd = new buzz.sound( BASE_URL+'sounds/cut', { formats: [ 'ogg', 'mp3' ], preload: true, volume:100 });
	
	$('.button,#prize-thumb,#big-prize').click(function(){
		btnSnd.play();
	});
	
	if(MUTE_SOUNDS){
		buzz.all().mute();
		$('#btn-mute').hide();
	}
	
	if($.cookie('sound') == 'off')
		$('#btn-mute').click();
		
	/**** Init tooltipster *****/
	$('.tooltip').tooltipster({animation: 'grow',delay: 150,contentAsHTML: true});
	$('.tooltip-admin').tooltipster({theme:'tooltipster-admin', animation: 'grow',delay: 150,contentAsHTML: true});
	$('#collected-raffles').tooltipster({theme:'tooltipster-default tooltipster-ticket',animation: 'grow',
	delay: 150,contentAsHTML: true,position: 'right',functionReady: function(){
		var top = parseInt($('.tooltipster-ticket').css('top'), 10);
		var height = $('.tooltipster-ticket').outerHeight();
        $('.tooltipster-ticket').css('top',top - height/2+37);
    }});

	/**** Extra chance buttons *****/
	$('.extra-chance-btn.no-plugin-btn').click(function(){
		if(isExtraChanceEnabled($(this))){
			var id = $(this).attr('id');
			switch(id){
				case 'invite': inviteFriends(); break;
				case 'share': shareApp(); break;
				case 'subscribe':
					useExtraChance(id);
			}
		}
	});
		
	/**** Track rules *****/
	$('#btn-rules').click(function(){
		trackPage('/rules-opened','Rules opened');
	});
	
	/**** Track prize *****/
	$('#prize-thumb').click(function(){
		trackPage('/big-prize-img-opened','Big prize image opened');
	});
	
	/**** Privacy Policy URL *****/
	if(window.location.hash == '#privacy-policy')
		showPage('rules');
	
	/**** Handle permission tutorial *****/
	$('#btn-forced-no').click(function(){
		$('#btn-gotobox').myFadeIn();
	});
	
	/**** Log into Facebook *****/
	$('#login-fb').click(function(){
		redirectUserToLoginFacebook();
	});
	
	/**** Price zoom *****/
	$('.img-zoom').click(function(){
		$(this).parent().find('.enlarge-thumb').click();
	});
	
	/**** Show loading after permission tutorial *****/
	$('#forced-ok').click(function(){
		$('#btn-gotobox').next('.loading').show();
	});
	
});

/**** If user liked the page *****/ 
function userLiked(){		// it called just one time
	trackPage('/new-like','Facebook - New like');
	NEW_LIKE = 1;
	IS_LIKED = 0;
	saveLike();
}

/**** If user installed the application *****/ 
function fbLoginComplete(){	// it called everytime when app installed
	APP_INSTALLED = APP_LOGGED_IN = 1;
	saveUser();				// save user to database

	showPage(permissionTarget);
}

/**** Registered user will logged in at the welcome screen *****/ 
function fbSilentLoginComplete(){
	if(GAME_STATE == 'active'){
		$('body').addClass('collected-raffles');
		$('#collected-raffles').myFadeIn();
		$('#btn-start').html(TEXTS['getMoreTxt']);
		checkPlayed();		// if user already played
	}
	else if(GAME_STATE == 'pre')
		showStartButton();
}

/**** User isn't logged into Facebook *****/
function userNotLoggedInFacebook(){
	$('.fb-init-loader,#btn-start,.game-not-start,#like-gate,#coupon-con').hide();
	$('.login-fb').show();
	$('.start-button-con').myFadeIn();
}

/**** Redirect user back to game after login *****/
function redirectUserToLoginFacebook(){
	var appURL = PAGE['tab'];
	if(GAME_PLATFORM == 'website' || IS_MOBILE_URL)
		appURL = PAGE['appUrl'];
	var redirectURL = 'https://www.facebook.com/login.php?next='+encodeURIComponent(appURL);
	top.location = redirectURL;
}

/**** Facebook JS API init complete *****/
function fbInitComplete(){
	FB_INIT = true;	
}

/**** APP_INSTALLED variable is ready  *****/
function fbLoginStatusComplete(){
	if(APP_INSTALLED){
		$('#like-gate').hide();
		$('#btn-start').addClass('show');
	}
	APP_INSTALL_CHECKED = 1;
	if(LIKE_GATE != 'disabled' && !APP_INSTALLED)
		$('#btn-like').customSocialButton({type:'facebook-like',pressed_offset_y:4,btn:{url:LIKE_URL}});
}

/**** Has user played? *****/ 
function checkPlayed(){
	$.post( 'bin/func/check_played.php', {fbid: FB_ID}, function(data) {
		result = JSON.parse(data);
		if(result.raffle_cnt >= MAX_TICKET_CNT){
			trackPage('/try-to-play-again','User try to play again');
			$('.fb-init-loader').myFadeOut();
			renderUserRaffles('#user-played .user-raffles',result.raffles)
			$('#btn-mute').addClass('always-top');
			if(IS_MOBILE_URL && phone_orientation == 'portrait')
				show_played = 1;
			else
				showPage('user-played');
		}
		else{
			if(result.raffle_cnt > 0){
				raffle_cnt = 0;
				var rafflesMini = renderUserRaffles('',result.raffles,5);
				$('#collected-raffles span').html(result.raffle_cnt);
				$('#collected-raffles').tooltipster('content',rafflesMini);
			}
			userCanPlay(result.extra_chances_used);	
		}
	}, 'text' );	
}

/**** user don't install app yet *****/
function userCanPlay(extraChanceUsed){
	if(typeof(extraChanceUsed) != 'undefined'){
		$.each(EXTRA_TICKETS, function(type, item){
			if(extraChanceUsed[type] == 1)
				$('#'+type).addClass('pressed disabled');
		});
	}
	showStartButton();			// show the button
}

/**** Show start button *****/
function showStartButton(){
	$('.fb-init-loader').hide();
	$('.start-button-con').myFadeIn();
}

/**** If app install checking is timed out, show start button after 10 sec *****/
$(window).bind('load', function(){
	setTimeout(function() {
		if(!APP_INSTALL_CHECKED){
			showStartButton();
		}
	}, 10000);
});

function isExtraChanceEnabled($el){
	if(!$el.hasClass('disabled'))
		return true
	return false;
}

function useExtraChance(id){
	EXTRA_TICKETS[id]['used'] = 1;
	updateRaffleCnt(id);
	$('#'+id).addClass('pressed disabled');
}

function shareCompleted(){
	useExtraChance('share');
}

function inviteCompleted(cnt){
	useExtraChance('invite');
	EXTRA_TICKETS['invite']['invited_cnt'] = cnt;
}

function updateRaffleCnt(type){
	if(type)
		$('#'+type).addClass('pressed');
	
	animateRaffleCnt(++raffle_cnt);
}

function animateRaffleCnt(idx,now){
	var counterPosY = raffle_cnt * $('.raffle-cnt-nmb').height();
	var rafflesOrigY = parseInt($('#raffles').css('top'), 10);
	var rafflesPosY = rafflesOrigY + $('.raffle').height();
	
	if(now){
		$('.raffle-cnt-slider').css("marginTop",-counterPosY);
	}
	else{
		TweenLite.to('.raffle-cnt-slider',0.7,{marginTop:-counterPosY,ease:Power3.easeOut});
		TweenLite.to('#raffles',0.7,{top:rafflesPosY,ease:Power3.easeOut});
	}
}

/**** Save user to database *****/
function saveUser(){
	$.post( 
		'bin/func/save_user.php', 
		{
			fbid: FB_ID, name: FB_NAME, firstname: FB_FIRSTNAME, lastname: FB_LASTNAME, email: FB_EMAIL, gender: FB_GENDER, 
			locale: FB_LOCALE, extratickets: JSON.stringify(EXTRA_TICKETS), 
			liked: IS_LIKED, newlike: NEW_LIKE, newinstall: NEW_INSTALL, mobile: IS_MOBILE_URL
		}, 
		function(data) { 
			result = JSON.parse(data);
			if(result.status = 'ok'){	// if new user
				if(result.raffle_cnt < MAX_TICKET_CNT)
					$('body').addClass('get-more-raffle');
				else
					$('body').removeClass('get-more-raffle');					
				userCanDropToBox(result.raffles,result.start_id);
			}
			else{
				userPlayed(); // user reinstall the app
			}
		}, 
		'text' 
	);
}

function userCanDropToBox(raffles,startId){
	$('#btn-gotobox').next('.ajax-loader').myFadeOut();
	var delay = animRaffleIds(startId);
	renderUserRaffles('#gameover .user-raffles',raffles);
	animRafflesToBox(delay);
}

function renderUserRaffles(el,raffles,wrapCnt){
	var rafflesArray = JSON.parse("[" + raffles + "]");
	
	var rafflesHTML = '';
	var row = '';
	if(wrapCnt)
		rafflesHTML = '<div class="mini-raffle-con">';
	for(var i=0;i<rafflesArray.length;i++){
		row = '<div class="mini-raffle"><span>'+(pad(rafflesArray[i],5))+'</span></div>';		
		if(el)
			row = '<div class="swiper-slide">' + row + '</div>';
		
		rafflesHTML += row;
		if(i%wrapCnt==wrapCnt-1)
			rafflesHTML += '</div><div class="mini-raffle-con">';
	}
	if(wrapCnt)
		rafflesHTML += '</div>';
	
	if(el){
		$('.mini-raffles',$(el)).remove();	
		rafflesHTML = '<div class="swiper-container mini-raffles"><div class="swiper-wrapper">' + rafflesHTML + '</div></div><div class="swiper-button-prev swiper-button-white"></div><div class="swiper-button-next swiper-button-white"></div>';
		
		$(el).append(rafflesHTML);
		
		var centered = rafflesArray.length == 1 ? true : false;
		
		new Swiper('.swiper-container', {
			slidesPerView: max_ticket_row_cnt,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			centeredSlides: centered,
			grabCursor: true,
			freeMode: true,
			mousewheelControl: true,
			keyboardControl: true
		});	 
		
		var collectedTxt = TEXTS['yourRafflesTxt'];
		collectedTxt = collectedTxt.replace('{rafflesCnt}', '<strong>'+rafflesArray.length+'</strong>');
		$('p',$(el)).html(collectedTxt);
	}

	return rafflesHTML;
}

function disableExtraChances(){
	var delay = 0;
	$('.extra-chance-btns').addClass('disabled');
	$('.extra-chance-btns .button').addClass('disabled').each(function(){
		TweenLite.to($(this), 0.6, {backgroundColor:'#B1B1B1',delay:delay});
		delay += 0.1;
	});
}

function animRaffleIds(startId){
	var delay = 0;
	var animTime = 0.4;
	$($('.raffle').get().reverse()).each(function() {
		var id = pad(startId++, 5);
		$(this).html('<span>'+id+'</span>');
		$el = $(this).find('span');
		if(IE==8){
			$el.myFadeIn();
		}
		else{
			TweenLite.to($el, 0, {scale:0, opacity:1});
			TweenLite.to($el, animTime, {scale:1, delay:delay});
		}
		delay += 0.10;
	});
	return delay+animTime;
}

function animRafflesToBox(delay){	
	TweenLite.to('#scissor', 0.6, {top:-6, delay:delay, ease:Power1.easeOut, onComplete:function(){
		setTimeout(function() {
			cutSnd.play();
			TweenLite.to('#cut', 0.5, {width:0, opacity:0, ease:Power3.easeOut});
			var scissorY = IE == 8 ? 20 : -6;
			TweenLite.to('#scissor', 0.5, {right:160,top:scissorY});
			TweenLite.to('.scissor-up', 0.35, {rotation:16});
			TweenLite.to('.scissor-bottom', 0.35, {rotation:-16});
			TweenLite.to('.raffle:eq('+(-raffle_cnt)+')', 1, {marginTop:480, ease:Sine.easeIn, onComplete:function (){
				setTimeout(function() {
					$('.raffle:gt('+(-raffle_cnt-1)+')').hide();
					TweenLite.to('.game-panel', .5, {top:-479});
					TweenLite.to('#scissor', .5, {top:-216});
					TweenLite.to('#raffles', .5, {top:- ((MAX_TICKET_CNT + 2) * ticket_height)});
					TweenLite.to('#box', .5, {bottom:-120, onComplete:function (){
						$('#raffles').hide();
						gameOver();
					}});
				}, 600);
			}});		
		}, 300);
	}});
}

function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function gameOver(){
	trackPage('/gameover','Gameover');
	showPage('gameover');
}

function userPlayed(){
	$('#user-played').addClass('reinstall');
	trackPage('/user-try-to-cheat','User try to cheat');
	showPage('user-played');
}

/**** Save like to database *****/
function saveLike(){
	if(FB_ID)
		$.post( 'bin/func/save_like.php', {fbid: FB_ID});	
}

function createSocialButtons(){
	if(!social_buttons_created){
		social_buttons_created = 1;
		$.each(EXTRA_TICKETS, function(type, item) {
			if(item['btn-plugin'] == 1 && isExtraChanceEnabled($('#'+type))){
				var opts = [];
				switch(type){
					case 'youtube-subscribe': opts = {type:type,pressed_offset_y:3,btn:{channel:EXTRA_TICKETS[type]['channel']},successEvent:extraChanceSuccessClick}; break;
					case 'google-plus': opts = {type:type,pressed_offset_y:3,btn:{url:EXTRA_TICKETS[type]['url']},successEvent:extraChanceSuccessClick}; break;
					case 'twitter-share': opts = {type:type,pressed_offset_y:3,btn:{
						url:EXTRA_TICKETS[type]['url'],
						text:EXTRA_TICKETS[type]['text'],
						hashtags:EXTRA_TICKETS[type]['hashtags']},successEvent:extraChanceSuccessClick
					}; break;
					case 'twitter-follow': opts = {type:type,pressed_offset_y:3,btn:{
						user:EXTRA_TICKETS[type]['user']},successEvent:extraChanceSuccessClick
					}; break;
					case 'linkedin-share': opts = {type:type,pressed_offset_y:3,btn:{
						url:EXTRA_TICKETS[type]['url']},successEvent:extraChanceSuccessClick
					}; break;
					
				}
				$('#'+type).customSocialButton(opts);
			}
			else{
				var btn = $('#'+type+' .button-txt');
				btn.html(btn.data('text'));
			}
		});
	}
}

function extraChanceSuccessClick(id){
	if(isExtraChanceEnabled($('#'+id))){
		switch(id){
			case 'invite': inviteFriends(); break;
			case 'share': shareApp(); break;
			case 'subscribe':
			default:
				useExtraChance(id);
		}
	}
}
	
/**** If page animation started callback *****/ 
function pageAnimStarted(currentId, targetId, $current, $target){
	
	/**** Animate out welcome items *****/
	if(currentId == 'welcome' && targetId == 'game'){
		themeSnd.fadeTo(60, 1000);
		hideTiming();
		$('#raffles').css('top',-((MAX_TICKET_CNT + 2) * ticket_height));	
	}	
	
	/**** Gameover screen *****/
	if(currentId == 'game' && targetId == 'gameover')
		themeSnd.fadeTo(100, 1000);
		
	/**** Go back to Game screen *****/
	if(currentId == 'gameover' && targetId == 'game'){
		raffle_cnt = 0;
		play_again = 1;
		NEW_INSTALL = 0;
		
		$('#scissor,.scissor-up,.scissor-bottom,#raffles .raffle,#cut,#btn-gotobox').removeAttr('style');
		$('#raffles').show();
		$('#raffles .raffle').html('');
		$('.extra-chance-btns,.extra-chance-btn').removeClass('disabled');
		TweenLite.to('.scissor-up', 0, {rotation:0});
		TweenLite.to('.scissor-bottom', 0, {rotation:0});			
		
		trackPage('/get-more-raffle','Get more raffle');
	}			
}

/**** If page animation finished callback *****/ 
function pageAnimEnded(currentId, targetId, $current, $target, isTestCall){

	/**** Animate game page items *****/
	if((currentId == 'welcome' || isTestCall) && targetId == 'game'){
		maximalizeWidth('.extra-chance .extra-chance-btn',15);
		prepareGameScreen();
	}
	
	/**** Animate game page when user come back from gameover *****/
	if(currentId == 'gameover' && targetId == 'game'){
		prepareGameScreen();
	}
	
	/**** Animate start page items *****/
	if(currentId == '' && targetId == 'welcome'){
		TweenLite.to('#welcome-con',0.9,{top:0, ease:Power3.easeOut,delay:0.4, onComplete:function (){
			showTiming();			
		}});
	}

	/**** Animate winner page items *****/
	if(targetId == 'winner'){
		trackPage('/winner-announcement','Winner announcement');
		TweenLite.to('#winner-con',0.9,{top:0, ease:Power3.easeOut,delay:0.4});
	}
	
	/**** Animate start page items *****/
	if(targetId == 'user-played' || targetId == 'gameover'){
		if($('#demo-features').css('z-index')<0)
		$('#demo-features').css('zIndex',10000).myFadeIn(800,0,800);
	}	

}

function prepareGameScreen(){
	TweenLite.to('#box',0.7,{bottom:0,ease:Power3.easeOut});	
	TweenLite.to('.game-panel',0.7,{top:0,ease:Power3.easeOut});
	
	var rafflesPosY = - ((MAX_TICKET_CNT + 2) * ticket_height) + 92 + raffle_cnt*ticket_height;
	
	TweenLite.to('#raffles',0.7,{top:rafflesPosY,ease:Power3.easeOut, onComplete: function (){
		TweenLite.to('#cut', 0.7, {width:176, left:468, ease:Power3.easeOut});
		createSocialButtons();
	}});
	
	animateRaffleCnt(raffle_cnt,1);
	trackPage('/game-started','Game started');
}

function showTiming(){
	$('#timing').show();
	
	 if(IE==8)
		$('#timing').myFadeIn();
 	else
		TweenLite.to('#timing', 0.5, {scale:1, opacity:1, ease:Power2.easeOut}); 	
}

function hideTiming(){
	if(IE==8)
		$('#timing').myFadeOut();
	else{
		TweenLite.to('#timing',0.4,{scale:1.8, opacity:0, ease:Power2.easeOut});	
		setTimeout(function() { $('#timing').remove(); }, 400);
	}
}

function initMobileVersion(){
	viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	viewPortWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	$('.active-orient').hide();
	phone_orientation = getOrientation(viewPortWidth,viewPortHeight);
	
	setTimeout(function() {
		if(phone_orientation == 'landscape')
			showGame();
		else
			hideGame();

		appHeight = $('#main').height();
		appWidth = $('#main').width();
		scaleRatio = (viewPortHeight/appHeight);
		
		newWidth = appWidth * scaleRatio;
		if(newWidth > viewPortWidth){
			scaleRatio = viewPortWidth / appWidth;
			$('body').addClass('width-max').removeClass('height-max');
		}
		else
			$('body').addClass('height-max').removeClass('width-max');
		
		alignMobilePopupFader(newWidth);
			
		TweenLite.to('#main',0,{scale:scaleRatio});		
	}, 250);
}

function alignMobilePopupFader(newWidth){
	var margin = - ( newWidth / 2 );
	$('#mobile-fader-left').css('marginLeft',margin);
	$('#mobile-fader-right').css('marginRight',margin);
}

function getOrientation(viewPortWidth,viewPortHeight){
	orient = viewPortWidth>viewPortHeight ? 'landscape' : 'portrait';

 	// if(!IS_MOBILE)
		orient = 'landscape';

	return orient;
}

function showGame(){
	$('#main').myFadeIn().addClass('active-orient');
	$('#orientation').hide().removeClass('active-orient');
	startGame();
}
function hideGame(){
	$('#orientation').myFadeIn().addClass('active-orient');
	$('#main').hide().removeClass('active-orient');
}

function startGame(){	
	if(!game_started){
		/**** Prepare CSS3 properties *****/
		if(IE!=8)
			TweenLite.to('#timing', 0, {scale:3});	
		
		var startPage = GAME_STATE == 'winner' ? 'winner' : 'welcome';
		
		if(show_played && IS_MOBILE_URL)
			showPage('user-played');
		else
			pageAnimEnded('',startPage);
			
		themeSnd.play();	
		game_started = 1;
	}
}

function appPreloadComplete(){
	if(!IS_MOBILE_URL)
		startGame()
}

function validateCouponCode(code){
	$.post( 'bin/func/check_coupon.php', {code: code}, function(result) {
		var msg = '';
		if(result == 'Valid')
			showPage('game');
		else if(result == 'Invalid')
			msg = TEXTS['couponInvalidTxt'];			
		else
			msg = TEXTS['couponUsedTxt'] + ' ' + result + '!';
		
		if(msg){
			$('#coupon-code').val(msg).addClass('invalid');
			var origTxt = $('#btn-start').data('orig-txt');
			$('#btn-start').html(origTxt).removeClass('pressed');
		}
		
	}, 'text' );	
}