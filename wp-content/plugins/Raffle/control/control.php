<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

function gr_print_form() {

    ob_start();

    // DEV Kit
    // error_reporting(E_ALL);
    // ini_set('display_errors','1');

    // Define some-some
    define ( 'DS', DIRECTORY_SEPARATOR );

    // Get some-some
    require ( __DIR__.DS.'_connect.php' ); // !
    require_once ( __DIR__.DS.'_somesome.php' );

    // Get version
    $app_info = file( __DIR__.DS.'..'.DS.'version.dat' );

    // Get values
    require ( __DIR__.DS.'..'.DS.'bin'.DS.'configuration.php' );
?>

<div id="control_page">

    <!-- Head -->

    <div id="control_header">

        <div class="logout_wrap">
            <button class="logout_btn anim_150">Logout</button>
        </div>
        <div class="control_top_noty_wrap">
            <a class="control_top_noty active js_inner_link" href="settings#game" data-sec="settings" data-hash="#game" <?php echo ( $appOnline == 1 ? '' : 'style="display:none;"' ); ?>>
                <span>Your application is active.</span>
                <span>From: <?php echo $startDate; ?> - To: <?php echo $endDate; ?></span>
            </a>
            <a class="control_top_noty inactive js_inner_link" href="settings#game" data-sec="settings" data-hash="#game" <?php echo ( $appOnline == 0 ? '' : 'style="display:none;"' ); ?>>
                <span>Your application is inactive.</span>
                <span>Go to the game settings, if you want to activate it.</span>
            </a>
        </div>

    </div>

    <!-- Side Panel -->

    <div id="control_side_panel">

        <!-- Logo -->

        <div class="control_logo cf">
            <div class="logo_top"></div>
            <div class="logo_box">
                <img src="img/raffle_logo.png" alt="Raffle" />
            </div>
        </div>

        <!-- Menu -->

        <div id="control_menu" class="control_menu cf">

            <!-- Menu Top -->

            <div class="control_menu_top cf">

                <div class="app_name">
                    <?php echo $appName; ?>
                </div>
                <div class="app_info">
                    Version <?php echo $app_info[0]; ?>
                </div>

            </div>

            <!-- Menupoints -->

            <div class="control_menupoints cf">

                <a class="menupoint diff" href="dashboard" data-sec="dashboard">Dashboard</a>
                <a class="menupoint base" href="users" data-sec="users">Users</a>
                <a class="menupoint diff" href="winners" data-sec="winners">Winners</a>
                <a class="menupoint base" href="settings" data-sec="settings">Settings</a>
                <a class="menupoint diff" href="restart_game" data-sec="restart_game">Restart game</a>

            </div>

            <!-- Menu Bottom -->

            <div class="control_menu_bottom cf">

                <div class="user_info">
                    Current session:<br/>
                    <span>
                        <!-- 2015 / 03 / 30 - 08:54<br/> -->
                        <?php echo $_SERVER['REMOTE_ADDR']; ?><br/>
                        <!-- $admin_info.lastlog_time|date_format:"%Y / %m / %d - %H:%M" -->
                        <!-- $admin_info.lastlog_ip -->
                    </span>
                    <!-- <br/> -->
                    <!-- Last logout:<br/> -->
                    <span>
                        <!-- 2015 / 03 / 30 - 09:37<br/> -->
                        <!-- $admin_info.lastlogout_time|date_format:"%Y / %m / %d - %H:%M" --><br/>
                    </span>
                </div>

                <div class="control_menu_end">&#9679; &#9679; &#9679;</div>

            </div>

        </div>

    </div>

    <!-- Content -->

    <div id="content_container" class="content_container">

    </div>

</div>

<?php

    $gr_form_layout = ob_get_contents();

    ob_end_clean();

    return $gr_form_layout;
}
?>