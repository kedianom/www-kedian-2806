<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// DEV Set
error_reporting(0);
ini_set( "display_errors", 0);

// Start the session
session_start();

// Get url, section, query onLoad
$tl_url = $_SERVER['REQUEST_URI'];
$tl_protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$tl_url_parsed = parse_url($tl_url);
$tl_url_parts = explode('/', $tl_url_parsed['path']);
$tl_url_parts = array_reverse($tl_url_parts);
$tl_sec_onload = $tl_url_parts[0];
$tl_url_parts[0] = '';
$tl_url_parts = array_reverse($tl_url_parts);
$tl_url_onload = $tl_protocol.$_SERVER['HTTP_HOST'].implode('/', $tl_url_parts);
$tl_query_onload = $tl_url_parsed['query'] != NULL ? '?'.$tl_url_parsed['query'] : '';
?>

<!DOCTYPE html>
<html lang="en-GB" data-url_onload="<?php echo $tl_url_onload; ?>" data-sec_onload="<?php echo $tl_sec_onload; ?>" data-query_onload="<?php echo $tl_query_onload; ?>">
<head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>ThemeLab Admin</title>
    <meta name="description" content="ThemeLab Admin">
    <meta name="author" content="ThemeLab" />
    <meta name="dcterms.rightsHolder" content="© 2015 All rights reserved!" />
    <meta name="robots" content="noindex, nofollow" />

    <!-- Icon -->
    <link href="favicon.ico" rel="favicon icon shortcut" />
    <link href="images/favios.png" rel="apple-touch-icon" />

    <!-- CSS -->
    <link href="css/plugins.css?v=1.0.1" type="text/css" rel="stylesheet">
    <link href="css/minicolors.css?v=1.0.2" type="text/css" rel="stylesheet">
    <link href="css/base.css?v=1.0.3" type="text/css" rel="stylesheet">

</head>
<body class="">

    <div id="control_frame">

        <div id="control_loader"></div>

        <div id="control_wrapper">

        <?php

        if ($_SESSION['user_status'] == 'logged_in') {

            // Show the control
            include_once ( 'control.php' );
            echo gr_print_form();

        } else {

            // Show the login
            include_once ( 'login.php' );
            echo gr_print_form();

        }

        ?>

        </div>

    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/greensock.js"></script>
    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/datetimepicker.js"></script>
    <script type="text/javascript" src="js/minicolors.js"></script>
    <script type="text/javascript" src="js/tooltipster.js"></script>
    <script type="text/javascript" src="js/chart.js"></script>
    <script type="text/javascript" src="js/columns.js"></script>
    <script type="text/javascript" src="js/base.js?v=1.1.0"></script>

</body>
</html>