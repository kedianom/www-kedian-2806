<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Get Values
require ( __DIR__.DS.'..'.DS.'bin'.DS.'configuration.php' );

// DB Pass
$db_HOST = $dbHost;
$db_USER = $dbUser;
$db_PASS = $dbPsw;
$db_NAME = $dbName;

// DB Connect
$link = mysqli_connect($db_HOST, $db_USER, $db_PASS, $db_NAME) or die("Error " . mysqli_error($link));

// DB Setup
$query = mysqli_query($link, "SET NAMES 'utf8'");

// DB Prefix
$db_prefix = $dbPrefix;

?>