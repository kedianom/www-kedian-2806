<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// CHECK OUT - Who tries to sneak a peak
function checkReferer(){

    $urldata = parse_url($_SERVER['HTTP_REFERER']);
    return isset( $_SERVER['HTTP_REFERER']) && $urldata['host'] == $_SERVER['HTTP_HOST'];
}

// GET & POST & REQUEST - Clean these!
function clean_input($cleanme) {

    if (is_array($cleanme)) {

        foreach ($cleanme as &$value) {
            $value = clean_input($value);
        }
        return $cleanme;
    }

    else {

        return strip_tags($cleanme); // return mysqli_real_escape_string($cleanme);
    }
}
$_GET = clean_input($_GET);
$_POST = clean_input($_POST);
$_REQUEST = clean_input($_REQUEST);

// CHECK URL - Is file exists
function check_file_url($url_exists) {

    $file_headers = @get_headers($url_exists);

    if (strpos($file_headers[0], '404 Not Found')) {

        return false;
    }

    else {

        return true;
    }
}
?>