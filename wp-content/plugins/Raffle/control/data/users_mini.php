<?php
// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Get some-some
define ( 'DS', DIRECTORY_SEPARATOR );
require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'lib'.DS.'db.class.php' );
require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php' );

// Get database
$db = new db($db_config);
$db->openConnection();

// Block any injection
$db->escapeArray($_POST);

// Switch by action
switch ($_POST['action']) {

    // - - - - - - - - - - - - -

    // List records
    case 'list':

        // Get user list
        $get_user_list = "SELECT ru.*, (SELECT COUNT(id) FROM ".$dbPrefix."_raffle_games AS rg WHERE rg._userId = ru.id) AS game_count FROM ".$dbPrefix."_raffle_users AS ru";
        $sql = $db->query($get_user_list);
        $user_list = array();
        while ($user_data = $db->fetchAssoc($sql)) {
            $user_list[] = $user_data;
        }

        // Set list schema
        $list_schema = array(
            array(
                'header' => 'Profile',
                'key' => '_fbuserID',
                'template' => '<div class="ta_center"><a href="//facebook.com/{{_fbuserID}}" target="_blank"><img class="profile_picture" src="//graph.facebook.com/{{_fbuserID}}/picture" alt="Profile" /></a></div>'
            ),
            array(
                'header' => 'Name',
                'key' => '_fbuserName',
                'template' => '{{_fbuserName}}'
            ),
            array(
                'header' => 'Email',
                'key' => '_fbuserEmail',
                'template' => '<a href="mailto:{{_fbuserEmail}}">{{_fbuserEmail}}</a>'
            ),
            array(
                'header' => 'Registered',
                'key' => '_regDate',
                'template' => '<div class="ta_center">{{_regDate}}</div>'
            ),
            array(
                'header' => 'Games',
                'key' => 'game_count',
                'template' => '<div class="ta_center">{{game_count}}</div>'
            )
        );

        // Pass it through
        $json['data'] = $user_list;
        $json['schema'] = $list_schema;
        $json['searchable'] = array('_fbuserName','_fbuserEmail','_regDate','game_count');
        $json['sortable'] = array('_fbuserName','_fbuserEmail','_regDate','game_count');
        $json['sortby'] = '_fbuserName';
        $json['size'] = 5;

        // Clear these
        unset($get_user_list);

        // Send back the data
        echo json_encode($json);

        break;

    // - - - - - - - - - - - - -

    // Delete records
    case 'delete':

        // Delete record
        $delete_record = "DELETE FROM ".$dbPrefix."_raffle_users
                          WHERE id = ".$_POST['record'];
        $result = $db->query($delete_record);

        // Delete games
        if ($result) {
            $delete_games = "DELETE FROM ".$dbPrefix."_raffle_games
                          WHERE _userId = ".$_POST['record'];
            $result = $db->query($delete_games);
        }

        // Feedback
        if ($result) {

            $json['feedback'] = 'deleted';

        } else {

            $json['feedback'] = 'error';
        }

        // Clear these
        unset($delete_record, $delete_games);

        // Send back the data
        echo json_encode($json);

        break;

    // - - - - - - - - - - - - -
}
?>