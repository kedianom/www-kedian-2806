<?php
// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Get some-some
define ( 'DS', DIRECTORY_SEPARATOR );
require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'lib'.DS.'db.class.php' );
require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php' );

// Get database
$db = new db($db_config);
$db->openConnection();

// Block any injection
$db->escapeArray($_POST);

// Switch by action
switch ($_POST['action']) {

    // - - - - - - - - - - - - -

    // List records
    case 'list':

        // Get user list
        $get_winner_list = "SELECT winners.*, (SELECT COUNT(id) FROM ".$dbPrefix."_raffle_games AS games WHERE games._userId = users.id) AS game_count, users.*
        FROM ".$dbPrefix."_raffle_winners AS winners
        INNER JOIN ".$dbPrefix."_raffle_games AS games ON winners._gameId = games.id
        INNER JOIN ".$dbPrefix."_raffle_users AS users ON games._userId = users.id";
        $sql = $db->query($get_winner_list);
        $winner_list = array();
        while ($winner_data = $db->fetchAssoc($sql)) {
            $winner_list[] = $winner_data;
        }

        // Set list schema
        $list_schema = array(
            array(
                'header' => 'Profile',
                'key' => 'profile',
                'template' => '<div class="ta_center"><a href="//facebook.com/{{_fbuserID}}" target="_blank"><img class="profile_picture" src="//graph.facebook.com/{{_fbuserID}}/picture" alt="Profile" /></a></div>'
            ),
            array(
                'header' => 'ID',
                'key' => '_fbuserID',
                'template' => '{{_fbuserID}}'
            ),
            array(
                'header' => 'Name',
                'key' => '_fbuserName',
                'template' => '{{_fbuserName}}'
            ),
            array(
                'header' => 'Email',
                'key' => '_fbuserEmail',
                'template' => '<a href="mailto:{{_fbuserEmail}}">{{_fbuserEmail}}</a>'
            ),
            array(
                'header' => 'Registered',
                'key' => '_regDate',
                'template' => '<div class="ta_center">{{_regDate}}</div>'
            ),
            array(
                'header' => 'Games',
                'key' => 'game_count',
                'template' => '<div class="ta_center">{{game_count}}</div>'
            ),
            array(
                'header' => 'Functions',
                'key' => 'id',
                'template' => '<div class="ta_center"><button class="js_delete_table_record anim" data-record_id="{{id}}" onClick="tableDeleteFunction($(this));"> </button></div>'
            )
        );

        // Pass it through
        $json['data'] = $winner_list;
        $json['schema'] = $list_schema;
        $json['searchable'] = array('_fbuserID','_fbuserName','_fbuserEmail','_regDate','game_count');
        $json['sortable'] = array('_fbuserID','_fbuserName','_fbuserEmail','_regDate','game_count');
        $json['sortby'] = '_fbuserName';
        $json['size'] = 25;

        // Clear these
        unset($get_winner_list);

        // Send back the data
        echo json_encode($json);

        break;

    // - - - - - - - - - - - - -

    // Delete records
    case 'delete':

        // Delete record
        $delete_record = "DELETE FROM raffle_winners
                          WHERE id = ".$_POST['record'];
        $result = $db->query($delete_record);

        // Delete games
        if ($result) {
            $delete_games = "DELETE FROM raffle_games
                          WHERE _userId = ".$_POST['record'];
            $result = $db->query($delete_games);
        }

        // Feedback
        if ($result) {

            $json['feedback'] = 'deleted';

        } else {

            $json['feedback'] = 'error';
        }

        // Clear these
        unset($delete_record, $delete_games);

        // Send back the data
        echo json_encode($json);

        break;

    // - - - - - - - - - - - - -
}
?>