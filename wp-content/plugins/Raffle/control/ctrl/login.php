<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Start the session
session_start();

// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Get some-some
require_once ( __DIR__.DS.'..'.DS.'_connect.php' );
require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

// You shall not pass!
if (!checkReferer() OR !$_POST) exit;

// Set the variables
$form_login = $_POST;

// Check if user exists
$query = "SELECT id
            FROM ".$db_prefix."_raffle_admin
            WHERE _username = '".mysqli_real_escape_string($link, $form_login['username'])."'
            AND _password = '".mysqli_real_escape_string($link, $form_login['password'])."'";
$result = mysqli_query ($link, $query);

if (mysqli_num_rows($result) === 1) {

    // Get the user info
    $user = mysqli_fetch_assoc($result);

    // Set the session
	$_SESSION['user_id'] = (int) $user['id'];
    $_SESSION['user_status'] = 'logged_in';

    // Show the control
    include_once ( __DIR__.DS.'..'.DS.'control.php' );
    $data['inputs'] = gr_print_form();

    $data['success'] = 'valid_user';
}
else {
    $data['error'] = 'invalid_user';
}

// Clear these
unset($form_login, $result);

// Send back the data
echo json_encode($data);
?>