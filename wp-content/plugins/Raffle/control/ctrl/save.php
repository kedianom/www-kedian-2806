<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Get some-some
require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

// You shall not pass!
if (!checkReferer() OR !$_POST) exit;

// Upload images
if (!empty($_FILES['brandLogo']['tmp_name'])) {

	$posted_file_temp = $_FILES['brandLogo']['tmp_name'];
	$posted_file_info = pathinfo($_FILES['brandLogo']['name']);
	$posted_file_ext = $posted_file_info['extension'];

	$allowed_file_ext = array("png");

	if (in_array($posted_file_ext, $allowed_file_ext)) {

		move_uploaded_file($posted_file_temp, __DIR__.DS.'..'.DS.'..'.DS.'images'.DS.'custom'.DS.'raffle-brand-logo.'.$posted_file_ext);

	} else {

		$data['error'] = 'file';
		$data['error_type'] = 'image';
		$data['error_msg'] = 'The logo image\'s filetype is not allowed.';
	}
}
if (!empty($_FILES['mainPrizeImg']['tmp_name'])) {

	$posted_file_temp = $_FILES['mainPrizeImg']['tmp_name'];
	$posted_file_info = pathinfo($_FILES['mainPrizeImg']['name']);
	$posted_file_ext = $posted_file_info['extension'];

	$allowed_file_ext = array("jpg", "jpeg");

	if (in_array($posted_file_ext, $allowed_file_ext)) {

		move_uploaded_file($posted_file_temp, __DIR__.DS.'..'.DS.'..'.DS.'images'.DS.'custom'.DS.'raffle-prize.'.$posted_file_ext);

	} else {

		$data['error'] = 'file';
		$data['error_type'] = 'image';
		$data['error_msg'] = 'The prize image\'s filetype is not allowed.';
	}
}

// Set the variables
$form_control = $_POST;

// Prepare the variables
if (RESTART !== true) {
	$form_control['fontColor'] = trim($form_control['fontColor'],'#');
	$form_control['appShareTxt'] = $form_control['appDescTxt'];
	$form_control['appInviteTxt'] = $form_control['appDescTxt'];
	$form_control['muteSounds'] = ($form_control['muteSounds'] == '1' ? 1 : 0);
	$form_control['onlyCoupon'] = ($form_control['onlyCoupon'] =='1' ? 1 : 0);
	$form_control['extraTickets']['invite']['status'] = ($form_control['extraTickets']['invite']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['share']['status'] = ($form_control['extraTickets']['share']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['youtube-subscribe']['status'] = ($form_control['extraTickets']['youtube-subscribe']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['google-plus']['status'] = ($form_control['extraTickets']['google-plus']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['twitter-share']['status'] = ($form_control['extraTickets']['twitter-share']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['twitter-follow']['status'] = ($form_control['extraTickets']['twitter-follow']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['linkedin-share']['status'] = ($form_control['extraTickets']['linkedin-share']['status'] == '1' ? 1 : 0);
	$form_control['extraTickets']['subscribe']['status'] = ($form_control['extraTickets']['subscribe']['status'] == '1' ? 1 : 0);
	$form_control['updatesEnabled'] = ($form_control['updatesEnabled'] == '1' ? 1 : 0);
}

// Set the config file's path
$config_path = __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php';

// Get vars before including config
$basic_values = get_defined_vars();

// Include config
include ( $config_path );

// Get vars after including config
$plus_values = get_defined_vars();

// Keep only the vars of config
$config_values = array_diff($plus_values, $basic_values);

// Update config with the posted values
$config_values = array_merge($config_values, $form_control);

// Build the new config file
$config_file = "<?php\n\n";
foreach ($config_values as $variable => $value) {

     $config_file .= "$".$variable." = ".var_export($value, TRUE).";\n";
}
$config_file .= "\n?>";

// Write it back to the file
$config_modified = file_put_contents ($config_path, $config_file);

// Give some feedback
if ($config_modified && !$data['error']): $data['success'] = 'allgood';
else: $data['error'] = 'write';
endif;

// Clear these
unset($form_control, $config_path, $config_file);

// Send back the data
echo json_encode($data);
?>