<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Truncate tables
function truncateTables() {

	// Get some-some
	require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'lib'.DS.'db.class.php' );
	require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php' );

	// Validate it
	if ($_POST['secret'] != $appSecret) {
		echo 'Invalid access!';
		die();
	}

	// Truncate tables in SQL
	$db = new db($db_config);
	$db->openConnection();
	$db->escapeArray($_POST);
	$db->query("TRUNCATE TABLE ".$dbPrefix."_raffle_games");
	$db->query("TRUNCATE TABLE ".$dbPrefix."_raffle_winners");
	$db->query("TRUNCATE TABLE ".$dbPrefix."_raffle_coupons");
	$db->closeConnection();
}
truncateTables();

// Set state and timing to default in configuration.php
// > Clear previously defined vars
unset($_POST['secret']);
// > Set the modified vars
$_POST['appOnline'] = 0;
$_POST['appConfigured'] = 0;
$_POST['startDate'] = "";
$_POST['endDate'] = "";
// > Set the basic vars
$_POST['extraTickets']['invite']['used'] = "0";
$_POST['extraTickets']['invite']['btn-plugin'] = "0";
$_POST['extraTickets']['invite']['invited_cnt'] = "0";
$_POST['extraTickets']['invite']['limit'] = "0";
$_POST['extraTickets']['share']['used'] = "0";
$_POST['extraTickets']['share']['btn-plugin'] = "0";
$_POST['extraTickets']['youtube-subscribe']['used'] = "0";
$_POST['extraTickets']['youtube-subscribe']['btn-plugin'] = "1";
$_POST['extraTickets']['google-plus']['used'] = "0";
$_POST['extraTickets']['google-plus']['btn-plugin'] = "1";
$_POST['extraTickets']['twitter-share']['used'] = "0";
$_POST['extraTickets']['twitter-share']['btn-plugin'] = "1";
$_POST['extraTickets']['twitter-follow']['used'] = "0";
$_POST['extraTickets']['twitter-follow']['btn-plugin'] = "1";
$_POST['extraTickets']['linkedin-share']['used'] = "0";
$_POST['extraTickets']['linkedin-share']['btn-plugin'] = "1";
$_POST['extraTickets']['subscribe']['used'] = "0";
$_POST['extraTickets']['subscribe']['btn-plugin'] = "0";
// > Merge the modified ones with the original configuration file ( + Noty )
define ( 'RESTART', true );
include_once ( __DIR__.DS.'..'.DS.'ctrl'.DS.'save.php' );

// Clear the cache directory
foreach (new DirectoryIterator( __DIR__.DS.'..'.DS.'..'.DS.'cache' ) as $fileInfo) {
    if (!$fileInfo->isDot()) {
        unlink ($fileInfo->getPathname());
    }
}
?>