<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Get some-some
require_once ( __DIR__.DS.'..'.DS.'_connect.php' );
require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

// You shall not pass!
if (!checkReferer() OR !$_POST) exit;

// Set the variables
$switch = $_POST;

// Set the sections
$selected_sec = __DIR__.DS.'..'.DS.'sec'.DS.$switch['sec'].'.php';
$default_sec = __DIR__.DS.'..'.DS.'sec'.DS.'dashboard.php';

// Include existing section
if (file_exists ( $selected_sec )) {

	include_once ( $selected_sec );
	$data['sec'] = gr_print_form(); /* * */
	$data['url'] = $switch['url'].$switch['sec'].( !empty($switch['query']) ? $switch['query'] : '' ).( !empty($switch['hash']) ? $switch['hash'] : '' );

} else {

	include_once ( $default_sec );
	$data['sec'] = gr_print_form();
	$data['url'] = $switch['url'].'dashboard';
}

// Clear these
unset($switch);

// Send back the data
echo json_encode($data);
?>