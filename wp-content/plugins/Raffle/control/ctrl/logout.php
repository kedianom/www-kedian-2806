<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

// DEV Kit
// error_reporting(E_ALL);
// ini_set('display_errors','1');

// Start the session
session_start();

// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Get some-some
require_once ( __DIR__.DS.'..'.DS.'_connect.php' );
require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

// You shall not pass!
if (!checkReferer()) exit;

session_destroy();

include_once ( __DIR__.DS.'..'.DS.'login.php' );
$data['inputs'] = gr_print_form();

// Send back the data
echo json_encode($data);
?>