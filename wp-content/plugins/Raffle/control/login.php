<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

function gr_print_form() {

    ob_start();

    // DEV Kit
    // error_reporting(E_ALL);
    // ini_set('display_errors','1');

    // Define some-some
    define ( 'DS', DIRECTORY_SEPARATOR );

    // Get some-some
    require ( __DIR__.DS.'_connect.php' ); // !
    require_once ( __DIR__.DS.'_somesome.php' );

?>

<div id="login_page">

    <!-- Head -->

    <header id="login_header" class="cf">

        <div class="login_logo_frame">
            <div class="login_logo_wrap_svg">
                <div class="logo_box">
                    <img src="img/theme_lab_logo_text.svg" alt="ThemeLab Text" class="logo_text" />
                    <img src="img/theme_lab_logo_icon.svg" alt="ThemeLab Icon" class="logo_icon" />
                    <img src="img/theme_lab_logo_sign.svg" alt="ThemeLab Sign" class="logo_sign" />
                    <div class="logo_bubble_1 bubble"></div>
                    <div class="logo_bubble_2 bubble"></div>
                    <div class="logo_bubble_3 bubble"></div>
                    <div class="logo_bubble_4 bubble"></div>
                    <div class="logo_bubble_5 bubble"></div>
                </div>
            </div>
        </div>

    </header>

    <!-- Content -->

    <div id="login_inputs" class="cf">

        <form id="login_form" method="post">

            <fieldset>

                <div class="title l_login">Login</div>

                <!-- Input -->

                <div class="form_set_line anim">
                    <label for="username" class="anim l_username">Username</label>
                    <input id="username" name="username" type="text" placeholder="Please enter your username" required="true" class="anim l_enter_username" />
                </div>

                <div class="form_set_line anim">
                    <label for="password" class="anim l_password">Password</label>
                    <input id="password" name="password" type="password" placeholder="Please enter your password" required="true" class="anim l_enter_password" />
                </div>

                <!-- Noty -->

                <div class="form_set_succes l_login_success">Success!</div>
                <div class="form_set_error l_login_error">Invalid username or password!</div>

                <!-- Enter -->

                <div class="form_set_send">
                    <input id="login_btn" type="submit" class="anim_150 l_enter" value="Enter" />
                </div>

            </fieldset>

        </form>

    </div>

    <!-- Footer -->

    <footer id="login_footer" class="cf">

    </footer>

</div>

<?php

    $gr_form_layout = ob_get_contents();

    ob_end_clean();

    return $gr_form_layout;
}
?>