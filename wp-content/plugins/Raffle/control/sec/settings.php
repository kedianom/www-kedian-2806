<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

function gr_print_form() {

    ob_start();

    // DEV Kit
    // error_reporting(E_ALL);
    // ini_set('display_errors','1');

    // Define some-some
    define ( 'DS', DIRECTORY_SEPARATOR );

    // Get some-some
    require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

    // Get values
    require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php' );
?>

    <div id="control_inputs" class="cf">

        <div class="control_breadcrumb">Admin / <span class="active">Settings</span></div>

        <form class="control_form" enctype="multipart/form-data" method="post">

            <fieldset>

                <div class="control_field">

                    <div class="tab_nav">

                        <div>
                            <div class="anim">
                                <a data-tab_name="company" class="anim">Company</a>
                            </div>
                            <div class="anim">
                                <a data-tab_name="game" class="anim">Game</a>
                            </div>
                            <div class="anim">
                                <a data-tab_name="tickets" class="anim">Tickets</a>
                            </div>
                            <div class="anim">
                                <a data-tab_name="design" class="anim">Design</a>
                            </div>
                            <div class="anim">
                                <a data-tab_name="prize" class="anim">Prize</a>
                            </div>
                            <div class="anim">
                                <a data-tab_name="date" class="anim">Date</a>
                            </div>
                            <div class="anim">
                                <a data-tab_name="contact" class="anim">Contact</a>
                            </div>
                        </div>

                    </div>

                    <div class="tab_wrapper anim_300">

                        <!-- Company -->

                        <div id="tab_company" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Your company
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="brandName" class="anim">
                                            Name
                                        </label>
                                        <input id="brandName" name="brandName" type="text" class="anim" value="<?php echo $brandName; ?>" placeholder="eg: Theme-lab" />
                                    </div>

                                    <div class="form_set_line anim">
                                        <label class="anim">
                                            Logo
                                            <i>(png)</i>
                                        </label>

                                        <label class="fileinput_custom_btn anim" for="brandLogo">
                                            <cite class="fileinput_custom_text">Upload file</cite>
                                            <input id="brandLogo" class="fileinput_custom_inp js_fileinput_btn" name="brandLogo" type="file" />
                                        </label>

                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="brandWebSite" class="anim">
                                            Website
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="If you fill out this input, logo will be clickable and open your site in a new window." />
                                        </label>
                                        <input id="brandWebSite" name="brandWebSite" type="text" class="anim" value="<?php echo $brandWebSite; ?>" placeholder="eg: http://yourdomain.com" />
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Game -->

                        <div id="tab_game" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Game
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="appName" class="noty_required anim">
                                            Title
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Maximum length 25 character!" />
                                        </label>
                                        <input id="appName" name="appName" type="text" class="js_input_validate anim" value="<?php echo $appName; ?>" placeholder="" maxlength="25"/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="appDescTxt" class="noty_required anim">
                                            Desc
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Description, this will be display if somebody share the app, or invite a friend." />
                                        </label>
                                        <textarea id="appDescTxt" name="appDescTxt" class="js_input_validate anim"><?php echo $appDescTxt; ?></textarea>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="startDate" class="noty_required anim">
                                            Start date
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Game will start at this day 8:00 AM." />
                                        </label>
                                        <input id="startDate" name="startDate" type="text" class="js_input_validate js_datetimepicker anim" value="<?php echo $startDate; ?>" placeholder=""/>
                                    </div>
                                    <div class="form_set_line anim">
                                        <label for="endDate" class="noty_required anim">
                                            End date
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Game will finish at this day 8:00 PM." />
                                        </label>
                                        <input id="endDate" name="endDate" type="text" class="js_input_validate js_datetimepicker anim" value="<?php echo $endDate; ?>" placeholder=""/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="muteSounds" class="anim">
                                            Mute sound
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="If checked, all game sounds will be disabled." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="muteSounds" name="muteSounds" type="checkbox" value="1" <?php if ($muteSounds == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="onlyCoupon" class="anim">
                                            Only coupon
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="If checked, only users with coupon can play! You have to ensure about coupon code generation and distribution!" />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="onlyCoupon" name="onlyCoupon" type="checkbox" value="1" <?php if ($onlyCoupon == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="likeUrl" class="noty_required anim">
                                            Like URL
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can like this URL." />
                                        </label>
                                        <input id="likeUrl" name="likeUrl" type="text" class="js_input_validate anim" value="<?php echo $likeUrl; ?>" placeholder="eg: https://www.facebook.com/theme.labor"/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label class="anim">
                                            Like gate
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="If enabled, users have to like your page for playing." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_30 anim">
                                                <input name="likeGate" type="radio" value="disabled" <?php if ($likeGate == 'disabled') { echo 'checked="checked"'; } ?> />
                                                Disabled
                                            </label>
                                            <label class="choose_30 anim">
                                                <input name="likeGate" type="radio" value="enabled" <?php if ($likeGate == 'enabled') { echo 'checked="checked"'; } ?> />
                                                Enabled
                                            </label>
                                            <label class="choose_30 anim">
                                                <input name="likeGate" type="radio" value="invisible" <?php if ($likeGate == 'invisible') { echo 'checked="checked"'; } ?> />
                                                Invisible <img class="info tooltip" src="img/info.png" alt="Info" title="If checked, users will automatically like your page when they click on the Play button." />
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="appOnline" class="noty_required anim">
                                            Application status:
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_50 anim">
                                                <input name="appOnline" type="radio" class="js_input_validate js_app_status" value="1" <?php if ($appOnline == 1) { echo 'checked="checked"'; } ?> />
                                                Active
                                            </label>
                                            <label class="choose_50 anim">
                                                <input name="appOnline" type="radio" class="js_input_validate js_app_status" value="0" <?php if ($appOnline == 0) { echo 'checked="checked"'; } ?> />
                                                Inactive
                                            </label>
                                        </div>
                                        <div class="custom_noty error">
                                            Fill all required fields to be able to activate the application!
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Tickets -->

                        <div id="tab_tickets" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Tickets
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="initTicketCnt" class="anim">
                                            Initial ticket count
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Number of initial tickets. If users want to earn extra tickets, they have to do some activities, which are listed below. " />
                                        </label>
                                        <input id="initTicketCnt" name="initTicketCnt" type="text" class="anim" value="<?php echo $initTicketCnt; ?>" placeholder=""/>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                Facebook
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="inviteEnabled" class="anim">
                                            Invite
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can invite friends to get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="inviteEnabled" name="extraTickets[invite][status]" data-require_group_selector="facebook-invite" type="checkbox" value="1" <?php if ($extraTickets['invite']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="inviteLimit" class="anim">
                                            Invite limit
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Minimal number of friends to invite for get an extra ticket." />
                                        </label>
                                        <input id="inviteLimit" name="extraTickets[invite][limit]" data-require_group="facebook-invite" type="text" class="anim" value="<?php echo $extraTickets['invite']['limit']; ?>" placeholder=""/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="ShareStatus" class="anim">
                                            Share
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can share the game to get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="ShareStatus" name="extraTickets[share][status]" type="checkbox" value="1" <?php if ($extraTickets['share']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                Youtube
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="youtubeSubscribeStatus" class="anim">
                                            Subscribe
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can subscribe to your Youtube channel for get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="youtubeSubscribeStatus" name="extraTickets[youtube-subscribe][status]" data-require_group_selector="youtube-subscribe" type="checkbox" value="1" <?php if ($extraTickets['youtube-subscribe']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="youtubeSubscribeChannel" class="anim">
                                            Channel name
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Name or ID of your Youtube channel." />
                                        </label>
                                        <input id="youtubeSubscribeChannel" name="extraTickets[youtube-subscribe][channel]" data-require_group="youtube-subscribe" type="text" class="anim" value="<?php echo $extraTickets['youtube-subscribe']['channel']; ?>" placeholder="eg: YourChannelName"/>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                Google
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="googlePlusStatus" class="anim">
                                            Plus
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can Google+ for get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="googlePlusStatus" name="extraTickets[google-plus][status]" data-require_group_selector="google-plus" type="checkbox" value="1" <?php if ($extraTickets['google-plus']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="googlePlusUrl" class="anim">
                                            URL
                                        </label>
                                        <input id="googlePlusUrl" name="extraTickets[google-plus][url]" data-require_group="google-plus" type="text" class="anim" value="<?php echo $extraTickets['google-plus']['url']; ?>" placeholder="eg: https://apps.facebook.com/raffle_game/"/>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                Twitter / Share
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="twitterShareStatus" class="anim">
                                            Share
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can share (tweet) on Twitter for get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="twitterShareStatus" name="extraTickets[twitter-share][status]" data-require_group_selector="twitter-share" type="checkbox" value="1" <?php if ($extraTickets['twitter-share']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="twitterShareUrl" class="anim">
                                            Share URL
                                        </label>
                                        <input id="twitterShareUrl" name="extraTickets[twitter-share][url]" data-require_group="twitter-share" type="text" class="anim" value="<?php echo $extraTickets['twitter-share']['url']; ?>" placeholder="eg: https://apps.facebook.com/raffle_game/"/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="twitterShareText" class="anim">
                                            Tweet text
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="The default, highlighted text a user sees in the Tweet Web Intent." />
                                        </label>
                                        <input id="twitterShareText" name="extraTickets[twitter-share][text]" data-require_group="twitter-share" type="text" class="anim" value="<?php echo $extraTickets['twitter-share']['text']; ?>" placeholder=""/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="twitterShareHashtags" class="anim">
                                            Hashtags
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="A list of hashtags to be appended to default Tweet text where appropriate." />
                                        </label>
                                        <input id="twitterShareHashtags" name="extraTickets[twitter-share][hashtags]" data-require_group="twitter-share" type="text" class="anim" value="<?php echo $extraTickets['twitter-share']['hashtags']; ?>" placeholder=""/>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                Twitter / Follow
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="twitterFollowStatus" class="anim">
                                            Follow
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can share (tweet) on Twitter for get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="twitterFollowStatus" name="extraTickets[twitter-follow][status]" data-require_group_selector="twitter-follow" type="checkbox" value="1" <?php if ($extraTickets['twitter-follow']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="twitterFollowUser" class="anim">
                                            User
                                        </label>
                                        <input id="twitterFollowUser" name="extraTickets[twitter-follow][user]" data-require_group="twitter-follow" type="text" class="anim" value="<?php echo $extraTickets['twitter-follow']['user']; ?>" placeholder="eg: theme_labor"/>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                LinkedIn
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="linkedinShareStatus" class="anim">
                                            Share
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can share on LinkedIn for get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="linkedinShareStatus" name="extraTickets[linkedin-share][status]" data-require_group_selector="linkedin-share" type="checkbox" value="1" <?php if ($extraTickets['linkedin-share']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="linkedinUrl" class="anim">
                                            URL
                                        </label>
                                        <input id="linkedinUrl" name="extraTickets[linkedin-share][url]" data-require_group="linkedin-share" type="text" class="anim" value="<?php echo $extraTickets['linkedin-share']['url']; ?>" placeholder="eg: https://apps.facebook.com/raffle_game"/>
                                    </div>

                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel_head">
                                Other
                                </div>
                                <div class="panel_content">

                                    <!-- * * * * * -->

                                    <div class="form_set_line anim">
                                        <label for="subscribeStatus" class="anim">
                                            E-mail subscribe
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Users can subscribe to your newsletter for get an extra ticket." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="subscribeStatus" name="extraTickets[subscribe][status]" type="checkbox" value="1"   <?php if ($extraTickets['subscribe']['status'] == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Design -->

                        <div id="tab_design" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Design scheme
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="backgroundSkin" class="anim">
                                            Background
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_30 anim">
                                                <input name="backgroundSkin" type="radio" value="red" <?php if ($backgroundSkin == 'red') { echo 'checked="checked"'; } ?> />
                                                Red <span class="sample_red"></span>
                                            </label>
                                            <label class="choose_30 anim">
                                                <input name="backgroundSkin" type="radio" value="green" <?php if ($backgroundSkin == 'green') { echo 'checked="checked"'; } ?> />
                                                Green <span class="sample_green"></span>
                                            </label>
                                            <label class="choose_30 anim">
                                                <input name="backgroundSkin" type="radio" value="blue" <?php if ($backgroundSkin == 'blue') { echo 'checked="checked"'; } ?> />
                                                Blue <span class="sample_blue"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="fontColor" class="anim">
                                            Font color
                                        </label>
                                        <input id="fontColor" name="fontColor" type="text" class="js_colorpicker anim" value="<?php echo '#'.$fontColor; ?>" placeholder=""/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label class="anim">
                                            Text direction
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Set the direction of text flow (e.g. right-to-left for Hebrew or Arabic)" />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_50 anim">
                                                <input name="textDirection" type="radio" value="ltr" <?php if ($textDirection == 'ltr') { echo 'checked="checked"'; } ?> />
                                                Left to Right (ltr)
                                            </label>
                                            <label class="choose_50 anim">
                                                <input name="textDirection" type="radio" value="rtl" <?php if ($textDirection == 'rtl') { echo 'checked="checked"'; } ?> />
                                                Right to Left (rtl)
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Prize -->

                        <div id="tab_prize" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Prize
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="prizeName" class="noty_required anim">
                                            Name
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Maximum length 30 character" />
                                        </label>
                                        <input id="prizeName" name="prizeName" type="text" class="js_input_validate anim" value="<?php echo $prizeName; ?>" placeholder="eg: Apple iPad Air 5" maxlength="30"/>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label class="anim">
                                            Image
                                            <i>(jpg)</i>
                                        </label>
                                        <label class="fileinput_custom_btn anim" for="mainPrizeImg">
                                            <cite class="fileinput_custom_text">Upload file</cite>
                                            <input id="mainPrizeImg" class="fileinput_custom_inp js_fileinput_btn" name="mainPrizeImg" type="file" />
                                        </label>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Date -->

                        <div id="tab_date" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Date format
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="locale" class="anim">
                                            Locale
                                        </label>
                                        <select id="locale" name="locale" class="anim">
                                            <option <?php if ($locale == 'aa_DJ') { echo'selected'; } ?> value="aa_DJ">Afar (Djibouti)</option>
                                            <option <?php if ($locale == 'aa_ER') { echo'selected'; } ?> value="aa_ER">Afar (Eritrea)</option>
                                            <option <?php if ($locale == 'aa_ET') { echo'selected'; } ?> value="aa_ET">Afar (Ethiopia)</option>
                                            <option <?php if ($locale == 'af_ZA') { echo'selected'; } ?> value="af_ZA">Afrikaans (South Africa)</option>
                                            <option <?php if ($locale == 'sq_AL') { echo'selected'; } ?> value="sq_AL">Albanian (Albania)</option>
                                            <option <?php if ($locale == 'sq_MK') { echo'selected'; } ?> value="sq_MK">Albanian (Macedonia)</option>
                                            <option <?php if ($locale == 'am_ET') { echo'selected'; } ?> value="am_ET">Amharic (Ethiopia)</option>
                                            <option <?php if ($locale == 'ar_DZ') { echo'selected'; } ?> value="ar_DZ">Arabic (Algeria)</option>
                                            <option <?php if ($locale == 'ar_BH') { echo'selected'; } ?> value="ar_BH">Arabic (Bahrain)</option>
                                            <option <?php if ($locale == 'ar_EG') { echo'selected'; } ?> value="ar_EG">Arabic (Egypt)</option>
                                            <option <?php if ($locale == 'ar_IN') { echo'selected'; } ?> value="ar_IN">Arabic (India)</option>
                                            <option <?php if ($locale == 'ar_IQ') { echo'selected'; } ?> value="ar_IQ">Arabic (Iraq)</option>
                                            <option <?php if ($locale == 'ar_JO') { echo'selected'; } ?> value="ar_JO">Arabic (Jordan)</option>
                                            <option <?php if ($locale == 'ar_KW') { echo'selected'; } ?> value="ar_KW">Arabic (Kuwait)</option>
                                            <option <?php if ($locale == 'ar_LB') { echo'selected'; } ?> value="ar_LB">Arabic (Lebanon)</option>
                                            <option <?php if ($locale == 'ar_LY') { echo'selected'; } ?> value="ar_LY">Arabic (Libya)</option>
                                            <option <?php if ($locale == 'ar_MA') { echo'selected'; } ?> value="ar_MA">Arabic (Morocco)</option>
                                            <option <?php if ($locale == 'ar_OM') { echo'selected'; } ?> value="ar_OM">Arabic (Oman)</option>
                                            <option <?php if ($locale == 'ar_QA') { echo'selected'; } ?> value="ar_QA">Arabic (Qatar)</option>
                                            <option <?php if ($locale == 'ar_SA') { echo'selected'; } ?> value="ar_SA">Arabic (Saudi Arabia)</option>
                                            <option <?php if ($locale == 'ar_SD') { echo'selected'; } ?> value="ar_SD">Arabic (Sudan)</option>
                                            <option <?php if ($locale == 'ar_SY') { echo'selected'; } ?> value="ar_SY">Arabic (Syria)</option>
                                            <option <?php if ($locale == 'ar_TN') { echo'selected'; } ?> value="ar_TN">Arabic (Tunisia)</option>
                                            <option <?php if ($locale == 'ar_AE') { echo'selected'; } ?> value="ar_AE">Arabic (United Arab Emirates)</option>
                                            <option <?php if ($locale == 'ar_YE') { echo'selected'; } ?> value="ar_YE">Arabic (Yemen)</option>
                                            <option <?php if ($locale == 'an_ES') { echo'selected'; } ?> value="an_ES">Aragonese (Spain)</option>
                                            <option <?php if ($locale == 'hy_AM') { echo'selected'; } ?> value="hy_AM">Armenian (Armenia)</option>
                                            <option <?php if ($locale == 'as_IN') { echo'selected'; } ?> value="as_IN">Assamese (India)</option>
                                            <option <?php if ($locale == 'ast_ES') { echo'selected'; } ?> value="ast_ES">Asturian (Spain)</option>
                                            <option <?php if ($locale == 'az_AZ') { echo'selected'; } ?> value="az_AZ">Azerbaijani (Azerbaijan)</option>
                                            <option <?php if ($locale == 'az_TR') { echo'selected'; } ?> value="az_TR">Azerbaijani (Turkey)</option>
                                            <option <?php if ($locale == 'eu_FR') { echo'selected'; } ?> value="eu_FR">Basque (France)</option>
                                            <option <?php if ($locale == 'eu_ES') { echo'selected'; } ?> value="eu_ES">Basque (Spain)</option>
                                            <option <?php if ($locale == 'be_BY') { echo'selected'; } ?> value="be_BY">Belarusian (Belarus)</option>
                                            <option <?php if ($locale == 'bem_ZM') { echo'selected'; } ?> value="bem_ZM">Bemba (Zambia)</option>
                                            <option <?php if ($locale == 'bn_BD') { echo'selected'; } ?> value="bn_BD">Bengali (Bangladesh)</option>
                                            <option <?php if ($locale == 'bn_IN') { echo'selected'; } ?> value="bn_IN">Bengali (India)</option>
                                            <option <?php if ($locale == 'ber_DZ') { echo'selected'; } ?> value="ber_DZ">Berber (Algeria)</option>
                                            <option <?php if ($locale == 'ber_MA') { echo'selected'; } ?> value="ber_MA">Berber (Morocco)</option>
                                            <option <?php if ($locale == 'byn_ER') { echo'selected'; } ?> value="byn_ER">Blin (Eritrea)</option>
                                            <option <?php if ($locale == 'bs_BA') { echo'selected'; } ?> value="bs_BA">Bosnian (Bosnia and Herzegovina)</option>
                                            <option <?php if ($locale == 'br_FR') { echo'selected'; } ?> value="br_FR">Breton (France)</option>
                                            <option <?php if ($locale == 'bg_BG') { echo'selected'; } ?> value="bg_BG">Bulgarian (Bulgaria)</option>
                                            <option <?php if ($locale == 'my_MM') { echo'selected'; } ?> value="my_MM">Burmese (Myanmar [Burma])</option>
                                            <option <?php if ($locale == 'ca_AD') { echo'selected'; } ?> value="ca_AD">Catalan (Andorra)</option>
                                            <option <?php if ($locale == 'ca_FR') { echo'selected'; } ?> value="ca_FR">Catalan (France)</option>
                                            <option <?php if ($locale == 'ca_IT') { echo'selected'; } ?> value="ca_IT">Catalan (Italy)</option>
                                            <option <?php if ($locale == 'ca_ES') { echo'selected'; } ?> value="ca_ES">Catalan (Spain)</option>
                                            <option <?php if ($locale == 'zh_CN') { echo'selected'; } ?> value="zh_CN">Chinese (China)</option>
                                            <option <?php if ($locale == 'zh_HK') { echo'selected'; } ?> value="zh_HK">Chinese (Hong Kong SAR China)</option>
                                            <option <?php if ($locale == 'zh_SG') { echo'selected'; } ?> value="zh_SG">Chinese (Singapore)</option>
                                            <option <?php if ($locale == 'zh_TW') { echo'selected'; } ?> value="zh_TW">Chinese (Taiwan)</option>
                                            <option <?php if ($locale == 'cv_RU') { echo'selected'; } ?> value="cv_RU">Chuvash (Russia)</option>
                                            <option <?php if ($locale == 'kw_GB') { echo'selected'; } ?> value="kw_GB">Cornish (United Kingdom)</option>
                                            <option <?php if ($locale == 'crh_UA') { echo'selected'; } ?> value="crh_UA">Crimean Turkish (Ukraine)</option>
                                            <option <?php if ($locale == 'hr_HR') { echo'selected'; } ?> value="hr_HR">Croatian (Croatia)</option>
                                            <option <?php if ($locale == 'cs_CZ') { echo'selected'; } ?> value="cs_CZ">Czech (Czech Republic)</option>
                                            <option <?php if ($locale == 'da_DK') { echo'selected'; } ?> value="da_DK">Danish (Denmark)</option>
                                            <option <?php if ($locale == 'dv_MV') { echo'selected'; } ?> value="dv_MV">Divehi (Maldives)</option>
                                            <option <?php if ($locale == 'nl_AW') { echo'selected'; } ?> value="nl_AW">Dutch (Aruba)</option>
                                            <option <?php if ($locale == 'nl_BE') { echo'selected'; } ?> value="nl_BE">Dutch (Belgium)</option>
                                            <option <?php if ($locale == 'nl_NL') { echo'selected'; } ?> value="nl_NL">Dutch (Netherlands)</option>
                                            <option <?php if ($locale == 'dz_BT') { echo'selected'; } ?> value="dz_BT">Dzongkha (Bhutan)</option>
                                            <option <?php if ($locale == 'en_AG') { echo'selected'; } ?> value="en_AG">English (Antigua and Barbuda)</option>
                                            <option <?php if ($locale == 'en_AU') { echo'selected'; } ?> value="en_AU">English (Australia)</option>
                                            <option <?php if ($locale == 'en_BW') { echo'selected'; } ?> value="en_BW">English (Botswana)</option>
                                            <option <?php if ($locale == 'en_CA') { echo'selected'; } ?> value="en_CA">English (Canada)</option>
                                            <option <?php if ($locale == 'en_DK') { echo'selected'; } ?> value="en_DK">English (Denmark)</option>
                                            <option <?php if ($locale == 'en_HK') { echo'selected'; } ?> value="en_HK">English (Hong Kong SAR China)</option>
                                            <option <?php if ($locale == 'en_IN') { echo'selected'; } ?> value="en_IN">English (India)</option>
                                            <option <?php if ($locale == 'en_IE') { echo'selected'; } ?> value="en_IE">English (Ireland)</option>
                                            <option <?php if ($locale == 'en_NZ') { echo'selected'; } ?> value="en_NZ">English (New Zealand)</option>
                                            <option <?php if ($locale == 'en_NG') { echo'selected'; } ?> value="en_NG">English (Nigeria)</option>
                                            <option <?php if ($locale == 'en_PH') { echo'selected'; } ?> value="en_PH">English (Philippines)</option>
                                            <option <?php if ($locale == 'en_SG') { echo'selected'; } ?> value="en_SG">English (Singapore)</option>
                                            <option <?php if ($locale == 'en_ZA') { echo'selected'; } ?> value="en_ZA">English (South Africa)</option>
                                            <option <?php if ($locale == 'en_GB') { echo'selected'; } ?> value="en_GB">English (United Kingdom)</option>
                                            <option <?php if ($locale == 'en_US') { echo'selected'; } ?> value="en_US">English (United States)</option>
                                            <option <?php if ($locale == 'en_ZM') { echo'selected'; } ?> value="en_ZM">English (Zambia)</option>
                                            <option <?php if ($locale == 'en_ZW') { echo'selected'; } ?> value="en_ZW">English (Zimbabwe)</option>
                                            <option <?php if ($locale == 'eo') { echo'selected'; } ?> value="eo">Esperanto</option>
                                            <option <?php if ($locale == 'et_EE') { echo'selected'; } ?> value="et_EE">Estonian (Estonia)</option>
                                            <option <?php if ($locale == 'fo_FO') { echo'selected'; } ?> value="fo_FO">Faroese (Faroe Islands)</option>
                                            <option <?php if ($locale == 'fil_PH') { echo'selected'; } ?> value="fil_PH">Filipino (Philippines)</option>
                                            <option <?php if ($locale == 'fi_FI') { echo'selected'; } ?> value="fi_FI">Finnish (Finland)</option>
                                            <option <?php if ($locale == 'fr_BE') { echo'selected'; } ?> value="fr_BE">French (Belgium)</option>
                                            <option <?php if ($locale == 'fr_CA') { echo'selected'; } ?> value="fr_CA">French (Canada)</option>
                                            <option <?php if ($locale == 'fr_FR') { echo'selected'; } ?> value="fr_FR">French (France)</option>
                                            <option <?php if ($locale == 'fr_LU') { echo'selected'; } ?> value="fr_LU">French (Luxembourg)</option>
                                            <option <?php if ($locale == 'fr_CH') { echo'selected'; } ?> value="fr_CH">French (Switzerland)</option>
                                            <option <?php if ($locale == 'fur_IT') { echo'selected'; } ?> value="fur_IT">Friulian (Italy)</option>
                                            <option <?php if ($locale == 'ff_SN') { echo'selected'; } ?> value="ff_SN">Fulah (Senegal)</option>
                                            <option <?php if ($locale == 'gl_ES') { echo'selected'; } ?> value="gl_ES">Galician (Spain)</option>
                                            <option <?php if ($locale == 'lg_UG') { echo'selected'; } ?> value="lg_UG">Ganda (Uganda)</option>
                                            <option <?php if ($locale == 'gez_ER') { echo'selected'; } ?> value="gez_ER">Geez (Eritrea)</option>
                                            <option <?php if ($locale == 'gez_ET') { echo'selected'; } ?> value="gez_ET">Geez (Ethiopia)</option>
                                            <option <?php if ($locale == 'ka_GE') { echo'selected'; } ?> value="ka_GE">Georgian (Georgia)</option>
                                            <option <?php if ($locale == 'de_AT') { echo'selected'; } ?> value="de_AT">German (Austria)</option>
                                            <option <?php if ($locale == 'de_BE') { echo'selected'; } ?> value="de_BE">German (Belgium)</option>
                                            <option <?php if ($locale == 'de_DE') { echo'selected'; } ?> value="de_DE">German (Germany)</option>
                                            <option <?php if ($locale == 'de_LI') { echo'selected'; } ?> value="de_LI">German (Liechtenstein)</option>
                                            <option <?php if ($locale == 'de_LU') { echo'selected'; } ?> value="de_LU">German (Luxembourg)</option>
                                            <option <?php if ($locale == 'de_CH') { echo'selected'; } ?> value="de_CH">German (Switzerland)</option>
                                            <option <?php if ($locale == 'el_CY') { echo'selected'; } ?> value="el_CY">Greek (Cyprus)</option>
                                            <option <?php if ($locale == 'el_GR') { echo'selected'; } ?> value="el_GR">Greek (Greece)</option>
                                            <option <?php if ($locale == 'gu_IN') { echo'selected'; } ?> value="gu_IN">Gujarati (India)</option>
                                            <option <?php if ($locale == 'ht_HT') { echo'selected'; } ?> value="ht_HT">Haitian (Haiti)</option>
                                            <option <?php if ($locale == 'ha_NG') { echo'selected'; } ?> value="ha_NG">Hausa (Nigeria)</option>
                                            <option <?php if ($locale == 'iw_IL') { echo'selected'; } ?> value="iw_IL">Hebrew (Israel)</option>
                                            <option <?php if ($locale == 'he_IL') { echo'selected'; } ?> value="he_IL">Hebrew (Israel)</option>
                                            <option <?php if ($locale == 'hi_IN') { echo'selected'; } ?> value="hi_IN">Hindi (India)</option>
                                            <option <?php if ($locale == 'hu_HU') { echo'selected'; } ?> value="hu_HU">Hungarian (Hungary)</option>
                                            <option <?php if ($locale == 'is_IS') { echo'selected'; } ?> value="is_IS">Icelandic (Iceland)</option>
                                            <option <?php if ($locale == 'ig_NG') { echo'selected'; } ?> value="ig_NG">Igbo (Nigeria)</option>
                                            <option <?php if ($locale == 'id_ID') { echo'selected'; } ?> value="id_ID">Indonesian (Indonesia)</option>
                                            <option <?php if ($locale == 'ia') { echo'selected'; } ?> value="ia">Interlingua</option>
                                            <option <?php if ($locale == 'iu_CA') { echo'selected'; } ?> value="iu_CA">Inuktitut (Canada)</option>
                                            <option <?php if ($locale == 'ik_CA') { echo'selected'; } ?> value="ik_CA">Inupiaq (Canada)</option>
                                            <option <?php if ($locale == 'ga_IE') { echo'selected'; } ?> value="ga_IE">Irish (Ireland)</option>
                                            <option <?php if ($locale == 'it_IT') { echo'selected'; } ?> value="it_IT">Italian (Italy)</option>
                                            <option <?php if ($locale == 'it_CH') { echo'selected'; } ?> value="it_CH">Italian (Switzerland)</option>
                                            <option <?php if ($locale == 'ja_JP') { echo'selected'; } ?> value="ja_JP">Japanese (Japan)</option>
                                            <option <?php if ($locale == 'kl_GL') { echo'selected'; } ?> value="kl_GL">Kalaallisut (Greenland)</option>
                                            <option <?php if ($locale == 'kn_IN') { echo'selected'; } ?> value="kn_IN">Kannada (India)</option>
                                            <option <?php if ($locale == 'ks_IN') { echo'selected'; } ?> value="ks_IN">Kashmiri (India)</option>
                                            <option <?php if ($locale == 'csb_PL') { echo'selected'; } ?> value="csb_PL">Kashubian (Poland)</option>
                                            <option <?php if ($locale == 'kk_KZ') { echo'selected'; } ?> value="kk_KZ">Kazakh (Kazakhstan)</option>
                                            <option <?php if ($locale == 'km_KH') { echo'selected'; } ?> value="km_KH">Khmer (Cambodia)</option>
                                            <option <?php if ($locale == 'rw_RW') { echo'selected'; } ?> value="rw_RW">Kinyarwanda (Rwanda)</option>
                                            <option <?php if ($locale == 'ky_KG') { echo'selected'; } ?> value="ky_KG">Kirghiz (Kyrgyzstan)</option>
                                            <option <?php if ($locale == 'kok_IN') { echo'selected'; } ?> value="kok_IN">Konkani (India)</option>
                                            <option <?php if ($locale == 'ko_KR') { echo'selected'; } ?> value="ko_KR">Korean (South Korea)</option>
                                            <option <?php if ($locale == 'ku_TR') { echo'selected'; } ?> value="ku_TR">Kurdish (Turkey)</option>
                                            <option <?php if ($locale == 'lo_LA') { echo'selected'; } ?> value="lo_LA">Lao (Laos)</option>
                                            <option <?php if ($locale == 'lv_LV') { echo'selected'; } ?> value="lv_LV">Latvian (Latvia)</option>
                                            <option <?php if ($locale == 'li_BE') { echo'selected'; } ?> value="li_BE">Limburgish (Belgium)</option>
                                            <option <?php if ($locale == 'li_NL') { echo'selected'; } ?> value="li_NL">Limburgish (Netherlands)</option>
                                            <option <?php if ($locale == 'lt_LT') { echo'selected'; } ?> value="lt_LT">Lithuanian (Lithuania)</option>
                                            <option <?php if ($locale == 'nds_DE') { echo'selected'; } ?> value="nds_DE">Low German (Germany)</option>
                                            <option <?php if ($locale == 'nds_NL') { echo'selected'; } ?> value="nds_NL">Low German (Netherlands)</option>
                                            <option <?php if ($locale == 'mk_MK') { echo'selected'; } ?> value="mk_MK">Macedonian (Macedonia)</option>
                                            <option <?php if ($locale == 'mai_IN') { echo'selected'; } ?> value="mai_IN">Maithili (India)</option>
                                            <option <?php if ($locale == 'mg_MG') { echo'selected'; } ?> value="mg_MG">Malagasy (Madagascar)</option>
                                            <option <?php if ($locale == 'ms_MY') { echo'selected'; } ?> value="ms_MY">Malay (Malaysia)</option>
                                            <option <?php if ($locale == 'ml_IN') { echo'selected'; } ?> value="ml_IN">Malayalam (India)</option>
                                            <option <?php if ($locale == 'mt_MT') { echo'selected'; } ?> value="mt_MT">Maltese (Malta)</option>
                                            <option <?php if ($locale == 'gv_GB') { echo'selected'; } ?> value="gv_GB">Manx (United Kingdom)</option>
                                            <option <?php if ($locale == 'mi_NZ') { echo'selected'; } ?> value="mi_NZ">Maori (New Zealand)</option>
                                            <option <?php if ($locale == 'mr_IN') { echo'selected'; } ?> value="mr_IN">Marathi (India)</option>
                                            <option <?php if ($locale == 'mn_MN') { echo'selected'; } ?> value="mn_MN">Mongolian (Mongolia)</option>
                                            <option <?php if ($locale == 'ne_NP') { echo'selected'; } ?> value="ne_NP">Nepali (Nepal)</option>
                                            <option <?php if ($locale == 'se_NO') { echo'selected'; } ?> value="se_NO">Northern Sami (Norway)</option>
                                            <option <?php if ($locale == 'nso_ZA') { echo'selected'; } ?> value="nso_ZA">Northern Sotho (South Africa)</option>
                                            <option <?php if ($locale == 'nb_NO') { echo'selected'; } ?> value="nb_NO">Norwegian Bokmal (Norway)</option>
                                            <option <?php if ($locale == 'nn_NO') { echo'selected'; } ?> value="nn_NO">Norwegian Nynorsk (Norway)</option>
                                            <option <?php if ($locale == 'oc_FR') { echo'selected'; } ?> value="oc_FR">Occitan (France)</option>
                                            <option <?php if ($locale == 'or_IN') { echo'selected'; } ?> value="or_IN">Oriya (India)</option>
                                            <option <?php if ($locale == 'om_ET') { echo'selected'; } ?> value="om_ET">Oromo (Ethiopia)</option>
                                            <option <?php if ($locale == 'om_KE') { echo'selected'; } ?> value="om_KE">Oromo (Kenya)</option>
                                            <option <?php if ($locale == 'os_RU') { echo'selected'; } ?> value="os_RU">Ossetic (Russia)</option>
                                            <option <?php if ($locale == 'pap_AN') { echo'selected'; } ?> value="pap_AN">Papiamento (Netherlands Antilles)</option>
                                            <option <?php if ($locale == 'ps_AF') { echo'selected'; } ?> value="ps_AF">Pashto (Afghanistan)</option>
                                            <option <?php if ($locale == 'fa_IR') { echo'selected'; } ?> value="fa_IR">Persian (Iran)</option>
                                            <option <?php if ($locale == 'pl_PL') { echo'selected'; } ?> value="pl_PL">Polish (Poland)</option>
                                            <option <?php if ($locale == 'pt_BR') { echo'selected'; } ?> value="pt_BR">Portuguese (Brazil)</option>
                                            <option <?php if ($locale == 'pt_PT') { echo'selected'; } ?> value="pt_PT">Portuguese (Portugal)</option>
                                            <option <?php if ($locale == 'pa_IN') { echo'selected'; } ?> value="pa_IN">Punjabi (India)</option>
                                            <option <?php if ($locale == 'pa_PK') { echo'selected'; } ?> value="pa_PK">Punjabi (Pakistan)</option>
                                            <option <?php if ($locale == 'ro_RO') { echo'selected'; } ?> value="ro_RO">Romanian (Romania)</option>
                                            <option <?php if ($locale == 'ru_RU') { echo'selected'; } ?> value="ru_RU">Russian (Russia)</option>
                                            <option <?php if ($locale == 'ru_UA') { echo'selected'; } ?> value="ru_UA">Russian (Ukraine)</option>
                                            <option <?php if ($locale == 'sa_IN') { echo'selected'; } ?> value="sa_IN">Sanskrit (India)</option>
                                            <option <?php if ($locale == 'sc_IT') { echo'selected'; } ?> value="sc_IT">Sardinian (Italy)</option>
                                            <option <?php if ($locale == 'gd_GB') { echo'selected'; } ?> value="gd_GB">Scottish Gaelic (United Kingdom)</option>
                                            <option <?php if ($locale == 'sr_ME') { echo'selected'; } ?> value="sr_ME">Serbian (Montenegro)</option>
                                            <option <?php if ($locale == 'sr_RS') { echo'selected'; } ?> value="sr_RS">Serbian (Serbia)</option>
                                            <option <?php if ($locale == 'sid_ET') { echo'selected'; } ?> value="sid_ET">Sidamo (Ethiopia)</option>
                                            <option <?php if ($locale == 'sd_IN') { echo'selected'; } ?> value="sd_IN">Sindhi (India)</option>
                                            <option <?php if ($locale == 'si_LK') { echo'selected'; } ?> value="si_LK">Sinhala (Sri Lanka)</option>
                                            <option <?php if ($locale == 'sk_SK') { echo'selected'; } ?> value="sk_SK">Slovak (Slovakia)</option>
                                            <option <?php if ($locale == 'sl_SI') { echo'selected'; } ?> value="sl_SI">Slovenian (Slovenia)</option>
                                            <option <?php if ($locale == 'so_DJ') { echo'selected'; } ?> value="so_DJ">Somali (Djibouti)</option>
                                            <option <?php if ($locale == 'so_ET') { echo'selected'; } ?> value="so_ET">Somali (Ethiopia)</option>
                                            <option <?php if ($locale == 'so_KE') { echo'selected'; } ?> value="so_KE">Somali (Kenya)</option>
                                            <option <?php if ($locale == 'so_SO') { echo'selected'; } ?> value="so_SO">Somali (Somalia)</option>
                                            <option <?php if ($locale == 'nr_ZA') { echo'selected'; } ?> value="nr_ZA">South Ndebele (South Africa)</option>
                                            <option <?php if ($locale == 'st_ZA') { echo'selected'; } ?> value="st_ZA">Southern Sotho (South Africa)</option>
                                            <option <?php if ($locale == 'es_AR') { echo'selected'; } ?> value="es_AR">Spanish (Argentina)</option>
                                            <option <?php if ($locale == 'es_BO') { echo'selected'; } ?> value="es_BO">Spanish (Bolivia)</option>
                                            <option <?php if ($locale == 'es_CL') { echo'selected'; } ?> value="es_CL">Spanish (Chile)</option>
                                            <option <?php if ($locale == 'es_CO') { echo'selected'; } ?> value="es_CO">Spanish (Colombia)</option>
                                            <option <?php if ($locale == 'es_CR') { echo'selected'; } ?> value="es_CR">Spanish (Costa Rica)</option>
                                            <option <?php if ($locale == 'es_DO') { echo'selected'; } ?> value="es_DO">Spanish (Dominican Republic)</option>
                                            <option <?php if ($locale == 'es_EC') { echo'selected'; } ?> value="es_EC">Spanish (Ecuador)</option>
                                            <option <?php if ($locale == 'es_SV') { echo'selected'; } ?> value="es_SV">Spanish (El Salvador)</option>
                                            <option <?php if ($locale == 'es_GT') { echo'selected'; } ?> value="es_GT">Spanish (Guatemala)</option>
                                            <option <?php if ($locale == 'es_HN') { echo'selected'; } ?> value="es_HN">Spanish (Honduras)</option>
                                            <option <?php if ($locale == 'es_MX') { echo'selected'; } ?> value="es_MX">Spanish (Mexico)</option>
                                            <option <?php if ($locale == 'es_NI') { echo'selected'; } ?> value="es_NI">Spanish (Nicaragua)</option>
                                            <option <?php if ($locale == 'es_PA') { echo'selected'; } ?> value="es_PA">Spanish (Panama)</option>
                                            <option <?php if ($locale == 'es_PY') { echo'selected'; } ?> value="es_PY">Spanish (Paraguay)</option>
                                            <option <?php if ($locale == 'es_PE') { echo'selected'; } ?> value="es_PE">Spanish (Peru)</option>
                                            <option <?php if ($locale == 'es_ES') { echo'selected'; } ?> value="es_ES">Spanish (Spain)</option>
                                            <option <?php if ($locale == 'es_US') { echo'selected'; } ?> value="es_US">Spanish (United States)</option>
                                            <option <?php if ($locale == 'es_UY') { echo'selected'; } ?> value="es_UY">Spanish (Uruguay)</option>
                                            <option <?php if ($locale == 'es_VE') { echo'selected'; } ?> value="es_VE">Spanish (Venezuela)</option>
                                            <option <?php if ($locale == 'sw_KE') { echo'selected'; } ?> value="sw_KE">Swahili (Kenya)</option>
                                            <option <?php if ($locale == 'sw_TZ') { echo'selected'; } ?> value="sw_TZ">Swahili (Tanzania)</option>
                                            <option <?php if ($locale == 'ss_ZA') { echo'selected'; } ?> value="ss_ZA">Swati (South Africa)</option>
                                            <option <?php if ($locale == 'sv_FI') { echo'selected'; } ?> value="sv_FI">Swedish (Finland)</option>
                                            <option <?php if ($locale == 'sv_SE') { echo'selected'; } ?> value="sv_SE">Swedish (Sweden)</option>
                                            <option <?php if ($locale == 'tl_PH') { echo'selected'; } ?> value="tl_PH">Tagalog (Philippines)</option>
                                            <option <?php if ($locale == 'tg_TJ') { echo'selected'; } ?> value="tg_TJ">Tajik (Tajikistan)</option>
                                            <option <?php if ($locale == 'ta_IN') { echo'selected'; } ?> value="ta_IN">Tamil (India)</option>
                                            <option <?php if ($locale == 'tt_RU') { echo'selected'; } ?> value="tt_RU">Tatar (Russia)</option>
                                            <option <?php if ($locale == 'te_IN') { echo'selected'; } ?> value="te_IN">Telugu (India)</option>
                                            <option <?php if ($locale == 'th_TH') { echo'selected'; } ?> value="th_TH">Thai (Thailand)</option>
                                            <option <?php if ($locale == 'bo_CN') { echo'selected'; } ?> value="bo_CN">Tibetan (China)</option>
                                            <option <?php if ($locale == 'bo_IN') { echo'selected'; } ?> value="bo_IN">Tibetan (India)</option>
                                            <option <?php if ($locale == 'tig_ER') { echo'selected'; } ?> value="tig_ER">Tigre (Eritrea)</option>
                                            <option <?php if ($locale == 'ti_ER') { echo'selected'; } ?> value="ti_ER">Tigrinya (Eritrea)</option>
                                            <option <?php if ($locale == 'ti_ET') { echo'selected'; } ?> value="ti_ET">Tigrinya (Ethiopia)</option>
                                            <option <?php if ($locale == 'ts_ZA') { echo'selected'; } ?> value="ts_ZA">Tsonga (South Africa)</option>
                                            <option <?php if ($locale == 'tn_ZA') { echo'selected'; } ?> value="tn_ZA">Tswana (South Africa)</option>
                                            <option <?php if ($locale == 'tr_CY') { echo'selected'; } ?> value="tr_CY">Turkish (Cyprus)</option>
                                            <option <?php if ($locale == 'tr_TR') { echo'selected'; } ?> value="tr_TR">Turkish (Turkey)</option>
                                            <option <?php if ($locale == 'tk_TM') { echo'selected'; } ?> value="tk_TM">Turkmen (Turkmenistan)</option>
                                            <option <?php if ($locale == 'ug_CN') { echo'selected'; } ?> value="ug_CN">Uighur (China)</option>
                                            <option <?php if ($locale == 'uk_UA') { echo'selected'; } ?> value="uk_UA">Ukrainian (Ukraine)</option>
                                            <option <?php if ($locale == 'hsb_DE') { echo'selected'; } ?> value="hsb_DE">Upper Sorbian (Germany)</option>
                                            <option <?php if ($locale == 'ur_PK') { echo'selected'; } ?> value="ur_PK">Urdu (Pakistan)</option>
                                            <option <?php if ($locale == 'uz_UZ') { echo'selected'; } ?> value="uz_UZ">Uzbek (Uzbekistan)</option>
                                            <option <?php if ($locale == 've_ZA') { echo'selected'; } ?> value="ve_ZA">Venda (South Africa)</option>
                                            <option <?php if ($locale == 'vi_VN') { echo'selected'; } ?> value="vi_VN">Vietnamese (Vietnam)</option>
                                            <option <?php if ($locale == 'wa_BE') { echo'selected'; } ?> value="wa_BE">Walloon (Belgium)</option>
                                            <option <?php if ($locale == 'cy_GB') { echo'selected'; } ?> value="cy_GB">Welsh (United Kingdom)</option>
                                            <option <?php if ($locale == 'fy_DE') { echo'selected'; } ?> value="fy_DE">Western Frisian (Germany)</option>
                                            <option <?php if ($locale == 'fy_NL') { echo'selected'; } ?> value="fy_NL">Western Frisian (Netherlands)</option>
                                            <option <?php if ($locale == 'wo_SN') { echo'selected'; } ?> value="wo_SN">Wolof (Senegal)</option>
                                            <option <?php if ($locale == 'xh_ZA') { echo'selected'; } ?> value="xh_ZA">Xhosa (South Africa)</option>
                                            <option <?php if ($locale == 'yi_US') { echo'selected'; } ?> value="yi_US">Yiddish (United States)</option>
                                            <option <?php if ($locale == 'yo_NG') { echo'selected'; } ?> value="yo_NG">Yoruba (Nigeria)</option>
                                            <option <?php if ($locale == 'zu_ZA') { echo'selected'; } ?> value="zu_ZA">Zulu (South Africa)</option>
                                        </select>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="timeFormat" class="anim">
                                            Time format
                                        </label>
                                        <select id="timeFormat" name="timeFormat" class="anim">
                                            <option value="12h" <?php if ($timeFormat == '12h') { echo 'selected'; } ?> >12 hour</option>
                                            <option value="24h" <?php if ($timeFormat == '24h') { echo 'selected'; } ?> >24 hour</option>
                                        </select>
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="dateFormat" class="noty_required anim">
                                            Date format
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Setup the format of date. You can modify the date separator character too." />
                                        </label>
                                        <input id="dateFormat" name="dateFormat" type="text" class="js_input_validate anim" value="<?php echo $dateFormat; ?>" placeholder="day/month/year" />
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="shortDateFormat" class="noty_required anim">
                                            Short date format
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Setup the order of short date." />
                                        </label>
                                        <input id="shortDateFormat" name="shortDateFormat" type="text" class="js_input_validate anim" value="<?php echo $shortDateFormat; ?>" placeholder="month_name. day." />
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Contact -->

                        <div id="tab_contact" class="tab">

                            <div class="panel">
                                <div class="panel_head">
                                Contact
                                </div>
                                <div class="panel_content">

                                    <div class="form_set_line anim">
                                        <label for="brandEmail" class="noty_required anim">
                                            E-mail
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="Winner will contact you at this email and you will get game notifications. This email will be the sender of the system messages." />
                                        </label>
                                        <input id="brandEmail" name="brandEmail" type="text" class="js_input_validate email anim" value="<?php echo $brandEmail; ?>" placeholder="eg: info@yourdomain.com" />
                                    </div>

                                    <div class="form_set_line anim">
                                        <label for="updatesEnabled" class="anim">
                                            Updates
                                            <img class="info tooltip" src="img/info.png" alt="Info" title="You will get notification about version updates and new theme-lab apps." />
                                        </label>
                                        <div class="input_box cf">
                                            <label class="choose_100 anim">
                                                <input id="updatesEnabled" name="updatesEnabled" type="checkbox" value="1" <?php if ($updatesEnabled == 1) { echo 'checked="checked"'; } ?> />
                                                Enable
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Hidden values -->
                        <input type="hidden" class="hidden" name="extraTickets[invite][used]" value="<?php echo $extraTickets['invite']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[invite][btn-plugin]" value="<?php echo $extraTickets['invite']['btn-plugin']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[invite][invited_cnt]" value="<?php echo $extraTickets['invite']['invited_cnt']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[share][used]" value="<?php echo $extraTickets['share']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[share][btn-plugin]" value="<?php echo $extraTickets['share']['btn-plugin']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[youtube-subscribe][used]" value="<?php echo $extraTickets['youtube-subscribe']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[youtube-subscribe][btn-plugin]" value="<?php echo $extraTickets['youtube-subscribe']['btn-plugin']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[google-plus][used]" value="<?php echo $extraTickets['google-plus']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[google-plus][btn-plugin]" value="<?php echo $extraTickets['google-plus']['btn-plugin']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[twitter-share][used]" value="<?php echo $extraTickets['twitter-share']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[twitter-share][btn-plugin]" value="<?php echo $extraTickets['twitter-share']['btn-plugin']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[twitter-follow][used]" value="<?php echo $extraTickets['twitter-follow']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[twitter-follow][btn-plugin]" value="<?php echo $extraTickets['twitter-follow']['btn-plugin']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[linkedin-share][used]" value="<?php echo $extraTickets['linkedin-share']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[linkedin-share][btn-plugin]" value="<?php echo $extraTickets['linkedin-share']['btn-plugin']; ?>" />

                        <input type="hidden" class="hidden" name="extraTickets[subscribe][used]" value="<?php echo $extraTickets['subscribe']['used']; ?>" />
                        <input type="hidden" class="hidden" name="extraTickets[subscribe][btn-plugin]" value="<?php echo $extraTickets['subscribe']['btn-plugin']; ?>" />

                    </div>

                </div>

                <!-- Save -->

                <div class="form_set_send">

                    <input class="control_save js_form_submit" type="submit" class="anim_100" value="Save" />

                    <!-- Noty -->

                    <div class="form_set_succes">Success!<div></div></div>
                    <div class="form_set_error">An error has occurred!<div></div></div>
                    <div class="form_set_required">Please fill all the required fields!<div></div></div>

                </div>

            </fieldset>

        </form>

    </div>

<?php

    $gr_form_layout = ob_get_contents();

    ob_end_clean();

    return $gr_form_layout;
}
?>