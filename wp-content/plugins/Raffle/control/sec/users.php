<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

function gr_print_form() {

    ob_start();

    // DEV Kit
    // error_reporting(E_ALL);
    // ini_set('display_errors','1');

    // Define some-some
    define ( 'DS', DIRECTORY_SEPARATOR );

    // Get some-some
    require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

    // Get values
    require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php' );
?>

    <div id="control_inputs" class="cf">

        <div class="control_breadcrumb">Admin / <span class="active">Users</span></div>

        <div class="control_field">

            <div class="panel">
                <div class="panel_head">
                Users
                </div>

                <div class="js_create_table" data-table_type="users"></div>

                </div>
            </div>

        </div>

    </div>

<?php

    $gr_form_layout = ob_get_contents();

    ob_end_clean();

    return $gr_form_layout;
}
?>