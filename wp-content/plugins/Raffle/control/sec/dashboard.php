<?php

// * * * * * * * * *

// ThemeLab Admin

// * * * * * * * * *

function gr_print_form() {

    ob_start();

    // DEV Kit
    // error_reporting(E_ALL);
    // ini_set('display_errors','1');

    // Define some-some
    define ( 'DS', DIRECTORY_SEPARATOR );

    // Get some-some
    require_once ( __DIR__.DS.'..'.DS.'_somesome.php' );

    // Get values
    require ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'configuration.php' );
?>

    <div id="control_inputs" class="cf">

        <div class="control_breadcrumb">Admin / <span class="active">Dashboard</span></div>

        <div class="control_field">

            <!-- <div class="panel_duo cf">
                <div class="panel even_height_1">
                    <div class="panel_head">
                    Statistics
                    </div>
                    <div class="panel_content statistics_list">
                    <?php

                    // Get some stat
                    // define ('_FAK', true);
                    // require_once ( __DIR__.DS.'..'.DS.'..'.DS.'bin'.DS.'admin'.DS.'get_stat.php' );
                    // echo $statList_table;

                    ?>
                    </div>
                </div> -->
                <div class="panel even_height_1">
                    <div class="panel_head">
                    Status
                    </div>
                    <div class="panel_content">

                        <table class="table_2_c">
                            <tbody>
                                <tr>
                                    <td>
                                        Application status:
                                    </td>
                                    <td>
                                        <span class="status <?php echo ( $appOnline == 1 ? 'active' : 'inactive' ); ?>">
                                            <?php
                                            // Show status
                                            echo ( $appOnline == 1 ? 'Active' : 'Inactive' );
                                            ?>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Start date:
                                    </td>
                                    <td>
                                        <?php echo $startDate; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        End date:
                                    </td>
                                    <td>
                                        <?php echo $endDate; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <a class="dashboard_goto js_inner_link" href="settings#game" data-sec="settings" data-hash="#game">Go to the game settings</a>

                    </div>
                </div>
            <!-- </div> -->

        </div>

    </div>

<?php

    $gr_form_layout = ob_get_contents();

    ob_end_clean();

    return $gr_form_layout;
}
?>