/* -------------------------------------------------------- */
/*                                                          */
/*                         B A S E                          */
/*                                                          */
/* -------------------------------------------------------- */

( function($) {

/* -------------------------------------------------------- */
/*                    V A R I A B L E S                     */
/* -------------------------------------------------------- */

var url_onload = $('html').data('url_onload'),
    sec_onload = $('html').data('sec_onload'),
    query_onload = $('html').data('query_onload'),
    hash_onload = window.location.hash;

var lgin_header = $('#login_header'),
    lgin_footer = $('#login_footer'),
    ctrl_wrapper = $('#control_wrapper'),
    ctrl_header = $('#control_header'),
    ctrl_footer = $('#control_footer');

var ctrl_tab = '',
    ctrl_tab_current = '',
    ctrl_tab_selector = '';

/* -------------------------------------------------------- */
/*                        R E A D Y                         */
/* -------------------------------------------------------- */

$(document).ready(function () {

    // C O N T R O L

    if ($('#control_page').length > 0) {

        // Update body class
        $('body').addClass('control');

        // Load after preload
        finishedPreloading('control');

        // Load control's functions
        controlFunction();

        // Add effect
        inputEffects();

    }

    // L O G I N

    else {

        // Update body class
        $('body').addClass('login');

        // Load after preload
        finishedPreloading('login');

        // Load login's functions
        loginFunction();

        // Add effect
        inputEffects();
    }

});


/* -------------------------------------------------------- */
/*                    F U N C T I O N S                     */
/* -------------------------------------------------------- */

/*                        B A S I C                         */
/* -------------------------------------------------------- */


// Preloader
function finishedPreloading (section) {

    if (section == 'login') {

        var pre = new TimelineMax({onComplete:logoAnim});
        pre.to( $('#control_loader'), 0.3, {autoAlpha:0,scale:'0.7',display:'none', ease:Quad.easeOut})
           .to( $('#control_wrapper'), 0.6, {autoAlpha:1,scale:'1.0', ease:Back.easeOut}, '+=0.1');

    } else if (section == 'control') {

        var pre = new TimelineMax();
        pre.to( $('#control_loader'), 0.3, {autoAlpha:0,scale:'0.7',display:'none', ease:Quad.easeOut})
           .to( $('#control_wrapper'), 0.6, {autoAlpha:1,scale:'1.0', ease:Back.easeOut}, '+=0.1');
    }
}

// Logo anim
function logoAnim () {

    if ($('html').hasClass('csstransitions')) {

        var logoanim = new TimelineMax(),
            logo = $('.login_logo_wrap_svg'),
            logotext = logo.find('.logo_text'),
            logosign = logo.find('.logo_sign'),
            logoicon = logo.find('.logo_icon');
        logoanim.set( [logo, logoicon, logosign, logotext], {autoAlpha:0,scale:'0.7'} )
                .to( logo, 0.3, {display:'block',autoAlpha:1,scale:'1.0', ease:Quad.easeOut}, '-=0.15'  )
                .to( logoicon, 0.3, {autoAlpha:1,scale:'1.0', ease:Quad.easeOut}, '-=0.15'  )
                .to( logosign, 0.3, {autoAlpha:1,scale:'1.0', ease:Back.easeOut}, '-=0.15' )
                .to( logotext, 0.3, {autoAlpha:1,scale:'1.0', ease:Back.easeOut}, '-=0.15' );

    } else {

        $('.login_logo_frame').html(
            '<div class="login_logo_wrap_png">' +
                '<div class="logo_box">' +
                    '<img src="img/theme_lab_logo.png" alt="ThemeLab" class="logo_img" />' +
                '</div>' +
            '</div>'
        );
        var logo = $('.login_logo_wrap_png');
        TweenMax.set( logo, {autoAlpha:0,scale:'0.7'});
        TweenMax.to( logo, 0.6, {display:'block',autoAlpha:1,scale:'1.0', ease:Back.easeOut});
    }
}

// Input focus/blur
function inputEffects () {

    $('#login_inputs input, #control_inputs input, #control_inputs textarea').on('focus', function () {
        $(this).closest('.form_set_line').addClass('focus');
    }).on('blur', function () {
        $(this).closest('.form_set_line').removeClass('focus');
    });
}

// Success noty
function successNoty (container,callback,msg) {

    if ($('.form_set_succes', container).is(':hidden')) {

        if (msg) {

            $('.form_set_succes > div', container).html('<div class="form_set_extra_msg">'+msg+'</div>');
        }

        $('.form_set_succes', container).slideDown(300,'easeOutQuad').delay(1000).slideUp(300,'easeInQuad', ( typeof callback !== 'undefined' ? function() {

            $('.form_set_succes > div', container).html('');

            callback();

        } : '' ) );
    }
}

// Error noty
function errorNoty (container,callback,msg) {

    if ($('.form_set_error', container).is(':hidden')) {

        if (msg) {

            $('.form_set_error > div', container).html('<div class="form_set_extra_msg">'+msg+'</div>');
        }

        $('.form_set_error', container).slideDown(300,'easeOutQuad').delay(3000).slideUp(300,'easeInQuad', ( typeof callback !== 'undefined' ? function() {

            $('.form_set_succes > div', container).html('');

            callback();

        } : '' ) );
    }
}


/*                        L O G I N                         */
/* -------------------------------------------------------- */

// Init basic functions before login
function loginFunction () {

    // Add effect
    inputEffects();

    // Tooltips
    $('.tooltip').tooltipster({
        position: 'bottom',
        speed: 200,
        animation: 'fade',
        maxWidth: 300
    });

    // Set login button
    if ($('form#login_form').length > 0) {
        $('input#login_btn').on('click', function(e) {
            e.preventDefault();
            loginControl($('form#login_form'));
        });
    }
}

// Login to control
function loginControl (form){

    $.ajax({
        url: 'ctrl/login.php',
        type: 'POST',
        data: form.serialize(),
        cache: false,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            if (data.success == 'valid_user') {
                successNoty(form, function() {
                    TweenMax.to( $('#control_loader'), 0.15, {autoAlpha:1,scale:'1.0', ease:Back.easeOut});
                    TweenMax.to( $('#control_wrapper'), 0.3, {autoAlpha:0,scale:'0.9', ease:Quad.easeOut, onComplete:function(){
                        // Update body class
                        $('body').removeClass('login').addClass('control');
                        // Load control
                        $('#control_wrapper').html(data.inputs);
                        // Load control's functions
                        controlFunction();
                        // Animate load
                        TweenMax.set( $('#control_wrapper'), {scale:'1.0'});
                        TweenMax.set( $('#control_header'), {y:'-85px'});
                        TweenMax.set( $('#control_side_panel'), {x:'-260px'});
                        TweenMax.to( $('#control_loader'), 0.15, {autoAlpha:0,scale:'0.9', ease:Quad.easeOut});
                        TweenMax.to( $('#control_wrapper'), 0.9, {autoAlpha:1, ease:Back.easeOut});
                        TweenMax.to( $('#control_header'), 0.3, {y:'0px', ease:Quad.easeOut});
                        TweenMax.to( $('#control_side_panel'), 0.6, {x:'0px', ease:Quad.easeOut});
                    }});
                });
            } else {
                errorNoty(form);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            errorNoty(form);
        }
    });
}


/*                      C O N T R O L                       */
/* -------------------------------------------------------- */

// Init basic functions after login
function controlFunction () {

    // On start load dashboard section
    if (sec_onload != '') {
        switchSection(sec_onload, query_onload, hash_onload);
    } else {
        switchSection('dashboard');
    }

    // Add effect
    inputEffects();

    // Tooltips
    $('.tooltip').tooltipster({
        position: 'bottom',
        speed: 200,
        animation: 'fade',
        maxWidth: 300
    });

    // Menu scrollbar
    $(window).on('load resize', function () {
        $('#control_menu').slimScroll({
            height:'auto'
        });
    });

    // Set menupoints
    $('.control_menupoints > .menupoint').on('click', function (e) {
        e.preventDefault();
        switchSection($(this).data('sec'));
    });

    // Set inner links
    $('#control_page a.js_inner_link').on('click', function (e) {
        e.preventDefault();
        switchSection($(this).data('sec'),$(this).data('query'),$(this).data('hash'));
    });

    // Set logout button
    $('.logout_btn').on('click', function (e) {
        e.preventDefault();
        logoutControl();
    });

    // If back or forward button is clicked
    window.onpopstate = function() {
        switchSection(window.location.pathname.split('/').reverse()[0], window.location.search, window.location.hash, 'backnforth');
    };
}

// Logout from control
function logoutControl (form){

    $.get('ctrl/logout.php', function(data,status) {
        if (status == 'success') {

            TweenMax.to( $('#control_loader'), 0.15, {autoAlpha:1,scale:'1.0', ease:Back.easeOut});
            TweenMax.to( $('#control_wrapper'), 0.3, {autoAlpha:0,scale:'0.9', ease:Quad.easeOut, onComplete:function(){
                // Update body class
                $('body').removeClass('control').addClass('login');
                // Update URL
                history.pushState({}, '', url_onload);
                // Load login
                $('#control_wrapper').html(data.inputs);
                // Load login's functions
                loginFunction();
                // Animate load
                finishedPreloading('login');
            }});

        } else {

            // Houston, we got a problem!
        }
    }, 'json');
}

// Switch section
function switchSection (sec,query,hash,direction) {

    // Switch menupoint
    var menupoint = $('.control_menupoints > .menupoint[data-sec='+sec+']');
    if (!menupoint.hasClass('active')) {

        $('.control_menupoints > .menupoint.active').removeClass('active');
        menupoint.addClass('active');
    }

    // Switch content
    $.ajax({
        url: 'ctrl/switch.php',
        type: 'POST',
        data: 'url=' + url_onload + '&sec=' + sec + ( typeof query !== 'undefined' ? '&query=' + query : '' ) + ( typeof hash !== 'undefined' ? '&hash=' + hash : '' ),
        cache: false,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {

            TweenMax.to( $('#control_loader'), 0.15, {autoAlpha:1,scale:'1.0', ease:Back.easeOut});
            TweenMax.to( $('#content_container'), 0.3, {autoAlpha:0,y:'50px', ease:Quad.easeOut, onComplete:function(){
                if (direction != 'backnforth') {
                    // Update URL
                    history.pushState({}, '', data.url);
                }
                // Load section
                $('#content_container').html(data.sec);
                // Load section's functions
                sectionFunction();
                // Animate load
                TweenMax.set( $('#content_container'), {y:'-30px'});
                TweenMax.to( $('#control_loader'), 0.15, {autoAlpha:0,scale:'0.9', ease:Quad.easeOut});
                TweenMax.to( $('#content_container'), 0.3, {autoAlpha:1,y:'0px', ease:Quad.easeOut});
            }});
        },
        error: function (jqXHR, textStatus, errorThrown) {  }
    });
}

// Init basic functions after switch
function sectionFunction () {

    // Add effect
    inputEffects();

    // Add field's functions
    fieldFunction();

    // Set inner links
    $('#content_container a.js_inner_link').on('click', function (e) {
        e.preventDefault();
        switchSection($(this).data('sec'),$(this).data('query'),$(this).data('hash'));
    });

    // Tooltips
    $('.tooltip').tooltipster({
        position: 'bottom',
        speed: 200,
        animation: 'fade',
        maxWidth: 300
    });

    // Match Height init
    $(function() {
        $('.even_height_1').matchHeight(); // Group 1
        $('.even_height_2').matchHeight(); // Group 2
        $('.even_height_3').matchHeight(); // Group 3
    });

    // Tabs
    if ($('.tab_wrapper').length > 0) {

        // > On load
        var hash_tab = (window.location.hash ? window.location.hash.substring(1) : $('.tab_nav > div > div:first-child > a').data('tab_name') );
        history.pushState({}, '', '#'+hash_tab);
        $('.tab_nav > div > div > a[data-tab_name="'+hash_tab+'"]').addClass('active');
        $('.tab_wrapper').height($('.tab_wrapper div#tab_'+hash_tab).height());
        setTimeout(function () {
            var init_first_tab = $('.tab_wrapper div#tab_'+hash_tab);
            TweenMax.set( init_first_tab, {autoAlpha:0,y:'-30px', onComplete:function(){ init_first_tab.addClass('active'); }});
            TweenMax.to( init_first_tab, 0.3, {autoAlpha:1,y:'0px', ease:Quad.easeOut});
        }, 150);

        // > On change
        $('.tab_nav a').each(function() {

            $(this).on('click', function(e) {

                e.preventDefault();

                history.pushState({}, '', '#'+$(this).data('tab_name'));

                tabSwitch($(this).data('tab_name'));
            });
        });
    }

    // Create tables if required
    if ($('.js_create_table').length > 0) {

        $('.js_create_table').each(function() {

            // Create table
            tableInit( $(this), function(table_created) {

                // Call table's functions
                tableFunction();

            });

        });
    }

    // Animate inputs in to the view
    TweenMax.to( $('#control_inputs'), 0.3, {autoAlpha:1, ease:Quad.easeInOut});
}

// Tab switch
function tabSwitch (tab) {

    // Get data
    var ctrl_tab = tab,
        ctrl_tab_current = $('.tab_nav a.active').data('tab_name'),
        ctrl_tab_nav = $('.tab_nav a[data-tab_name="'+tab+'"]'),
        ctrl_tab_selector = '.tab_wrapper #tab_' + ctrl_tab;

    if (ctrl_tab != ctrl_tab_current) {

        // Activate/deactivate tab button
        $('.tab_nav a.active').removeClass('active');
        ctrl_tab_nav.addClass('active');

        // Activate/deactivate tab content
        TweenMax.to( $('.tab_wrapper div.tab.active'), 0.3, {autoAlpha:0,y:'50px', ease:Quad.easeOut, onComplete:function(){
            $('.tab_wrapper').height($(ctrl_tab_selector).height());
            $('.tab_wrapper div.tab.active').removeClass('active');
            TweenMax.set( $(ctrl_tab_selector), {autoAlpha:0,y:'-30px', onComplete:function(){ $(ctrl_tab_selector).addClass('active'); }});
            TweenMax.to( $(ctrl_tab_selector), 0.3, {autoAlpha:1,y:'0px', ease:Quad.easeOut});
        }});
    }
}

// Save control form
function saveControl (form){

    var formData = new FormData(form[0]);

    $.ajax({
        url: 'ctrl/save.php',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        beforeSend: function () {

            $('.form_set_send input', form).prop('disabled', true);

            $.each($('input[type=file]',form), function() {

                var selected_file = $(this).filter(function(){ return $.trim(this.value) != '' }).length > 0 ;

                if (selected_file) {

                    $(this).parent('label').append('<img class="form_img_loader" style="display:none;" src="img/img_loader.gif" alt="Loading" />');
                    $(this).siblings('.form_img_loader').slideDown(150, function() {
                        $('.tab_wrapper').height($('.tab_wrapper .tab.active').height());
                    });
                }
            });

        },
        success: function (data, textStatus, jqXHR) {

            $('.form_set_send input', form).prop('disabled', false);

            if ($('.form_img_loader').length > 0) {

                $('.form_img_loader').slideUp(150, function() {
                    $(this).remove();
                    $('.tab_wrapper').height($('.tab_wrapper .tab.active').height());
                });
            }

            if (data.success == 'allgood') {

                successNoty(form);

            } else {

                errorNoty(form,undefined,data.error_msg);

                if (data.error_msg == 'image') {
                    $('input[type=file]', form).empty();
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            errorNoty(form);
        }
    });
}


/*                       F I E L D S                        */
/* -------------------------------------------------------- */

// Init field's functions
function fieldFunction () {

    // Init form validation
    initFormValidate($('form.control_form'));

    // File input custom button
    $('.js_fileinput_btn').on('change', function () {
        $(this).parent('label').addClass('file_selected');
        $(this).siblings('.fileinput_custom_text').html('File selected');
    });

    // Radio/Checkbox check
    $('[type="radio"], [type="checkbox"]').each(function() {

        if ($(this).is(':checked')) {

            $(this).parent('label').addClass('input_selected');
        }
        $(this).on('change', function () {

            $('[name="' + $(this).attr('name') + '"]').parent('label').removeClass('input_selected');

            if ($(this).is(':checked')) {

                $(this).parent('label').addClass('input_selected');
            }
        });
    });

    // Date-time picker
    $('.js_datepicker').datetimepicker({
        timepicker:false,
        format:'Y-m-d'
    });
    $('.js_datetimepicker').datetimepicker({
        format:'m/d/Y h:i A'
    });

    // Color picker
    $('.js_colorpicker').minicolors({

    });

    // Restart application button
    $('.js_restart_app').on('click', function(e) {

        e.preventDefault();

        if (!confirm('Are you sure?')) { return false; }

        var container = $(this).closest('div'),
            app_secret = {'secret':$(this).data('app_secret')};

        $.ajax({
            url: 'ctrl/restart.php',
            type: 'POST',
            data: app_secret,
            cache: false,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.success == 'allgood') {
                    successNoty(container);
                } else {
                    errorNoty(container);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                errorNoty(container);
            }
        });
    });
}

/*                  V A L I D A T I O N S                   */
/* -------------------------------------------------------- */

// Init validation
initFormValidate = function(form,success){

    // Select it required (on check)
    selectRequired(form);

    // Init numeric validation
    numericInput($('input.numeric', form));

    // On change
    $('.js_input_validate', form).focus(function() {
        $(this).addClass('visited');
    });
    $('.js_input_validate', form).change(function() {
        if ($(this).hasClass('js_app_status')) {
            validateForm(form, 'all');
        } else {
            validateForm(form, 'visited');
        }
    });

    // On submit
    $('.js_form_submit', form).click(function(e){

        e.preventDefault();

        if (!$(this).hasClass('required')) {

            if (validateForm(form, 'all')) {

                // Save form
                saveControl(form);

            } else {

                // Scroll to top
                TweenMax.to($('html, body'), 0.7, { scrollTop:form.offset().top });
            }
        }

        return false;
    });
}

// Form validation
function validateForm(form, selector) {

    var form_validity = true,
        form_container = $(form).closest('.mol_form_contact_container'),
        form_noty_required = $('.form_set_required', form),
        form_noty_app_status = $('input.js_app_status', form).closest('.form_set_line').find('.custom_noty.error');

    // Each input
    $($('.js_input_validate'+( (selector == 'all') ? '' : '.'+selector ), form).get().reverse()).each(function(){

        // Set vars
        var extraParam = true,
            el = $(this),
            val = el.val(),
            name = el.attr('name'),
            length = val.length,
            minLength = el.data('minlength') ? el.data('minlength') : true,
            placeholder = el.data('placeholder');

        // Setup
        if (el.hasClass('email')) {     extraParam = checkEmail(val);       }
        if (el.hasClass('pick')) {      extraParam = checkPick(el);         }
        if (length < minLength) {       minLength = false;                  }

        // Not valid
        if (val == placeholder || !extraParam || !minLength) {

            // Tab
            if (el.closest('.tab').length > 0) {
                var required_tab = el.closest('.tab').attr('id').substring(4);
                $('.tab_nav > div > div > a[data-tab_name="'+required_tab+'"]').addClass('required');
            }

            // Label
            if (el.parent('label').parent('.input_box').parent('label').length > 0) {
                el.parent('label').parent('.input_box').parent('label').addClass('required');
            }
            else if (el.prev('label').length > 0) {
                el.prev('label').addClass('required');
            }
            // Input
            el/*.focus()*/.addClass('required');

            form_validity = false;
        }

        // Valid
        else {

            // Tab
            if (el.closest('.tab').length > 0) {
                var required_tab = el.closest('.tab').attr('id').substring(4);
                $('.tab_nav > div > div > a[data-tab_name="'+required_tab+'"]').removeClass('required');
            }

            // Label
            if (el.parent('label').parent('.input_box').parent('label').hasClass('required')) {
                el.parent('label').parent('.input_box').parent('label').removeClass('required');
            }
            else if (el.prev('label').hasClass('required')) {
                el.prev('label').removeClass('required');
            }
            // Input
            if (el.hasClass('required')) {

                el.removeClass('required');

                // If there is another required field (for visited validation)
                if ($('.js_input_validate.required', form).length > 0) {

                    form_validity = false;
                }
            }
        }

        // Show the required noty
        if (form_validity == false && form_noty_required.is(':hidden')) {

            form_noty_required.slideDown(500);

            $('.control_save', form).addClass('required');
        }

        // Hide the required noty
        else if (form_validity != false && !form_noty_required.is(':hidden')) {

            form_noty_required.slideUp(500);

            $('.control_save', form).removeClass('required');
        }

        // Show the custom (app status) noty
        if (form_validity == false && $('input.js_app_status:checked', form).val() == '1' && form_noty_app_status.is(':hidden')) {

                form_noty_app_status.slideDown(300, function() { $('.tab_wrapper').height($('.tab_wrapper div.tab.active').height()); });
                $('input.js_app_status:checked', form).parent('label.input_selected').addClass('error');
        }

        // Hide the custom (app status) noty
        else if ((form_validity != false || $('input.js_app_status:checked', form).val() == '0') && !form_noty_app_status.is(':hidden')) {

                form_noty_app_status.slideUp(300, function() { $('.tab_wrapper').height($('.tab_wrapper div.tab.active').height()); });
                $('input.js_app_status:checked', form).parent('label.input_selected').removeClass('error');
        }

    });

    return form_validity;
}

// Select it required
function selectRequired(form) {

    $.each( $('[data-require_group_selector]', form), function() {

        var require_group_selector = $(this).data('require_group_selector'),
            require_group = $('[data-require_group='+require_group_selector+']');

        if ($(this).prop('checked') == true) {

            require_group.addClass('js_input_validate');

            if (require_group.parent('label').parent('.input_box').parent('label').length > 0) {
                require_group.parent('label').parent('.input_box').parent('label').addClass('noty_required');
            }
            else if (require_group.prev('label').length > 0) {
                require_group.prev('label').addClass('noty_required');
            }
        }

        $(this).on('change', function() {

            if ($(this).prop('checked') == true) {

                require_group.addClass('js_input_validate visited');

                if (require_group.parent('label').parent('.input_box').parent('label').length > 0) {
                    require_group.parent('label').parent('.input_box').parent('label').addClass('noty_required');
                }
                else if (require_group.prev('label').length > 0) {
                    require_group.prev('label').addClass('noty_required');
                }

            } else {

                require_group.removeClass('js_input_validate visited required');

                if (require_group.parent('label').parent('.input_box').parent('label').length > 0) {
                    require_group.parent('label').parent('.input_box').parent('label').removeClass('noty_required required');
                }
                else if (require_group.prev('label').length > 0) {
                    require_group.prev('label').removeClass('noty_required required');
                }
            }

            validateForm(form, 'visited');
        });

    });
}

// Numeric validation
function numericInput(input){

    $(input).keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
            return;
        else if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ))
            event.preventDefault();
    });
}

// Email validation
function checkEmail(email) {

    AtPos = email.indexOf("@");StopPos = email.lastIndexOf(".");
    if ( (email == "") ||(StopPos - AtPos == 1) || (StopPos < AtPos) || (AtPos == -1 || StopPos == -1) ) { return false;  }
    return true;
}

// Checkbox validation
function checkPick(input) {

    if ($(input).length > 0) { return true; }
    else { return false; }
}

/*                       T A B L E S                        */
/* -------------------------------------------------------- */

// Init tables
function tableInit (table_container, callback) {

    var table_type = table_container.data('table_type'),
        table_request = {'action':'list'};

    $.ajax({
        url:'data/'+table_type+'.php',
        type: 'POST',
        data: table_request,
        dataType: 'json',
        success: function(json) {

            // Create table
            table_created = table_container.columns({
                data:json.data,
                schema:json.schema,
                searchableFields:json.searchable,
                sortableFields:json.sortable,
                sortBy:json.sortby,
                size:json.size,
                templateFile:'css/tmpl/columns.mst',
                plugins: ['columnsFunction']
            });

            return callback(table_created);
        }
    });
}

// Init tables's functions
function tableFunction () {

    // Delete records
    // $('.js_delete_table_record').on('click', function() {

    //     tableDeleteFunction ($(this));
    // });
}


} ) ( jQuery );

// Delete table's record
function tableDeleteFunction (element) {

    if (!confirm('Are you sure?')) { return false; }

    var table_type = element.closest('.js_create_table').data('table_type'),
        record_row = element.closest('tr'),
        record_id = element.data('record_id'),
        table_request = {'action':'delete', 'record':record_id};

    $.ajax({
        url:'data/'+table_type+'.php',
        type: 'POST',
        data: table_request,
        dataType: 'json',
        success: function(json) {

            if (json.feedback == 'deleted') {

                record_row.slideUp(150);
            }
        }
    });
}