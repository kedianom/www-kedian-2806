<?php
/**
 * Raffle Game Facebook application
 *
 * Customizable Facebook game with installer.
 * For DEBUG check debug_config.php!
 *
 * @author     Ferenc Krix <k.feri86@gmail.com>
 * @copyright  2014 theme-lab.com
 */
  
require 'bin/controller.php';

?>

<!DOCTYPE html>
<html class="<?php echo $layout;?>">
	<head prefix="<?php echo $fbHeadPrefix; ?>">
	
		<?php echo createFacebookOpenGraphTags(); ?>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="dcterms.rightsHolder" content="theme-lab.com" />
		<meta name="dcterms.dateCopyrighted" content="2014">
		<?php echo setViewPort(); ?>
		
		<title><?php echo $appFullName; ?></title>
		<link type="text/css" href="<?php echo getResource('css/base.css')?>" rel="stylesheet" />		
		<?php includeAdditionalStyles(); ?>
		
		<script src="js/modernizr.js"></script>		
	</head>
	<body class="<?php echo $gameFlags;?> <?php echo $textDirection;?>">
	
		<!-- Define JS variables -->
		<?php echo initTemplate(); ?>
		
		<!-- APP PRELOADER -->
		<div id="fb-preloader">
			<div class="preload-up"></div>
			<div class="preload-progress"></div>
			<div class="preload-down"></div>
		</div>
		<!-- END OF APP PRELOADER -->
		
		<?php if($isMobileURL): ?>
		<!-- MOBILE ORIENTATION MSG -->
		<div id="orientation">
			<div class="rotate-device"></div>
			<div class="orient-msg text_dir"><?php echo $orientWarningTxt;?></div>
		</div>		
		<!-- END OF MOBILE ORIENTATION MSG -->

		<!-- MOBILE FADER LAYER -->
		<div id="mobile-fader-left" class="mobile-fader"></div>
		<div id="mobile-fader-right" class="mobile-fader"></div>		
		<!-- END OF MOBILE FADER LAYER -->
		
		<?php endif; ?>
		
		<!-- MAIN CONTAINER -->
		<div id="main">

			<?php renderCustomizer(); ?>

			<?php renderAdmin(); ?>
			
			<!-- FIXED: items wich always have to visible -->
			<div id="fixed">
				<div id="btn-rules" class="button button-round page-control" data-target="rules"></div>
				<div id="btn-mute" class="button button-round"></div>
				<div id="timing" class="tooltip" title="<?php renderToolTipDuration(); ?>">
					<span class="text_dir"><?php echo $durationTxt;?></span>
					<h3 class="text_dir"><?php echo $startShortDateTxt.'<br />-<br />'.$endShortDateTxt;?></h3>
				</div>				
			</div>
			<!-- END OF FIXED -->			

			<!-- PAGER CONTAINER: contains the pages of the app, more info at pager.js header -->
			<div id="pager">
			
				<!-- WELCOME -->
				<div id="welcome" class="page <?php echo $welcomeClass;?>" data-effect="slide-right">
					<div class="page-inner">
						<div id="welcome-con" class="paper-box">
							<?php renderLogo('welcome'); ?>
							<h1 class="ribbon text_dir"><?php echo $appName;?></h1>
							<div class="prize-con">
								<div class="prize-txt">
									<h3 class="text_dir"><?php echo $mainPrizeTxt;?><span><?php echo $prizeName;?></span></h3>
								</div>
								<div class="prize-img" style="width: <?php echo $prizeImgWidth;?>px;">
								<?php if($prizeImgExists): ?>
									<div class="prize-sep-left prize-sep"></div>
									<img id="prize-thumb" class="enlarge-thumb page-control" src="<?php echo $prizeImgUrl; ?>" height="<?php echo $prizeImgHeight; ?>" alt="<?php echo $prizeName; ?>" data-target="enlarge-prize" />
									<div class="prize-sep-right prize-sep"></div>		
									<div class="img-zoom"></div>									
								<?php endif; ?>
								</div>		
							</div>
							<div class="start-con">
								<div class="start-button-con">
										<div class="login-fb highlight text_dir">
											<span><?php echo $loginFacebookTxt; ?></span>
											<div id="login-fb" class="button button-brick fb-btn text_dir"><?php echo $loginFacebookBtnTxt;?></div>
										</div>
									<?php if($gameState == "pre"): ?>
										<div class="game-not-start highlight text_dir"><?php echo $gameNotStartedMsg; ?></div>
									<?php else: ?>
										<?php if($onlyCoupon): ?>
										<div id="coupon-con">
											<p class="coupont-txt text_dir"><?php echo $couponCodeTxt; ?> </p>
											<input type="text" id="coupon-code" placeholder="<?php echo $couponTypeTxt; ?>" />	
										</div>
										<?php endif; ?>
										<div id="collected-raffles" class="text_dir" data-display="table">
											<?php echo $collectedRafflesTxt; ?> <span>0</span>
										</div>									
										<div id="btn-start" class="button page-control text_dir" data-target="game" data-orig-txt="<?php echo $playTxt; ?>"><?php echo $playTxt; ?></div>
										<div id="like-gate">
											<div id="btn-like" class="<?php echo $likeGateClass?>button button-brick page-control text_dir" data-target="game"><?php echo $likeButtonTxt; ?></div>
										</div>										
									<?php endif; ?>
								</div>
								<div class="fb-init-loader loading text_dir"><span><?php echo $loadingTxt; ?></span><img src="images/ajax-loader.gif" alt="" /></div>
							</div>
							<div class="paper-bottom"></div>
						</div>
					</div>
				</div>
				<!-- END OF WELCOME -->		
				
				<!-- WINNER -->
				<div id="winner" class="page <?php echo $winnerClass;?>" data-effect="slide-right">
					<div class="page-inner">
						<div id="winner-con" class="paper-box">
							<div class="winner-head">
								<h3 class="text_dir"><?php echo $gameFinishedTxt; ?></h3>
							</div>
							<h1 class="ribbon text_dir"><?php echo $winnerIsTxt;?></h1>
							<?php if($gameState == 'winner' && $winnerId): ?>
							<div class="winner-info">
								<div class="mini-raffle"><span class="text_dir"><?php echo $winnerId;?></span></div>
								<?php if($winnerFbId): ?>
								<div class="winner-info-img">
									<img src="https://graph.facebook.com/<?php echo $winnerFbId;?>/picture" width="50" height="50" alt="" />
								</div>
								<h4 class="text_dir"><?php echo $winnerName;?></h4>
								<?php endif; ?>
							</div>
							<?php endif; ?>
							<div class="prize-txt">
								<p class="text_dir"><?php echo $congratTxt; ?><strong><?php echo $prizeName;?></strong></p>
							</div>
							<?php renderAfterGameCallToAction(); ?>
							<?php renderLogo('end'); ?>						
							<div class="paper-bottom"></div>
						</div>
					</div>
				</div>
				<!-- END OF WELCOME -->						
				
				<!-- ENLARGE PRIZE PLAYED -->
				<div id="enlarge-prize" class="page page-enlarge" data-effect="popup-in" data-close-button="on" data-vertical-pos-adjust="-35">
					<div class="page-inner com-box">
						<div class="com-box-top">
							<h3 class="text_dir"><?php echo $mainPrizeTxt;?><?php echo $prizeName;?></h3>
						</div>
						<div class="com-box-con">
							<img id="big-prize" class="enlarged-img page-control" data-target="HIDE_POPUP" src="<?php echo $bigPrizeImgUrl;?>" height="<?php echo $bigPrizeImgHeight; ?>" alt="<?php echo $prizeName; ?>" />
							<div class="com-box-bottom"></div>
						</div>
					</div>
				</div>				
				<!-- END OF ENLARGE PRIZE -->						
				
				<!-- USER PLAYED -->
				<div id="user-played" class="page" data-effect="popup-in" data-vertical-pos-adjust="-10">
					<div class="page-inner com-box">
						<div class="com-box-top">
							<h3 class="text_dir"><?php echo $alreadyPlayed;?></h3>
						</div>
						<div class="com-box-con">
							<div class="user-raffles">
								<p class="text_dir"><?php echo $yourRafflesTxt;?></p>
							</div>
							<h4 class="text_dir"><?php echo $drawDateMsg;?></h4>
							<p class="text_dir"><?php echo $checkWinnerTxt;?></p>
							<?php renderAfterGameCallToAction(); ?>	
							<div class="com-box-bottom"></div>
						</div>
					</div>
				</div>				
				<!-- END OF USER PLAYED -->					
				
				<!-- RULES -->
				<div id="rules" class="page" data-effect="popup-in" data-close-button="on" data-vertical-pos-adjust="-10">
					<div class="page-inner com-box">
						<div class="com-box-top">
							<h3 class="text_dir"><?php echo $rulesTxt;?></h3>
						</div>						
						<div class="com-box-con">
							<div class="custom-scroll text_dir text_align">
								<?php renderRules(); ?>
							</div>
							<div class="com-box-bottom"></div>
						</div>
						<div id="rules-scroll-up" class="btn-arrow btn-arrow-up button button-round"></div>
						<div id="rules-scroll-down" class="btn-arrow btn-arrow-down button button-round"></div>
					</div>
				</div>				
				<!-- END OF RULES -->			
				
				<!-- GAME -->
				<div id="game" class="page" data-effect="slide-up" data-forced-install-target="forced-install">
					<div class="page-inner">
						<div class="game-panel paper-box">
							<div class="raffle-counter">
								<h4 class="text_dir"><?php echo $youHaveTxt;?></h4>
								<div class="raffle-cnt">
									<div class="raffle-cnt-slider">
										<?php renderTicketCounter();?>
									</div>
								</div>
								<h5 class="text_dir"><?php echo $raffleTxt;?></h5>
							</div>
							<?php renderExtraTickets(); ?>
							<div class="submit-con">
								<div id="btn-gotobox" class="button button-brick page-control text_dir" data-ajax-loading="on" data-mode="permission" data-target="gameover"><?php echo $goToBoxTxt;?></div>
							</div>
							<div class="paper-bottom"></div>
							<div id="extra-scroll-up" class="btn-arrow btn-arrow-up button button-round"></div>
							<div id="extra-scroll-down" class="btn-arrow btn-arrow-down button button-round"></div>							
						</div>
						<div id="box">
							<?php if($brandLogoExists): ?>
								<div class="box-logo">
									<?php renderLogo('box'); ?>
								</div>
							<?php endif; ?>
							<div class="box-bottom"></div>
						</div>
						<div id="raffles">
							<?php renderTickets();?>
						</div>
						<div id="cut"></div>
						<div id="scissor">
							<div class="scissor-up"></div>
							<div class="scissor-bottom"></div>
						</div>						
					</div>
				</div>				
				<!-- END OF GAME -->					
				
				<!-- FORCED INSTALL -->
				<div id="forced-install" class="page" data-effect="popup-in" data-vertical-pos-adjust="-80">		
					<div class="page-inner fb-box com-box">
						<div class="com-box-top">
							<h3 class="text_dir"><?php echo $forcedInstallTitleTxt;?></h3>
						</div>
						<div class="com-box-con">
							<h4 class="text_dir"><?php echo $forcedInstallInfoTxt;?></h4>
							<p class="text_dir"><?php echo $publicDataTxt;?></p>
							<div class="permit-it text_dir"><?php echo $permitTxt;?></div>
							<div id="btn-forced-ok" class="button button-brick page-control text_dir" data-target="HIDE_POPUP"><?php echo $permitYesTxt;?></div>
							<div id="btn-forced-no" class="button button-brick page-control text_dir" data-target="HIDE_POPUP"><?php echo $permitNoTxt;?></div>
							<div class="com-box-bottom"></div>
						</div>
					</div>
				</div>
				<!-- END OF FORCED INSTALL -->						

				<!-- GAMEOVER -->
				<div id="gameover" class="page" data-effect="slide-down">
					<div class="page-inner">
						<div class="paper-box">				
							<h2 class="text_dir"><?php echo $gameOverTxt;?></h2>
							<div class="user-raffles">
								<p class="text_dir"><?php echo $yourRafflesTxt;?></p>
							</div>
							<h4 class="text_dir"><?php echo $drawDateMsg;?></h4>
							<p class="text_dir"><?php echo $checkWinnerTxt;?></p>
							<?php renderAfterGameCallToAction(); ?>
							<?php renderLogo('end'); ?>						
							<div class="paper-bottom"></div>
						</div>
					</div>
				</div>	
				<!-- END OF GAMEOVER -->									
	
				<!-- SYSTEM MESSAGES -->
				<div id="system-msg" class="page" data-effect="popup-in" data-close-button="on" data-vertical-pos-adjust="-10">
					<div class="page-inner com-box">
						<div class="com-box-top">
							<h3 class="text_dir"></h3>
						</div>						
						<div class="com-box-con">
							<div class="msg-content"></div>
							<div class="com-box-bottom"></div>
						</div>
					</div>
				</div>				
				<!-- END OF SYSTEM MESSAGES -->					
	
				<!-- POPUP FADER: for popup background -->
				<div id="popup-fader"></div>
				<!-- END OF POPUP FADER -->		
				
			</div>
			<!-- END OF PAGES -->
			
		</div>
		<!-- END OF MAIN CONTAINER -->
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenLite.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/easing/EasePack.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/CSSPlugin.min.js"></script>	
		<script src="<?php echo getResource('js/fbfunc.js')?>"></script>
		<script src="<?php echo getResource('js/plugins.js')?>"></script>
		<script src="<?php echo getResource('js/pager.js')?>"></script>
		<script src="<?php echo getResource('js/common.js')?>"></script>
		<?php includeAdditionalScripts(); ?>
	</body>
</html>


