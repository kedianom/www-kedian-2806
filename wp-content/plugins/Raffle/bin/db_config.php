<?php
	/****  Database config for database class *****/

	require_once('configuration.php');

	$is_localhost = $_SERVER['HTTP_HOST'] == 'localhost' ? 1 : 0;

	// online database parameters
	$database = array(
		'host' => $dbHost,														
		'user' => $dbUser,                                                     	
		'psw' => $dbPsw,                                                 
		'db' => $dbName,												
		'type' => $dbType,                                                       
		'prefix' => $dbPrefix
	);	

?>