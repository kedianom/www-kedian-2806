<?php

	defined('_FAK') or die;	
	
	require('configuration.php');
	require_once('lib/tools.php');

	/*============================================================================
		Additional params
	=========================================================================== */
	$appScope = "email";
	$appLike = "likebutton";		// likebox, likebutton, none
	$multiForceInstall = true;		// after user denied forced install, can install?

	/*============================================================================
		Base settings
	=========================================================================== */
	$is_localhost = $_SERVER['HTTP_HOST'] == 'localhost' ? 1 : 0;
	$appVersion = getVersion();
	$base = getBaseUrl();
	$baseDomain = getBaseDomain($base);
	$isMobileURL = isset($_GET['mobile']) && $_GET['mobile'] == 1 ? true : false;
	$layout = $isMobileURL ? 'resp' : 'fix';
	$isVirtualDir = strstr($base,'~') ? true : false;
	$thumbBaseURL = $isVirtualDir ? $thumbBase : $base;
	$additionalInlineStyles = '';

	/*============================================================================
		Game settings
	=========================================================================== */
	
	// heights of brand's logos
	$logosHeight = array(
		'welcome'  => array(100, 350),												
		'box' => array(55, 200),							
		'end' => array(70, 250)							
	);		
	
	$prizeImgFile = 'raffle-prize.jpg';
	$brandLogoImgFile = 'raffle-brand-logo.png';
	
	$prizeImgExists = file_exists('images/custom/'.$prizeImgFile);
	
	if($prizeImgExists){
		$prizeImg = 'images/custom/'.$prizeImgFile;
		if($is_localhost)
			$prizeImg = $thumbBaseURL.$prizeImg;
		$prizeImgHeight = 163;
		$prizeImgBorderWidth = 3;
		$prizeImgWidth = getImgDimensionRatio($prizeImg,0,$prizeImgHeight) + $prizeImgBorderWidth*2;
		$prizeImgUrl = $thumbBaseURL.'img.php?src='.$prizeImg.'&amp;h='.$prizeImgHeight;
		
		$bigPrizeImgHeight = 300;
		$bigPrizeImgWidth = 420;
		$bigPrizeImgUrl = $thumbBaseURL.'img.php?src='.$prizeImg.'&amp;h='.$bigPrizeImgHeight.'&amp;w='.$bigPrizeImgWidth;
	}
	
	$brandLogoExists = file_exists('images/custom/'.$brandLogoImgFile);
	
	$likeGateClass = $likeGate == 'enabled' ? 'fb-btn ' : '';
	$likeButtonTxt = $likeGate == 'enabled' ? $likeGateTxt : $startTxt;
	
	$gameFlags = $onlyCoupon ? 'only-coupon ' : '';
	$gameFlags .= $likeGate == 'enabled' ? 'like-gate ' : '';
	$gameFlags .= $gamePlatform == 'facebook' || $gamePlatform == 'facebook-website' ? 'facebook-platform ' : '';	
	
	$maxTicketCnt = $initTicketCnt;	
	
	foreach($extraTickets as $key => $value){
		if($value['status'] > 0){
			$gameFlags .= $key.' ';
			$maxTicketCnt += $value['status'];
			addInlineStyle(".$key #$key-rule{display:list-item;}");
		}
	}
	
	$extraTicketsEnabled = $maxTicketCnt == $initTicketCnt ? 0 : 1;
	$gameFlags .= $extraTicketsEnabled ? 'extra-tickets ' : '';
	$gameFlags .= $maxTicketCnt >= 10 ? 'two-digit-ticket-cnt ' : '';
	
	$inviteLimit = $extraTickets['invite']['limit'];
	$inviteBtnTxt = customizeTxt($inviteBtnTxt,array("inviteLimit" => $inviteLimit));
		
	/*============================================================================
		Datetime settings
	=========================================================================== */
	if($timeZone != 'not defined')
		date_default_timezone_set($timeZone);	
	
	setlocale(LC_ALL,$locale);
	
	$startDateTxt = formatDate($startDate);
	$endDateTxt = formatDate($endDate);
	
	$startShortDateTxt = formatShortDate($startDate);
	$endShortDateTxt = formatShortDate($endDate);

	$startTimeTxt = formatTime($startDate);
	$endTimeTxt = formatTime($endDate);
	
	$startDateTimeTxt = $startDateTxt.' '.$startTimeTxt;
	$endDateTimeTxt = $endDateTxt.' '.$endTimeTxt;
	
	$endStamp = strtotime($endDate);
	$startStamp = strtotime($startDate);
	$currentStamp = time();
		
	$gameNotStartedMsg = customizeTxt($gameNotStartedTxt,array("startTimeTxt" => $startTimeTxt,"startShortDateTxt" => $startShortDateTxt));
	$drawDateMsg = customizeTxt($drawDateTxt,array("endTimeTxt" => $endTimeTxt,"endShortDateTxt" => $endShortDateTxt));

	/*============================================================================
		Application parameters
	=========================================================================== */

	// application config
	$config = array(
		'appId'  => $appId,												
		'secret' => $appSecret,							
		'fileUpload' => false,
	);	

	// application namespace	
	$appFullName = strstr($appName,$brandName) ? $appName : $brandName.' | '.$appName;
	$appUrl = 'https://apps.facebook.com/'.$appNameSpace.'/'; 

	//google analytics tracking code id
	$analytics = array(
		'id' => $analyticsId,													
		'domain' => $baseDomain
	);

	$urlParam = strstr($brandUrl,'/pages') ? '?sk=app_' : '/app_';
	$tabUrl = $brandUrl.$urlParam.$config['appId'];
	if($gamePlatform == 'website')
		$tabUrl = $brandUrl = $websiteUrl;

	// create array from page parameters
	$page = array(
		'brand' => $brandName,
		'title' => $appName,
		'appFullName' => $appFullName,
		'type' => 'game',
		'logo' => $base.'images/skin/'.$backgroundSkin.'/fb-feed.png',	
		'id'  => $brandId,
		'url' => $brandUrl,
		'appUrl' => $appUrl,
		'mobileUrl' => $base.'index.php?mobile=1',
		'tab' => $tabUrl,
		'desc' => $appDescTxt,
		'invite' => $appInviteTxt,
		'wallpost' => $appShareTxt,
		'share' => $appShareTxt
	);	
		
	// texts to pass JS
	$texts = array(
		'yourRafflesTxt' => $yourRafflesTxt,
		'couponUsedTxt' => $couponUsedTxt,
		'couponInvalidTxt' => $couponInvalidTxt,
		'getMoreTxt' => $getMoreTxt,
		'didntWinTxt' => $didntWinTxt,
		'inviteFailedTitleTxt' => $inviteFailedTitleTxt,
		'inviteFailedBodyTxt' => $inviteFailedBodyTxt,
		'shareFailedTitleTxt' => $shareFailedTitleTxt,
		'shareFailedBodyTxt' => $shareFailedBodyTxt
	);		
		
	// render brand's logo 
	function renderLogo($type){
		global $logosHeight, $thumbBaseURL, $brandWebSite, $brandName, $brandLogoExists, $brandLogoImgFile, $is_localhost;

		if($brandLogoExists){
			
			$logoURL = 'images/custom/'.$brandLogoImgFile;
			if($is_localhost)
				$logoURL = $thumbBaseURL.$logoURL;
			list($origWidth, $origHeight) = getimagesize($logoURL);
			
			$maxWidth = $logosHeight[$type][1];
			$newHeight = $logosHeight[$type][0];
			$ratio = $origWidth / $origHeight;
			$newWidth = $newHeight * $ratio;
			if($newWidth > $maxWidth)
				$newHeight = $maxWidth / $ratio;
				
			$conHeight = $logosHeight[$type][0];
			$marginTop = ($conHeight - $newHeight) / 2;
						
			$imgURL = $thumbBaseURL.'img.php?src='.$logoURL.'&amp;h='.$newHeight;	
			$imgHTML = '<img src="'.$imgURL.'" height="'.$newHeight.'" style="margin-top:'.$marginTop.'px;" alt="'.$brandName.'" />';
			
		}
		else
			$imgHTML = '';
		
		if(!empty($brandWebSite))
			$imgHTML = '<a href="'.$brandWebSite.'" target="_blank">'.$imgHTML.'</a>';		
		
		$logoHTML = '<div class="brand-logo" style="height:'.$logosHeight[$type][0].'px;">'.$imgHTML.'</div>';
		
		echo $logoHTML;		
	}
		
	// get base url
	function getBaseUrl(){
		$base = getBaseProtocol() . $_SERVER['HTTP_HOST'] . strtok($_SERVER['REQUEST_URI'],'?');
		if(strstr($base,'.php')){
			$lastSepPos = strrpos($base,'/');
			$base = substr($base,0,$lastSepPos+1);
		}
		return $base;
	}	
	
	// get base protocol
	function getBaseProtocol(){
		if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) 
		|| isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
			$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
		return $protocol;
	}

	// get hostname from url
	function getBaseDomain($url){
		if($GLOBALS['is_localhost']){
			$domain = 'localhost';
		}
		else{
			$parse = parse_url($url);
			$host_names = explode(".", $parse['host']);
			$domain = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
		}
		return $domain;
	}

	// get facebook page infos
	function getGraphApi($url,$fields){
		if ($url != ''){
			$info = file_get_contents('https://graph.facebook.com/?ids='.$url.'&fields='.$fields);
			$info = json_decode($info,true);
			return $info[$url];
		}
	}
	
	// Get image width or height based on ratio
	function getImgDimensionRatio($path,$width,$height=0){
		list($origWidth, $origHeight) = getimagesize($path);
		$ratio = $origWidth / $origHeight;
		if(!$width)
			return floor($height * $ratio);
		return floor($width / $ratio);
	}

	function formatDate($txt){
		global $dateFormat;
		
		$format = str_replace('year','%Y',$dateFormat);
		$format = str_replace('month','%m',$format);
		$format = str_replace('day', '%d',$format);
		$date = strtotime($txt);
		return strftime($format,$date);
	}

	function formatShortDate($txt){
		global $shortDateFormat;
		
		$format = str_replace('month_name','%b',$shortDateFormat);
		$format = str_replace('day', '%d',$format);
		$date = strtotime($txt);
		return strftime($format,$date);
	}
	
	function formatTime($txt){
		global $timeFormat;
		
		$format = $timeFormat == '12h' ? 'h:i A' : 'H:i';
		$date = strtotime($txt);
		return date($format,$date);
	}
	
?>