<?php

$purchaseCode = 'fddfc351-1b37-492b-9670-5e09309b546d';
$brandUrl = 'https://www.facebook.com/kedianomics';
$brandName = 'Kedianomisc';
$brandId = '214288652246214';
$brandEmail = 'anupam.osx86@gmail.com';
$brandWebSite = 'http://www.kedianomics.com';
$appOnline = '1';
$appName = 'Kedianomics';
$appNameSpace = 'kedianomics_game';
$appId = '177667082378622';
$appSecret = '2fb3ee0af098d3dc0112e5f0aac967f9';
$appConfigured = '0';
$gamePlatform = 'facebook-website';
$websiteUrl = '';
$likeUrl = 'https://www.facebook.com/kedianomics';
$likeGate = 'enabled';
$headLineTxt = 'Get all the raffle tickets and win!';
$startDate = '07/01/2016 01:00 AM';
$endDate = '07/08/2016 12:25 AM';
$timeZone = 'Asia/Kolkata';
$prizeName = 'Free Coupon ( Test )';
$muteSounds = 1;
$onlyCoupon = 0;
$initTicketCnt = '1';
$locale = 'en_GB';
$timeFormat = '12h';
$dateFormat = 'day/month/year';
$shortDateFormat = 'month_name. day.';
$fontColor = 'f75e3c';
$backgroundSkin = 'red';
$textDirection = 'ltr';
$mainPrizeTxt = 'The prize: ';
$youHaveTxt = 'You have';
$raffleTxt = 'tickets';
$extraChanceTxt = 'Get extra tickets:';
$goToBoxTxt = 'Submit';
$forcedInstallTitleTxt = 'We need to access your public data!';
$forcedInstallInfoTxt = 'Without it you can\'t enter the raffle.<br />Please give access to:';
$publicDataTxt = 'your facebook id<br />your name<br />your email address';
$permitTxt = 'Do you allow access?';
$permitYesTxt = 'yes';
$permitNoTxt = 'no';
$rulesTxt = 'Terms and conditions';
$likeBtnTxt = 'Like our page';
$loadingTxt = 'loading...';
$likeToStartTxt = 'to start!';
$gameOverTxt = 'Thank you for playing!';
$yourRafflesTxt = 'You collected <strong>{rafflesCnt}</strong> ticket:';
$drawDateTxt = 'The draw will be on {endShortDateTxt} at {endTimeTxt}.';
$checkWinnerTxt = 'You can check the winner by returning to this application.<br />The winner will be notified by email and Facebook notification!';
$whileWaitTxt = 'While you\'re waiting:';
$didntWinTxt = 'Didn\'t you win? Don\'t worry!';
$checkPostsTxt = 'Check out our posts!';
$alreadyPlayed = 'You\'ve already played...';
$winnerIsTxt = 'The Winner Is:';
$finishTxt = 'Finish';
$playTxt = 'Play';
$startTxt = 'Start';
$gameFinishedTxt = 'This game has finished...';
$congratTxt = 'We congratulate the winner!<br />The prize is: ';
$durationTxt = 'duration';
$gameNotStartedTxt = 'Game starts on {startShortDateTxt} at {startTimeTxt}.<br />Don\'t miss it!';
$loginFacebookTxt = 'Please login to continue!';
$loginFacebookBtnTxt = 'Login';
$orientWarningTxt = 'Please change to<br />landscape view!';
$likeOurPageTxt = 'Like our page!';
$likeInfoTxt = 'So you can find us easily next time.';
$couponCodeTxt = 'Coupon code:';
$couponTypeTxt = 'Type here to start!';
$couponInvalidTxt = 'Invalid code!';
$couponUsedTxt = 'Used at';
$likeGateTxt = 'Like to play!';
$incChanceTxt = 'Increase your chances of winning:';
$getMoreRaffleTxt = 'Get more ticket!';
$getMoreTxt = 'Get more!';
$collectedRafflesTxt = 'Collected tickets:';
$buttonLoadingTxt = 'Loading...';
$shareBtnTxt = 'Share';
$shareFailedTitleTxt = 'Sharing failed';
$shareFailedBodyTxt = 'Did you press the Share button in the Facebook popup?<br /><br />Please try again to get the extra ticket!';
$inviteBtnTxt = 'Invite {inviteLimit}';
$inviteFailedTitleTxt = 'Invitation failed';
$inviteFailedBodyTxt = 'You have invited <strong>{invitedCnt}</strong> friends.<br /><br /><strong>You have to invite minimum {inviteLimit} friends!</strong><br />Please try again to get the extra ticket!';
$youtubeSubscribeBtnTxt = 'Subscribe';
$twitterShareBtnTxt = 'Tweet';
$twitterFollowBtnTxt = 'Follow';
$googlePlusBtnTxt = 'Google+';
$linkedinShareBtnTxt = 'Share';
$subscribeBtnTxt = 'Subscribe';
$winnerNotiNoEmailTxt = 'Congratulation, you\'re the winner! Please send your address and phone number to !';
$winnerNotiTxt = 'Congratulation, you\'re the winner! We will contact you!';
$winnerMailSubjectTxt = 'You\'re the winner!';
$winnerMailBodyTxt = 'Congratulation, you\'re the winner of the  facebook application!<br /> The prize: .<br />We will contact you!';
$brandMailWinnerSubject = 'Winner information';
$brandMailWinnerTxt = 'Ticket id: {raffle_id}<br />Name: {name}<br />E-mail: {email}<br />Facebook id: {fb_id}<br />Facebook url: {fb_url}';
$notProvidedTxt = 'not provided';
$winnerWillContactTxt = '<br /><br />Winner will contact you!';
$statMailSubject = 'Statistics';
$statMailUserList = 'Click here to download user list!';
$statMailSubscriberList = 'Click here to download subscriber list!';
$statMailGA = 'For detailed statistics, please check your Google Analytics account.';
$appDescTxt = 'Get all the raffle tickets for more chances to win the best offer and bonus from Kedianomis.';
$appInviteTxt = 'Get all the raffle tickets for more chances to win the best offer and bonus from Kedianomis.';
$appShareTxt = 'Get all the raffle tickets for more chances to win the best offer and bonus from Kedianomis.';
$analyticsId = 'UA-77656424-1';
$dbHost = 'localhost';
$dbUser = 'kedianom_raffle';
$dbPsw = '%!z#v(tkyQfF';
$dbName = 'kedianom_raffle';
$dbType = 'mysqli';
$dbPrefix = 'cpcz';
$thumbBase = 'https://kedianomics.com/wp-content/plugins/Raffle/';
$mailServer = 'local';
$updatesEnabled = 1;
$extraTickets = array (
  'invite' => 
  array (
    'limit' => '0',
    'used' => '0',
    'btn-plugin' => '0',
    'invited_cnt' => '0',
    'status' => 0,
  ),
  'youtube-subscribe' => 
  array (
    'channel' => '',
    'used' => '0',
    'btn-plugin' => '1',
    'status' => 0,
  ),
  'google-plus' => 
  array (
    'url' => '',
    'used' => '0',
    'btn-plugin' => '1',
    'status' => 0,
  ),
  'twitter-share' => 
  array (
    'url' => '',
    'text' => '',
    'hashtags' => '',
    'used' => '0',
    'btn-plugin' => '1',
    'status' => 0,
  ),
  'twitter-follow' => 
  array (
    'user' => '',
    'used' => '0',
    'btn-plugin' => '1',
    'status' => 0,
  ),
  'linkedin-share' => 
  array (
    'url' => '',
    'used' => '0',
    'btn-plugin' => '1',
    'status' => 0,
  ),
  'share' => 
  array (
    'used' => '0',
    'btn-plugin' => '0',
    'status' => 0,
  ),
  'subscribe' => 
  array (
    'used' => '0',
    'btn-plugin' => '0',
    'status' => 0,
  ),
);

?>