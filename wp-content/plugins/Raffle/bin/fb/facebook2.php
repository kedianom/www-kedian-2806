<?php

require_once 'facebook.php';

class Facebook2 extends Facebook {

	public static function parse_signed_request($signed_request, $secret){
		list($encoded_sig, $payload) = explode('.', $signed_request, 2);

		// decode the data
		$sig = self::base64_url_decode($encoded_sig);
		$data = json_decode(self::base64_url_decode($payload), true);

		if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
			error_log('Unknown algorithm. Expected HMAC-SHA256');
			return null;
		}

		// check sig
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		if ($sig !== $expected_sig) {
			error_log('Bad Signed JSON signature!');
			return null;
		}

		return $data;
	}

	private static function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_', '+/'));
	}

	public static function redirect_to_login($perms){
		$redrop = site_url('main/fb_redrop').'/';
		if (uri_string()) {
			$redrop .= '?part='.rawurlencode(uri_string());
		}

		$url = $FB->getLoginUrl(array(
			'next'   => $redrop,
			'cancel' => $redrop,
			'req_perms' => join(',', $perms),
		));

		die("<span>&nbsp;</span><script>window.top.location = '".$url."';</script>");
	}

	private static function has_permission($FB, $uid, $perms) {
		if (empty($perms)) {
			$query = "select uid from user where uid = '".$uid."' and is_app_user = 1";
		} else {
			$perms_str = join(' = 1 and ', $perms).' = 1';
			$query = "select uid from permissions where uid = '".$uid."' and ".$perms_str;
		}

		try {
			$row = $FB->api(array(
				'method' => 'fql.query',
				'query'  => $query,
			));

			return $row && $row[0]['uid'] == $uid;
		} catch (Exception $e) {
			return false;
		}
	}

	public static function is_fan($FB, $uid, $page_id, $access_token = null){
		try {
			$params = array(
				'method' => 'fql.query',
				'query' => "select uid from page_fan where uid ='".$uid."'  and page_id = ".$page_id,
			);
			if ($access_token !== null) {
				$params['access_token'] = $access_token;
			}
			$resp = $FB->api($params);
			return $is_fan = (count($resp) !== 0);
		} catch (exception $e) {
			error_log($e->getMessage());
			return false;
		}
	}
}
