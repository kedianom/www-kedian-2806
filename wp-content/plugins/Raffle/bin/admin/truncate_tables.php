<?php
			
	require_once '../lib/db.class.php';
	
	/**** Validate call *****/
	if($_POST['secret'] != $appSecret){
		echo 'Invalid access!';
		die();
	}	

	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** MySQL Inject defense *****/
	$db->escapeArray($_POST);

	/**** URL Hack defense *****/
	if($db->checkReferer()){
		$db->query("TRUNCATE TABLE {}_raffle_games");
		$db->query("TRUNCATE TABLE {}_raffle_winners");
		$db->query("TRUNCATE TABLE {}_raffle_coupons");
		
		echo 'truncated';
	}

	$db->closeConnection();
	
?>