<?php
	/*============================================================================
		Export subscribers to csv
	=========================================================================== */
	
	require_once '../lib/db.class.php';
	
	/**** Validate call *****/
	if($_GET['secret'] != $appSecret){
		echo 'Invalid access!';
		die();
	}

	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** Get users *****/
	$sql = $db->query("SELECT _fbuserID,_fbuserName,_fbuserFirstName,_fbuserLastName,_fbuserEmail,_fbuserGender,_fbuserLocale,_regDate FROM {}_raffle_users WHERE _getNews = 1");
	$result = $db->fetchAllAssoc($sql);
	
	array_to_csv_download($result,'subscriber_list.csv');
	
	function array_to_csv_download($array, $filename = "export.csv") {
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="'.$filename.'";');

		$f = fopen('php://output', 'w');

		// output the column headings
		fputcsv($f, array('Facebook ID', 'Full name', 'First name', 'Last name', 'Email', 'Gender', 'Locale', 'Register date'));

		foreach ($array as $line) {
			fputcsv($f, $line);
		}
	}   	

?>