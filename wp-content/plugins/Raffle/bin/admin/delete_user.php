<?php
			
	require_once '../lib/db.class.php';
	require_once '../debug_config.php';
	
	/**** Validate call *****/
	if(!defined('_CUSTOMIZER') && $_POST['secret'] != $appSecret){
		echo 'Invalid access!';
		die();
	}
	
	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** MySQL Inject defense *****/
	$db->escapeArray($_POST);

	/**** URL Hack defense *****/
	if($db->checkReferer()){

		/**** Set values *****/	
		$fbid = $_POST['fbid'];

		if(empty($fbid) || $fbid == '0'){
			echo 'Invalid Facebook id!';
		}
		else{
			$sql = $db->query("SELECT id FROM {}_raffle_users WHERE _fbuserID = $fbid");
			$userId = $db->fetchSingle($sql);
			
			$db->query("DELETE FROM {}_raffle_users WHERE id = $userId");
			$db->query("DELETE FROM {}_raffle_games WHERE _userId = $userId");
			
			echo 'deleted';
		}

	}

	$db->closeConnection();
	
?>