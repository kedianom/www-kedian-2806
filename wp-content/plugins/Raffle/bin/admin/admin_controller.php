<?php	
		
	defined('_FAK') or die;	
	
	if($is_admin)
		require 'bin/admin/get_stat.php';
		
	function includeAdminScript(){
		global $is_admin, $appSecret;
		$secret = defined('_CUSTOMIZER') ? 'DEMO' : $appSecret;
		
		if($is_admin){
			echo '<script src="'.getResource('js/admin.js').'"></script>';
			echo '<script>APP_SECRET = "'.$secret.'"</script>';
		}
	}

	function includeAdminStyles(){
		global $is_admin;
		
		if($is_admin){
			echo '<link type="text/css" href="'.getResource('css/admin.css').'" rel="stylesheet" />';
		}
	}	

	function renderAdmin(){
		global $is_admin, $page, $adminStats, $statList, $appSecret;
		
		$userListURL = 'bin/admin/get_users_xls.php?secret='.$appSecret;
		$subscriberListURL = 'bin/admin/get_subscribers_list.php?secret='.$appSecret;
		
		if(defined('_CUSTOMIZER')){
			$userListURL = 'bin/admin/example/user_list.xls';
			$subscriberListURL = 'bin/admin/example/subscriber_list.csv';				
		}
		
		
		if($is_admin){
		?>
			<!-- ADMIN PANEL -->
			<div id="show-admin" class="button button-round"></div>
			<div id="admin">
				<div class="close-admin button button-round">X</div>
				<div class="admin-nav-con">
					<div class="admin-nav active"><span>Quick statistics</span></div>
					<div class="admin-nav"><span>Tools</span></div>
					<div class="admin-nav"><span>Debug</span></div>
				</div>
				<div class="admin-pages-con">
					<div class="admin-pages">
						<div class="admin-panel" id="admin-stat">
							<div class="admin-panel-inner custom-scroll">
								<?php echo $statList; ?>
							</div>
						</div>						
						<div class="admin-panel" id="admin-downloads">
							<div class="admin-panel-inner">						
								<div class="button-con">
									<a href="<?php echo $userListURL;?>" class="button">User list (xls)</a>
								</div>
								<div class="button-con">
									<a href="<?php echo $subscriberListURL;?>" class="button">Subscriber list (csv)</a>
								</div>
							</div>
						</div>	
						<div class="admin-panel" id="admin-debug">
							<div class="admin-panel-inner">						
								<div class="button-con">
									<div id="admin-login" class="button">Get permission</div>
								</div>
								<div class="button-con">
									<div id="remove-perm" class="button">Remove permission</div>
								</div>
								<div class="button-con">
									<div id="remove-db" class="button">Remove user from db</div>
								</div>
								<div class="button-con">
									<a id="reload-app" href="." class="button">Reload application</a>
								</div>
								<div class="debug-likebox"><?php echo createLikeBoxIframe($page['url']); ?></div>
							</div>
						</div>						
					</div>
				</div>
				<a class="goto_admin_btn" href="control" target="_blank">Admin page</a>
			</div>
			<div id="admin-fader"></div>
			<!-- END OF ADMIN PANEL -->
		<?php
		}
	}

		
?>