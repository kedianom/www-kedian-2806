<?php
	/*============================================================================
		Export users to xls
	=========================================================================== */
	
	require_once '../lib/db.class.php';
	
	/**** Validate call *****/
	if($_GET['secret'] != $appSecret){
		echo 'Invalid secret code!';
		die();
	}
	
	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** Get users *****/
	$sql = $db->query("SELECT * FROM {}_raffle_users");
	$result = $db->fetchAllAssoc($sql);
		
	$filename = "user_list.xls";	

	/**** Create output *****/
 	header("Content-Type: application/vnd.ms-excel; charset=utf-8"); 
	header("Content-Disposition: attachment; filename=$filename");  
	header("Pragma: no-cache"); 
	header("Expires: 0");   
	
	/******* Start of Formatting for Excel *******/   
	//define separator (defines columns in excel & tabs in word)
	$sep = "\t"; //tabbed character
	//start of printing column names as names of MySQL fields
	
 	foreach($result[0] as $key => $value){
		printUTF($key . "\t");
	}
	print("\n");    
	//end of printing column names  
	//start while loop to get data
		foreach($result as $row){
			$schema_insert = "";
			foreach($row as $field){
				if(!isset($field))
					$schema_insert .= "NULL".$sep;
				elseif ($field != "")
					$schema_insert .= "$field".$sep;
				else
					$schema_insert .= "".$sep;
			}
			$schema_insert = str_replace($sep."$", "", $schema_insert);
			$schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
			$schema_insert .= "\t";
			printUTF(trim($schema_insert));
			print "\n";
		} 

	function printUTF($text){
		// echo mb_convert_encoding($text,'utf-16','utf-8');
		echo $text;
	}
?>