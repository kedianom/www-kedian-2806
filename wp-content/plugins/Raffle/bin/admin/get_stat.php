<?php

	defined('_FAK') or die;

	$originalPath = getcwd();
	$restorePath = false;

	if(!strstr($originalPath,'bin/func')){
		chdir($originalPath.'/bin/func');
		$restorePath = true;
	}

	// Define some-some
    define ( 'DS', DIRECTORY_SEPARATOR );

    // Get DB Class
	require_once (__DIR__.DS.'..'.DS.'lib'.DS.'db.class.php');

	/**** Config database *****/
	global $likeGate;
	$db = new db($db_config);
	$db->openConnection();

	$adminStats = array();

	/**** Count unique users *****/
	$visitor_db_file = __DIR__.DS.'..'.DS.'lib'.DS.'user_count.txt';
	$vistior_file = fopen($visitor_db_file,"r");
	$adminStats['visitor_cnt'] = fread($vistior_file, filesize($visitor_db_file));
	fclose($vistior_file);

	/**** Count players *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_users");
	$adminStats['player_cnt'] = $db->fetchSingle($sql);

	/**** Count fans *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_users WHERE _isFan = 1");
	$adminStats['fan_cnt'] = $db->fetchSingle($sql);
	$fansStat = $likeGate != 'disabled' ? '<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Players, who was already liked your Facebook page. They were fans before the game started."></span> Fans:<b> '.$adminStats['fan_cnt'].' </b><br />' : '';

	/**** New fans *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_users WHERE _newFan = 1");
	$adminStats['new_fan'] = $db->fetchSingle($sql);

	/**** Count mobile version *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_users WHERE _mobileVersion = 1");
	$adminStats['mobile'] = $db->fetchSingle($sql);

	/**** Count subscribes *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_users WHERE _getNews = 1");
	$adminStats['subscribe'] = $db->fetchSingle($sql);

	/**** Count invitations *****/
	$sql = $db->query("SELECT SUM(_invitedCnt) FROM {}_raffle_users");
	$adminStats['invited_cnt'] = $db->fetchSingle($sql);

	/**** Count shares *****/
	$sql = $db->query("SELECT SUM(_shared) FROM {}_raffle_users");
	$adminStats['shared_cnt'] = $db->fetchSingle($sql);

	/**** Count Google plused *****/
	$sql = $db->query("SELECT SUM(_googlePlused) FROM {}_raffle_users");
	$adminStats['google_plus'] = $db->fetchSingle($sql);

	/**** Count Youtube subscribes *****/
	$sql = $db->query("SELECT SUM(_youtubeSubscribed) FROM {}_raffle_users");
	$adminStats['youtube_subscribe'] = $db->fetchSingle($sql);

	/**** Count Twitter shares *****/
	$sql = $db->query("SELECT SUM(_twitterShared) FROM {}_raffle_users");
	$adminStats['twitter_share'] = $db->fetchSingle($sql);

	/**** Count Twitter followers *****/
	$sql = $db->query("SELECT SUM(_twitterFollowed) FROM {}_raffle_users");
	$adminStats['twitter_follow'] = $db->fetchSingle($sql);

	/**** Count Linkedin shares *****/
	$sql = $db->query("SELECT SUM(_linkedinShared) FROM {}_raffle_users");
	$adminStats['linkedin_share'] = $db->fetchSingle($sql);

	/**** Count cheaters *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_users WHERE _comment = 'reinstall'");
	$adminStats['reinstall'] = $db->fetchSingle($sql);

	/**** Count games *****/
	$sql = $db->query("SELECT COUNT(*) FROM {}_raffle_games");
	$adminStats['games'] = $db->fetchSingle($sql);

	/**** Avarage game count *****/
	$adminStats['avg_game_cnt'] = $adminStats['games'] / $adminStats['player_cnt'];
	$avgGameCount = round($adminStats['avg_game_cnt'], 1);

	$statList =	'<p><span class="tooltip-admin glyphicon glyphicon-info-sign" title="Estimated number of visitors, based on cookies."></span> Visitors:<b> '.$adminStats['visitor_cnt'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Visitors, who installed the application."></span> Players:<b> '.$adminStats['player_cnt'].' </b><br />'
				.$fansStat.'
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Likes, wich generated in the game."></span> New likes:<b> '.$adminStats['new_fan'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Players, who used mobile version."></span> Mobile/tablet:<b> '.$adminStats['mobile'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Players, who subscribed to your newsletter."></span> Subscribes:<b> '.$adminStats['subscribe'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of invited users at Facebook."></span> Facebook invitations:<b> '.$adminStats['invited_cnt'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of game sharings on user\'s wall at Facebook."></span> Facebook shares:<b> '.$adminStats['shared_cnt'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of Google+ shares."></span> Google+ share:<b> '.$adminStats['google_plus'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of Youtube subscribes."></span> Youtube subscribes:<b> '.$adminStats['youtube_subscribe'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of Twitter shares/tweets."></span> Twitter shares:<b> '.$adminStats['twitter_share'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of Twitter followers."></span> Twitter followers:<b> '.$adminStats['twitter_follow'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Number of LinkedIn shares."></span> LinkedIn shares:<b> '.$adminStats['linkedin_share'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="They removed the application, then try to play again."></span> Try to cheat:<b> '.$adminStats['reinstall'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Total number of collected raffle tickets."></span> Raffle tickets:<b> '.$adminStats['games'].' </b><br />
				<span class="tooltip-admin glyphicon glyphicon-info-sign" title="Avarage raffle count by users."></span> Avg ticket count:<b> '.$avgGameCount.' </b></p>';

	$statList_table = '<table class="table_2_c">
				<tbody>
					<tr>
						<td>
							Visitors
							<img class="info tooltip" src="img/info.png" alt="Info" title="Estimated number of visitors, based on cookies." />
						</td>
						<td>
							'.(empty($adminStats['visitor_cnt']) ? '0' : $adminStats['visitor_cnt'] ).'
						</td>
					</tr>
					<tr>
						<td>
							Players
							<img class="info tooltip" src="img/info.png" alt="Info" title="Visitors, who installed the application." />
						</td>
						<td>
							'.$adminStats['player_cnt'].'
						</td>
					</tr>
					<tr>
						<td>
							New likes
							<img class="info tooltip" src="img/info.png" alt="Info" title="Likes, wich generated in the game." />
						</td>
						<td>
							'.$adminStats['new_fan'].'
						</td>
					</tr>
					<tr>
						<td>
							Mobile/tablet
							<img class="info tooltip" src="img/info.png" alt="Info" title="Players, who used mobile version." />
						</td>
						<td>
							'.$adminStats['mobile'].'
						</td>
					</tr>
					<tr>
						<td>
							Subscribes
							<img class="info tooltip" src="img/info.png" alt="Info" title="Players, who subscribed to your newsletter." />
						</td>
						<td>
							'.$adminStats['subscribe'].'
						</td>
					</tr>
					<tr>
						<td>
							Facebook invitations
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of invited users at Facebook." />
						</td>
						<td>
							'.$adminStats['invited_cnt'].'
						</td>
					</tr>
					<tr>
						<td>
							Facebook shares
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of game sharings on user\'s wall at Facebook." />
						</td>
						<td>
							'.$adminStats['shared_cnt'].'
						</td>
					</tr>
					<tr>
						<td>
							Google+ share
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of Google+ shares." />
						</td>
						<td>
							'.$adminStats['google_plus'].'
						</td>
					</tr>
					<tr>
						<td>
							Youtube subscribes
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of Youtube subscribes" />
						</td>
						<td>
							'.$adminStats['youtube_subscribe'].'
						</td>
					</tr>
					<tr>
						<td>
							Twitter shares
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of Twitter shares/tweets." />
						</td>
						<td>
							'.$adminStats['twitter_share'].'
						</td>
					</tr>
					<tr>
						<td>
							Twitter followers
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of Twitter followers." />
						</td>
						<td>
							'.$adminStats['twitter_follow'].'
						</td>
					</tr>
					<tr>
						<td>
							LinkedIn shares
							<img class="info tooltip" src="img/info.png" alt="Info" title="Number of LinkedIn shares." />
						</td>
						<td>
							'.$adminStats['linkedin_share'].'
						</td>
					</tr>
					<tr>
						<td>
							Try to cheat
							<img class="info tooltip" src="img/info.png" alt="Info" title="They removed the application, then try to play again." />
						</td>
						<td>
							'.$adminStats['reinstall'].'
						</td>
					</tr>
					<tr>
						<td>
							Raffle tickets
							<img class="info tooltip" src="img/info.png" alt="Info" title="Total number of collected raffle tickets." />
						</td>
						<td>
							'.$adminStats['games'].'
						</td>
					</tr>
					<tr>
						<td>
							Avg ticket count
							<img class="info tooltip" src="img/info.png" alt="Info" title="Avarage raffle count by users." />
						</td>
						<td>
							'.$avgGameCount.'
						</td>
					</tr>
				</tbody>
			</table>';

	$db->closeConnection();

	/**** Restore working dir *****/
	chdir($originalPath);

?>