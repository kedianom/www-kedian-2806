<?php

/**** Debug config *****/
require_once 'debug_config.php';	

/**** If need to install *****/
if(is_dir('installer') && !defined('_SKIP_INSTALL')){
	header( 'Location: installer' );
	return;
}
	
/**** Define security enum *****/
define("_FAK",true);

/**** Error reporting *****/
if(defined('_ERROR_REPORT')){
	error_reporting(E_ALL);ini_set( "display_errors", 1);
}
else{
	error_reporting(0);ini_set( "display_errors", 0);
}
	
/**** Include helpers *****/
require 'bin/configurator.php';
require 'bin/facebook_func.php';
require 'bin/admin/admin_controller.php';
require 'bin/lib/counter.php';

/**** If game is offline *****/
$adminRedirect = 0;
if(!$appOnline)
	$adminRedirect = $is_admin ? 'control' : $brandUrl;

/**** Need to like? *****/
if(defined('_SKIP_LIKE'))
	$is_fan = true;
	
/**** Game states: pre (game not started), active, winner(game finished, announcment of winner)  *****/
$gameState = 'active';
$welcomeClass = 'active';
$winnerClass = '';

/**** Config database *****/	
$realWorkingDir = getcwd();
chdir(realpath('.').'/bin/func');
require_once '../lib/db.class.php';	

$db = new db($db_config);
$db->openConnection();

/**** Game not started yet *****/
if($currentStamp < $startStamp){
	$gameState = 'pre';
}
/**** Game has finished, show winner *****/
else if($currentStamp > $endStamp){
	$gameState = 'winner';
	$welcomeClass = '';
	$winnerClass = 'active';
	
	/**** Winner is generated? *****/
	$getWinnerSql = "SELECT w._gameId, u._fbuserName, u._fbuserID, u._fbuserEmail FROM {}_raffle_winners w INNER JOIN {}_raffle_games g on g._gameId = w._gameId INNER JOIN {}_raffle_users u ON u.id = g._userId";
	$sql = $db->query($getWinnerSql);
	$winner = $db->fetchAssoc($sql);
	
	/**** Generate winner if database winners table is empty *****/
	if(empty($winner['_gameId'])){
		$sql = $db->query("SELECT MAX(_gameId) FROM {}_raffle_games");
		$maxId = $db->fetchSingle($sql);
		
		$winnerId = rand(1,$maxId);
		$db->query("INSERT INTO {}_raffle_winners (_gameId) VALUES ($winnerId)");
		
		/**** Get generated winner infos *****/
		$sql = $db->query($getWinnerSql);
		$winner = $db->fetchAssoc($sql);
		
		/**** If winner has email *****/
		if(isset($winner['_fbuserEmail']) && $winner['_fbuserEmail'] != '')
			sendHasEmailWinnerNotis($winner);
		else
			sendNoEmailWinnerNotis($winner);
			
		sendStatEmail();
	}
		
	$winnerId = str_pad($winner['_gameId'], 5, "0", STR_PAD_LEFT);
	$winnerName = $winner['_fbuserName'];
	$winnerFbId = $winner['_fbuserID'];
}

/**** Winner has mail *****/
function sendHasEmailWinnerNotis($winner){
	
	global $winnerNotiTxt,$brandMailWinnerTxt,$winnerInfoBody,$brandMailWinnerSubject,$appName,$brandEmail,$winnerMailSubjectTxt,$winnerMailBodyTxt;
	
	/**** Send a facebook notification to winner *****/
	sendWinnerNotification($winner['_fbuserID'], $winnerNotiTxt);
	
	/**** Send mail to brand manager about winner *****/
	$winnerInfoBody = createWinnerInfoBody($winner,$brandMailWinnerTxt);
	sendMailToBrand($brandMailWinnerSubject,$winnerInfoBody);			// send email to owner
	
	/**** Send mail to winner *****/
	sendMail($appName,$brandEmail,$winner['_fbuserEmail'],$winnerMailSubjectTxt,$winnerMailBodyTxt);		// send email to winner
}

/**** Winner doesn't have email *****/
function sendNoEmailWinnerNotis($winner){
	
	global $winnerNotiNoEmailTxt,$brandMailWinnerTxt,$brandMailWinnerSubject,$winnerInfoBody;
	
	/**** Send a facebook notification to winner *****/
	sendWinnerNotification($winner['_fbuserID'], $winnerNotiNoEmailTxt);
	
	/**** Send mail to brand manager about winner *****/
	$winnerInfoBody = createWinnerInfoBody($winner,$brandMailWinnerTxt);
	sendMailToBrand($brandMailWinnerSubject,$winnerInfoBody);			// send email to owner
}

function sendStatEmail(){
	require 'admin/get_stat.php';
	global $base,$statMailGA,$statMailUserList,$statMailSubscriberList,$statMailSubject,$appSecret;
	
	$userListUrl = $base.'bin/admin/get_users_xls.php?secret='.$appSecret;
	$subscriberListURL = $base.'bin/admin/get_subscribers_list.php?secret='.$appSecret;	
	
	$downloadUserList = '<p><a href="'.$userListUrl.'">'.$statMailUserList.'</a></p>';
	$downloadSubscriberList = '<p><a href="'.$subscriberListURL.'">'.$statMailSubscriberList.'</a></p>';
	$body = $statList.$downloadUserList.$downloadSubscriberList.'<p>'.$statMailGA.'</p>';
	
	// echo $body;
	sendMailToBrand($statMailSubject, $body);
}

function sendMailToBrand($subject, $body){
	global $appName,$brandEmail,$brandEmail;
	
	sendMail($appName,$brandEmail,$brandEmail,$subject,$body);
}

function createWinnerInfoBody($winner, $msg){
	global $notProvidedTxt,$winnerWillContactTxt;
	
	$email = $winner['_fbuserEmail'] ? $winner['_fbuserEmail'] : $notProvidedTxt;
	$vars = array(
		"raffle_id" => $winner['_gameId'],
		"name" => $winner['_fbuserName'],
		"email" => $email,
		"fb_id" => $winner['_fbuserID'],
		"fb_url" => "https://www.facebook.com/".$winner['_fbuserID']
	);
	
	if($email == $notProvidedTxt)
		$msg .= $winnerWillContactTxt;
	
	return customizeTxt($msg,$vars);	
}

function sendWinnerNotification($fbid, $msg){
	if(isset($fbid)){
	
		require_once '../fb/facebook.php';
		
		if(defined('_NOTI_TEST_ID'))
			$fbid = _NOTI_TEST_ID;

		/**** Send notification *****/
		$facebook = new Facebook( array( 'appId' => $GLOBALS['appId'], 'secret' => $GLOBALS['appSecret'] ) );
			
		try {
			$facebook->api("/$fbid/notifications", 'post', array(
				'access_token'	=> $facebook->getAppId() . '|' . $facebook->getApiSecret(),
				'href'			=> '',
				'template'		=> $msg
				)
			);
		}
		catch(FacebookApiException $e) {
			if(defined('_ERROR_REPORT'))
				var_dump($e);
		}

	}
}

/**** Restore working dir *****/
chdir($realWorkingDir);

?>