<?php 

	header('Content-Type: text/html; charset=utf-8');

	/**** Direct URL call hack defense *****/
	define('TOKEN','abc123');

	if($_GET['token'] != TOKEN)
		exit('Invalid token!');	

	/**** Include libraries *****/
	require_once '../lib/db.class.php';
	require_once '../fb/facebook.php';

	/**** Facebook user id *****/
	$userIds = array('100003230858769', '100002591327599');	// modify to App specific Facebook ID
	$msg = 'Here comes the message';

	/**** Send notification *****/
	$facebook = new Facebook( array( 'appId' => $appId, 'secret' => $appSecret ) );

	foreach($userIds as $id){
		$error = false;
		
		try {
			$facebook->api("/$id/notifications", 'post', array(
				'access_token'	=> $facebook->getAppId() . '|' . $facebook->getApiSecret(),
				'href'			=> '',
				'template'		=> $msg
				)
			);
		}
		catch(FacebookApiException $e) {
			$error = true;
			var_dump($e);
		}

		if(!$error)
			echo 'Notification successfully sent!';
	}


?>