<?php
	
	/**** Show only errors  *****/
	error_reporting(E_ERROR);
	ini_set( "display_errors", 1);

	require_once '../lib/db.class.php';

	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** MySQL Inject defense *****/
	$db->escapeArray($_POST);

	/**** URL Hack defense *****/
	if($db->checkReferer()){

		/**** Set values *****/	
		$fbid = $_POST['fbid'];
		$name = $_POST['name'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$gender = $_POST['gender'];
		$locale = $_POST['locale'];
		$liked = $_POST['liked'];
		$newlike = $_POST['newlike'];
		$newinstall = $_POST['newinstall'];
		$mobile = $_POST['mobile'];
		$comment = '';
		$getDefaultRaffle = 0;
		$status = 'ok';
		$extraTickets = stripslashes($_POST['extratickets']);	
		$extraTickets = str_replace('\"', '"', $extraTickets);
		$extraTickets = json_decode($extraTickets,true);	
		
		$shared = $extraTickets['share']['used'];
		$shareEnabled = $extraTickets['share']['status'];

		$invitedCnt = $extraTickets['invite']['invited_cnt'];
		$inviteEnabled = $extraTickets['invite']['status'];		
		$inviteLimit = $extraTickets['invite']['limit'];		
		
		$youtubeSubscribe = $extraTickets['youtube-subscribe']['used'];
		$youtubeSubscribeEnabled = $extraTickets['youtube-subscribe']['status'];
		
		$googlePlus = $extraTickets['google-plus']['used'];
		$googlePlusEnabled = $extraTickets['google-plus']['status'];

		$twitterShare = $extraTickets['twitter-share']['used'];
		$twitterShareEnabled = $extraTickets['twitter-share']['status'];
		
		$twitterFollow = $extraTickets['twitter-follow']['used'];
		$twitterFollowEnabled = $extraTickets['twitter-follow']['status'];	
		
		$linkedinShare = $extraTickets['linkedin-share']['used'];
		$linkedinShareEnabled = $extraTickets['linkedin-share']['status'];		
		
		$subscribe = $extraTickets['subscribe']['used'];
		$subscribeEnabled = $extraTickets['subscribe']['status'];		
		
		if($newlike == 1)	// if like was before the app install
			$liked = 0;		
			
		/**** Check user *****/
		$sql = $db->query("SELECT id, _getNews, _invitedCnt, _shared, _youtubeSubscribed, _twitterShared, _twitterFollowed, _googlePlused, _linkedinShared FROM {}_raffle_users WHERE _fbuserID = $fbid");
		$userInfo = $db->fetchAssoc($sql);
		$userId = $userInfo['id'];
		$saveRaffles = false;	
		
		/**** New user *****/
		if(empty($userId)) {
		
			// save user
			$sql = "INSERT INTO {}_raffle_users (_fbuserID, _fbuserName, _fbuserFirstName, _fbuserLastName, _fbuserEmail, _fbuserGender, _fbuserLocale, _getNews, _invitedCnt, _shared, _youtubeSubscribed, _twitterShared, _twitterFollowed, _googlePlused, _linkedinShared, _isFan, _newFan, _mobileVersion, _comment ) VALUES ($fbid, '$name', '$firstname', '$lastname', '$email', '$gender', '$locale', $subscribe, $invitedCnt, $shared, $youtubeSubscribe, $twitterShare, $twitterFollow, $googlePlus, $linkedinShare, $liked, $newlike, $mobile, '$comment' )";
			
			$result = $db->query($sql);
			
			$userIdSql = $db->query("SELECT MAX(id) FROM {}_raffle_users");
			$userId = $db->fetchSingle($userIdSql);
			
			$getDefaultRaffle = $initTicketCnt;
			$saveRaffles = true;
		}
		
		/**** User is registered *****/
		else{
		
			// check user's raffles
			$sql = $db->query("SELECT COUNT(g.id) FROM {}_raffle_games g INNER JOIN {}_raffle_users u ON u.id = g._userId WHERE u._fbuserID = $fbid");
			
			$userRaffleCnt = $db->fetchSingle($sql);
			
			// user has raffles and finished app install in this session
			if($userRaffleCnt > 0 && $newinstall){
				$sql = $db->query("UPDATE {}_raffle_users SET _comment = 'reinstall' WHERE _fbuserID = $fbid");			
				$status = 'reinstall'; 
			}
			
			//user can play
			else{
				$saveRaffles = true;
				if($userRaffleCnt > 0)
					$getDefaultRaffle = 0;
				else
					$getDefaultRaffle = $initTicketCnt;

				$invitedCnt = $inviteEnabled && $userInfo['_invitedCnt'] < $inviteLimit && $invitedCnt >= $inviteLimit ? $invitedCnt : 0;
				$shared = $shareEnabled && $userInfo['_shared'] != 1 && $shared == 1 ? 1 : 0;
				$youtubeSubscribe = $youtubeSubscribeEnabled && $userInfo['_youtubeSubscribed'] != 1 && $youtubeSubscribe == 1 ? 1 : 0;
				$googlePlus = $googlePlusEnabled && $userInfo['_googlePlused'] != 1 && $googlePlus == 1 ? 1 : 0;
				$twitterShare = $twitterShareEnabled && $userInfo['_twitterShared'] != 1 && $twitterShare == 1 ? 1 : 0;
				$twitterFollow = $twitterFollowEnabled && $userInfo['_twitterFollowed'] != 1 && $twitterFollow == 1 ? 1 : 0;
				$linkedinShare = $linkedinShareEnabled && $userInfo['_linkedinShared'] != 1 && $linkedinShare == 1 ? 1 : 0;
				$subscribe = $subscribeEnabled && $userInfo['_getNews'] != 1 && $subscribe == 1 ? 1 : 0;
				
				$updateList = array();
				if($invitedCnt)
					array_push($updateList, "_invitedCnt = $invitedCnt");
				if($shared)
					array_push($updateList, "_shared = $shared");
				if($youtubeSubscribe)
					array_push($updateList, "_youtubeSubscribed = $youtubeSubscribe");			
				if($googlePlus)
					array_push($updateList, "_googlePlused = $googlePlus");				
				if($twitterShare)
					array_push($updateList, "_twitterShared = $twitterShare");					
				if($twitterFollow)
					array_push($updateList, "_twitterFollowed = $twitterFollow");					
				if($linkedinShare)
					array_push($updateList, "_linkedinShared = $linkedinShare");				
				if($subscribe)
					array_push($updateList, "_getNews = $subscribe");				

					
				$updateSql =  implode(",", $updateList);
					
				// update user table
				if(!empty($updateSql))
					$sql = $db->query("UPDATE {}_raffle_users SET $updateSql WHERE _fbuserID = $fbid");
			}
			
		}
		
		/**** Save raffles *****/
		if($saveRaffles){			
			$raffleCntSave = $getDefaultRaffle + $subscribe + $youtubeSubscribe + $googlePlus + $twitterFollow + $twitterShare + $linkedinShare + $shared;
			if($inviteEnabled && $invitedCnt >= $inviteLimit)
				++$raffleCntSave;
							
			if($raffleCntSave>0){
			$sql = $db->query("SELECT MAX(_gameId) FROM {}_raffle_games");
			$startId = $db->fetchSingle($sql);
			
			for ($i = ++$startId; $i < $startId+$raffleCntSave; $i++) {
				$sql = "INSERT INTO {}_raffle_games (_userId, _gameId) VALUES ($userId, $i)";	
				$db->query($sql);		
			}
		}
		}
		
		/**** Get all raffle *****/
		$sql = $db->query("SELECT g._gameId FROM {}_raffle_games g INNER JOIN {}_raffle_users u ON u.id = g._userId WHERE u._fbuserID = $fbid ORDER BY g._gameId ASC");
		$userRaffles = $db->fetchAllSingle($sql);

 		echo json_encode(array(
			"status" => $status,
			"raffles" => implode(",", $userRaffles),
			"raffle_cnt" => count($userRaffles),
			"start_id" => $startId
		)); 
		
	}
	else{

		echo 'Cheater';
		
	}

	$db->closeConnection();
	
?>