<?php
	
	/**** Show only errors  *****/
	error_reporting(E_ERROR);
	ini_set( "display_errors", 1);
	
	require_once '../lib/db.class.php';

	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** MySQL Inject defense *****/
	$db->escapeArray($_POST);

	/**** URL Hack defense *****/
	if($db->checkReferer()){

		/**** Set values *****/	
		$fbid = $_POST['fbid'];

		/**** Get all raffle *****/
		$sql = $db->query("SELECT g._gameId FROM {}_raffle_games g INNER JOIN {}_raffle_users u ON u.id = g._userId WHERE u._fbuserID = $fbid ORDER BY g._gameId ASC");
		$userRaffles = $db->fetchAllSingle($sql);
		
		/**** Get user extra raffle options *****/
		$sql = $db->query("SELECT * FROM {}_raffle_users WHERE _fbuserID = $fbid");
		$extraChanceUsed = $db->fetchAssoc($sql);
		$invited = $extraChanceUsed['_invitedCnt'] >= $extraTickets['invite']['limit'] ? 1 : 0;
		
		echo json_encode(array(
			"raffles" => implode(",", $userRaffles), 
			"raffle_cnt" => count($userRaffles),
			"extra_chances_used" => array(
				"invite" => $invited,
				"share" => $extraChanceUsed['_shared'],
				"youtube-subscribe" => $extraChanceUsed['_youtubeSubscribed'],
				"google-plus" => $extraChanceUsed['_googlePlused'],
				"twitter-share" => $extraChanceUsed['_twitterShared'],
				"twitter-follow" => $extraChanceUsed['_twitterFollowed'],
				"linkedin-share" => $extraChanceUsed['_linkedinShared'],
				"subscribe" => $extraChanceUsed['_getNews'],
			)
		));
	}
	else{

		echo 'Cheater';
		
	}

	$db->closeConnection();
	
?>