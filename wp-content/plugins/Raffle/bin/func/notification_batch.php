<?php 

	header('Content-Type: text/html; charset=utf-8');

	/**** Direct URL call hack defense *****/
	define('TOKEN','abc123');

	if($_GET['token'] != TOKEN)
		exit('Invalid token!');	

	/**** Variables *****/
	define("DEBUG", true);		//	If debug true, script send test message
	$debugIds = array("100003230858769", "100002591327599");	// modify to App specific Facebook ID
	$msg = "Here comes the message";	// notification message

	/**** Include libraries *****/
	require_once '../lib/db.class.php';
	require_once '../fb/facebook.php';

	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** Get all user *****/
	$sql = $db->query("SELECT _fbuserID FROM users");
	$result = $db->fetchAll($sql);

	$db->closeConnection();

	/**** Send notification *****/
	$facebook = new Facebook( array( 'appId' => $appId, 'secret' => $appSecret ) );
	$batch_array = array();

	$i = 0;
	$result_length = count($result);

	if(DEBUG){
		foreach($debugIds as $item){
				echo '<pre>';
					var_dump($batch_array);
				echo '</pre>';	
		
			$fbid = $item;
			$batch_array[] = array('method' => 'POST', 'relative_url' => "/$fbid/notifications", "body" =>  http_build_query(array('template' => $msg, "href" =>  ""))); 
			$response = $facebook->batch($batch_array);	
			var_dump($response);	
		}
	}
	else{
		foreach($result as $item){
				
			$fbid = $item[0];
			
			// build notification row
			$batch_array[] = array('method' => 'POST', 'relative_url' => "/$fbid/notifications", "body" =>  http_build_query(array('template' => $msg, "href" =>  ""))); 
			
			if(count($batch_array)>=50 || $i == $result_length - 1){	// reach the maximum length send notifications and clear array
				$response = $facebook->batch($batch_array);	
				
				echo '<pre>';
					var_dump($batch_array);
				echo '</pre>';
				
				var_dump($response);	
				
				// clear array
				unset($batch_array);
				$batch_array = array();
			}
			
			$i++;
		}
	}




?>