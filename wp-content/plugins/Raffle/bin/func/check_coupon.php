<?php

	require_once '../lib/db.class.php';

	/**** Config database *****/	
	$db = new db($db_config);
	$db->openConnection();

	/**** MySQL Inject defense *****/
	$db->escapeArray($_POST);

	/**** URL Hack defense *****/
	if($db->checkReferer()){
	
		$result = 'Valid';

		/**** Set values *****/	
		$code = $_POST['code'];
		
		/**** Check *****/
		$sql = $db->query("SELECT * FROM {}_raffle_coupons WHERE _code = '$code'");
		$row = $db->fetchAssoc($sql);
		
		if(empty($row))
			$result = 'Invalid';
		else if(!$row['_active']){
			$date = explode(' ',$row['_usedDate']);
			$time = strtotime($date[0]);
			$result = date('d/m/Y',$time);
		}
		else
			$sql = $db->query("UPDATE {}_raffle_coupons SET _active = 0, _usedDate=now() WHERE _code = '$code'");
			
		echo $result; 
	}
	else{

		echo 'Cheater';
		
	}

	$db->closeConnection();
		
?>