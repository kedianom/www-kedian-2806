<?php
		
	defined('_FAK') or die;	

	if($is_localhost){
		$is_fan = true;
		$is_tab = true;
		$is_admin = true;
		$app_data_params = 0;
		$facebook_frame = false;
	}
	else{
		require 'fb/facebook2.php';

		$facebook = new Facebook($config);

		$is_fan = $likeGate != 'disabled' ? true : false;
		$is_tab = false;
		$is_admin = false;
		$facebook_frame = true;
		
		if(isset($_REQUEST['signed_request'])) {
			$signed_request = Facebook2::parse_signed_request($_REQUEST['signed_request'], $config['secret']);
			if(!$signed_request)
				die('failed to parse signed request');
			
			if(isset($signed_request['page'])){
				$is_tab = true;
				
				if($signed_request['page']['admin'] )
					$is_admin = true;
					
				if(isset($signed_request['app_data'])){
					$app_data = $signed_request['app_data'];
					if(!empty($app_data)){
						parse_str(
							preg_replace(
								array('/\$/','/\~/'), 
								array('=','&'), 
								$app_data
							), 
							$app_data_params
						);
					}
				}
			}
		}
		
		if(defined('_ADMIN'))
			$is_admin = true;		
	}
	
	// Force admin for website platform or testing
	if(isset($_GET['secret']) && $_GET['secret'] == $appSecret)
		$is_admin = true;

	if(empty($_REQUEST['signed_request'])){		// call directly the canvas URL
		$is_tab = true;
		$facebook_frame = false;		
		$app_data_params = 0;
	}
	
?>