<?php 


// Define some-some
define ( 'DS', DIRECTORY_SEPARATOR );

// Get DB Class
require_once (__DIR__.DS.'..'.DS.'db_config.php');


$db_config = new db_config($database['host'], $database['user'], $database['psw'], $database['db'], $database['prefix'], $database['type']); 


/**** Database class *****/
class db
{
	private $connection;
	private $selectdb;
	private $lastQuery;
	private $lastQueryString;
	private $config;
	private $errorLog;

	function __construct($config)
	{
		$this->config = $config;
	}
	
	function __destruct()
	{
		
	}

	public function openConnection()
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				$this->connection = mysql_connect($this->config->hostname, $this->config->username, $this->config->password);
				if(!$this->connection)
					throw new Exception(mysql_error());	
			 	else{	
				mysql_set_charset("utf8",$this->connection);
					$this->selectdb = mysql_select_db($this->config->database);
					if(!$this->selectdb)
						throw new Exception(mysql_error());
					return true;					
				}
			}
			elseif($this->config->connector == "mysqli")
			{
				$this->connection = mysqli_connect($this->config->hostname, $this->config->username, $this->config->password);
				if(!$this->connection)
					throw new Exception(mysqli_connect_error());	
			 	else{					
				$this->connection->set_charset("utf8");
					$this->selectdb = mysqli_select_db($this->connection, $this->config->database);
					if(!$this->selectdb)
						throw new Exception(mysqli_error($this->connection));
					return true;
				}
			}
		}
		catch(exception $e)
		{	
			$this->handleError($e);
		}
	}

	public function closeConnection()
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				mysql_close($this->connection);
			}
			elseif($this->config->connector == "mysqli")
			{
				mysqli_close($this->connection);
			}
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}
	
	public function escapeString($string)
	{
		if($this->config->connector == "mysql")
		{
			return mysql_real_escape_string($string,$this->connection);
		}
		elseif($this->config->connector == "mysqli")
		{
			return mysqli_real_escape_string($this->connection,$string);
		}	
	}

	/* doesn't work properly
	public function lastId() {
		return $this->connection->insert_id;
	}
	*/

	public function query($query,$print_error = true)
	{
		$query = str_replace("}", "", $query);
		$query = str_replace("{", $this->config->prefix, $query);
		$this->lastQueryString = $query;
	
		try
		{
			if(empty($this->connection))
			{
				$this->openConnection();
			}	
			if($this->config->connector == "mysql")
			{
				$this->lastQuery = mysql_query($query);
			}
			elseif($this->config->connector == "mysqli")
			{
				$this->lastQuery = mysqli_query($this->connection,$query);
			}
			
			if($print_error)
				$this->handleError();
			
			return $this->lastQuery;
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}

	public function lastQuery()
	{
		return $this->lastQuery;
	}
	
	public function handleError(exception $e = NULL)
	{
		$error = '';
		
		if(empty($e)){
			
		if($this->config->connector == "mysql")
		{
			$error = mysql_error($this->connection);
		}
		elseif($this->config->connector == "mysqli")
		{
			$error = mysqli_error($this->connection);
		}
		}
		else{
			$error = $e->getMessage();
		}

		if(!empty($error)){
			if($this->config->show_error){
			printf("MySQL Error: %s\n", $error);
				printf("Query: %s\n", $this->lastQueryString);
			}
			
			$this->errorLog = $error;
		}
			
	}	

	public function pingServer()
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				if(!mysql_ping($this->connection))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			elseif($this->config->connector == "mysqli")
			{
				if(!mysqli_ping($this->connection))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}
	
	public function tableExists($table)
	{
		return $this->query("SELECT 1 FROM $table",false) !== FALSE;
	}
	
	public function hasRows($result)
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				if(mysql_num_rows($result)>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			elseif($this->config->connector == "mysqli")
			{
				if(mysqli_num_rows($result)>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}
	
	public function countRows($result)
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				return mysql_num_rows($result);
			}
			elseif($this->config->connector == "mysqli")
			{
				return mysqli_num_rows($result);
			}
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}
	
	public function fetchAssoc($result)
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				return mysql_fetch_assoc($result);
			}
			elseif($this->config->connector == "mysqli")
			{
				return mysqli_fetch_assoc($result);
			}
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}
	
	public function fetchSingle($result)
	{
		$row = $this->fetchArray($result);
		return $row[0] ? $row[0] : 0;
	}	
	
	public function fetchArray($result)
	{
		try
		{
			if($this->config->connector == "mysql")
			{
				return mysql_fetch_array($result);
			}
			elseif($this->config->connector == "mysqli")
			{
				return mysqli_fetch_array($result);
			}
		}
		catch(exception $e)
		{
			$this->handleError($e);
		}
	}

	public function fetchAll($result)
	{
		$array = array();
		while($row = $this->fetchArray($result))
			$array[] = $row;
		
		return $array;
	}	
	
	public function fetchAllSingle($result)
	{
		$array = array();
		while($row = $this->fetchArray($result))
			$array[] = $row[0];
		
		return $array;
	}	
	
	public function fetchAllAssoc($result)
	{
		$assocArray = array();
		while($row = $this->fetchAssoc($result))
			$assocArray[] = $row;
		
		return $assocArray;
	}		
	
	public function escapeArray(&$array){
		foreach($array as &$item)
			$item = strip_tags($this->escapeString($item));
	}
	
	public function checkReferer(){
		$urldata = parse_url($_SERVER['HTTP_REFERER']);
		return isset( $_SERVER['HTTP_REFERER']) && $urldata['host'] == $_SERVER['HTTP_HOST'];
	}		
	
	public function showErrors(){
		$this->config->show_error = true;
	}	
	
	public function hideErrors(){
		$this->config->show_error = false;
	}	
	
	public function getErrorLog(){
		return $this->errorLog;
	}		
}

class db_config
{
	public $hostname;
	public $username;
	public $password;
	public $database;
	public $prefix;
	public $connector;
	public $show_error;
	
	function __construct($hostname = NULL, $username = NULL, $password = NULL, $database = NULL, $prefix = NULL, $connector = NULL, $show_error = NULL)
	{
		$this->hostname = !empty($hostname) ? $hostname : "";
		$this->username = !empty($username) ? $username : "";
		$this->password = !empty($password) ? $password : "";
		$this->database = !empty($database) ? $database : "";
		$this->prefix = !empty($prefix) ? $prefix : "";
		$this->connector = !empty($connector) ? $connector : "mysqli";
		$this->show_error = $show_error === false ? false : true;
	}
	
	function __destruct()
	{
		
	}
}

?>