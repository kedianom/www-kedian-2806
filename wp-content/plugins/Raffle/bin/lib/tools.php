<?php
	
	defined('_FAK') or die;	
	
	$fbHeadPrefix = 'og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# game: http://ogp.me/ns/game#';
	
	setIECompatibleMode();

	function initTemplate(){
		createFBRoot();
		defineJSParams();
		createAnalytics($GLOBALS['analytics']);
		redirectApp();
	}
	
	function getVersion(){
		return file_get_contents('version.dat');
	}
	
	function getResource($path){
		global $appVersion;
		return $path.'?v='.$appVersion;
	}
	
	function setViewPort(){
		if($GLOBALS['isMobileURL'])
			echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">'."\n";
	}
	
	function setIECompatibleMode(){
		header('X-UA-Compatible: IE=edge,chrome=1');
	}
	
	function includeAdditionalScripts(){
		includeAdminScript();
		includeCustomizerScript();
		includeSocialScripts();
	}

	function includeAdditionalStyles(){
		includeAdminStyles();
		includeColorStyles();
		includeCustomizerStyles();
		renderInlineStyles();
	}
	
	function includeSocialScripts(){
		global $extraTickets;		
		
		foreach($extraTickets as $key => $value){
			if($value['status'] > 0){
				switch($key){
					case 'youtube-subscribe': 
						echo '<script src="https://apis.google.com/js/platform.js"></script>';break;					
					case 'google-plus': 
						echo '<script src="https://apis.google.com/js/platform.js" async defer>{parsetags: "explicit"}</script>';break;
					case 'twitter-share': 
					case 'twitter-follow': 
						echo '<script>window.twttr=function(t,e,r){var n,i=t.getElementsByTagName(e)[0],w=window.twttr||{};return t.getElementById(r)?w:(n=t.createElement(e),n.id=r,n.src="https://platform.twitter.com/widgets.js",i.parentNode.insertBefore(n,i),w._e=[],w.ready=function(t){w._e.push(t)},w)}(document,"script","twitter-wjs");</script>';break;
					case 'linkedin-share':
						echo '<script src="//platform.linkedin.com/in.js?async=true" type="text/javascript"> lang: en_US</script>';break;					
				}
			}
		}
				
	}
	
	function includeCustomizerScript(){
		if(defined('_CUSTOMIZER'))
			echo '<script src="'.getResource('js/customizer.js').'"></script>';		
	}	
	
	function includeCustomizerStyles(){
		if(defined('_CUSTOMIZER'))
			echo '<link type="text/css" href="'.getResource('css/customizer.css').'" rel="stylesheet" />';
	}	
	
	function includeColorStyles(){
		$fontColor = $GLOBALS['fontColor'];
		$backgroundSkin = $GLOBALS['backgroundSkin'];
		$base = $GLOBALS['base'];
		
		$css = file_get_contents('css/colors.css');
		$css = str_replace('[COLOR-1]','#'.$fontColor,$css);
		$css = str_replace('[BG-SKIN]',$backgroundSkin,$css);
		$css = str_replace('[BASE]',$base,$css);
		
		addInlineStyle($css);
	}
	
	function addInlineStyle($style){
		global $additionalInlineStyles;
		$additionalInlineStyles .= $style."\n";
	}
	
	function renderInlineStyles(){
		global $additionalInlineStyles;
		
		echo "	
		<style>
			$additionalInlineStyles
		</style>";
	}
	
	function renderRules(){
		$rules = file_get_contents('rules.html');
		echo customizeRules($rules);
	}
	
	function customizeRules($rules){
		if(preg_match_all('/\{(.*?)\}/',$rules,$match)) {            
			foreach($match[1] as $item){
				$rules = str_replace('{'.$item.'}',$GLOBALS[$item],$rules);
			}          
		}
		return $rules;
	}
	
	function customizeTxt($txt,$arr){
		if(preg_match_all('/\{(.*?)\}/',$txt,$match)) {            
			foreach($match[1] as $item){
				$txt = str_replace('{'.$item.'}',$arr[$item],$txt);
			}          
		}
		return $txt;
	}
	
	function renderCustomizer(){
		if(defined('_CUSTOMIZER')){
		?>
			<div id="show-customizer" class="button button-round"><span></span></div>
			<div id="customizer">
				<div class="panel">
					<h4>Color<br />selection</h4>
					<h5>Background</h5>
					<div class="custom-bgs-con">
						<span data-color="red" class="background-preview active red"><em></em></span>
						<span data-color="green" class="background-preview green"><em></em></span>
						<span data-color="blue" class="background-preview blue"><em></em></span>
					</div>
					<h5>Text</h5>
					<input class="color-picker" type="text">
					<a href="#" target="_blank" id="mini-buy-game-btn" class="button button-brick buy-btn">Buy</a>
				</div>
				<div id="customizer-icon"></div>
			</div>	
			<div id="demo-features">
				<p>Demo features</p>
				<div id="restart-btn" class="button button-brick page-control">Restart game</div>	
				<div id="demo-winner-btn" class="button button-brick page-control">Demo winner</div>	
				<a href="#" target="_blank" id="buy-game-btn" class="button button-brick buy-btn">Buy Now</a>
			</div>
		<?php		
		}
	}
	
	function createFBRoot(){
		echo '<div id="fb-root"></div>';	
	}
	
	function defineJSParams(){
		$localFbId = 10000;
		$localFbName = 'Tester Tom';
		if(defined('_RANDOM_USER')){
			$localFbId = rand(10000,90000);
			$localFbName = 'Tester Tom '.rand(10000,90000);
		}
		
		$is_localhost = $GLOBALS['is_localhost'];
		$fbid = $is_localhost ? $localFbId : 0;
		$name = $is_localhost ? $localFbName : '';
		$firstname = $is_localhost ? 'Tom' : '';
		$lastname = $is_localhost ? 'Tester' : '';
		$email = $is_localhost ? 'tom@tester.com' : '';
		$gender = $is_localhost ? 'male' : '';
		$locale = $is_localhost ? 'en_US' : '';
		$page = json_encode($GLOBALS['page']);
		$texts = json_encode($GLOBALS['texts']);
		$extraTickets = json_encode($GLOBALS['extraTickets']);
		$appId = $GLOBALS['appId'];
		$appLike = $GLOBALS['appLike'];
		$multiForceInstall = $GLOBALS['multiForceInstall'];
		$initTicketCnt = $GLOBALS['initTicketCnt'];
		$maxTicketCnt = $GLOBALS['maxTicketCnt'];
		$inviteLimit = $GLOBALS['inviteLimit'];
		$is_fan = $GLOBALS['is_fan'] ? 1 : 0;
		$app_data_params = isset($GLOBALS['app_data_params']) ? json_encode($GLOBALS['app_data_params']) : 0;
		$app_scope = $GLOBALS['appScope'];
		$base = $GLOBALS['base'];
		$facebook_frame = $GLOBALS['facebook_frame'] ? 1 : 0;
		$gameState = $GLOBALS['gameState'];
		$muteSounds = defined('_MUTE_SOUNDS') || $GLOBALS['muteSounds']  ? 1 : 0;
		$localAppInstalled = defined('_LOCAL_APP_INSTALLED') ? 1 : 0;
		$isMobileURL = $GLOBALS['isMobileURL'] ? 1 : 0;
		$teaserMode = $GLOBALS['gameState'] == 'pre' ? 1 : 0;
		$isDemo = defined('_CUSTOMIZER') ? 1 : 0;
		$isTestNav = defined('_TEST_NAV') ? 1 : 0;
		$onlyCoupon = $GLOBALS['onlyCoupon'] ? 1 : 0;
		$gamePlatform = $GLOBALS['gamePlatform'];
		$likeUrl = $GLOBALS['likeUrl'];
		$likeGate = $GLOBALS['likeGate'];
		
		echo
		
		"<script>
			FB_ID = $fbid;
			FB_NAME = \"$name\";
			FB_FIRSTNAME = \"$firstname\";
			FB_LASTNAME = \"$lastname\";
			FB_EMAIL = \"$email\";
			FB_GENDER = \"$gender\";
			FB_LOCALE = \"$locale\";
			FB_INIT = false;
			LOCALHOST = $is_localhost;
			PAGE = $page;
			TEXTS = $texts;
			APP_ID = \"$appId\";
			IS_LIKED = $is_fan;
			NEW_LIKE = 0;
			APP_LIKE = \"$appLike\";
			APP_DATA = $app_data_params;
			APP_SCOPE = \"$app_scope\";
			APP_LOGGED_IN = 0;
			APP_INSTALLED = 0;
			APP_INSTALL_CHECKED = 0;
			NEW_INSTALL = 0;
			FORCE_INSTALL_ENABLED = 1;
			FORCE_INSTALLED = 0;
			MULTI_FORCE_INSTALL = $multiForceInstall;
			BASE_URL = \"$base\";
			FACEBOOK_FRAME = $facebook_frame;
			EXTRA_TICKETS = $extraTickets;
			INIT_TICKET_CNT = $initTicketCnt;
			MAX_TICKET_CNT = $maxTicketCnt;
			INVITE_LIMIT = $inviteLimit;
			GAME_STATE = \"$gameState\";
			MUTE_SOUNDS = $muteSounds;
			LOCAL_APP_INSTALLED = $localAppInstalled;
			IS_MOBILE_URL = $isMobileURL;
			IS_TEASER = $teaserMode;
			IS_DEMO = $isDemo;
			IS_TEST_NAV = $isTestNav;
			ONLY_COUPON = $onlyCoupon;
			GAME_PLATFORM = \"$gamePlatform\";
			LIKE_URL = \"$likeUrl\";
			LIKE_GATE = \"$likeGate\";
		</script>";
	}
	
	function renderAfterGameCallToAction(){
		echo '<div class="highlight-box">';
		
		if(!$GLOBALS['likeGate'])
			renderLikeModule();
		else
			renderCheckPostsModule();
		renderGetMoreRaffle();
			
		echo '</div>';
	}
	
	function renderGetMoreRaffle(){
		global $incChanceTxt,$getMoreRaffleTxt;
		?>		
			<div class="getmoreraffle-module">
				<h4><?php echo $incChanceTxt;?></h4>
				<div id="btn-getmore" class="inverse-color button button-brick page-control text_dir" data-target="game"><?php echo $getMoreRaffleTxt;?></div>
			</div>
		<?php
	}
	
	function renderCheckPostsModule(){
		global $whileWaitTxt,$brandUrl,$checkPostsTxt,$didntWinTxt,$gameState;
		$hl = $gameState == 'winner' ? $didntWinTxt : $whileWaitTxt;
		?>
			<div class="checkpost-module">
				<h4><?php echo $hl;?></h4>
				<a href="<?php echo $brandUrl;?>" target="_blank" class="inverse-color button button-brick"><?php echo $checkPostsTxt;?></a>
			</div>
		<?php
	}
	
	function createLikeButton($url){
		return 
		
		'<div class="fb-like" data-href="'.$url.'" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
		';
	}
	
	function createLikeBox($url){
		return 
		'<div class="fb-like-box" data-href="'.$url.'" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false" data-width="300"></div>';
	}	
	
	function createLikeBoxIframe($url){
		return 
		'<iframe src="//www.facebook.com/plugins/likebox.php?href='.$url.'&amp;width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:62px;" allowTransparency="true"></iframe>';
	}	
	
	function renderLikeModule(){
	?>
		
		<div class="like-box">
			<h4 class="text_dir"><?php echo $GLOBALS['likeOurPageTxt'];?></h4>
			<p class="text_dir"><?php echo $GLOBALS['likeInfoTxt']?></p>
			<?php echo createLikeButton($GLOBALS['likeUrl']); ?>	
		</div>	
	<?php		
	}

	function renderToolTipDuration(){
		global $startTxt,$startDateTxt,$startTimeTxt,$finishTxt,$endDateTxt,$endTimeTxt;
		?>
			<table class='duration-table'>	
				<tr>
					<td><em><?php echo $startTxt;?>:</em></td>
					<td><strong><?php echo $startDateTxt.' '.$startTimeTxt;?></strong></td>
				</tr>
				<tr>
					<td><em><?php echo $finishTxt;?>:</em></td>
					<td><strong><?php echo $endDateTxt.' '.$endTimeTxt;?></strong></td>
				</tr>
			</table>
		<?php		
	}
	
	function renderExtraTickets(){
		global $extraTicketsEnabled,$extraChanceTxt,$extraTickets,$maxTicketCnt,$initTicketCnt;
		$scroll = $maxTicketCnt - $initTicketCnt > 4 ? ' custom-scroll-mini' : '';
		
		if($extraTicketsEnabled){
			?>
			<div class="extra-chance">
				<h3 class="text_dir"><?php echo $extraChanceTxt;?></h3>
				<div class="extra-chance-btns<?php echo $scroll;?>">
				<?php
					foreach($extraTickets as $key => $value)
						if($value['status'] > 0)
							renderExtraTicketButton($key,$value['btn-plugin']);
				?>
				</div>
			</div>
			<?php
		}
	}
	
	function renderTicketCounter(){
		global $maxTicketCnt;
		
		for ($i = 0; $i <= $maxTicketCnt; $i++) {
			echo '<div class="raffle-cnt-nmb">'.$i.'</div>';
		}
	}
	
	function renderTickets(){
		global $maxTicketCnt;
		
		for ($i = 0; $i <= $maxTicketCnt+1; $i++) {
			echo '<div class="raffle"></div>';
		}		
	}
	
	function renderExtraTicketButton($btn,$plugin){
		global $buttonLoadingTxt;
		$txt = getButtonText($btn);		
		$buttonText = $plugin ? $buttonLoadingTxt : $txt;
		$textAfterLoading = $txt;
		$pluginClass = $plugin ? 'plugin-btn' : 'no-plugin-btn';
		echo '<div id="'.$btn.'" class="button button-brick extra-chance-btn text_dir '.$pluginClass.'"><span class="button-txt" data-text="'.$textAfterLoading.'">'.$buttonText.'</span></div>';
	}
	
	function getButtonText($btn){
		$btn = explode('-', $btn);
		$btn = array_map('ucfirst', $btn);
		$btn = lcfirst(implode($btn));
		return eval('global $'. $btn . 'BtnTxt; return $'. $btn . 'BtnTxt;');
	}

	function redirectApp(){			
		if(isset($_GET['redirect'])) { 	// eg: www-index-hu~valami
			$redirectURI = str_replace("-",".",$_GET['redirect']);
			$redirectURI = "https://".str_replace("~","/",$redirectURI);		
		?>
		
		<script>
			top.location.href='<?php echo $redirectURI; ?>';
		</script>
		<?php
		}
		else if(!$GLOBALS['is_tab']) { ?>
		
		<script>
			top.location.href='<?php echo $GLOBALS['page']['tab']; ?>';
		</script>
		<?php }
		
		else if(!empty($GLOBALS['adminRedirect'])) { ?>
		
		<script>
			top.location.href='<?php echo $GLOBALS['adminRedirect']; ?>';
		</script>
		<?php }
	}
	
	function createFacebookOpenGraphTags(){
		$page = $GLOBALS['page'];
		$config = $GLOBALS['config'];
		$base = $GLOBALS['base'];
		
		return 
		
		'<meta property="fb:app_id"      content="'.$config['appId'].'" />
		<meta property="og:title"       content="'.$page['appFullName'].'" />
		<meta property="og:type"        content="'.$page['type'].'" />
		<meta property="og:url"         content="'.$base.'" />
		<meta property="og:image"       content="'.$page['logo'].'" />
		<meta property="og:description" content="'.$page['desc'].'" />'."\n";
	}
	
	function createAnalytics($analytics){
		$id = $analytics['id'];
		$domain = $analytics['domain'];
		$is_localhost = $GLOBALS['is_localhost'];
		$analyticsId = $GLOBALS['analyticsId'];
		$ga_disabled = $is_localhost || $analyticsId=='';
		$campaign = '';
		
		if(isset($GLOBALS['app_data_params'])){
			$app_data_params = $GLOBALS['app_data_params'];
			
			if($app_data_params && $app_data_params['campaignName']){ // ?app_data=campaignName$name~campaignSource$source~campaignMedium$medium
				$campaign = "
				ga('set', 'campaignSource', '".$app_data_params['campaignSource']."');
				ga('set', 'campaignMedium', '".$app_data_params['campaignMedium']."');
				ga('set', 'campaignName', '".$app_data_params['campaignName']."');";
			}		
		}
		
		$ga = $ga_disabled ? '' :
		"	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', '$id', '$domain');
			ga('send', 'pageview');	
			$campaign\n";
		
		$script =
		
		"\n<script>
		".$ga."
			function trackPage(page,title,url){
				var params = {'page': page,'title': title};
				console.log('Page tracked: ',params);".
				($ga_disabled ? "" : "ga('send', 'pageview', params)")
				."
				if(url) 
					setTimeout(function() {top.location.href=url;}, 200);
			}	

			function trackEvent(category,action,label,value){
				console.log('Event tracked: ',category+' / ',action+' / ',label+' / ',value);".
				($ga_disabled ? "" : "ga('send', 'event', category, action, label, value);")
				."				
			}		

		</script>";
		
		echo $script;
	}
	
	function file_get_contents_curl($url,$fields = null) {
		
		$fields_string = '';
		if(isset($fields)){	
			foreach($fields as $key=>$value)
				$fields_string .= $key.'='.$value.'&';
			rtrim($fields_string, '&');
		}
	
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		if($fields_string){
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);		
		}
	 
		$data = curl_exec($ch);
		curl_close($ch);
	 
		return $data;
	}	
	
	function sendMailLocal($sender_name,$sender_mail,$to_mail,$subject,$body){
		require_once 'class.phpmailer.php';

		if(!empty($sender_mail) && !empty($to_mail)){

			/**** Send mail *****/
			$mail = new PHPMailer(); 
			$mail->CharSet = "utf-8";
			$mail->SetFrom($sender_mail, $sender_name);
			$mail->AddAddress($to_mail);

			$mail->Subject = $subject;
			$mail->MsgHTML($body);
				
			$mail->Send();		
		}
	}
	
	function sendMail($sender_name,$sender_mail,$to_mail,$subject,$body){
		global $mailServer;
		
		if($mailServer == 'external'){
			$url = 'http://theme-lab.com/services/raffle/send_mail.php';
			$fields = array(
							'sender-name' => $sender_name,
							'sender-email' => $sender_mail, 
							'to-email' => $to_mail,
							'subject' => $subject,
							'body' => $body
						);
			$result = file_get_contents_curl($url, $fields);
		}
		else{
			sendMailLocal($sender_name,$sender_mail,$to_mail,$subject,$body);
		}
	}	
	
?>