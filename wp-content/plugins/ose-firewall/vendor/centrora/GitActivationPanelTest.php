<?php
require_once(dirname(__FILE__).'/BaseTest.php');
class GitActivationPanelTest extends BaseTest
{
    protected function setPosttest ($action) {
        $_REQUEST['action'] = $action;
        $_REQUEST['qatest'] = true;
    }
    protected function systemTest () {
        ini_set("display_errors", "on");
        $ready = oseFirewall::preRequisitiesCheck();
        $this->showHeader("Checking System is Ready");
        $this->assertEquals('1', $ready);
        $this->showResults(true, 'Checking System is Ready: expecting 1, actual: '. $ready);

    }
    public function testSystemReady()
    {
        $this->systemTest();
        $action = 'test';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $result['status']);
        $this->showResults(true, 'Checking if the Git has changes: expecting 1, actual: ' . $result['status']);
        $this->CheckSystemInfo();



    }

    //to cehck if all the pre-requisites for the gitbackuo fucntion are supported by the website
    public function CheckSystemInfo()
    {
        $action = 'checksystemInfo';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $result);
        $this->showResults(true, 'Checking the system requirements : expecting 1, actual: ' . $result);
    }


}
?>