<?php


class BaseTest extends PHPUnit_Framework_TestCase
{
    protected $live_url = '';

    protected function includeJoomla () {
        @session_start();
        if (!defined('_JEXEC')) define('_JEXEC', 1);
        ini_set('session.use_cookies', 0);
        ini_set('session.cache_limiter', 'nocache');
        if (file_exists(dirname(dirname(dirname(__DIR__))) . '/defines.php'))
        {
            include_once dirname(dirname(dirname(__DIR__))) . '/defines.php';
        }

        if (!defined('_JDEFINES'))
        {
            if (!defined('JPATH_BASE')) define('JPATH_BASE', dirname(dirname(dirname(__DIR__))));
            require_once JPATH_BASE . '/includes/defines.php';
        }
        require_once JPATH_BASE . '/includes/framework.php';
    }
    protected function setup (){
        if (file_exists(dirname(dirname(dirname(__DIR__))) . '/configuration.php')) {
            $this->includeJoomla();
        }
        else {
            $this->initWordPress();
        }
        parent::setup();
    }
    protected function initWordPress () {
        @session_start();
        require_once( dirname(dirname(dirname(dirname(dirname(__DIR__))))) .'/wp-load.php' );
        $this->oseFirewall = new oseFirewall();
        $systemReady = $this->oseFirewall -> checkSystem () ;
        $this->oseFirewall -> initSystem ();
    }

    protected function showPassed($status, $message) {
        if ($status == true) {
            echo '<div style="color: green; float:left; width: 100%;margin:10px;"> [&#10004;] '.$message.'</div>';
        }
    }

    protected function showFailed($status, $message) {
        if ($status == false) {
            echo '<div style="color: red; float:left; width: 100%;margin:10px;"> [&#10006;] '.$message.'</div>';
        }
    }

    protected function showHeader($message) {
        echo '<div style="color: black; float:left; font-weight: bold; width: 100%;margin:20px 10px;"> '.$message.'</div>';
    }
    protected function showResults($status, $message) {
        if ($status == true) {
            $this->showPassed($status, $message);
        }
        else {
            $this->showFailed($status, $message);
        }
    }
}
?>