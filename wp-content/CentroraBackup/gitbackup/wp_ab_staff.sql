--
-- Table structure for `wp_ab_staff`
--

CREATE TABLE `wp_ab_staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wp_user_id` bigint(20) unsigned DEFAULT NULL,
  `attachment_id` int(10) unsigned DEFAULT NULL,
  `full_name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `info` text,
  `google_data` text,
  `google_calendar_id` varchar(255) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '9999',
  `visibility` enum('public','private') NOT NULL DEFAULT 'public',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_ab_staff`
--

INSERT INTO `wp_ab_staff` (`id`, `wp_user_id`, `attachment_id`, `full_name`, `email`, `phone`, `info`, `google_data`, `google_calendar_id`, `position`, `visibility`)  VALUES 
(1, 4, NULL, 'Vinay', 'vinaypunamiya.nmims@gmail.com', '', '', NULL, NULL, 9999, 'public'),
(2, 0, NULL, 'test', NULL, NULL, NULL, NULL, NULL, 9999, 'public');

