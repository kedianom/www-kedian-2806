--
-- Table structure for `wp_ihc_notifications`
--

CREATE TABLE `wp_ihc_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_type` varchar(200) DEFAULT NULL,
  `level_id` varchar(200) DEFAULT NULL,
  `subject` text,
  `message` text,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_ihc_notifications`
--

INSERT INTO `wp_ihc_notifications` (`id`, `notification_type`, `level_id`, `subject`, `message`, `status`)  VALUES 
(1, 'email_check', -1, '{blogname}: Email Verification', '<p>Hi {first_name} {last_name},</p><br/>\n\n<p>You must confirm/validate your Email Account before logging in.</p><br/>\n\n<p>Please click on the following link to successfully activate your account:<br/>\n{verify_email_address_link}</p><br/>\n\n<p>Have a nice day!</p>', 1),
(2, 'email_check_success', -1, '{blogname}: Email Verification Successfully', '<p>Hi {first_name} {last_name},</p><br/>\n\n<p>Your account is now verified at {blogname}.</p><br/>\n\n<p>Have a nice day!</p>', 1),
(3, 'reset_password', -1, '{blogname}: Reset Password request', '<p>Hi {first_name} {last_name},</p></br>\n\n<p>You or someone else has requested to change password for your account: {username}</p></br>\n\n<p>Your new Password is: <strong>{NEW_PASSWORD}</strong></p></br>\n\n<p>To update your Password once you are logged from your Profile Page:<br/>\n{account_page}</p></br>\n\n<p>If you did not request for a new password, please ignore this Email notification.</p>', 1),
(4, 'admin_user_register', -1, '{blogname}: New Membership User registration', '<html><head></head><body><p>New Membership User registration on: <strong> {blogname} </strong></p>\r\n				\r\n<p><strong> Username:</strong> {username}</p>\r\n		\r\n<p><strong> Email:</strong> {user_email}</p>\r\n		\r\n<p><strong> Level Name:</strong> {level_name}</p>\r\n		\r\n<p>Have a nice day!</p>\r\n</body></html>', 1);

