--
-- Table structure for `wp_ab_categories`
--

CREATE TABLE `wp_ab_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_ab_categories`
--

INSERT INTO `wp_ab_categories` (`id`, `name`, `position`)  VALUES 
(1, 'Consultancy', 9999);

