--
-- Table structure for `wp_ose_app_admin`
--

CREATE TABLE `wp_ose_app_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

