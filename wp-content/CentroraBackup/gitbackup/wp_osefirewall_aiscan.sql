--
-- Table structure for `wp_osefirewall_aiscan`
--

CREATE TABLE `wp_osefirewall_aiscan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(250) NOT NULL,
  `content` tinyint(1) NOT NULL DEFAULT '0',
  `name` tinyint(1) NOT NULL,
  `md5` tinyint(1) NOT NULL,
  `date` tinyint(1) NOT NULL,
  `pattern` tinyint(1) NOT NULL,
  `size` tinyint(1) NOT NULL,
  `score` tinyint(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

