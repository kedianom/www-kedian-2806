--
-- Table structure for `wp_newsletter_stats`
--

CREATE TABLE `wp_newsletter_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email_id` int(11) NOT NULL DEFAULT '0',
  `link_id` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `anchor` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `email_id` (`email_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_newsletter_stats`
--

INSERT INTO `wp_newsletter_stats` (`id`, `user_id`, `email_id`, `link_id`, `created`, `url`, `anchor`, `ip`, `country`)  VALUES 
(1, 4, 2, 0, '2016-06-16 15:27:24', '', '', '64.233.173.85', ''),
(2, 4, 2, 0, '2016-06-16 15:27:37', '', '', '64.233.173.85', ''),
(3, 4, 3, 0, '2016-06-16 16:49:51', '', '', '64.233.173.85', '');

