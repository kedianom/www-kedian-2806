--
-- Table structure for `wp_ab_staff_schedule_items`
--

CREATE TABLE `wp_ab_staff_schedule_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL,
  `day_index` int(10) unsigned NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_ids_idx` (`staff_id`,`day_index`)) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_ab_staff_schedule_items`
--

INSERT INTO `wp_ab_staff_schedule_items` (`id`, `staff_id`, `day_index`, `start_time`, `end_time`)  VALUES 
(1, 1, 1, NULL, NULL),
(2, 1, 2, '08:00:00', '18:00:00'),
(3, 1, 3, '08:00:00', '18:00:00'),
(4, 1, 4, '08:00:00', '18:00:00'),
(5, 1, 5, '08:00:00', '18:00:00'),
(6, 1, 6, '08:00:00', '18:00:00'),
(7, 1, 7, NULL, NULL),
(8, 2, 1, NULL, '19:45:00'),
(9, 2, 2, '16:00:00', '24:00:00'),
(10, 2, 3, '16:00:00', '24:00:00'),
(11, 2, 4, '16:00:00', '24:00:00'),
(12, 2, 5, '16:00:00', '24:00:00'),
(13, 2, 6, '16:00:00', '24:00:00'),
(14, 2, 7, NULL, '22:30:00');

