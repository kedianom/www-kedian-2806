--
-- Table structure for `wp_rs_templates_email`
--

CREATE TABLE `wp_rs_templates_email` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `template_name` longtext NOT NULL,
  `sender_opt` varchar(10) NOT NULL DEFAULT 'woo',
  `from_name` longtext NOT NULL,
  `from_email` longtext NOT NULL,
  `subject` longtext NOT NULL,
  `message` longtext NOT NULL,
  `earningpoints` longtext NOT NULL,
  `redeemingpoints` longtext NOT NULL,
  `mailsendingoptions` longtext NOT NULL,
  `rsmailsendingoptions` longtext NOT NULL,
  `minimum_userpoints` longtext NOT NULL,
  `sendmail_options` varchar(10) NOT NULL DEFAULT '1',
  `sendmail_to` longtext NOT NULL,
  `sending_type` varchar(20) NOT NULL,
  `rs_status` varchar(10) NOT NULL DEFAULT 'DEACTIVATE',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rs_templates_email`
--

INSERT INTO `wp_rs_templates_email` (`id`, `template_name`, `sender_opt`, `from_name`, `from_email`, `subject`, `message`, `earningpoints`, `redeemingpoints`, `mailsendingoptions`, `rsmailsendingoptions`, `minimum_userpoints`, `sendmail_options`, `sendmail_to`, `sending_type`, `rs_status`)  VALUES 
(1, 'Default', 'woo', 'Admin', 'sushilkedia@gmail.com', 'SUMO Rewards Point', 'Hi {rsfirstname} {rslastname}, <br><br> You have Earned Reward Points: {rspoints} on {rssitelink}  <br><br> You can use this Reward Points to make discounted purchases on {rssitelink} <br><br> Thanks', '', '', 2, 3, 0, 1, '', '', 'DEACTIVATE');

