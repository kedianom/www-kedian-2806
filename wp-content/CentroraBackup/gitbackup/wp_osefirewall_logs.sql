--
-- Table structure for `wp_osefirewall_logs`
--

CREATE TABLE `wp_osefirewall_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `comp` varchar(3) NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_logs`
--

INSERT INTO `wp_osefirewall_logs` (`id`, `date`, `comp`, `status`)  VALUES 
(1, '2016-04-26 05:36:29', 'dom', 'www.kedianomics.com'),
(2, '2016-08-13 12:37:15', 'dom', 'kedianomics.com');

