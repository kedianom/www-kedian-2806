--
-- Table structure for `wp_popup_form_results`
--

CREATE TABLE `wp_popup_form_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `popup_id` int(11) DEFAULT NULL,
  `meta_name` varchar(255) DEFAULT NULL,
  `meta_value` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

