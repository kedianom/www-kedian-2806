--
-- Table structure for `wp_osefirewall_fileuploadlog`
--

CREATE TABLE `wp_osefirewall_fileuploadlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_id` int(11) NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `file_type_id` int(11) NOT NULL,
  `validation_status` tinyint(1) NOT NULL,
  `vs_scan_status` tinyint(1) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `osefirewall_fileuploadlog_idx1` (`id`),
  KEY `ip_id` (`ip_id`),
  KEY `file_type_id` (`file_type_id`)) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_fileuploadlog`
--

INSERT INTO `wp_osefirewall_fileuploadlog` (`id`, `ip_id`, `file_name`, `file_type_id`, `validation_status`, `vs_scan_status`, `datetime`)  VALUES 
(4, 30, 'Self photo.JPG', 59, 0, 0, '2016-06-13 09:12:23'),
(5, 295, 'revslider.zip', 151, 0, 0, '2017-04-01 04:17:34');

