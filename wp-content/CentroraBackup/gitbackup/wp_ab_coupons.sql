--
-- Table structure for `wp_ab_coupons`
--

CREATE TABLE `wp_ab_coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `discount` decimal(3,0) NOT NULL DEFAULT '0',
  `deduction` decimal(10,2) NOT NULL DEFAULT '0.00',
  `usage_limit` int(10) unsigned NOT NULL DEFAULT '1',
  `used` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

