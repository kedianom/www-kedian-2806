--
-- Table structure for `wp_osefirewall_advancepatterns`
--

CREATE TABLE `wp_osefirewall_advancepatterns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patterns` text NOT NULL,
  `type_id` tinyint(3) NOT NULL,
  `confidence` tinyint(3) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `osefirewall_vspatterns_idx1` (`type_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

