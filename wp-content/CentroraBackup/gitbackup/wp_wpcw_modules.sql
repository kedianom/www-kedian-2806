--
-- Table structure for `wp_wpcw_modules`
--

CREATE TABLE `wp_wpcw_modules` (
  `module_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_course_id` int(11) unsigned NOT NULL DEFAULT '0',
  `module_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `module_title` varchar(150) NOT NULL,
  `module_desc` text,
  `module_order` int(11) unsigned NOT NULL DEFAULT '10000',
  `module_number` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

