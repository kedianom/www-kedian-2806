--
-- Table structure for `wp_newsletter_sent`
--

CREATE TABLE `wp_newsletter_sent` (
  `email_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `open` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `error` varchar(100) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`email_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `email_id` (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_newsletter_sent`
--

INSERT INTO `wp_newsletter_sent` (`email_id`, `user_id`, `status`, `open`, `time`, `error`, `ip`)  VALUES 
(2, 4, 0, 0, 1466090826, '', ''),
(3, 4, 0, 0, 1466095762, '', ''),
(3, 5, 0, 0, 1466095762, '', '');

