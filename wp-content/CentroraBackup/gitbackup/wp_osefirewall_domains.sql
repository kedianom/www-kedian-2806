--
-- Table structure for `wp_osefirewall_domains`
--

CREATE TABLE `wp_osefirewall_domains` (
  `D_id` int(11) NOT NULL AUTO_INCREMENT,
  `D_address` varchar(200) NOT NULL,
  PRIMARY KEY (`D_id`),
  UNIQUE KEY `D_address` (`D_address`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_domains`
--

INSERT INTO `wp_osefirewall_domains` (`D_id`, `D_address`)  VALUES 
(3, 'www.bhagawadgeeta.com'),
(1, 'www.kedianomics.com');

