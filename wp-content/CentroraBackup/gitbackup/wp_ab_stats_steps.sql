--
-- Table structure for `wp_ab_stats_steps`
--

CREATE TABLE `wp_ab_stats_steps` (
  `step` varchar(10) NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`step`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

