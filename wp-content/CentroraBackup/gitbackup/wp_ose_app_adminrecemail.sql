--
-- Table structure for `wp_ose_app_adminrecemail`
--

CREATE TABLE `wp_ose_app_adminrecemail` (
  `email_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  PRIMARY KEY (`email_id`,`admin_id`),
  KEY `idx1_adminrecemail` (`admin_id`),
  KEY `idx2_adminrecemail` (`email_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

