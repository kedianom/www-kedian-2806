--
-- Table structure for `wp_rsrecordpoints`
--

CREATE TABLE `wp_rsrecordpoints` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `earnedpoints` float DEFAULT NULL,
  `redeempoints` float DEFAULT NULL,
  `userid` int(99) DEFAULT NULL,
  `earneddate` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expirydate` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkpoints` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `earnedequauivalentamount` int(99) DEFAULT NULL,
  `redeemequauivalentamount` int(99) DEFAULT NULL,
  `orderid` int(99) DEFAULT NULL,
  `productid` int(99) DEFAULT NULL,
  `variationid` int(99) DEFAULT NULL,
  `refuserid` int(99) DEFAULT NULL,
  `reasonindetail` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalpoints` float DEFAULT NULL,
  `showmasterlog` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `showuserlog` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomineeid` int(99) DEFAULT NULL,
  `nomineepoints` int(99) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rsrecordpoints`
--

INSERT INTO `wp_rsrecordpoints` (`id`, `earnedpoints`, `redeempoints`, `userid`, `earneddate`, `expirydate`, `checkpoints`, `earnedequauivalentamount`, `redeemequauivalentamount`, `orderid`, `productid`, `variationid`, `refuserid`, `reasonindetail`, `totalpoints`, `showmasterlog`, `showuserlog`, `nomineeid`, `nomineepoints`)  VALUES 
(1, 0, 0, 2, '08-06-2016 05:20:24 PM', 999999999999, 'RPBSRP', 0, 0, 12978, 12855, 0, 0, '', NULL, 'false', 'false', 0, 0),
(2, 0, 0, 6, '11-08-2016 09:25:18 AM', 999999999999, 'RRP', 0, 0, 0, 0, 0, 0, '', NULL, 'false', 'false', 0, 0),
(3, 0, 0, 7, '12-11-2016 10:36:44 AM', 999999999999, 'RRP', 0, 0, 0, 0, 0, 0, '', NULL, 'false', 'false', 0, 0);

