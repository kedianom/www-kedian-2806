--
-- Table structure for `wp_ab_series`
--

CREATE TABLE `wp_ab_series` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repeat` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

