--
-- Table structure for `wp_osefirewall_versions`
--

CREATE TABLE `wp_osefirewall_versions` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(32) NOT NULL,
  `type` varchar(4) NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_osefirewall_versions`
--

INSERT INTO `wp_osefirewall_versions` (`version_id`, `number`, `type`)  VALUES 
(1, 20170610053001, 'avs'),
(2, 20170224, 'ath');

