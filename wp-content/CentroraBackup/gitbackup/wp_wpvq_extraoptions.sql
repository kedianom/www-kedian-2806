--
-- Table structure for `wp_wpvq_extraoptions`
--

CREATE TABLE `wp_wpvq_extraoptions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quizId` int(200) NOT NULL,
  `optionName` varchar(191) DEFAULT NULL,
  `optionValue` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpvq_extraoptions`
--

INSERT INTO `wp_wpvq_extraoptions` (`id`, `quizId`, `optionName`, `optionValue`)  VALUES 
(1, 1, 'squeezePage', 1),
(2, 1, 'refreshBrowser', 1),
(3, 1, 'forceContinueButton', 0);

