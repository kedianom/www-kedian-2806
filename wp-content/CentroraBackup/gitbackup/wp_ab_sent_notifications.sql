--
-- Table structure for `wp_ab_sent_notifications`
--

CREATE TABLE `wp_ab_sent_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` int(10) unsigned DEFAULT NULL,
  `gateway` enum('email','sms') NOT NULL DEFAULT 'email',
  `type` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_id_idx` (`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

