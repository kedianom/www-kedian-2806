--
-- Table structure for `wp_wpcw_member_levels`
--

CREATE TABLE `wp_wpcw_member_levels` (
  `course_id` int(11) NOT NULL,
  `member_level_id` varchar(100) NOT NULL,
  UNIQUE KEY `course_id` (`course_id`,`member_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

