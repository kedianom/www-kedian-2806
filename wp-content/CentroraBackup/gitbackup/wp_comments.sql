--
-- Table structure for `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10)),
  KEY `woo_idx_comment_type` (`comment_type`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`)  VALUES 
(23, 13110, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-09-27 02:22:58', '2016-09-26 20:52:58', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(24, 13113, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-09-27 19:59:37', '2016-09-27 14:29:37', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(25, 13153, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-11-25 07:48:22', '2016-11-25 02:18:22', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(4, 7775, 'Gregor Codex', 'codexgregor@gmail.com', '', '178.122.0.113', '2015-02-05 12:39:37', '2015-02-05 12:39:37', 'test', 0, 1, '', '', 0, 0),
(5, 11011, 'Mr WordPress', '', 'https://wordpress.org/', '', '2014-07-17 06:44:58', '2014-07-17 06:44:58', 'Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.', 0, 1, '', '', 0, 0),
(7, 2859, 'admin', 'sergey@kreado.com', '', '178.122.245.81', '2014-11-04 14:06:12', '2014-11-04 14:06:12', 567567575, 0, 1, '', '', 0, 0),
(8, 5553, 'Jenny Forester', 'jennyforester88@gmail.com', '', '217.84.205.201', '2015-03-20 15:14:54', '2015-03-20 15:14:54', 'Vestibulum id urna quis mauris auctor interdum. Morbi mollis purus at quam venenatis sodales. Sed tincidunt feugiat facilisis. Quisque congue, magna sit amet mattis placerat, ligula arcu sollicitudin risus, nec congue dolor erat eget enim. Nulla venenatis tincidunt eros, id convallis metus rhoncus vitae?', 0, 1, '', '', 0, 0),
(9, 5553, 'Gregor Codex', 'codexgregor@gmail.com', '', '217.84.205.201', '2015-03-20 15:16:30', '2015-03-20 15:16:30', 'Compellingly disseminate backward-compatible partnerships after market positioning information. Collaboratively pursue prospective data and economically sound e-business. Dramatically utilize professional e-commerce rather than enabled core competencies. Compellingly foster distinctive materials vis-a-vis holistic results. Proactively incubate alternative intellectual capital without ethical technologies.', 0, 1, '', '', 8, 0),
(10, 5553, 'Kontramax', 'kontrastock@gmail.com', 'http://kontramax.com', '37.44.92.229', '2015-03-26 07:19:00', '2015-03-26 07:19:00', 'Lorem ipsum first mover advantage metrics channels burn rate incubator social media disruptive market direct mailing long tail assets. Vesting period early adopters prototype advisor venture customer pivot user experience entrepreneur. Stealth infographic twitter business plan alpha pivot scrum project market interaction design. Innovator assets customer long tail channels.', 0, 1, '', '', 0, 0),
(11, 11243, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-04-18 13:12:48', '2016-04-18 07:42:48', 'Order status changed from Pending Payment to Failed.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(12, 11243, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-04-18 13:12:49', '2016-04-18 07:42:49', 'Failed', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(13, 11243, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-04-18 13:12:49', '2016-04-18 07:42:49', '', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(14, 11244, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-04-18 13:40:33', '2016-04-18 08:10:33', 'Order status changed from Pending Payment to Processing.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(15, 11244, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-04-18 13:40:34', '2016-04-18 08:10:34', 'CCAvenue payment successful<br/>Bank Ref Number: 4878985', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(16, 12857, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-05-27 01:52:49', '2016-05-26 20:22:49', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(17, 12867, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-05-27 11:45:53', '2016-05-27 06:15:53', 'Order status changed from Pending Payment to Failed.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(18, 12867, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-05-27 11:45:54', '2016-05-27 06:15:54', 'Failed', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(19, 12867, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-05-27 11:45:54', '2016-05-27 06:15:54', '', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(20, 12978, 'Webmaster Kedianomics', 'webmaster@kedianomics.com', '', '', '2016-06-08 17:20:24', '2016-06-08 11:50:24', 'Order status changed from Pending Payment to Completed.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(22, 13047, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-06-17 00:03:39', '2016-06-16 18:33:39', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(26, 13155, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-11-26 20:22:58', '2016-11-26 14:52:58', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(27, 13159, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-12-02 09:43:33', '2016-12-02 04:13:33', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0),
(28, 13160, 'WooCommerce', 'woocommerce@kedianomics.com', '', '', '2016-12-02 09:43:34', '2016-12-02 04:13:34', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, 1, 'WooCommerce', 'order_note', 0, 0);

