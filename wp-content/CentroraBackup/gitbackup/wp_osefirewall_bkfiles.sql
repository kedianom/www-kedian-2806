--
-- Table structure for `wp_osefirewall_bkfiles`
--

CREATE TABLE `wp_osefirewall_bkfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` text NOT NULL,
  `ext` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `checked` tinyint(1) DEFAULT NULL,
  `datechecked` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

