--
-- Table structure for `wp_wpcw_user_courses`
--

CREATE TABLE `wp_wpcw_user_courses` (
  `user_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  `course_progress` int(11) NOT NULL DEFAULT '0',
  `course_final_grade_sent` varchar(30) NOT NULL DEFAULT '',
  `course_enrolment_date` datetime DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `user_id` (`user_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

