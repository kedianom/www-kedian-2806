--
-- Table structure for `wp_popup_visits`
--

CREATE TABLE `wp_popup_visits` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `popup_id` int(9) DEFAULT NULL,
  `visitor` varchar(25) NOT NULL,
  `visit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ref` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `country` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_popup_visits`
--

INSERT INTO `wp_popup_visits` (`id`, `popup_id`, `visitor`, `visit`, `ref`, `user_agent`, `country`)  VALUES 
(6, 2, 'a9eda3299c85e35409c4f8', '2016-05-23 08:58:22', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=9e59f64494&preview=true', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36', ''),
(5, 2, '95a9bcd7bc9aa448701e0c', '2016-05-23 08:44:10', 'http://www.kedianomics.com/wp-admin/admin.php?page=ips_admin&tab=ips_manage&preview_id=2', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:46.0) Gecko/20100101 Firefox/46.0', ''),
(3, 2, '95a9bcd7bc9aa448701e0c', '2016-05-23 08:42:19', 'http://www.kedianomics.com/wp-admin/admin.php?page=ips_admin&tab=ips_manage&preview_id=2', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:46.0) Gecko/20100101 Firefox/46.0', ''),
(4, 2, '95a9bcd7bc9aa448701e0c', '2016-05-23 08:43:12', 'http://www.kedianomics.com/wp-admin/admin.php?page=ips_admin&tab=ips_manage&preview_id=2', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:46.0) Gecko/20100101 Firefox/46.0', ''),
(7, 2, 'a9eda3299c85e35409c4f8', '2016-05-23 08:58:41', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=9e59f64494&preview=true', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36', ''),
(8, 2, 'a9eda3299c85e35409c4f8', '2016-05-23 08:59:26', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=9e59f64494&preview=true', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36', ''),
(9, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 09:12:04', 'http://www.kedianomics.com/pages/about-us-kedia-2/', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(10, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 09:12:11', 'http://www.kedianomics.com/pages/about-us-kedia-2/', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(11, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 09:12:15', 'http://www.kedianomics.com/pages/about-us-kedia-2/', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(12, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 09:12:32', 'http://www.kedianomics.com/pages/about-us-kedia-2/', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(13, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 09:14:17', 'http://www.kedianomics.com/pages/about-us-kedia-2/', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(14, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:17:22', 'http://www.kedianomics.com/pages/about-us-kedia-2/', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(15, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:23:15', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=53ae5235d7&preview=true', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(16, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:23:23', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=53ae5235d7&preview=true', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(17, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:23:26', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=53ae5235d7&preview=true', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(18, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:23:31', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=53ae5235d7&preview=true', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(19, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:39:03', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=53ae5235d7&preview=true', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(20, 2, '0e22d0e46e856a86e5ceee', '2016-05-23 10:39:54', 'http://www.kedianomics.com/pages/about-us-kedia-2/?preview_id=12711&preview_nonce=53ae5235d7&preview=true', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', ''),
(21, 2, 'a9eda3299c85e35409c4f8', '2016-05-23 10:46:53', 'http://www.kedianomics.com/why-kedia/', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36', '');

