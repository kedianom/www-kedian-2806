--
-- Table structure for `wp_wpvq_questions`
--

CREATE TABLE `wp_wpvq_questions` (
  `ID` int(200) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `quizId` int(200) NOT NULL,
  `position` int(200) NOT NULL,
  `pictureId` bigint(20) NOT NULL,
  `content` text,
  `pageAfter` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpvq_questions`
--

INSERT INTO `wp_wpvq_questions` (`ID`, `label`, `quizId`, `position`, `pictureId`, `content`, `pageAfter`)  VALUES 
(1, 'Hello! What\\\'s your name?', 1, 2, 12484, '', 0),
(2, 'Hello! What\\\'s your name?', 1, 2, 12484, '', 0);

