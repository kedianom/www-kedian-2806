--
-- Table structure for `wp_wpcw_user_progress_quizzes`
--

CREATE TABLE `wp_wpcw_user_progress_quizzes` (
  `user_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `quiz_id` bigint(20) NOT NULL,
  `quiz_attempt_id` int(11) NOT NULL DEFAULT '0',
  `quiz_completed_date` datetime NOT NULL,
  `quiz_started_date` datetime NOT NULL,
  `quiz_correct_questions` int(11) unsigned NOT NULL,
  `quiz_grade` float(8,2) NOT NULL DEFAULT '-1.00',
  `quiz_question_total` int(11) unsigned NOT NULL,
  `quiz_data` longtext,
  `quiz_is_latest` varchar(50) DEFAULT 'latest',
  `quiz_needs_marking` int(11) unsigned NOT NULL DEFAULT '0',
  `quiz_needs_marking_list` text,
  `quiz_next_step_type` varchar(50) DEFAULT '',
  `quiz_next_step_msg` text,
  `quiz_paging_status` varchar(20) NOT NULL DEFAULT 'complete',
  `quiz_paging_next_q` int(11) NOT NULL DEFAULT '0',
  `quiz_paging_incomplete` int(11) NOT NULL DEFAULT '0',
  `quiz_completion_time_seconds` bigint(20) NOT NULL DEFAULT '0',
  UNIQUE KEY `unique_progress_item` (`user_id`,`unit_id`,`quiz_id`,`quiz_attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

