--
-- Table structure for `wp_osefirewall_backupath`
--

CREATE TABLE `wp_osefirewall_backupath` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` text,
  `time` datetime NOT NULL,
  `fileNum` bigint(20) NOT NULL,
  `fileTotal` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_backupath`
--

INSERT INTO `wp_osefirewall_backupath` (`id`, `path`, `time`, `fileNum`, `fileTotal`)  VALUES 
(1, '/home/kedianom/public_html/wp-content/CentroraBackup/BackupFiles/-filesbackup-20160810_190007/-filesbackup-20160810_190007.zip', '2016-08-10 19:00:07', 0, 0),
(2, '', '0000-00-00 00:00:00', 0, 0);

