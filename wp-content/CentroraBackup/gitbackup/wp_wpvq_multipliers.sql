--
-- Table structure for `wp_wpvq_multipliers`
--

CREATE TABLE `wp_wpvq_multipliers` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `quizId` int(200) NOT NULL,
  `questionId` int(200) NOT NULL,
  `answerId` int(200) NOT NULL,
  `appreciationId` int(200) NOT NULL,
  `multiplier` int(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpvq_multipliers`
--

INSERT INTO `wp_wpvq_multipliers` (`id`, `quizId`, `questionId`, `answerId`, `appreciationId`, `multiplier`)  VALUES 
(4, 1, 2, 1, 1, 0),
(2, 1, 1, 2, 1, 0),
(3, 1, 1, 3, 1, 0);

