--
-- Table structure for `wp_ab_services`
--

CREATE TABLE `wp_ab_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '900',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `color` varchar(255) NOT NULL DEFAULT '#FFFFFF',
  `category_id` int(10) unsigned DEFAULT NULL,
  `capacity` int(11) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '9999',
  `padding_left` int(11) NOT NULL DEFAULT '0',
  `padding_right` int(11) NOT NULL DEFAULT '0',
  `info` text,
  `type` enum('simple','compound') NOT NULL DEFAULT 'simple',
  `sub_services` text NOT NULL,
  `visibility` enum('public','private') NOT NULL DEFAULT 'public',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_ab_services`
--

INSERT INTO `wp_ab_services` (`id`, `title`, `duration`, `price`, `color`, `category_id`, `capacity`, `position`, `padding_left`, `padding_right`, `info`, `type`, `sub_services`, `visibility`)  VALUES 
(1, 'Speak to Sushil Kedia', 900, 0, '#570F8A', 1, 1, 9999, 0, 0, '', 'simple', '[]', 'public'),
(2, 'Meet Sushil Kedia', 1800, 0, '#3713C8', 1, 2, 9999, 0, 0, '', 'simple', '[]', 'public'),
(3, 'testing services', 900, 100, '#00EA04', 1, 4, 9999, 900, 900, 'this is the demo test created from bookly', 'simple', '[]', 'public');

