--
-- Table structure for `wp_wpcw_units_meta`
--

CREATE TABLE `wp_wpcw_units_meta` (
  `unit_id` int(11) unsigned NOT NULL,
  `parent_module_id` int(11) unsigned NOT NULL DEFAULT '0',
  `parent_course_id` int(11) unsigned NOT NULL DEFAULT '0',
  `unit_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `unit_order` int(11) unsigned NOT NULL DEFAULT '0',
  `unit_number` int(11) unsigned NOT NULL DEFAULT '0',
  `unit_drip_type` varchar(50) NOT NULL DEFAULT '',
  `unit_drip_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unit_drip_interval` int(11) NOT NULL DEFAULT '432000',
  `unit_drip_interval_type` varchar(15) NOT NULL DEFAULT 'interval_days',
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

