--
-- Table structure for `wp_osefirewall_attacktype`
--

CREATE TABLE `wp_osefirewall_attacktype` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `tag` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_attacktype`
--

INSERT INTO `wp_osefirewall_attacktype` (`id`, `name`, `tag`)  VALUES 
(1, 'Layer 1 Intrusion', 'lay1'),
(2, 'Cross-site scripting', 'xss'),
(3, 'Cross-site request forgery', 'csrf'),
(4, 'SQL Injection', 'sqli'),
(5, 'Remote File Inclusion', 'rfe'),
(6, 'Local File Inclusion', 'lfi'),
(7, 'Layer 2 Intrusion', 'id'),
(8, 'Directory Traversal', 'dt'),
(9, 'Denial-of-Service Attack', 'dos'),
(10, 'Local File Modification Attempt', 'files'),
(11, 'Spamming', 'spam'),
(12, 'Format String Attack', 'fstr'),
(13, 'Inconsistent File Type', 'incf'),
(14, 'Virus File', 'visf'),
(15, 'Brute Force Attack', 'bf');

