--
-- Table structure for `wp_osefirewall_vars`
--

CREATE TABLE `wp_osefirewall_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyname` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_osefirewall_vars`
--

INSERT INTO `wp_osefirewall_vars` (`id`, `keyname`, `status`)  VALUES 
(1, 'COOKIE.CFCLIENT_AVON', 3),
(2, 'COOKIE.CFCLIENT_LAUSANNE', 3),
(3, 'COOKIE.CFCLIENT_CFGLOBALS', 3),
(4, 'COOKIE.omp__super_properties', 3),
(5, 'COOKIE._okbk', 3),
(6, 'COOKIE.__utmz', 3),
(7, 'POST.siteurl', 3),
(8, 'POST.redirect_to', 3),
(9, 'POST.return_to', 3),
(10, 'POST.home', 3),
(11, 'POST.current_url', 3),
(12, 'POST.referredby', 3),
(13, 'POST._wp_original_http_referer', 3),
(14, 'POST.link', 3),
(15, 'POST._jd_wp_twitter', 3),
(16, 'post.post_data', 1),
(17, 'post.options', 1),
(18, 'post.sliderData', 1),
(19, 'post.params', 1),
(20, 'post.data', 3),
(21, 'server.HTTP_CLIENT_IPbs', 1),
(22, 'post.customized', 1),
(23, 'post.wplauncher_contact_message', 1),
(24, 'post.html', 3),
(25, 'server.HTTP_USER_AGENT', 1),
(26, 'post.getpaymenttitle', 1),
(27, 'post.\");_?>set_time_limit(0);\n$ip_', 1),
(28, 'server.FILE_TYPE', 1),
(29, 'post.custom_fields', 1),
(30, 'get.pad', 1),
(31, 'post.<?xml_version', 1),
(32, 'POST.json', 3),
(33, 'POST.jform', 3),
(34, 'get.fileurl', 3),
(35, 'get.file', 1),
(36, 'server.REQUEST_URI', 1),
(37, 'post.gform_unique_id', 1),
(38, 'post.6z4tr804', 1),
(39, 'post.z0', 1),
(40, 'post.4bp4r406', 1),
(41, 'post.fields', 1),
(42, 'post.hide_my_wp', 1),
(43, 'post.import_options', 1),
(44, 'post.import_field', 1),
(45, 'get.img', 1),
(46, 'get.array', 1),
(47, 'get.request', 1),
(48, 'post.hbkzjeejig', 1),
(49, 'post.<?php_echo_7457737_736723;_$raPo_rZluoE', 1),
(50, 'post.settings', 1);

