--
-- Table structure for `wp_wpcw_quizzes`
--

CREATE TABLE `wp_wpcw_quizzes` (
  `quiz_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_title` varchar(150) NOT NULL,
  `quiz_desc` text,
  `quiz_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `parent_unit_id` int(11) unsigned NOT NULL DEFAULT '0',
  `parent_course_id` int(11) NOT NULL DEFAULT '0',
  `quiz_type` varchar(15) NOT NULL,
  `quiz_pass_mark` int(11) NOT NULL DEFAULT '0',
  `quiz_show_answers` varchar(15) NOT NULL DEFAULT 'no_answers',
  `quiz_show_survey_responses` varchar(15) NOT NULL DEFAULT 'no_responses',
  `quiz_attempts_allowed` int(11) NOT NULL DEFAULT '-1',
  `show_answers_settings` varchar(500) NOT NULL DEFAULT '',
  `quiz_paginate_questions` varchar(15) NOT NULL DEFAULT 'no_paging',
  `quiz_paginate_questions_settings` varchar(500) NOT NULL DEFAULT '',
  `quiz_timer_mode` varchar(25) NOT NULL DEFAULT 'no_timer',
  `quiz_timer_mode_limit` int(11) unsigned NOT NULL DEFAULT '15',
  `quiz_results_downloadable` varchar(10) NOT NULL DEFAULT 'on',
  `quiz_results_by_tag` varchar(10) NOT NULL DEFAULT 'on',
  `quiz_results_by_timer` varchar(10) NOT NULL DEFAULT 'on',
  `quiz_recommended_score` varchar(20) NOT NULL DEFAULT 'no_recommended',
  `show_recommended_percentage` int(10) unsigned NOT NULL DEFAULT '50',
  PRIMARY KEY (`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

