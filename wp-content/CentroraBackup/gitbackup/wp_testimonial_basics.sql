--
-- Table structure for `wp_testimonial_basics`
--

CREATE TABLE `wp_testimonial_basics` (
  `tb_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `tb_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tb_group` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_order` int(4) unsigned NOT NULL,
  `tb_approved` int(1) unsigned NOT NULL,
  `tb_name` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_location` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_email` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_pic_url` char(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_url` char(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_rating` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_testimonial` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_title` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_custom1` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_custom2` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tb_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_testimonial_basics`
--

INSERT INTO `wp_testimonial_basics` (`tb_id`, `tb_date`, `tb_group`, `tb_order`, `tb_approved`, `tb_name`, `tb_location`, `tb_email`, `tb_pic_url`, `tb_url`, `tb_rating`, `tb_testimonial`, `tb_title`, `tb_custom1`, `tb_custom2`)  VALUES 
(1, '2016-05-14 13:08:07', '', 0, 1, 'Jeniffer Burns', '', '', '', '', 4, '�Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod', '', '', ''),
(2, '2016-05-22 15:12:12', 'All', 0, 1, 'Vinay Punamiya', '', 'vnpnm637@gmail.com', '', '', 0, 'demo testimonial testing', '', '', ''),
(3, '2016-05-22 16:03:31', 'All', 0, 0, 'Vinay Punamiya', '', 'vnpnm637@gmail.com', '', '', 0, 'demo', '', '', ''),
(4, '2016-06-10 21:39:29', 'All', 0, 0, 'Vinay Punamiya', '', 'vnpnm637@gmail.com', '', '', 0, 'Hi,\r\n\r\nDemo Kedianomics testimonial testing\r\n\r\nRegards,\r\nVinay', '', '', ''),
(5, '2016-11-04 17:16:49', 'All', 0, 0, 'Vinay Punamiya', '', 'vinay@atma.ac', '', '', 0, 'demo testimonial', '', '', '');

