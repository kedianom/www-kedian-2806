--
-- Table structure for `wp_wpvq_quizzes`
--

CREATE TABLE `wp_wpvq_quizzes` (
  `ID` int(200) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `authorId` bigint(20) NOT NULL,
  `dateCreation` int(200) NOT NULL,
  `dateUpdate` int(200) NOT NULL,
  `showSharing` smallint(6) NOT NULL,
  `showCopyright` smallint(6) NOT NULL,
  `askInformations` varchar(100) DEFAULT NULL,
  `forceToShare` varchar(100) DEFAULT NULL,
  `skin` varchar(50) NOT NULL,
  `randomQuestions` int(100) DEFAULT NULL,
  `isRandomAnswers` smallint(6) DEFAULT NULL,
  `meta` text,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpvq_quizzes`
--

INSERT INTO `wp_wpvq_quizzes` (`ID`, `type`, `name`, `authorId`, `dateCreation`, `dateUpdate`, `showSharing`, `showCopyright`, `askInformations`, `forceToShare`, `skin`, `randomQuestions`, `isRandomAnswers`, `meta`)  VALUES 
(1, 'WPVQGamePersonality', 'Quiz Test', 1, 1463979215, 1463979487, 1, 0, '', '', 'buzzfeed', 0, 1, 'a:3:{s:9:\"playAgain\";i:1;s:14:\"hideRightWrong\";i:0;s:15:\"redirectionPage\";s:26:\"http://www.kedianomics.com\";}');

