--
-- Table structure for `wp_wpvq_answers`
--

CREATE TABLE `wp_wpvq_answers` (
  `ID` int(200) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `pictureId` bigint(20) NOT NULL,
  `weight` smallint(30) NOT NULL,
  `content` text NOT NULL,
  `questionId` int(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpvq_answers`
--

INSERT INTO `wp_wpvq_answers` (`ID`, `label`, `pictureId`, `weight`, `content`, `questionId`)  VALUES 
(1, 'um...uh....oh no I forgot!!', 0, 0, '', 2),
(2, 'Hi, my name is [yur name]', 0, 0, '', 1),
(3, 'im eating a bagel and cream cheese', 0, 0, '', 1);

