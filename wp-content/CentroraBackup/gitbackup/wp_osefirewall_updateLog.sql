--
-- Table structure for `wp_osefirewall_updateLog`
--

CREATE TABLE `wp_osefirewall_updateLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

