--
-- Table structure for `wp_osefirewall_backup`
--

CREATE TABLE `wp_osefirewall_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `type` smallint(6) NOT NULL,
  `dbBackupPath` text,
  `fileBackupPath` text,
  `server` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

