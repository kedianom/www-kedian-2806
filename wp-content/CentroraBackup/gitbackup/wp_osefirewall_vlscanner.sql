--
-- Table structure for `wp_osefirewall_vlscanner`
--

CREATE TABLE `wp_osefirewall_vlscanner` (
  `vls_id` int(11) NOT NULL AUTO_INCREMENT,
  `vls_type` tinyint(4) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`vls_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

