--
-- Table structure for `wp_rspointexpiry`
--

CREATE TABLE `wp_rspointexpiry` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `earnedpoints` float DEFAULT NULL,
  `usedpoints` float DEFAULT NULL,
  `expiredpoints` float DEFAULT NULL,
  `userid` int(99) DEFAULT NULL,
  `earneddate` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expirydate` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkpoints` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderid` int(99) DEFAULT NULL,
  `totalearnedpoints` int(99) DEFAULT NULL,
  `totalredeempoints` int(99) DEFAULT NULL,
  `reasonindetail` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

