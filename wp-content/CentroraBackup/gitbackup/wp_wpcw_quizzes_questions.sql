--
-- Table structure for `wp_wpcw_quizzes_questions`
--

CREATE TABLE `wp_wpcw_quizzes_questions` (
  `question_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `question_type` varchar(20) NOT NULL DEFAULT 'multi',
  `question_question` text,
  `question_answers` text,
  `question_data_answers` text,
  `question_correct_answer` varchar(300) NOT NULL,
  `question_answer_type` varchar(50) NOT NULL DEFAULT '',
  `question_answer_hint` text,
  `question_answer_explanation` text,
  `question_image` varchar(300) NOT NULL DEFAULT '',
  `question_answer_file_types` varchar(300) NOT NULL DEFAULT '',
  `question_usage_count` int(11) unsigned DEFAULT '0',
  `question_expanded_count` int(11) unsigned DEFAULT '1',
  `question_multi_random_enable` int(2) unsigned DEFAULT '0',
  `question_multi_random_count` int(4) unsigned DEFAULT '5',
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

