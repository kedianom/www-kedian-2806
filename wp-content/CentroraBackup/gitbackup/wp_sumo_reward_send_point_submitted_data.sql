--
-- Table structure for `wp_sumo_reward_send_point_submitted_data`
--

CREATE TABLE `wp_sumo_reward_send_point_submitted_data` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `userid` int(225) DEFAULT NULL,
  `userloginname` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pointstosend` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sendercurrentpoints` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selecteduser` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

