--
-- Table structure for `wp_osefirewall_vstypes`
--

CREATE TABLE `wp_osefirewall_vstypes` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_vstypes`
--

INSERT INTO `wp_osefirewall_vstypes` (`id`, `type`)  VALUES 
(1, 'O_SHELL_CODES'),
(2, 'O_BASE64_CODES'),
(3, 'O_JS_INJECTION_CODES'),
(4, 'O_PHP_INJECTION_CODES'),
(5, 'O_IFRAME_INJECTION_CODES'),
(6, 'O_SPAMMING_MAILER_CODES'),
(7, 'O_EXEC_MAILICIOUS_CODES'),
(8, 'O_OTHER_MAILICIOUS_CODES'),
(9, 'O_CLAMAV');

