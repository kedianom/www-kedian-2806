--
-- Table structure for `wp_wpcw_user_progress`
--

CREATE TABLE `wp_wpcw_user_progress` (
  `user_id` int(11) unsigned NOT NULL,
  `unit_id` int(11) unsigned NOT NULL,
  `unit_completed_date` datetime DEFAULT NULL,
  `unit_completed_status` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`,`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

