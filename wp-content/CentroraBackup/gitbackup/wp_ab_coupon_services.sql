--
-- Table structure for `wp_ab_coupon_services`
--

CREATE TABLE `wp_ab_coupon_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `service_id` (`service_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

