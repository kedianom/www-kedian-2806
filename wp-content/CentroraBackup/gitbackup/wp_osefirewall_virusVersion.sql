--
-- Table structure for `wp_osefirewall_virusVersion`
--

CREATE TABLE `wp_osefirewall_virusVersion` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(200) NOT NULL,
  `plugin` varchar(200) NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

