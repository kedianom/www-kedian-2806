--
-- Table structure for `wp_indeed_members_payments`
--

CREATE TABLE `wp_indeed_members_payments` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(100) DEFAULT NULL,
  `u_id` int(9) DEFAULT NULL,
  `payment_data` text,
  `history` text,
  `paydate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

