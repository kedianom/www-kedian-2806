--
-- Table structure for `wp_ab_schedule_item_breaks`
--

CREATE TABLE `wp_ab_schedule_item_breaks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_schedule_item_id` int(10) unsigned NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_schedule_item_id` (`staff_schedule_item_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

