--
-- Table structure for `wp_wpcw_question_tags_map`
--

CREATE TABLE `wp_wpcw_question_tags_map` (
  `question_id` bigint(20) unsigned NOT NULL,
  `tag_id` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `question_tag_id` (`question_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

