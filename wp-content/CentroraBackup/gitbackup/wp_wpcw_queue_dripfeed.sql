--
-- Table structure for `wp_wpcw_queue_dripfeed`
--

CREATE TABLE `wp_wpcw_queue_dripfeed` (
  `queue_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue_unit_id` int(11) unsigned NOT NULL,
  `queue_course_id` int(11) unsigned NOT NULL,
  `queue_user_id` int(11) NOT NULL,
  `queue_trigger_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `queue_enqueued_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

