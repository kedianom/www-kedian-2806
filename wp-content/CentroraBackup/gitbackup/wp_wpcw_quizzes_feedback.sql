--
-- Table structure for `wp_wpcw_quizzes_feedback`
--

CREATE TABLE `wp_wpcw_quizzes_feedback` (
  `qfeedback_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `qfeedback_tag_id` bigint(20) unsigned NOT NULL,
  `qfeedback_quiz_id` int(1) unsigned NOT NULL,
  `qfeedback_summary` varchar(300) NOT NULL,
  `qfeedback_score_type` varchar(20) NOT NULL DEFAULT 'below',
  `qfeedback_score_grade` int(11) unsigned NOT NULL DEFAULT '50',
  `qfeedback_message` text NOT NULL,
  PRIMARY KEY (`qfeedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

