--
-- Table structure for `wp_rg_lead_detail`
--

CREATE TABLE `wp_rg_lead_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` int(10) unsigned NOT NULL,
  `form_id` mediumint(8) unsigned NOT NULL,
  `field_number` float NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `lead_id` (`lead_id`),
  KEY `lead_field_number` (`lead_id`,`field_number`),
  KEY `lead_field_value` (`value`(191))
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rg_lead_detail`
--

INSERT INTO `wp_rg_lead_detail` (`id`, `lead_id`, `form_id`, `field_number`, `value`)  VALUES 
(1, 1, 1, 1, 'Vinay Punamiya'),
(2, 1, 1, 2, 'vnpnm637@gmail.com'),
(3, 1, 1, 3, 'Developer'),
(4, 1, 1, 4, 'testing demo testimonial final'),
(5, 1, 1, 5, 'http://www.kedianomics.com/wp-content/uploads/gravity_forms/1-b9a7326992de27a0858670f2da1d245f/2016/06/182230_155652051157947_1622092_n.jpg');

