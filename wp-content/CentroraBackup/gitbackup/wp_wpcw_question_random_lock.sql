--
-- Table structure for `wp_wpcw_question_random_lock`
--

CREATE TABLE `wp_wpcw_question_random_lock` (
  `question_user_id` int(11) unsigned NOT NULL,
  `rand_question_id` int(11) unsigned NOT NULL,
  `parent_unit_id` int(11) unsigned NOT NULL,
  `question_selection_list` text NOT NULL,
  UNIQUE KEY `wpcw_question_rand_lock` (`question_user_id`,`rand_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

