--
-- Table structure for `wp_sumo_reward_encashing_submitted_data`
--

CREATE TABLE `wp_sumo_reward_encashing_submitted_data` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `userid` int(225) DEFAULT NULL,
  `userloginname` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pointstoencash` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pointsconvertedvalue` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encashercurrentpoints` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reasonforencash` longtext COLLATE utf8mb4_unicode_ci,
  `encashpaymentmethod` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypalemailid` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otherpaymentdetails` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

