--
-- Table structure for `wp_ab_customer_appointments`
--

CREATE TABLE `wp_ab_customer_appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `appointment_id` int(10) unsigned NOT NULL,
  `location_id` int(10) unsigned DEFAULT NULL,
  `number_of_persons` int(10) unsigned NOT NULL DEFAULT '1',
  `extras` text,
  `custom_fields` text,
  `status` enum('pending','approved','cancelled','rejected') NOT NULL DEFAULT 'approved',
  `token` varchar(255) DEFAULT NULL,
  `time_zone_offset` int(11) DEFAULT NULL,
  `locale` varchar(8) DEFAULT NULL,
  `payment_id` int(10) unsigned DEFAULT NULL,
  `compound_service_id` int(10) unsigned DEFAULT NULL,
  `compound_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `appointment_id` (`appointment_id`),
  KEY `payment_id` (`payment_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

