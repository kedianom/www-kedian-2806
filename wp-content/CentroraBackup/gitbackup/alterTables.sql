--
-- Alter table structure for `wp_ab_appointments`
--

ALTER TABLE `wp_ab_appointments` ADD   CONSTRAINT `wp_ab_appointments_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `wp_ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_appointments` ADD   CONSTRAINT `wp_ab_appointments_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `wp_ab_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_appointments` ADD   CONSTRAINT `wp_ab_appointments_ibfk_3` FOREIGN KEY (`series_id`) REFERENCES `wp_ab_series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_appointments` ADD   CONSTRAINT `wp_ab_appointments_ibfk_4` FOREIGN KEY (`series_id`) REFERENCES `wp_ab_series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_appointments` ADD   CONSTRAINT `wp_ab_appointments_ibfk_5` FOREIGN KEY (`series_id`) REFERENCES `wp_ab_series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_coupon_services`
--

ALTER TABLE `wp_ab_coupon_services` ADD   CONSTRAINT `wp_ab_coupon_services_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `wp_ab_coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_coupon_services` ADD   CONSTRAINT `wp_ab_coupon_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `wp_ab_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_customer_appointments`
--

ALTER TABLE `wp_ab_customer_appointments` ADD   CONSTRAINT `wp_ab_customer_appointments_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `wp_ab_customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_customer_appointments` ADD   CONSTRAINT `wp_ab_customer_appointments_ibfk_2` FOREIGN KEY (`appointment_id`) REFERENCES `wp_ab_appointments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_customer_appointments` ADD   CONSTRAINT `wp_ab_customer_appointments_ibfk_3` FOREIGN KEY (`payment_id`) REFERENCES `wp_ab_payments` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;


--
-- Alter table structure for `wp_ab_holidays`
--

ALTER TABLE `wp_ab_holidays` ADD   CONSTRAINT `wp_ab_holidays_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `wp_ab_staff` (`id`) ON DELETE CASCADE;


--
-- Alter table structure for `wp_ab_schedule_item_breaks`
--

ALTER TABLE `wp_ab_schedule_item_breaks` ADD   CONSTRAINT `wp_ab_schedule_item_breaks_ibfk_1` FOREIGN KEY (`staff_schedule_item_id`) REFERENCES `wp_ab_staff_schedule_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_sent_notifications`
--

ALTER TABLE `wp_ab_sent_notifications` ADD   CONSTRAINT `wp_ab_sent_notifications_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `wp_ab_notifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_services`
--

ALTER TABLE `wp_ab_services` ADD   CONSTRAINT `wp_ab_services_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `wp_ab_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_staff_preference_orders`
--

ALTER TABLE `wp_ab_staff_preference_orders` ADD   CONSTRAINT `wp_ab_staff_preference_orders_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `wp_ab_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_staff_preference_orders` ADD   CONSTRAINT `wp_ab_staff_preference_orders_ibfk_2` FOREIGN KEY (`staff_id`) REFERENCES `wp_ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_staff_schedule_items`
--

ALTER TABLE `wp_ab_staff_schedule_items` ADD   CONSTRAINT `wp_ab_staff_schedule_items_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `wp_ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_staff_services`
--

ALTER TABLE `wp_ab_staff_services` ADD   CONSTRAINT `wp_ab_staff_services_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `wp_ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_staff_services` ADD   CONSTRAINT `wp_ab_staff_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `wp_ab_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ab_sub_services`
--

ALTER TABLE `wp_ab_sub_services` ADD   CONSTRAINT `wp_ab_sub_services_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `wp_ab_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_ab_sub_services` ADD   CONSTRAINT `wp_ab_sub_services_ibfk_2` FOREIGN KEY (`sub_service_id`) REFERENCES `wp_ab_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_ose_app_adminrecemail`
--

ALTER TABLE `wp_ose_app_adminrecemail` ADD   CONSTRAINT `wp_ose_app_adminrecemail_ibfk_1` FOREIGN KEY (`email_id`) REFERENCES `wp_ose_app_email` (`id`) ON UPDATE CASCADE;
ALTER TABLE `wp_ose_app_adminrecemail` ADD   CONSTRAINT `wp_ose_app_adminrecemail_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `wp_ose_app_admin` (`id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_acl`
--

ALTER TABLE `wp_osefirewall_acl` ADD   CONSTRAINT `wp_osefirewall_acl_ibfk_1` FOREIGN KEY (`referers_id`) REFERENCES `wp_osefirewall_referers` (`id`) ON UPDATE CASCADE;
ALTER TABLE `wp_osefirewall_acl` ADD   CONSTRAINT `wp_osefirewall_acl_ibfk_2` FOREIGN KEY (`pages_id`) REFERENCES `wp_osefirewall_pages` (`id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_adminemails`
--

ALTER TABLE `wp_osefirewall_adminemails` ADD   CONSTRAINT `wp_osefirewall_adminemails_ibfk_1` FOREIGN KEY (`D_id`) REFERENCES `wp_osefirewall_domains` (`D_id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_advancepatterns`
--

ALTER TABLE `wp_osefirewall_advancepatterns` ADD   CONSTRAINT `wp_osefirewall_advancepatterns_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `wp_osefirewall_vstypes` (`id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_detattacktype`
--

ALTER TABLE `wp_osefirewall_detattacktype` ADD   CONSTRAINT `wp_osefirewall_detattacktype_ibfk_1` FOREIGN KEY (`attacktypeid`) REFERENCES `wp_osefirewall_attacktype` (`id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_detcontdetail`
--

ALTER TABLE `wp_osefirewall_detcontdetail` ADD   CONSTRAINT `wp_osefirewall_detcontdetail_ibfk_1` FOREIGN KEY (`var_id`) REFERENCES `wp_osefirewall_vars` (`id`) ON UPDATE CASCADE;
ALTER TABLE `wp_osefirewall_detcontdetail` ADD   CONSTRAINT `wp_osefirewall_detcontdetail_ibfk_2` FOREIGN KEY (`detattacktype_id`) REFERENCES `wp_osefirewall_detattacktype` (`id`) ON UPDATE CASCADE;
ALTER TABLE `wp_osefirewall_detcontdetail` ADD   CONSTRAINT `wp_osefirewall_detcontdetail_ibfk_3` FOREIGN KEY (`detcontent_id`) REFERENCES `wp_osefirewall_detcontent` (`id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_detected`
--

ALTER TABLE `wp_osefirewall_detected` ADD   CONSTRAINT `wp_osefirewall_detected_ibfk_1` FOREIGN KEY (`acl_id`) REFERENCES `wp_osefirewall_acl` (`id`) ON UPDATE CASCADE;
ALTER TABLE `wp_osefirewall_detected` ADD   CONSTRAINT `wp_osefirewall_detected_ibfk_2` FOREIGN KEY (`detattacktype_id`) REFERENCES `wp_osefirewall_detattacktype` (`id`) ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_fileuploadlog`
--

ALTER TABLE `wp_osefirewall_fileuploadlog` ADD   CONSTRAINT `wp_osefirewall_fileuploadlog_ibfk_1` FOREIGN KEY (`ip_id`) REFERENCES `wp_osefirewall_acl` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `wp_osefirewall_fileuploadlog` ADD   CONSTRAINT `wp_osefirewall_fileuploadlog_ibfk_2` FOREIGN KEY (`file_type_id`) REFERENCES `wp_osefirewall_fileuploadext` (`ext_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table structure for `wp_osefirewall_iptable`
--

ALTER TABLE `wp_osefirewall_iptable` ADD   CONSTRAINT `wp_osefirewall_iptable_ibfk_1` FOREIGN KEY (`acl_id`) REFERENCES `wp_osefirewall_acl` (`id`) ON UPDATE CASCADE;


