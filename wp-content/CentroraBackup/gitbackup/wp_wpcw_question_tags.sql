--
-- Table structure for `wp_wpcw_question_tags`
--

CREATE TABLE `wp_wpcw_question_tags` (
  `question_tag_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question_tag_name` varchar(150) NOT NULL,
  `question_tag_usage` int(11) unsigned NOT NULL,
  `question_tag_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

