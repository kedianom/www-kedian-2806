--
-- Table structure for `wp_wpcw_certificates`
--

CREATE TABLE `wp_wpcw_certificates` (
  `cert_user_id` int(11) NOT NULL,
  `cert_course_id` int(11) NOT NULL,
  `cert_access_key` varchar(50) NOT NULL,
  `cert_generated` datetime NOT NULL,
  UNIQUE KEY `cert_user_id` (`cert_user_id`,`cert_course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

