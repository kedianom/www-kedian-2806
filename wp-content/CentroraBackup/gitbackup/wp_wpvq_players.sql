--
-- Table structure for `wp_wpvq_players`
--

CREATE TABLE `wp_wpvq_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quizId` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `result` text NOT NULL,
  `date` int(11) NOT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

