--
-- Table structure for `wp_wpcw_quizzes_questions_map`
--

CREATE TABLE `wp_wpcw_quizzes_questions_map` (
  `parent_quiz_id` bigint(20) unsigned DEFAULT NULL,
  `question_id` bigint(20) unsigned NOT NULL,
  `question_order` int(11) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `question_assoc_id` (`parent_quiz_id`,`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

