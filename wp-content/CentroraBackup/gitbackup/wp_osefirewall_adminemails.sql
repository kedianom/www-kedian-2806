--
-- Table structure for `wp_osefirewall_adminemails`
--

CREATE TABLE `wp_osefirewall_adminemails` (
  `A_id` int(11) NOT NULL AUTO_INCREMENT,
  `A_name` text NOT NULL,
  `A_email` text NOT NULL,
  `A_status` varchar(10) NOT NULL,
  `D_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`A_id`),
  KEY `wp_osefirewall_adminemails_idx1` (`D_id`)) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_osefirewall_adminemails`
--

INSERT INTO `wp_osefirewall_adminemails` (`A_id`, `A_name`, `A_email`, `A_status`, `D_id`)  VALUES 
(1, 'SushilKedia', 'sushilkedia@gmail.com', 'active', 1),
(2, 'SushilKedia', 'sushilkedia@gmail.com', 'active', 3);

