--
-- Table structure for `wp_ihc_debug_payments`
--

CREATE TABLE `wp_ihc_debug_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(200) DEFAULT NULL,
  `message` text,
  `insert_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

