--
-- Table structure for `wp_ab_staff_services`
--

CREATE TABLE `wp_ab_staff_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `deposit` varchar(100) NOT NULL DEFAULT '100%',
  `capacity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_ids_idx` (`staff_id`,`service_id`),
  KEY `service_id` (`service_id`)) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_ab_staff_services`
--

INSERT INTO `wp_ab_staff_services` (`id`, `staff_id`, `service_id`, `price`, `deposit`, `capacity`)  VALUES 
(5, 1, 1, 100, '100%', 1),
(6, 1, 2, 200, '100%', 2),
(7, 2, 3, 100, '100%', 4);

