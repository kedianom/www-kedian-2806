--
-- Table structure for `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_item_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`)  VALUES 
(1, 'Test Product', 'line_item', 11243),
(2, 'Test Product', 'line_item', 11244),
(3, 'Example One-time Product', 'line_item', 12857),
(4, 'Test Product', 'line_item', 12867),
(5, 'Platinum Members', 'line_item', 12978),
(8, 'Test Product', 'line_item', 13047),
(9, 'Consultancy Service', 'line_item', 13110),
(10, 'Consultancy Service', 'line_item', 13110),
(16, 'Consultancy Service', 'line_item', 13113),
(15, 'Consultancy Service', 'line_item', 13113),
(17, 'Consultancy Service', 'line_item', 13153),
(18, 'Paid Product (DAP)', 'line_item', 13153),
(19, 'Consultancy Service', 'line_item', 13155),
(20, 'Consultancy Service', 'line_item', 13159),
(21, 'Consultancy Service', 'line_item', 13160),
(22, 'Consultancy Service', 'line_item', 13160),
(23, 'Consultancy Service', 'line_item', 13160);

