<?php
	if( Dap_Session::isLoggedIn() ) { 
		$session = Dap_Session::getSession();
		$user = $session->getUser();
	}
?>
<script language="JavaScript" type="text/javascript" src="/dap/javascript/ajaxWrapper.js"></script>
<script language="JavaScript" type="text/javascript" src="/dap/javascript/common.js"></script>
<script language="javascript" type="text/javascript" src="/dap/javascript/jsstrings.php"></script>
<script language="JavaScript" type="text/javascript">

var affId = <?php echo $user->getId(); ?>;
var globalError = "";
var whatRequest = "";
var site_url = "<?php echo SITE_URL_DAP; ?>";

function processChangea(responseText, responseStatus, responseXML) {
	if (responseStatus == 200) {// 200 means "OK"
		var resource = eval('(' + responseText + ')');
		if(resource.whatRequest == 'loadAffiliatePayments') {
			changeDiv('aff_payments_div',resource.responseJSON);
		} else if(resource.whatRequest == 'loadAffiliateEarningsSummary') {
			changeDiv('aff_earnings_summary_div',resource.responseJSON);
		} else if(resource.whatRequest == 'loadAffiliateEarningsDetails') { 
			changeDiv('aff_earnings_details_div',resource.responseJSON);
		} else if(resource.whatRequest == 'loadUser') {
			changeLoadUser(resource.responseJSON);
		} else if(resource.whatRequest == 'updateUser') {
			changeDiv('user_msg_div',resource.responseJSON);
			NLBfadeBg('user_msg_div','#009900','#FFFFFF','1000');
			loadUser(affId);
		}  else if(resource.whatRequest == 'loadAffiliateStats') {
			changeDiv('aff_stats_div',resource.responseJSON);
		}  else if(resource.whatRequest == 'loadAffiliatePerformanceSummary') {
			changeDiv('aff_perf_summary_div',resource.responseJSON);
		}
	} else {// anything else means a problem
		//alert("There was a problem in the returned data:\n");
	}
}


function loadAffiliateStats() {
	changeDiv('aff_stats_div','Please wait... Loading Affiliate Stats ...<br><img src="/dap/images/progressbar.gif">');
	url  =  '/dap/admin/ajax/loadAffiliateStatsAjax.php';
	var request = new ajaxObject(url,processChangea);
	request.update('email=<?php echo $user->getEmail(); ?>' + '&whatRequest=loadAffiliateStats');
}


function loadAffiliatePayments() {
	changeDiv('aff_payments_div','Please wait... Loading Affiliate Payments ...<br><img src="/dap/images/progressbar.gif">');
	url = '/dap/admin/ajax/loadAffiliatePaymentsAjax.php';
	request = new ajaxObject(url,processChangea);
	request.update('email=<?php echo $user->getEmail(); ?>' + '&whatRequest=loadAffiliatePayments');
}

function loadAffiliateEarningsSummary() {
	changeDiv('aff_earnings_summary_div','Please wait... Loading Earnings Summary ...<br><img src="/dap/images/progressbar.gif">');
	url = '/dap/admin/ajax/loadAffiliateEarningsSummaryAjax.php';
	request = new ajaxObject(url,processChangea);
	request.update('email=<?php echo $user->getEmail(); ?>' + '&whatRequest=loadAffiliateEarningsSummary');
}

function loadAffiliateEarningsDetails() {
	changeDiv('aff_earnings_details_div','Please wait... Loading Earnings Details ...<br><img src="/dap/images/progressbar.gif">');
	url = '/dap/admin/ajax/loadAffiliateEarningsDetailsAjax.php';
	request = new ajaxObject(url,processChangea);
	request.update('email=<?php echo $user->getEmail(); ?>' + '&whatRequest=loadAffiliateEarningsDetails','POST');
}

function loadAffiliatePerformanceSummary() {
	showLongProgressBar('aff_perf_summary_div', 'Please wait... Loading Your Affiliate Performance Summary...<br>');
	url  =  '/dap/admin/ajax/loadAffiliateInfoPerformanceSummaryAjax.php';
	var start_date = '01-01-2008';
	var request = new ajaxObject(url,processChangea);
	request.update(
		'email=<?php echo $user->getEmail(); ?>' + 
		'&start_date=' + start_date + 
		'&whatRequest=loadAffiliatePerformanceSummary'
	);
	return;
}

</script>

<style>@media only screen and (max-width: 767px) {  
  		/* Force table to not be like tables anymore */		
		.responsive-table-element table, .responsive-table-element thead, .responsive-table-element tbody, .responsive-table-element th, .responsive-table-element td, .responsive-table-element tr, #responsive-table-element table, #responsive-table-element table, #responsive-table-element thead, #responsive-table-element tbody, #responsive-table-element th, #responsive-table-element td, #responsive-table-element tr { 
		display: block; 
		font-size: 12px;
		margin: 0;  
		min-height: 35px; 
		padding: 0; 
		word-wrap: break-word;
		} 	
		/* Hide table headers (but not display: none;, for accessibility) */

		.responsive-table-element thead tr { 
		position: absolute;		
		top: -9999px;	
		left: -9999px;	
		} 	
		.responsive-table-element tr {
			border: 1px solid #ccc; 
			} 
		.responsive-table-element td { 	
		/* Behave  like a "row" */		
		border: none;		
		border-bottom: 1px solid #eee;
		position: relative;	
		white-space: normal;	
		text-align:left;	
		min-height: 20px;	
		padding:5px;	
		min-height: 25px;	
		padding: 5px 5px 5px 160px !important;
		} 	
		
		.responsive-table-element td:before { 
		/* Now like a table header */
		position: absolute;		
		/* Top/left values mimic padding */	
		top: 6px;		
		left: 6px;	
		width: 45%; 	
		padding-right: 10px; 	
		white-space: nowrap;
		text-align:left;	
		font-weight: bold;
		}       
		/*    	Label the data    	*/
		
		.responsive-table-element td:before { 
		content: attr(data-title);
		}
		}
		.scriptsubheading td {  
		white-space: nowrap;
		}		
		.table.dap_affiliate_table_main {
		background-color:inherit;
		}
		.responsive-table-element table {
		border-style: solid;
		border-width: 1px;
		border-color:rgb(230, 230, 230);
		margin: 0px
		}

.dap_affinfo_table_main span input, .dap_affinfo_table_main p input {  float: left;  width: 100%;  box-sizing: border-box;}			

#responsive-table-element span input, #responsive-table-element p input {  float: left;  width: 100%;  box-sizing: border-box;}
	
.responsive-table-element {overflow: auto;}

.responsive-table-element th {font-size: 14px;line-height: 20px;}

.responsive-table-element tr {font-size: 12px;line-height: normal;}

</style>


<?php getCss(); ?>

<form name="AffLinksForm" id="AffLinksForm">
  <div id="affiliate_details_div"> <a name="affdetails" id="affdetails"></a>	<div id="responsive-table-element">
    <div class="dap_affinfo_table_main">
         <div class="affinfo_section_heading"><?php echo AFFILIATE_INFO_PERFSUMM_SUBHEADING; ?></div>
         <div id="aff_perf_summary_div" align="center"  class="responsive-table-element"></div>
          </div>
          
          <table width="98%"  border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td style="padding-bottom:10px;padding-top:30px"><div class="affinfo_section_heading"><?php echo AFFILIATE_INFO_AFFLINK_SUBHEADING; ?></div></td>
            </tr>
            <tr>
              <td style="padding-top:0px"><p align="left"><span class="affinfo_sub_heading" style="padding-left:5px"><?php echo AFFILIATE_INFO_YOURAFFLINKHOME_LABEL; ?></span><br>
                <span style="padding-left:5px"> <input name="textfield2" onClick="this.select();" type="text" value="<?php echo SITE_URL_DAP . "/dap/a/?a=".$user->getId(); ?>" size="60"></span>
                  <a style="padding-left:5px" href="<?php echo SITE_URL_DAP . "/dap/a/?a=".$user->getId(); ?>" target="_blank"><?php echo AFFILIATE_INFO_TEST_TEXT; ?></a></p>
                <p align="left" class="affinfo_sub_heading" style="padding-left:5px"><?php echo AFFILIATE_INFO_AFFLINKSPECIFIC_LABEL; ?></p>
                <p align="left" class="regulartextLarge" style="padding-left:5px"><?php echo AFFILIATE_INFO_AFFLINKSPECIFIC_EXTRA_TEXT; ?><br>
                <input name="textfield22" style="padding-left:5px" onClick="this.select();" type="text" value="<?php echo SITE_URL_DAP . "/dap/a/?a=".$user->getId(); ?>&p=www.example.com/somepage.html" size="80">
                </p></td>
            </tr>
          </table>

          <table width="98%"  border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td style="padding-bottom:10px;padding-top:30px"><div class="affinfo_section_heading"><?php echo AFFILIATE_INFO_PAYMENT_DETAILS_SUBHEADING; ?></div></td>
            </tr>
            <tr>
              <td style="padding-top:0px"><div id="aff_payments_div" class="responsive-table-element"></div></td>
            </tr>
          </table>

          <table width="98%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td style="padding-bottom:0px;padding-top:30px"><div class="affinfo_section_heading"><?php echo AFFILIATE_INFO_EARNINGS_DETAILS_SUBHEADING; ?></div></td>
            </tr>
            <tr>
              <td style="padding-top:0px"><div id="aff_earnings_details_div" class="responsive-table-element"></div></td>
            </tr>
          </table>
          <?php 
	  	if(!$session->isAdmin()) { ?>
          <script>loadAffiliateEarningsDetails();</script>
          <?php } else { ?>
          
          <script>document.getElementById('aff_earnings_details_div').innerHTML = '<br><strong><span style="padding-left:5px">NOTE: You are seeing this message only because you are a DAP Admin</span></strong><br/><br/><span style="padding-left:5px">Sorry, this table has too much data to view within WordPress. <a href="/dap/">Click here to view it on your Admin Home Page</a></span>';</script>
          <?php } ?>
        
          <div width="98%"  border="0" cellspacing="0" cellpadding="5">
              <div style="padding-bottom:10px;padding-top:30px"><div class="affinfo_section_heading"><?php echo AFFILIATE_INFO_TRAFFIC_STATISTICS_SUBHEADING; ?></div></div>
             <div id="aff_stats_div" style="position:relative; width:100%; height:300px; background-color:#ffffff; overflow:scroll;" class="responsive-table-element"></div>
          </div>
		  </td>
      </tr>
    </table>    </div>
  </div>
</form>
<script language="JavaScript" type="text/javascript">
loadAffiliatePayments();
//loadAffiliateEarningsSummary();
loadAffiliateStats();
loadAffiliatePerformanceSummary();
</script>
