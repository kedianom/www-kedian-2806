<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_REQUEST['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

	$lldocroot = defined('SITEROOT') ? SITEROOT : $_SERVER['DOCUMENT_ROOT'];
	if(file_exists($lldocroot . "/dap/dap-config.php")) include_once ($lldocroot . "/dap/dap-config.php");
	//error_reporting(-1);
	// foreach($_REQUEST as $loc=>$item){
		 // $_REQUEST[$loc] = urldecode(base64_decode($item)); // to decode the encoded params
	// }
   
	
	
	if( !Dap_Session::isLoggedIn() ) { 
		//send viewer to login page
		exit;
	}
	else if( Dap_Session::isLoggedIn() ) { 
		//get userid
		$session = Dap_Session::getSession();
		$user = $session->getUser();
		$user = Dap_User::loadUserById($user->getId()); //reload User object
		if(!isset($user)) {
			//send viewer to login page
			exit;
		} 
	}
	
	$admin_email = $_SESSION['invadminemail'];
	if(!isset($admin_email) && $admin_email == "") {
		$admin_email = Dap_Config::get("ADMIN_EMAIL");
	}
	
	$companyname = $_SESSION['invcompanyname'];
	if(!isset($companyname) && $companyname == "") {
		$companyname = Dap_Config::get("SITE_URL_DAP");
	}
	$companyurl = $_SESSION['invcompanyurl'];
	if(!isset($companyurl) && $companyurl == "") {
		$companyurl = Dap_Config::get("SITE_URL_DAP");
	}
	 //$product = Dap_Product::loadProduct($_SESSION['invpid']);
	 $product = Dap_Product::loadProduct($_REQUEST["productId"]);
	 
	 if($product) {
		$itemName=$product->getName();
		$description=$product->getDescription();
	 }		
//	logToFile("loadUserByEmail: productId=".$_REQUEST["productId"],LOG_DEBUG_DAP);
//	logToFile("loadUserByEmail: description=".$description,LOG_DEBUG_DAP);
	
//	logToFile("loadUserByEmail: logo=".$_SESSION['invlogo'],LOG_DEBUG_DAP);

	// get the HTML
    ob_start();
	$template=$_SESSION['invinvoicetemplate'];
	$lldocroot = defined('SITEROOT') ? SITEROOT : $_SERVER['DOCUMENT_ROOT'];
	$fulltemplate=$lldocroot ."/wp-content/plugins/DAP-WP-LiveLinks/includes/transactions/invoices/".$template."/invoicetemplate.html";
	
		
    //include(dirname(__FILE__).'/res/exemple01.php');
    //include($blogpath. "/invoices/".$_REQUEST['invtemplate']."/invoicetemplate.html");
	$fullcustominvoicehtml =  $lldocroot ."/wp-content/plugins/DAP-WP-LiveLinks/includes/transactions/invoices/".$template."/custominvoicetemplate.html";   
	if(file_exists($fullcustominvoicehtml)) {
		 include($fullcustominvoicehtml);
	}
	else
	{
		 include($fulltemplate);
	}
	
	$temp_content = ob_get_clean();
	//String replace for merge tags//
	$current_msg=$temp_content;
	
	logToFile("loadUserByEmail: logo=".$_SESSION['invlogo'],LOG_DEBUG_DAP);

	
	
	$current_msg = str_replace( '%%TRANSACTIONID%%', $_REQUEST['transactionId'], $current_msg); 
	$current_msg = str_replace( '%%EMAIL_ID%%', $_REQUEST['email'], $current_msg); 
	$current_msg = str_replace( '%%PAYMENTTYPE%%', "Payment: " . $_REQUEST['ptype'], $current_msg); 
	$current_msg = str_replace( '%%ITEMNAME%%', $itemName, $current_msg); 
	$current_msg = str_replace( '%%AMOUNT%%', $_REQUEST['amount'], $current_msg);
	$current_msg = str_replace( '%%FIRST_NAME%%', $_REQUEST['fname'], $current_msg);
	$current_msg = str_replace( '%%LAST_NAME%%', $_REQUEST['lname'], $current_msg);
	$companyname = str_replace('http://', "", $companyname);
	$current_msg = str_replace( '%%COMPANYNAME%%', $companyname, $current_msg); 
	$current_msg = str_replace( '%%COMPANYURL%%', $companyurl, $current_msg);
	$current_msg = str_replace( '%%ADMINEMAIL%%', $admin_email, $current_msg);  
	$current_msg = str_replace( '%%logo%%', $_REQUEST['logo'], $current_msg);
	$current_msg = str_replace( '%%TRANSACTIONDATE%%', $_REQUEST['tdate'], $current_msg);
	$current_msg = str_replace( '%%CURRENCY%%', $_REQUEST['currency'], $current_msg);
	$current_msg = str_replace( '%%DESCRIPTION%%', strip_tags($description), $current_msg);
	$content = $current_msg; 
	
    // convert in PDF
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
//      $html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content, isset($_REQUEST['vuehtml']));
        $html2pdf->Output('exemple01.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
