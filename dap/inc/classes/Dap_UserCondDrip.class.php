<?php

class Dap_UserCondDrip extends Dap_Base {

   	var $user_id;
   	var $product_id;
   	var $resource_id;
   	var $score;
   	var $result;
   	var $comments;	
			
	function getUser_id()  {
	   return $this->user_id;
	}
	function setUser_id($o) {
	  $this->user_id = $o;
	}	
	
	function getProduct_id()  {
	   return $this->product_id;
	}
	function setProduct_id($o) {
	  $this->product_id = $o;
	}	
	
	function getResource_id()  {
	   return $this->product_id;
	}
	function setResource_id($o) {
	  $this->product_id = $o;
	}	
	
	function getScore()  {
	   return $this->score;
	}
	function setScore($o) {
	  $this->score = $o;
	}	

	function getResult()  {
	   return $this->result;
	}
	function setResult($o) {
	  $this->result = $o;
	}	

	function getComments()  {
	   return $this->comments;
	}
	function setComments($o) {
	  $this->comments = $o;
	}
		
	public static function addConditionalAccess ($userId, $productId, $resourceId, $transId, $result, $score, $comments="User-Added") {
	  try {
		  logToFile("In Dap_UserCondDrip: addConditionalAccess(): Enter",LOG_DEBUG_DAP); 
		  $sql = "";
			
		  $dap_dbh = Dap_Connection::getConnection();
		  $dap_dbh->beginTransaction(); //begin the transaction
		  
		  logToFile("In addConditionalAccess userId=".$userId,LOG_DEBUG_DAP); 
		  logToFile("In addConditionalAccess productId=".$productId,LOG_DEBUG_DAP); 
		  logToFile("In addConditionalAccess resourceId=".$resourceId,LOG_DEBUG_DAP); 
		  logToFile("In addConditionalAccess result=".$result,LOG_DEBUG_DAP); 
		  logToFile("In addConditionalAccess score=".$score,LOG_DEBUG_DAP); 
		  logToFile("In addConditionalAccess comments=".$comments,LOG_DEBUG_DAP); 
		  
		  $transactionId=0;
		  
		  //Next add to credits history
		  $sql = "insert into 
					dap_users_cond_content 
				  set
					user_id =:user_id,
					product_id =:product_id,
					resource_id=:resource_id,
					result =:result,
					score =:score,
					datetime = now(),
					comments =:comments
				";
		  
		  //logToFile("SSS: $sql",LOG_DEBUG_DAP); 
		  $stmt = $dap_dbh->prepare($sql);
		  $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
		  $stmt->bindParam(':product_id', $productId, PDO::PARAM_INT);
		  $stmt->bindParam(':resource_id', $resourceId, PDO::PARAM_INT);
		  $stmt->bindParam(':result', $result, PDO::PARAM_INT);
		  $stmt->bindParam(':score', $score, PDO::PARAM_INT);
		  $stmt->bindParam(':comments', $comments, PDO::PARAM_STR);
		  $stmt->execute();
		  
		  $dap_dbh->commit(); //commit the transaction
	
	  
		  $stmt = null;
		  $dap_dbh = null;
		  
		  return;
	  } catch (PDOException $e) {
		if(stristr($e->getMessage(), "Dap_UsersCredits:addConditionalAccess(): SQLSTATE[23000]: Integrity constraint violation: ") == FALSE) 
		{
		  logToFile($e->getMessage(),LOG_FATAL_DAP);
		  $dap_dbh->rollback();
		  throw $e;
		  return false;
		}
		else {
			logToFile("Dap_UsersCredits:addConditionalAccess(): ignore integrity constraint error: " . $e->getMessage(),LOG_DEBUG_DAP);
		}
   
	  } catch (Exception $e) {
		$dap_dbh->rollback();
		logToFile($e->getMessage(),LOG_FATAL_DAP);
		throw $e;
	  }
	  
	}
	
	
	public static function updateConditionalAccessResult ($userId, $productId, $resourceId, $transId, $result, $score, $comments="User-Added") {
	  try {
		  logToFile("In Dap_UserCondDrip: updateConditionalAccessResult(): Enter",LOG_DEBUG_DAP); 
		  $sql = "";
			
		  $dap_dbh = Dap_Connection::getConnection();
		  $dap_dbh->beginTransaction(); //begin the transaction
		  
		  logToFile("In updateConditionalAccessResult userId=".$userId,LOG_DEBUG_DAP); 
		  logToFile("In updateConditionalAccessResult productId=".$productId,LOG_DEBUG_DAP); 
		  logToFile("In addConditionalAccess resourceId=".$resourceId,LOG_DEBUG_DAP); 
		  logToFile("In updateConditionalAccessResult result=".$result,LOG_DEBUG_DAP); 
		  logToFile("In updateConditionalAccessResult score=".$score,LOG_DEBUG_DAP); 
		  logToFile("In updateConditionalAccessResult comments=".$comments,LOG_DEBUG_DAP); 
		  
		  $transactionId=0;
		  
		  //Next add to credits history
		/*  $sql = "update
					dap_users_cond_content 
				  set
					result =:result,
					datetime = now(),
					comments =:comments
				  where
					user_id =:user_id and
					product_id =:product_id and
					resource_id=:resource_id
				";
		  */
		  
		  $sql = "update
					dap_users_cond_content 
				  set
					result =:result,
					datetime = now(),
					comments =:comments
				  where
					user_id =:user_id and
					resource_id=:resource_id
				";
		  //logToFile("SSS: $sql",LOG_DEBUG_DAP); 
		  $stmt = $dap_dbh->prepare($sql);
		  
		  $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
		 // $stmt->bindParam(':product_id', $productId, PDO::PARAM_INT);
		  $stmt->bindParam(':resource_id', $resourceId, PDO::PARAM_INT);
		  $stmt->bindParam(':result', $result, PDO::PARAM_STR);
		  $stmt->bindParam(':comments', $comments, PDO::PARAM_STR);
		 
		  $stmt->execute();
		  
		  $dap_dbh->commit(); //commit the transaction
	
		  $stmt = null;
		  $dap_dbh = null;
		  
		  return;
	  } catch (Exception $e) {
		$dap_dbh->rollback();
		logToFile($e->getMessage(),LOG_FATAL_DAP);
		throw $e;
	  }
	  
	}
	
	
	//For a given user, load credit history
	public static function loadUserCondDripHistory($userId,$productId="") {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			$userCondDripHistoryArray = array();
			$sql = "SELECT 
						uc.user_id,
						uc.product_id,
						uc.resource_id,
						uc.result,
						uc.score,
						uc.datetime,
						uc.comments
					FROM 
						dap_users_cond_content uc
					WHERE
						uc.user_id = :userId";
						
			if($productId>0) {			
				$sql .=" and uc.product_id=:productId";			
			}
			
			$sql .=	" order by 
						datetime desc";

			logToFile($sql,LOG_DEBUG_DAP);
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			if($productId>0)
				$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			
			$stmt->execute();	
			
			while($obj = $stmt->fetch(PDO::FETCH_OBJ)) {
				$userCondDripHistoryArray[] = $obj;
					
			}
			
			$stmt = null;
			$dap_dbh = null;
			
			return $userCondDripHistoryArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}
	}

	public static function hasAccessTo($userId, $productId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			$sql = "SELECT 
				count(*) as count
			FROM 
				dap_users_cond_content duc			
			where
				duc.product_id = :productId and
				duc.user_id = :userId";
				
			//logToFile($sql,LOG_DEBUG_DAP);
				
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			
			$stmt->execute();	
			$found=false;	
			logToFile("Dap_UserCondDrip::hasAccessTo: $found");
			  
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$count=$row["count"];
				logToFile("Dap_UserCondDrip::count=" . $count);
				if($count>0){ 
				  $found=true;
				  logToFile("Dap_UserCondDrip::hasAccessTo: FOUND IT");
				}
				
				$dap_dbh = null;
			}
			return $found;
					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}
	
	
	public static function hasAccessToResources($userId, $productId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			$sql = "SELECT 
				count(*) as count
			FROM 
				dap_users_cond_content duc			
			where
				duc.product_id = :productId and
				duc.user_id = :userId and 
				(duc.resource_id is NOT NULL and duc.resource_id != 0)";
				
			//logToFile($sql,LOG_DEBUG_DAP);
				
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			
			$stmt->execute();	
			$found=false;	
			logToFile("Dap_UserCondDrip::hasAccessTo: $found");
			  
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$count=$row["count"];
				logToFile("Dap_UserCondDrip::count=" . $count);
				if($count>0){ 
				  $found=true;
				  logToFile("Dap_UserCondDrip::hasAccessTo: FOUND IT");
				}
				
				$dap_dbh = null;
			}
			return $found;
					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}
	
	public static function hasAccessToResourcesCount($userId, $productId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			$sql = "SELECT 
				count(*) as count
			FROM 
				dap_users_cond_content duc			
			where
				duc.product_id = :productId and
				duc.user_id = :userId and 
				(duc.resource_id is NOT NULL and duc.resource_id != 0)";
				
			//logToFile($sql,LOG_DEBUG_DAP);
				
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			
			$stmt->execute();	
			$found=false;	
			logToFile("Dap_UserCondDrip::hasAccessTo: $found");
			$count=0;
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$count=$row["count"];
				logToFile("Dap_UserCondDrip::count=" . $count);
				if($count>0){ 
				  $found=true;
				  logToFile("Dap_UserCondDrip::hasAccessTo: FOUND IT");
				}
				
				$dap_dbh = null;
			}
			return $count;
					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}
	
	
	
	public static function findAndAddNextConditionalDrip($userId, $productId, $rettype="array",$add="Y",$comments="User Activated") {
		try {

			//Load resource details from database
			$dap_dbh = Dap_Connection::getConnection();
			$dap_dbh->beginTransaction(); //begin the transaction
			 
			$sql = "SELECT fr.id, fr.url, fr.name, fr.display_in_list, prj.id AS ID, prj.resource_id AS resource_id, prj.cond_drip_order AS cond_drip_order, prj.display_order AS display_order
			  FROM dap_file_resources fr, dap_products_resources_jn prj, dap_users_products_jn upj
			  WHERE fr.id = prj.resource_id
			  AND upj.user_id =:userId
			  AND upj.product_id =:productId
			  AND prj.product_id =:productId
			  AND prj.resource_type = 'F'
			  AND prj.cond_drip_order
			  IN (
			  
			  SELECT min( cond_drip_order )
			  FROM dap_products_resources_jn dpr
			  WHERE dpr.product_id =:productId
			  AND dpr.cond_drip_order >0
			  AND dpr.resource_id NOT
			  IN (
				SELECT resource_id
				FROM dap_users_cond_content duc
				WHERE duc.user_id =:userId
				AND duc.product_id =:productId
			  )
			  )
			  ORDER BY prj.cond_drip_order, prj.display_order";
			
			
			$stmt = $dap_dbh->prepare($sql);
			
			logToFile("Dap_UserCondDrip.class.php: findAndAddNextConditionalDrip(): userId=". $productId,LOG_DEBUG_DAP);
			logToFile("Dap_UserCondDrip.class.php: findAndAddNextConditionalDrip(): productId=". $productId,LOG_DEBUG_DAP);
			
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);		
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			
			$stmt->execute();
			
			$resourceArray = array();
			
			$i=0;
			logToFile("Dap_UserCondDrip.class.php: findAndAddNextConditionalDrip(): minconddriporder=". $minconddriporder,LOG_DEBUG_DAP);
		
			while ($obj = $stmt->fetch()) {
				//$resourceArray[] = $obj;
				$i++;
				logToFile("Dap_UserCondDrip.class.php: findAndAddNextConditionalDrip(): productId=". $productId,LOG_DEBUG_DAP);
				$resourceArray[$i]["resource_id"]=$obj['resource_id'];
				$resourceArray[$i]["cond_drip_order"]=$obj['cond_drip_order'];
				$resourceArray[$i]["id"]=$obj['ID'];
				
				logToFile("Dap_UserCondDrip.class.php: findAndAddNextConditionalDrip(): found resource: ". $obj['resource_id'],LOG_DEBUG_DAP);
				
				try {
				  if($add=="Y") {
					  Dap_UserCondDrip::addConditionalAccess ($userId, $productId, $obj['resource_id'], -1, NULL, $score, $comments="User-Initiated");
					  logToFile("Dap_UserCondDrip: addConditionalAccess: res=". $obj['resource_id'], LOG_DEBUG_DAP);
				  }
				} catch (PDOException $e) {
					if(stristr($e->getMessage(), "Dap_UserCondDrip:addConditionalAccess(): SQLSTATE[23000]: Integrity constraint violation: ") == FALSE) 
					{
					  logToFile($e->getMessage(),LOG_FATAL_DAP);
					  $dap_dbh->rollback();
					  throw $e;
					  return false;
					}
					else {
						logToFile("Dap_UserCondDrip:addConditionalAccess(): ignore integrity constraint error: " . $e->getMessage(),LOG_DEBUG_DAP);
					}
				} catch (Exception $e) {
					logToFile($e->getMessage(),LOG_FATAL_DAP);
					throw $e;
				}
				
				logToFile("Dap_UserCondDrip.class.php: loadFileResourceFirstConditionalDrip() id=". $obj["id"], LOG_DEBUG_DAP);
				logToFile("Dap_UserCondDrip.class.php: loadFileResourceFirstConditionalDrip() resource found=". $obj["name"], LOG_DEBUG_DAP);
				logToFile("Dap_UserCondDrip.class.php: loadFileResourceFirstConditionalDrip(): resourceId=". $obj['resource_id'],LOG_DEBUG_DAP);
				
			}

			
			return $i;
		
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}  catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}
	}
	
	
	public static function releaseAccessToFirstModule($userId, $productId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			//check if user purchased full product, if yes, give user access to all resourcses.
			//if no, check if  resource exists in usercredits table for that user->product
			
			$sql = "SELECT 
				count(*) as count
			FROM 
				dap_users_cond_content duc
			where
				duc.product_id = :productId and
				duc.user_id = :userId and
				(duc.result is NULL OR duc.result = '')";
						
			$usercredits="";
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			$stmt->bindParam(':resId', $resId, PDO::PARAM_INT);
				
			$stmt->execute();	
			
			$id=-1;
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$id=$row["id"];
				logToFile("Dap_UserCondDrip::checkUserAccessToResource: FOUND IT");
				
			}
			
			$dap_dbh = null;
			return $id;

					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}
	
	public static function releaseAccessToNextModule($userId, $productId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			//check if user purchased full product, if yes, give user access to all resourcses.
			//if no, check if  resource exists in usercredits table for that user->product
			
			$sql = "SELECT 
				count(*) as count
			FROM 
				dap_users_cond_content duc
				
			where
				duc.product_id = :productId and
				duc.user_id = :userId and
				(duc.result is NULL OR duc.result = '')";
						
			$usercredits="";
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			$stmt->bindParam(':resId', $resId, PDO::PARAM_INT);
				
			$stmt->execute();	
			
			$id=-1;
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$id=$row["id"];
				logToFile("Dap_UserCondDrip::checkUserAccessToResource: FOUND IT");
				
			}
			
			$dap_dbh = null;
			return $id;

					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}	
	
	public static function checkUserAccessToResourceTitle($userId, $productId, $resurl) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			//check if user purchased full product, if yes, give user access to all resourcses.
			//if no, check if  resource exists in usercredits table for that user->product
			
			$sql = "select fr.id as id, fr.url as url, fr.name as name, fr.display_in_list 
			from dap_file_resources fr
			where fr.url=:url or fr.url=:noslashurl";
			
			$noslashurl = rtrim($resurl, '/');
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':url', $resurl, PDO::PARAM_STR);
			$stmt->bindParam(':noslashurl', $noslashurl, PDO::PARAM_STR);
			$stmt->execute();	
			
			logToFile("Dap_UserCondDrip::checkUserAccessToResource: resurl=".$resurl);
			logToFile("Dap_UserCondDrip::checkUserAccessToResource: noslashurl=".$noslashurl);
			
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_UserCondDrip::checkUserAccessToResource: found in file_resource, id=".$row["id"]);
				logToFile("Dap_UserCondDrip::checkUserAccessToResource: found in file_resource, userId=".$userId);
				logToFile("Dap_UserCondDrip::checkUserAccessToResource: found in file_resource, productId=".$productId);
				
				$resId=$row["id"];
				
				/*$sql = "SELECT 
					*
				FROM 
					dap_users_cond_content duc
				where
					duc.product_id = :productId and
					duc.user_id = :userId and
					duc.resource_id = :resId";
				*/	
				
				$sql = "SELECT 
					*
				FROM 
					dap_users_cond_content duc
				where
					duc.user_id = :userId and
					duc.resource_id = :resId";
				$usercredits="";
				
			
				$stmt = $dap_dbh->prepare($sql);
				$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
				//$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
				$stmt->bindParam(':resId', $resId, PDO::PARAM_INT);
					
				$stmt->execute();	
				
				if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$id=$row["resource_id"];
					$result=$row["result"];
					if( !isset($result) || ($result!="P") )return $id;
					else if($result=="P") return -2;

					logToFile("Dap_UserCondDrip::checkUserAccessToResourceTitle: FOUND IT");
					
				}
				
				$dap_dbh = null;
				return -1;
			}
			
			$dap_dbh = null;
			return -1;
					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}	
	
						
						
	public static function checkUserAccessToResource($userId, $productId, $resourceId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			//check if user purchased full product, if yes, give user access to all resourcses.
			//if no, check if  resource exists in usercredits table for that user->product
			
			$resId=0;
			
			$sql = "SELECT 
				count(*) as count
			FROM 
				dap_users_cond_content duc
			where
				duc.product_id = :productId and
				duc.user_id = :userId and
				duc.resource_id = :resId";
			
			$usercredits="";
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			$stmt->bindParam(':resId', $resourceId, PDO::PARAM_INT);
				
			$stmt->execute();	
			$count=0;
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$count=$row["count"];
				logToFile("Dap_UserCondDrip::count=" . $count);
				if($count>0){ 
				  $found=true;
				  logToFile("Dap_UserCondDrip::checkUserAccessToResource: FOUND IT");
				}
				
				$dap_dbh = null;
			}
			
			return $count;
					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}	
	
	
//For a given user, load products that user does NOT have access to
	public static function loadResourceDetail($userId, $productId, $resourceId) {
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			//check if user purchased full product, if yes, give user access to all resourcses.
			//if no, check if  resource exists in usercredits table for that user->product
			
			$userCondDripArray[]=array();
							
			$sql = "SELECT 
				*
			FROM 
				dap_users_cond_content duc
			where
				duc.product_id = :productId and
				duc.user_id = :userId and
				duc.resource_id = :resourceId";
				
			//logToFile($sql,LOG_DEBUG_DAP);
			$usercredits="";
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			$stmt->bindParam(':resourceId', $resourceId, PDO::PARAM_INT);
				
			$stmt->execute();	
						
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$userCondDripArray[] = $obj;
				return $userCondDripArray;
			}
					
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}		
	}	
	
	public static function removeUserResources($userId, $productId) {
		try {
			logToFile("Dap_UserCondDrip.class: removeUserResources(): $userId, $productId",LOG_DEBUG_DAP); 
			$dap_dbh = Dap_Connection::getConnection();
			$sql = "delete from dap_users_cond_content where user_id = :userId and product_id = :productId";
			
			$stmt = $dap_dbh->prepare($sql);
			
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			
			$stmt->execute();
			$dap_dbh = null;
			
			return;
		} catch (PDOException $e) {
			$dap_dbh->rollback();
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			$dap_dbh->rollback();
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}	
	}
	
	public static function removeFullProduct($userId, $productId) {
		try {
			logToFile("Dap_UserCondDrip.class: removeFullProduct() : $userId, $productId",LOG_DEBUG_DAP); 

			$dap_dbh = Dap_Connection::getConnection();
			$sql = "delete from dap_users_cond_content where user_id = :userId and product_id = :productId";
			
			$stmt = $dap_dbh->prepare($sql);
			
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			
			$stmt->execute();
			
			$dap_dbh = null;

			return;
		} catch (PDOException $e) {
			$dap_dbh->rollback();
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			$dap_dbh->rollback();
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}	
	}
	
	public static function removeThisRow($userId, $productId, $resourceId, $datetime, $comments) {
		try {
			logToFile("Dap_UserCondDrip.class: removeThisRow(): $userId, $productId",LOG_DEBUG_DAP); 
			$dap_dbh = Dap_Connection::getConnection();
			
			logToFile("Dap_UserCondDrip.class: removeThisRow: datetime=".$datetime,LOG_DEBUG_DAP); 
//			logToFile("Dap_UserCondDrip.class: removeThisRow: $userId, $productId, $resourceId, $datetime, $comments",LOG_DEBUG_DAP); 
			$sql = "delete from dap_users_cond_content where user_id=:userId and product_id=:productId and resource_id=:resourceId and comments='" . $comments . "' and datetime='" . $datetime . "'" ;
			
			$stmt = $dap_dbh->prepare($sql);
			
			$stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
			$stmt->bindParam(':resourceId', $resourceId, PDO::PARAM_INT);
			//$stmt->bindParam(':comments', $comments, PDO::PARAM_STR);
		//	$stmt->bindParam(':datetime', $datetime, PDO::PARAM_STR);
			
			$stmt->execute();
			
			$dap_dbh = null;
	
			return;
		} catch (PDOException $e) {
			$dap_dbh->rollback();
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			$dap_dbh->rollback();
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}	
	}

	public static function addCourseReadyCheckToCron($actionType,$payload) {
		//  SEQ/BE-COLLECTIONCODE/SUBJECT||BODY||ATTACHMENTS(multiple attachments separated by commas)
		try {
			$dap_dbh = Dap_Connection::getConnection();
			$key = mktime();
			//Insert into dap_mass_actions

			$sql = "insert into dap_mass_actions
						(actionType, actionKey, payload, status)
						values
						(:actionType, :key, :payload, 'NEW')";		
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':actionType', $actionType, PDO::PARAM_STR);
			$stmt->bindParam(':key', $key, PDO::PARAM_STR);
			$stmt->bindParam(':payload', $payload, PDO::PARAM_STR);
			$stmt->execute();
			
			$stmt = null;
			$dap_dbh = null;
			return $key;
		} catch (PDOException $e) {
			logToFile("addCourseReadyCheckToCron: " . $e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile("addCourseReadyCheckToCron: " . $e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}	
	}
	
	public static function addAllUsersToCondProductResourceToCron($actionType,$payload) {
		//  SEQ/BE-COLLECTIONCODE/SUBJECT||BODY||ATTACHMENTS(multiple attachments separated by commas)
		try {
			$dap_dbh = Dap_Connection::getConnection();
			$key = mktime();
			//Insert into dap_mass_actions

			$sql = "insert into dap_mass_actions
						(actionType, actionKey, payload, status)
						values
						(:actionType, :key, :payload, 'NEW')";		
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':actionType', $actionType, PDO::PARAM_STR);
			$stmt->bindParam(':key', $key, PDO::PARAM_STR);
			$stmt->bindParam(':payload', $payload, PDO::PARAM_STR);
			$stmt->execute();
			
			$stmt = null;
			$dap_dbh = null;
			return $key;
		} catch (PDOException $e) {
			logToFile("bulkEmailInsertCollectionCode: " . $e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile("bulkEmailInsertCollectionCode: " . $e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}	
	}
	
		
	public static function loadAndAssignResourceToUsersOfCondDripProduct($csvname, $productId, $resourceId) {
		try {

			//Load resource details from database
			$dap_dbh = Dap_Connection::getConnection();
			$dap_dbh->beginTransaction(); //begin the transaction
			 
			$sql = "SELECT distinct(u.id) as id, u.email as email
			  FROM dap_users u, dap_products p, dap_file_resources fr, dap_products_resources_jn prj, dap_users_products_jn upj
			  WHERE fr.id=:resourceId 
			  AND fr.id = prj.resource_id
			  AND upj.product_id =:productId
			  AND prj.product_id = upj.product_id
			  AND prj.resource_type = 'F' 
			  AND prj.cond_drip_order > 0
			  AND p.id=prj.product_id 
			  AND u.id = upj.user_id";
			
			$stmt = $dap_dbh->prepare($sql);
			
			logToFile("Dap_UserCondDrip.class.php: loadAndAssignResourceToUsersOfCondDripProduct(): productId=". $productId,LOG_DEBUG_DAP);
			logToFile("Dap_UserCondDrip.class.php: loadAndAssignResourceToUsersOfCondDripProduct(): resourceId=". $resourceId,LOG_DEBUG_DAP);
			
			$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);		
			$stmt->bindParam(':resourceId', $resourceId, PDO::PARAM_INT);
			
			$stmt->execute();
			
			$resourceArray = array();
			
			$i=0;
			
			while ($row = $stmt->fetch()) {
				//$resourceArray[] = $obj;
				$i++;
				$userId=$row["id"];
				$email=$row["email"];
				
				logToFile("Dap_UserCondDrip.class.php: loadAndAssignResourceToUsersOfCondDripProduct(): userId=".$userId.", email=".$email,LOG_DEBUG_DAP);
				
				try {
					Dap_UserCondDrip::addConditionalAccess ($userId, $productId, $resourceId, -1, NULL, $score, $comments="Admin-Cron-Initiated");
					writeToFile($data,$csvname);

				} catch (PDOException $e) {
					if(stristr($e->getMessage(), "Dap_UserCondDrip:addConditionalAccess(): SQLSTATE[23000]: Integrity constraint violation: ") == FALSE) 
					{
					  logToFile($e->getMessage(),LOG_FATAL_DAP);
					  $dap_dbh->rollback();
					  throw $e;
					  return false;
					}
					else {
						logToFile("Dap_UserCondDrip:addConditionalAccess(): ignore integrity constraint error: " . $e->getMessage(),LOG_DEBUG_DAP);
					}
				} catch (Exception $e) {
					logToFile($e->getMessage(),LOG_FATAL_DAP);
					throw $e;
				}
				
				logToFile("Dap_UserCondDrip: addConditionalAccess(): addded userId=$email, prod=$productId, res=". $resourceId, LOG_DEBUG_DAP);
				
			}
			
			if($i==0)
			  $reporttext='No action taken. No users found with access to productId='.$productId;
			else 
				$reporttext='Total # of users assigned access=' . $i . ", Report uploaded - " .$csvname;	
			
			return $reporttext;
		
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}  catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}
		
		
	}
	
	
}
?>