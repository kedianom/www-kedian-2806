<?php

class Dap_Reports extends Dap_Base {

	public static function loadUniqueActiveMembersPerPeriod($start_date, $end_date, $group) {
		try {
			
			$dap_dbh = Dap_Connection::getConnection();
			
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			
			$stmt = null;
			$sql = "SELECT 
							count(*) as active,
							YEAR(upj.access_start_date) as ty,
							MONTH(upj.access_start_date) as tm
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = p.id and
							CURDATE() <= upj.access_end_date and
							CURDATE() >= upj.access_start_date and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')";
				
			if($group=="month") {		
						$sql.=" GROUP BY
							YEAR(upj.access_start_date),MONTH(upj.access_start_date)
				  	    ORDER BY
						upj.access_start_date, upj.product_id";
					}
			else {
						$sql.=" GROUP BY
							YEAR(upj.access_start_date)
				  	    ORDER BY
						upj.access_start_date, upj.product_id";
			}
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_Reports.class: loadUniqueActiveMembersPerPeriod(): Start, end: $start_date, $end_date"); 
				$resultsArray[] = $row;
			}
			
			$row = null;
			$stmt = null;
			$dap_dbh = null;
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	public static function loadUniqueExpiredMembersPerPeriod($start_date, $end_date, $group) {
		try {
			
			$dap_dbh = Dap_Connection::getConnection();
			
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			
			$stmt = null;
			

			$sql = "SELECT 
							count(*) as expired,
							YEAR(upj.access_start_date) as ty,
							MONTH(upj.access_start_date) as tm
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = p.id and
							upj.access_end_date < CURDATE() and
							CURDATE() >= upj.access_start_date and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')";
				
					
			if($group=="month") {		
				$sql.=" GROUP BY YEAR( upj.access_start_date ), MONTH(upj.access_start_date)
						ORDER BY upj.access_start_date, upj.product_id";
			}
			else {
				$sql.=" GROUP BY YEAR( upj.access_start_date )
						ORDER BY upj.access_start_date, upj.product_id";
			}
			
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_Reports.class: loadUniqueExpiredMembersPerPeriod(): Start, end: $start_date, $end_date"); 
				$resultsArray[] = $row;
			}
			
			$row = null;
			$stmt = null;
			$dap_dbh = null;
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	
	public static function loadActiveMemberSummary($start_date, $end_date, $group) {
		try {
			
			$dap_dbh = Dap_Connection::getConnection();
			
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			
			$stmt = null;
			$sql = "SELECT 
							count(*) as active,
							p.id,
							p.name, 
							YEAR(upj.access_start_date) as ty,
							MONTH(upj.access_start_date) as tm
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = p.id and
							CURDATE() <= upj.access_end_date and
							CURDATE() >= upj.access_start_date and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')";
					
					
			if($group=="month") {		
				$sql.=" GROUP BY
							upj.product_id,YEAR(upj.access_start_date),MONTH(upj.access_start_date)
					ORDER BY
						upj.access_start_date, upj.product_id";
			}
			else {
				$sql.=" GROUP BY
							upj.product_id,YEAR(upj.access_start_date)
					ORDER BY
						upj.access_start_date, upj.product_id";
			}
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_Reports.class: loadUniqueActiveMembers(): Start, end: $start_date, $end_date"); 
				$resultsArray[] = $row;
			}
			
			$row = null;
			$stmt = null;
			$dap_dbh = null;
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	
		public static function loadExpiredMemberSummary($start_date, $end_date) {
		try {
			
			$dap_dbh = Dap_Connection::getConnection();
			
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			
			$stmt = null;
			$sql = "SELECT 
							count(*) as expired,
							p.id,
							p.name, 
							YEAR(upj.access_end_date) as ty,
							MONTH(upj.access_end_date) as tm
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							p.id=upj.product_id and
							upj.access_end_date < CURDATE() and
							CURDATE() >= upj.access_start_date and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')";
					
			if($group=="month") {		
				$sql.=" GROUP BY
							upj.product_id,YEAR(upj.access_end_date),MONTH(upj.access_end_date)
					ORDER BY
						upj.access_end_date, upj.product_id";
			}
			else {
				$sql.=" GROUP BY
							upj.product_id,YEAR(upj.access_end_date)
					ORDER BY
						upj.access_end_date, upj.product_id";
			}
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_Reports.class: loadExpiredMemberSummary(): Start, end: $start_date, $end_date"); 
				$resultsArray[] = $row;
			}
			
			$row = null;
			$stmt = null;
			$dap_dbh = null;
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	
	public static function loadUniqueMembers($start_date, $end_date) {
		try {
			logToFile("Start, end: $start_date, $end_date"); 
			$dap_dbh = Dap_Connection::getConnection();
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			$stmt = null;
			$sql = "select id, name from dap_products";
			$stmt = $dap_dbh->prepare($sql);
			$stmt->execute();
			$productsArray = array();
			$i=0;
			$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$productId = $row["id"];
				$productName = $row["name"];
				$productRowCount = 0;
				$productsArray[$i] = array();
				$productsArray[$i]["id"] = $productId;
				$productsArray[$i]["name"] = $productName;
				$productsArray[$i]["active"] = 0;
				$productsArray[$i]["expired"] = 0;
				
				/** Select all ACTIVE users **/
				$sql2 = "SELECT 
							count(*) as count,
							YEAR(upj.access_start_date) as ty,
							MONTH(upj.access_start_date) as tm
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = " . $productId . " and
							upj.product_id = p.id and
							CURDATE() <= upj.access_end_date and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')
						GROUP BY
							upj.product_id,YEAR(upj.access_start_date),MONTH(upj.access_start_date)
						";
							
				//logToFile("ACTIVE sql: $sql2"); 
				$stmt2 = $dap_dbh->prepare($sql2);
				$stmt2->bindParam(':start_date', $start_date, PDO::PARAM_STR);
				$stmt2->bindParam(':end_date', $end_date, PDO::PARAM_STR);
				$stmt2->execute();
				
				if ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
					//$productsArray[$i]["name"] = $row2["name"];
					$name=$row2["name"];
					$mon=$row2['tm'];
					$my = $months[$mon] . "-" . $row2['ty'];
				
					$productsArray[$name][$my]["active"] = $row2["count"];
					logToFile("product id: " . $productId . ", productsArray[name][my]['active'] " . $row2["count"]); 
				}
				
				$productsArray[$name][$my]["rowTotal"] = $productRowCount;
				
				$row2 = null;
				$stmt2 = null;
				$sql2 = null;
			
			
				/** Select all EXPIRED users **/
				$sql2 = "SELECT 
							count(*) as count,
							YEAR(upj.access_start_date) as ty,
							MONTH(upj.access_start_date) as tm
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = " . $productId . " and
							upj.product_id = p.id and 
							upj.access_end_date < CURDATE() and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')
						GROUP BY
							p.name,YEAR(upj.access_start_date),MONTH(upj.access_start_date)					
						";
				//logToFile("FREE sql: $sql2"); 
				$stmt2 = $dap_dbh->prepare($sql2);
				$stmt2->bindParam(':start_date', $start_date, PDO::PARAM_STR);
				$stmt2->bindParam(':end_date', $end_date, PDO::PARAM_STR);
				$stmt2->execute();
				
				if ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
					//$productsArray[$i]["name"] = $row2["name"];
					$name=$row2["name"];
					$mon=$row2['tm'];
					$my = $months[$mon] . "-" . $row2['ty'];
				
					$productsArray[$name][$my]["expired"] = $row2["count"];
					logToFile("product id: " . $productId . ", productsArray[name][my]['expired'] " . $row2["count"]); 
				}
				
				$row2 = null;
				$stmt2 = null;
				$sql2 = null;

				//logToFile("-----------------------------------"); 
			
			
				$productsArray[$name][$my]["rowTotal"] = $productRowCount;
				$i++;
			}
			
			$row = null;
			$stmt = null;
			$dap_dbh = null;
			return $productsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	public static function loadMemberSummary($start_date, $end_date) {
		try {
			logToFile("Start, end: $start_date, $end_date"); 
			$dap_dbh = Dap_Connection::getConnection();
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			$stmt = null;
			$sql = "select id, name from dap_products";
			$stmt = $dap_dbh->prepare($sql);
			$stmt->execute();
			$productsArray = array();
			$i=0;
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$productId = $row["id"];
				$productName = $row["name"];
				$productRowCount = 0;
				$productsArray[$i] = array();
				$productsArray[$i]["id"] = $productId;
				$productsArray[$i]["name"] = $productName;
				$productsArray[$i]["active"] = 0;
				$productsArray[$i]["expired"] = 0;
				$productsArray[$i]["rowTotal"] = 0;
				
				/** Select all ACTIVE users **/
				$sql2 = "SELECT 
							count(*) as count
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = " . $productId . " and
							upj.product_id = p.id and
							upj.access_start_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y') and
							CURDATE() <= upj.access_end_date
						GROUP BY
							p.id
						";
							
				//logToFile("PAID sql: $sql2"); 
				$stmt2 = $dap_dbh->prepare($sql2);
				$stmt2->bindParam(':start_date', $start_date, PDO::PARAM_STR);
				$stmt2->bindParam(':end_date', $end_date, PDO::PARAM_STR);
				$stmt2->execute();
				
				if ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
					//$productsArray[$i]["name"] = $row2["name"];
					$productsArray[$i]["active"] = $row2["count"];
					$productRowCount += $row2["count"];
					logToFile("product id: " . $productId . ", productsArray[$i][active]: " . $row2["count"]); 
				}
				
				$row2 = null;
				$stmt2 = null;
				$sql2 = null;
			
			
				/** Select all EXPIRED users **/
				$sql2 = "SELECT 
							count(*) as count 
						FROM 
							dap_users_products_jn upj,
							dap_products p
						WHERE 
							upj.product_id = " . $productId . " and
							upj.product_id = p.id and 
							upj.access_end_date < CURDATE() and 
							upj.access_end_date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')
						GROUP BY
							p.id							
						";
				//logToFile("FREE sql: $sql2"); 
				$stmt2 = $dap_dbh->prepare($sql2);
				$stmt2->bindParam(':start_date', $start_date, PDO::PARAM_STR);
				$stmt2->bindParam(':end_date', $end_date, PDO::PARAM_STR);
				$stmt2->execute();
				
				if ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
					//$productsArray[$i]["name"] = $row2["name"];
					$productsArray[$i]["expired"] = $row2["count"];
					$productRowCount += $row2["count"];
					logToFile("product id: " . $productId . ", productsArray[$i][expired]: " . $row2["count"]); 
				}
				
				$row2 = null;
				$stmt2 = null;
				$sql2 = null;

				//logToFile("-----------------------------------"); 
			
			
				$productsArray[$i]["rowTotal"] = $productRowCount;
				$i++;
			}
			
			$row = null;
			$stmt = null;
			$dap_dbh = null;
			return $productsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	
	public static function loadEarningsSummary($start_date, $end_date, $group) {
		try {
			logToFile("Start, end: $start_date, $end_date"); 
			$dap_dbh = Dap_Connection::getConnection();
			
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			
		//	if($group=="year")
			//	$end_date=date('m-d-Y', strtotime('+1 year', strtotime($end_date)) );
			

			$stmt = null;
			$sql = "SELECT
						YEAR(t.date) as ty,
						MONTH(t.date) as tm,
						p.id,
						p.name, 
						sum(CASE WHEN t.payment_value > 0 THEN t.payment_value ELSE 0 end) as product_total_sales,
						sum(CASE WHEN t.payment_value < 0 THEN t.payment_value ELSE 0 end) as product_total_refunds,
						sum(CASE WHEN t.payment_value > 0 THEN 1 ELSE 0 end) as num_sales,
						sum(CASE WHEN t.payment_value < 0 THEN 1 ELSE 0 end) as num_refunds,
						count(*) as total_transactions,
						sum(t.payment_value) as product_net_sales
					FROM
						dap_transactions t,
						dap_products p
					WHERE
						t.product_id = p.id and
						t.payment_status in ('Completed', 'Refund') and
						t.trans_type <> 'subscr_signup' and
						t.date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')";
				
			if($group=="month") {		
				$sql.="GROUP BY
						t.product_id, YEAR(t.date),MONTH(t.date)
					ORDER BY
						t.date, t.product_id
					";
			}
			else {
				logToFile("group by year"); 
				$sql.="GROUP BY
						t.product_id,YEAR(t.date)
					ORDER BY
						t.date, t.product_id
					";
			}
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_Reports.class: Start, end: $start_date, $end_date"); 
				$resultsArray[] = $row;
			}
			
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	
	public static function loadEarningsSummaryOLD($start_date, $end_date, $group) {
		try {
			logToFile("Start, end: $start_date, $end_date"); 
			$dap_dbh = Dap_Connection::getConnection();
			
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			
			$stmt = null;
			$sql = "SELECT
						YEAR(t.date) as ty,
						MONTH(t.date) as tm,
						p.id,
						p.name, 
						sum(CASE WHEN t.payment_value > 0 THEN t.payment_value ELSE 0 end) as product_total_sales,
						sum(CASE WHEN t.payment_value < 0 THEN t.payment_value ELSE 0 end) as product_total_refunds,
						sum(CASE WHEN t.payment_value > 0 THEN 1 ELSE 0 end) as num_sales,
						sum(CASE WHEN t.payment_value < 0 THEN 1 ELSE 0 end) as num_refunds,
						count(*) as total_transactions,
						sum(t.payment_value) as product_net_sales
					FROM
						dap_transactions t,
						dap_products p
					WHERE
						t.product_id = p.id and
						t.payment_status in ('Completed', 'Refund') and
						t.trans_type <> 'subscr_signup' and
						t.date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')
					GROUP BY
						t.product_id, YEAR(t.date),MONTH(t.date)
					ORDER BY
						t.date, t.product_id
					";
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				logToFile("Dap_Reports.class: Start, end: $start_date, $end_date"); 
				$resultsArray[] = $row;
			}
			
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
	public static function loadEarningsSummaryByMonth($start_date, $end_date, $group) {
		try {
			//logToFile("Start, end: $start_date, $end_date"); 
			$dap_dbh = Dap_Connection::getConnection();
			if($start_date == "") $start_date = date("m-d-Y");
			if($end_date == "") $end_date = date("m-d-Y");
			$stmt = null;
			$sql = "
						SELECT 
							YEAR(date) as ty, 
							MONTH(date) as tm,
						  	COUNT(*) AS numtrans,
						  	sum(payment_value) as total
						FROM 
							dap_transactions
						WHERE
							payment_status in ('Completed', 'Refund') and
							trans_type <> 'subscr_signup' and
							date between str_to_date(:start_date, '%m-%d-%Y') and str_to_date(:end_date, '%m-%d-%Y')";
								
			if($group=="month") {		
				$sql.="GROUP BY 
							YEAR(date), MONTH(date)
						ORDER BY 
							date
					";
			}
			else {
				logToFile("group by year"); 
				$sql.="GROUP BY 
							YEAR(date)
						ORDER BY 
							date
					";
			}
			
			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':start_date', $start_date, PDO::PARAM_STR);
			$stmt->bindParam(':end_date', $end_date, PDO::PARAM_STR);
			$stmt->execute();
			$resultsArray = array();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$resultsArray[] = $row;
			}
			
			return $resultsArray;
		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}			
	}
	
}
?>
