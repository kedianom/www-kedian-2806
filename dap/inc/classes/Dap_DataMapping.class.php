<?php

class Dap_DataMapping {
   	var $id;
   	var $name;
	var $value;
	
	function getId() {
		return $this->id;
	}
	function setId($o) {
		$this->id = $o;
	}

	function getName() {
		return $this->code;
	}
	function setName($o) {
		$this->code = $o;
	}

	function getValue() {
		return $this->value;
	}
	function setValue($o) {
		$this->value = $o;
	}
	
	public function create() {
		try {
			$dap_dbh = Dap_Connection::getConnection();
		
			$sql = "insert into dap_data_mapping
						(name, value)
					values
						(:name, :value)";

			$stmt = $dap_dbh->prepare($sql);
			
			$stmt->bindParam(':name', $this->getName(), PDO::PARAM_STR);
			$stmt->bindParam(':value', $this->getValue(), PDO::PARAM_STR);

			$stmt->execute();

			return $dap_dbh->lastInsertId();
			$stmt = null;
			$dap_dbh = null;

		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}
	}

	public function update($id) {
		logToFile("functions admin, name= sdfs",LOG_INFO_DAP);
		try {
			$dap_dbh = Dap_Connection::getConnection();
			
			
				$sql = "update 
							dap_data_mapping 
						set
							name = :name,
							value = :value
						where id = :keyId";

			$stmt = $dap_dbh->prepare($sql);
			$stmt->bindParam(':name', $this->getName(), PDO::PARAM_STR);
			$stmt->bindParam(':value', $this->getValue(), PDO::PARAM_STR);
			
			$stmt->bindParam(':keyId', $this->getId(), PDO::PARAM_INT);

			logToFile("functions admin, name=" . $this->getName(),LOG_INFO_DAP);
			logToFile("functions admin, value=" . $this->getValue(),LOG_INFO_DAP);
			
			$stmt->execute();
			$stmt = null;
			$dap_dbh = null;

			return;

		} catch (PDOException $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		} catch (Exception $e) {
			logToFile($e->getMessage(),LOG_FATAL_DAP);
			throw $e;
		}
	}
	

	public static function loadAllDataMappingRows() {
		$dap_dbh = Dap_Connection::getConnection();
		$dataMapping = null;
		//logToFile("Dap_DataMapping.class: loadCustomFields() enter");
		
		//Load CustomFields details from database
		$sql = "select * from dap_data_mapping";

		$stmt = $dap_dbh->prepare($sql);
		//$stmt->bindParam(':id', $keyId, PDO::PARAM_STR);
		$stmt->execute();

		$dataMapping = array();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$dataMapping[] = $row;
			//logToFile("Dap_DataMapping.class: loadCustomFields(): name=" . $row['name']);
		}
		
		
		return $dataMapping;
	}
	
	
	public static function loadDataMappingById($keyId) {
		
		logToFile("Dap_DataMapping.class.php, loadDataMappingById ():",LOG_INFO_DAP);
		
		$dap_dbh = Dap_Connection::getConnection();
		$dataMapping = null;
	
		//Load CustomFields details from database
		$sql = "select * from dap_data_mapping where id=:key_id";

		$stmt = $dap_dbh->prepare($sql);
		$stmt->bindParam(':key_id', $keyId, PDO::PARAM_INT);
		$stmt->execute();
				
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			//logToFile("functions admin, loadDataMappingById: instantiate Dap_DataMapping",LOG_INFO_DAP);
			$dataMapping = new Dap_DataMapping();
			$dataMapping->setId( $row["id"] );
			$dataMapping->setName( stripslashes($row["name"]) );
			$dataMapping->setValue( stripslashes($row["value"]) );
			//logToFile("functions admin, loadDataMappingById: description=" . $row["description"],LOG_INFO_DAP);
		}
		
		return $dataMapping;
	}
	
	public static function isExists($keyId) {
		$dap_dbh = Dap_Connection::getConnection();

		//Load CustomFields details from database
		$sql = "select id
			from
				dap_data_mapping
			where
				id = :keyId";

		$stmt = $dap_dbh->prepare($sql);
		$stmt->bindParam(':id', $keyId, PDO::PARAM_INT);
		$stmt->execute();

		//echo "sql: $sql<br>"; exit;
		//$result = mysql_query($sql);
		//echo "rows returned: " . mysql_num_rows($result) . "<br>";

		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		  return TRUE;
		}

		return FALSE;
	}	

	public static function loadDataMappingByName($name) {
		$dap_dbh = Dap_Connection::getConnection();

		//Load CustomFields details from database
		$sql = "select *
			from
				dap_data_mapping
			where
				name = :name";

		$stmt = $dap_dbh->prepare($sql);
		$stmt->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt->execute();

		//echo "sql: $sql<br>"; exit;
		//$result = mysql_query($sql);
		//echo "rows returned: " . mysql_num_rows($result) . "<br>";

		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$dataMapping = new Dap_DataMapping();
			$dataMapping->setId( $row["id"] );
			$dataMapping->setName( stripslashes($row["name"]) );
			$dataMapping->setValue( stripslashes($row["value"]) );

			
			return $dataMapping;
		}

		return;
	}

	

}
?>