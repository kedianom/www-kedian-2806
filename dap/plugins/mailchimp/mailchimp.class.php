<?php

class mailchimp {

   function mailchimp() // constructor
   {
   }

   //======== USER REGISTRATION / UPDATE / DELETE ========
	// Add if user not there,  else update 
	function register($userId, $productId, $params)
	{
		logToFile("mailchimp.class.php: register(): calling storeAddress", LOG_INFO_DAP);
		
		$this->storeAddress($userId, $productId, $params);
	}
		
	function storeAddress($userId, $productId, $params){
		
		$dapuser = Dap_User::loadUserById($userId);
		$email = trim($dapuser->getEmail());
		$username = trim($dapuser->getUser_name());
		$firstname = trim($dapuser->getFirst_name());
		$lastname = trim($dapuser->getLast_name());
		$password = $dapuser->getPassword();
		
		// Added by Veena - Retrieve Custom Fields for the user - 11/19/11
		$customfieldlist = Dap_API::loadCustomFieldsForUser($dapuser);
		if(isset($customfieldlist) && ($customfieldlist != NULL) && ($customfieldlist != "")) {
		  foreach ($customfieldlist as $key=>$val) {
		   // $key is the name of the field and $val is the value of the custom field
			logToFile("mailchimp.class.php:key=value: " . $key . "=" . $val, LOG_INFO_DAP);
		  }
		}
		// End custom field retrieval
		
		$data = explode(":",$params);
		
		logToFile("mailchimp.class.php: storeAddress(): email =" . $email, LOG_INFO_DAP);
		
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $email)) {
			logToFile("mailchimp.class.php: storeAddress(): invalid email", LOG_INFO_DAP);
			return "Email address is invalid"; 
		}
	
		require_once('MCAPI.class.php');
		
		// grab an API Key from http://admin.mailchimp.com/account/api/
		$api_key = $data[1];
		$api = new MCAPI($api_key);

		logToFile("mailchimp.class.php: storeAddress(): api_key =" . $api_key, LOG_INFO_DAP);
		
		$list_id = $data[2];
		logToFile("mailchimp.class.php: storeAddress(): list_id =" . $list_id, LOG_INFO_DAP);
		
		//$api = new MCAPI('4f34766900f66832570f205fb08f164a-us2');
		
		// grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
		// Click the "settings" link for the list - the Unique Id is at the bottom of that page. 
		//$list_id = "df11c84ab4";
	
		// Merge variables are the names of all of the fields your mailing list accepts
		// Ex: first name is by default FNAME
		// You can define the names of each merge variable in Lists > click the desired list > list settings > Merge tags for personalization
		// Pass merge values to the API in an array as follows
		
		
		$mergeVars = array('FNAME'=>$firstname,
							'LNAME'=>$lastname,
							'MEMBERPASS'=>$password);
	
		if($api->listSubscribe($list_id, $email, $mergeVars) === true) {
			logToFile("mailchimp.class.php: listSubscribe(): success", LOG_INFO_DAP);
			// It worked!	
			return 0;
		}else{
			// An error ocurred, return error message
			logToFile("mailchimp.class.php: listSubscribe(): error: ".$api->errorMessage, LOG_INFO_DAP);			
			return 'Error: ' . $api->errorMessage;
		}
		
	}
	
	function unregister($userId, $productId, $params)
	{
		logToFile("mailchimp.class.php: register(): calling storeAddress", LOG_INFO_DAP);
		
		$this->removeAddress($userId, $productId, $params);
	}
	
	
	function removeAddress($userId, $productId, $params){
		
		$dapuser = Dap_User::loadUserById($userId);
		$email = trim($dapuser->getEmail());
	
		$data = explode(":",$params);
		
		logToFile("mailchimp.class.php: removeAddress(): email =" . $email, LOG_INFO_DAP);
		
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $email)) {
			return "Email address is invalid"; 
		}
	
		require_once('MCAPI.class.php');
		
		// grab an API Key from http://admin.mailchimp.com/account/api/
		$api_key = $data[1];
		$api = new MCAPI($api_key);

		logToFile("mailchimp.class.php: removeAddress(): api_key =" . $api_key, LOG_INFO_DAP);
		
		$list_id = $data[2];
		logToFile("mailchimp.class.php: removeAddress(): list_id =" . $list_id, LOG_INFO_DAP);
		
		//$api = new MCAPI('4f34766900f66832570f205fb08f164a-us2');
		
		// grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
		// Click the "settings" link for the list - the Unique Id is at the bottom of that page. 
		//$list_id = "df11c84ab4";
	
		// Merge variables are the names of all of the fields your mailing list accepts
		// Ex: first name is by default FNAME
		// You can define the names of each merge variable in Lists > click the desired list > list settings > Merge tags for personalization
		// Pass merge values to the API in an array as follows
		
		
			 
		if($api->listUnsubscribe($list_id, $email) === true) {
			// It worked!	
			return 0;
		}else{
			// An error ocurred, return error message	
			return 'listUnsubscribe() Error: ' . $api->errorMessage;
		}
	
	}
			
} 
?>