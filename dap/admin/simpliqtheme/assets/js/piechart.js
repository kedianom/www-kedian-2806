var piedata = new Array();
function doPieChart(element, data, height) {
    piedata[element]=data;
    $(element).parent().prev().find('.fa-search-plus').click(function () {
        doPieChart(".chartpopup", piedata[element], 500)
    })
    if (typeof height == 'undefined') {
        var chart = c3.generate({
            bindto: element,
            data: {
                // iris data from R
                columns: data,
                type: 'pie',
                onclick: function (d, i) {
                    doPieChart(".chartpopup", data, 500)
                },
                onmouseover: function (d, i) {
                    //console.log("onmouseover", d, i);
                },
                onmouseout: function (d, i) {
                    //console.log("onmouseout", d, i);
                }
            },
            tooltip: {
                grouped: false // Default true
            },
            'legend': {
                show: false
            },
            color: {
                pattern: ["#E60101", "#599AF0", "#008000", "#C0C0C0", "#008080", "#800080", "#285999", "#FFFF00", "#9E2180", "#187459", "#802222", "#9AC648", "#630A93", "#732C88", "#3E07B3", "#C3C9AE", "#235F44", "#F426C7", "#C0D012", "#94F4C6", "#18DE45", "#1B0105", "#B555D6", "#28B32B", "#0CFF97", "#CBA22C", "#8A63D3", "#19011A", "#433DB0", "#15C774", "#2AE1A0", "#67C18B", "#282A3B", "#C199D1", "#93BEC7", "#E628A4", "#08B19C", "#38198E", "#1C0454", "#F4F24E", "#089FD7", "#3EB3D7", "#C9D06B", "#9EF146", "#57DB95", "#7B1905", "#57B105", "#1CA19B", "#C68A9A", "#106908", "#7B9D8F", "#20235A", "#6445D5", "#503A2D", "#ADC736", "#996B07", "#6655CB", "#ECC52A", "#A4EF18", "#18FC6C", "#8157AE", "#10F0C4", "#8BA63F", "#E585BD", "#ADEFA8", "#7ECA66", "#8512E8", "#1031FE", "#2DCBF1", "#9F0B46", "#16BA8A", "#6B5257", "#8A7B37", "#C8485F", "#D557ED", "#C8558F", "#4AD7D1", "#A4A417", "#45FFFF", "#4F4F76", "#97BF6B", "#46372A", "#649CB9", "#D56D12", "#75D841", "#BEA376", "#B16E51", "#B73BFC", "#ECC430", "#9CBB70", "#1AAE11", "#534974", "#6FE192", "#61BB1A", "#D79C68", "#F05374", "#512C09", "#5D2BD7", "#D6F544", "#9330E7", "#A74E41", "#B18F81", "#036531", "#19A6C8", "#AD2E4D", "#3A591E", "#F41C46", "#D21847"]
            },
        });
    } else {
        
        
        $(".chartoverlay,.chartpopup").show();
        var chart = c3.generate({
            bindto: element,
            size: {
                height: height
            },
            data: {
                // iris data from R
                columns: data,
                type: 'pie',
                onclick: function (d, i) {

                },
                onmouseover: function (d, i) {
                    //console.log("onmouseover", d, i);
                },
                onmouseout: function (d, i) {
                    //console.log("onmouseout", d, i);
                }
            },
            tooltip: {
                grouped: false // Default true
            },
            'legend': {
                show: false
            },
            color: {
                pattern: ["#E60101", "#599AF0", "#008000", "#C0C0C0", "#008080", "#800080", "#285999", "#FFFF00","#9E2180", "#187459", "#802222", "#9AC648", "#630A93", "#732C88", "#3E07B3", "#C3C9AE", "#235F44", "#F426C7", "#C0D012", "#94F4C6", "#18DE45", "#1B0105", "#B555D6", "#28B32B", "#0CFF97", "#CBA22C", "#8A63D3", "#19011A", "#433DB0", "#15C774", "#2AE1A0", "#67C18B", "#282A3B", "#C199D1", "#93BEC7", "#E628A4", "#08B19C", "#38198E", "#1C0454", "#F4F24E", "#089FD7", "#3EB3D7", "#C9D06B", "#9EF146", "#57DB95", "#7B1905", "#57B105", "#1CA19B", "#C68A9A", "#106908", "#7B9D8F", "#20235A", "#6445D5", "#503A2D", "#ADC736", "#996B07", "#6655CB", "#ECC52A", "#A4EF18", "#18FC6C", "#8157AE", "#10F0C4", "#8BA63F", "#E585BD", "#ADEFA8", "#7ECA66", "#8512E8", "#1031FE", "#2DCBF1", "#9F0B46", "#16BA8A", "#6B5257", "#8A7B37", "#C8485F", "#D557ED", "#C8558F", "#4AD7D1", "#A4A417", "#45FFFF", "#4F4F76", "#97BF6B", "#46372A", "#649CB9", "#D56D12", "#75D841", "#BEA376", "#B16E51", "#B73BFC", "#ECC430", "#9CBB70", "#1AAE11", "#534974", "#6FE192", "#61BB1A", "#D79C68", "#F05374", "#512C09", "#5D2BD7", "#D6F544", "#9330E7", "#A74E41", "#B18F81", "#036531", "#19A6C8", "#AD2E4D", "#3A591E", "#F41C46", "#D21847"]
            },
        });

    }
    $(".chartpopup").prepend('<div class="btn-cls-pop" style="position: absolute;right: -11px;top: -15px;cursor:pointer"><i class="fa fa-times fa-2x"></i></div>');
}