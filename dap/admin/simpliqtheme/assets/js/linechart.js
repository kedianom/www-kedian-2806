var linedata = new Array();
function doLineChart(element, data, categories, width) {
  //  console.log(width);
    linedata[element] = data;

    $(element).parent().prev().find('.fa-search-plus').click(function () {
        if (typeof element == 'object') {
            attrid = "#" + element.attr('id');
        } else {
            attrid = element;
        }
        doLineChart(".chartpopup", linedata[attrid], categories, 1000);
    })
    if (typeof width == 'undefined') {
        window[element] = c3.generate({
            bindto: element,
            data: {
                columns: data,
                onclick: function () {
                    doLineChart(".chartpopup", data, categories, 1000);


                }
            },
            tooltip: {
                grouped: false // Default true
            },
            'legend': {
                show: false
            }, color: {
                pattern: ["#E60101", "#599AF0", "#008000", "#C0C0C0", "#008080", "#800080", "#285999", "#FFFF00","#9E2180", "#187459", "#802222", "#9AC648", "#630A93", "#732C88", "#3E07B3", "#C3C9AE", "#235F44", "#F426C7", "#C0D012", "#94F4C6", "#18DE45", "#1B0105", "#B555D6", "#28B32B", "#0CFF97", "#CBA22C", "#8A63D3", "#19011A", "#433DB0", "#15C774", "#2AE1A0", "#67C18B", "#282A3B", "#C199D1", "#93BEC7", "#E628A4", "#08B19C", "#38198E", "#1C0454", "#F4F24E", "#089FD7", "#3EB3D7", "#C9D06B", "#9EF146", "#57DB95", "#7B1905", "#57B105", "#1CA19B", "#C68A9A", "#106908", "#7B9D8F", "#20235A", "#6445D5", "#503A2D", "#ADC736", "#996B07", "#6655CB", "#ECC52A", "#A4EF18", "#18FC6C", "#8157AE", "#10F0C4", "#8BA63F", "#E585BD", "#ADEFA8", "#7ECA66", "#8512E8", "#1031FE", "#2DCBF1", "#9F0B46", "#16BA8A", "#6B5257", "#8A7B37", "#C8485F", "#D557ED", "#C8558F", "#4AD7D1", "#A4A417", "#45FFFF", "#4F4F76", "#97BF6B", "#46372A", "#649CB9", "#D56D12", "#75D841", "#BEA376", "#B16E51", "#B73BFC", "#ECC430", "#9CBB70", "#1AAE11", "#534974", "#6FE192", "#61BB1A", "#D79C68", "#F05374", "#512C09", "#5D2BD7", "#D6F544", "#9330E7", "#A74E41", "#B18F81", "#036531", "#19A6C8", "#AD2E4D", "#3A591E", "#F41C46", "#D21847"]
            },
            axis: {
                x: {
                    type: 'category',
                    label: 'X Label',
                    categories: categories
                },
                y: {
                    label: 'Y Label'
                },
            }
        });
    } else {
        $(".chartoverlay").click(function () {
            $(".chartoverlay,.chartpopup").html("").hide();
        });
        $(".chartoverlay,.chartpopup").show();

        window[element] = c3.generate({
            bindto: element,
            y: [0, 1, 2, 3, 5, 6, 7],
            zoom: {
                enabled: true
            },
            size: {
                width: width
            },
            data: {
                type: 'bar',
                columns: data,
                onclick: function () {
                },
            },
            color: {
                pattern:["#E60101", "#599AF0", "#008000", "#C0C0C0", "#008080", "#800080", "#285999", "#FFFF00","#9E2180", "#187459", "#802222", "#9AC648", "#630A93", "#732C88", "#3E07B3", "#C3C9AE", "#235F44", "#F426C7", "#C0D012", "#94F4C6", "#18DE45", "#1B0105", "#B555D6", "#28B32B", "#0CFF97", "#CBA22C", "#8A63D3", "#19011A", "#433DB0", "#15C774", "#2AE1A0", "#67C18B", "#282A3B", "#C199D1", "#93BEC7", "#E628A4", "#08B19C", "#38198E", "#1C0454", "#F4F24E", "#089FD7", "#3EB3D7", "#C9D06B", "#9EF146", "#57DB95", "#7B1905", "#57B105", "#1CA19B", "#C68A9A", "#106908", "#7B9D8F", "#20235A", "#6445D5", "#503A2D", "#ADC736", "#996B07", "#6655CB", "#ECC52A", "#A4EF18", "#18FC6C", "#8157AE", "#10F0C4", "#8BA63F", "#E585BD", "#ADEFA8", "#7ECA66", "#8512E8", "#1031FE", "#2DCBF1", "#9F0B46", "#16BA8A", "#6B5257", "#8A7B37", "#C8485F", "#D557ED", "#C8558F", "#4AD7D1", "#A4A417", "#45FFFF", "#4F4F76", "#97BF6B", "#46372A", "#649CB9", "#D56D12", "#75D841", "#BEA376", "#B16E51", "#B73BFC", "#ECC430", "#9CBB70", "#1AAE11", "#534974", "#6FE192", "#61BB1A", "#D79C68", "#F05374", "#512C09", "#5D2BD7", "#D6F544", "#9330E7", "#A74E41", "#B18F81", "#036531", "#19A6C8", "#AD2E4D", "#3A591E", "#F41C46", "#D21847"]
                       
            },
            tooltip: {
                grouped: false // Default true
            },
            'legend': {
                show: false
            },
            axis: {
                x: {
                    type: 'category',
                    label: 'X Label',
                    categories: categories
                },
                y: {
                    label: 'Y Label',
                }

            }
        });

    }
    $('.fa-bar-chart').click(function (e) {
        e.preventDefault();
        element = $(this.parentNode.parentNode.parentNode.parentNode).find('.box-content > .c3');

        forid = element.attr("for");
        window[forid].transform("bar");
        jQuery(this).parent().hide();
        jQuery(this).parent().prev('a').show();
    });
    $(document).on('click', 'i.fa-refresh', function (e) {
        e.preventDefault();
        forid = element.attr("for");

        element = $(this.parentNode.parentNode.parentNode.parentNode).find('.box-content > .c3');


        window[forid].transform("line");
        jQuery(this).parent().hide();
        jQuery(this).parent().next('a').show();
    })
    $(".chartpopup").prepend('<div class="btn-cls-pop" style="position: absolute;right: -11px;top: -15px;cursor:pointer"><i class="fa fa-times fa-2x"></i></div>');
}
$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        $(".chartoverlay,.chartpopup").html("").hide();
    }
})
$(document).on('click', ".chartoverlay,.btn-cls-pop", function () {
    $(".chartoverlay,.chartpopup").html("").hide();
})