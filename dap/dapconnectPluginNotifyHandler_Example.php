<?php 

$lldocroot = defined('SITEROOT') ? SITEROOT : $_SERVER['DOCUMENT_ROOT'];
if(file_exists($lldocroot . "/dap/dap-config.php")) 
	include_once ($lldocroot . "/dap/dap-config.php");

$post = array();
foreach ($_REQUEST as $key => $value) {
  $value = urlencode(stripslashes($value));
  $post[$key] = urldecode($value);
  logToFile("processDAPConnectWebhook: " . $key . "=" . $value, LOG_DEBUG_DAP);
}

if(isset($_REQUEST["orderDetails"])) {
	$order=$_REQUEST["orderDetails"];
	foreach ($order as $key => $value) {
  		$value = urlencode(stripslashes($value));
		$post[$key] = urldecode($value);
  		logToFile("processDAPConnectWebhook: ORDER DETAILS " . $key . "=" . $value, LOG_DEBUG_DAP);
	}
}

if(isset($_REQUEST["fields"])) {
	$fields=$_REQUEST["fields"];
	foreach ($fields as $key => $value) {
  		$value = urlencode(stripslashes($value));
		$post[$key] = urldecode($value);
  		logToFile("processDAPConnectWebhook: Fields " . $key . "=" . $value, LOG_DEBUG_DAP);
	}
}


?>